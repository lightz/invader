-- phpMyAdmin SQL Dump
-- version 4.1.13
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2017 at 03:33 AM
-- Server version: 5.5.58-0+deb8u1-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `128945_sql`
--

-- --------------------------------------------------------

--
-- Table structure for table `Map`
--

CREATE TABLE IF NOT EXISTS `Map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `world_name` varchar(64) NOT NULL,
  `spawn_room` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`world_name`),
  UNIQUE KEY `spawn_room` (`spawn_room`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MapObjective`
--

CREATE TABLE IF NOT EXISTS `MapObjective` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `map_id` (`map_id`),
  KEY `objective_id` (`objective_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MapTeam`
--

CREATE TABLE IF NOT EXISTS `MapTeam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  KEY `map_id` (`map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Objective`
--

CREATE TABLE IF NOT EXISTS `Objective` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `lock_on_capture` tinyint(1) NOT NULL DEFAULT '0',
  `control` int(11) NOT NULL,
  `starting_team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `starting_team_id` (`starting_team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ObjectiveZone`
--

CREATE TABLE IF NOT EXISTS `ObjectiveZone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objective_id` (`objective_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Spawn`
--

CREATE TABLE IF NOT EXISTS `Spawn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `world` varchar(64) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  `pitch` double NOT NULL,
  `yaw` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Team`
--

CREATE TABLE IF NOT EXISTS `Team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `primary_color` varchar(32) NOT NULL DEFAULT 'RED',
  `secondary_color` varchar(32) NOT NULL DEFAULT 'BLACK',
  `chat_color` varchar(32) NOT NULL DEFAULT 'BLUE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TeamSpawn`
--

CREATE TABLE IF NOT EXISTS `TeamSpawn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `spawn_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  KEY `spawn_id` (`spawn_id`),
  KEY `objective_id` (`objective_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Zone`
--

CREATE TABLE IF NOT EXISTS `Zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `height` double NOT NULL,
  `radius` int(11) NOT NULL,
  `world` varchar(64) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `z` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Map`
--
ALTER TABLE `Map`
  ADD CONSTRAINT `Map_ibfk_1` FOREIGN KEY (`spawn_room`) REFERENCES `Spawn` (`id`);

--
-- Constraints for table `MapObjective`
--
ALTER TABLE `MapObjective`
  ADD CONSTRAINT `MapObjective_ibfk_2` FOREIGN KEY (`objective_id`) REFERENCES `Objective` (`id`),
  ADD CONSTRAINT `MapObjective_ibfk_1` FOREIGN KEY (`map_id`) REFERENCES `Map` (`id`);

--
-- Constraints for table `MapTeam`
--
ALTER TABLE `MapTeam`
  ADD CONSTRAINT `MapTeam_ibfk_2` FOREIGN KEY (`map_id`) REFERENCES `Map` (`id`),
  ADD CONSTRAINT `MapTeam_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `Team` (`id`);

--
-- Constraints for table `Objective`
--
ALTER TABLE `Objective`
  ADD CONSTRAINT `Objective_ibfk_1` FOREIGN KEY (`starting_team_id`) REFERENCES `Team` (`id`);

--
-- Constraints for table `ObjectiveZone`
--
ALTER TABLE `ObjectiveZone`
  ADD CONSTRAINT `ObjectiveZone_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `Zone` (`id`),
  ADD CONSTRAINT `ObjectiveZone_ibfk_1` FOREIGN KEY (`objective_id`) REFERENCES `Objective` (`id`);

--
-- Constraints for table `TeamSpawn`
--
ALTER TABLE `TeamSpawn`
  ADD CONSTRAINT `TeamSpawn_ibfk_3` FOREIGN KEY (`objective_id`) REFERENCES `Objective` (`id`),
  ADD CONSTRAINT `TeamSpawn_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `Team` (`id`),
  ADD CONSTRAINT `TeamSpawn_ibfk_2` FOREIGN KEY (`spawn_id`) REFERENCES `Spawn` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
