package ca.lightz.invasion.io;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.constructor.SiegePlayerBuilder;
import ca.lightz.invasion.io.database.repository.*;
import ca.lightz.invasion.io.database.source.MySQLDataSource;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.LinkedList;

public class GameLoader {
    private final MySQLDataSource dataSource;

    private LinkedList<SQLRepository> repositories = new LinkedList<>();

    private PlayerRepository playerRepository;
    private MapRepository mapRepository;
    private SpawnRepository spawnRepository;

    public GameLoader(Game game, Plugin plugin) {
        this.dataSource = new MySQLDataSource(plugin);

        this.setUpRepositories(game);
        this.load();

        game.setMaps(this.mapRepository.getAll());
    }

    //Order IS important for Database loading
    private void setUpRepositories(Game game) {
        ZoneRepository zoneRepository = new ZoneRepository();
        this.repositories.add(zoneRepository);

        this.spawnRepository = new SpawnRepository();
        this.repositories.add(this.spawnRepository);

        TeamRepository teamRepository = new TeamRepository();
        this.repositories.add(teamRepository);

        ObjectiveZoneRepository objectiveZoneRepository = new ObjectiveZoneRepository(zoneRepository);
        this.repositories.add(objectiveZoneRepository);
        ObjectiveFlagZoneRepository objectiveFlagZoneRepository = new ObjectiveFlagZoneRepository(zoneRepository);
        this.repositories.add(objectiveFlagZoneRepository);

        ObjectiveSpawnRepository objectiveSpawnRepository = new ObjectiveSpawnRepository(this.spawnRepository);
        this.repositories.add(objectiveSpawnRepository);

        ObjectiveRepository objectiveRepository = new ObjectiveRepository(teamRepository, objectiveSpawnRepository, objectiveZoneRepository, objectiveFlagZoneRepository);
        this.repositories.add(objectiveRepository);

        MapTeamRepository mapTeamRepository = new MapTeamRepository(teamRepository);
        this.repositories.add(mapTeamRepository);

        MapObjectiveRepository mapObjectiveRepository = new MapObjectiveRepository(objectiveRepository);
        this.repositories.add(mapObjectiveRepository);

        this.mapRepository = new MapRepository(zoneRepository, this.spawnRepository, mapObjectiveRepository, mapTeamRepository);
        this.repositories.add(this.mapRepository);

        this.repositories.add(new StructureRepository(this.mapRepository));

        SiegePlayerBuilder playerBuilder = new SiegePlayerBuilder(game);
        this.playerRepository = new PlayerRepository(playerBuilder);
        this.repositories.add(this.playerRepository);
    }

    private void load() {
        for (SQLRepository repository : this.repositories) {
            repository.loadAll(this.dataSource);
        }
    }

    public SiegePlayer get(Player player) {
        return this.playerRepository.getPlayer(player, this.dataSource);
    }

    public void saveAllPlayers() {
        this.playerRepository.saveAll(this.dataSource);
    }

    public void updateSpawn(Location location, int id) {
        this.spawnRepository.updateSpawn(location, id, this.dataSource);
    }

    public void cleanPlayers() {
        this.playerRepository.cleanPlayers();
    }
}
