package ca.lightz.invasion.io.config;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.item.component.LoreComponent;
import ca.lightz.invasion.core.lore.LoreBlock;
import ca.lightz.invasion.core.lore.LoreType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemConfig {
    public String id;
    public String name;
    public String material;
    public List<String> lore;

    private LoreItem instance;

    public LoreItem get() {
        if (this.instance == null) {
            this.buildItem();
        }
        return this.instance;
    }

    private void buildItem() {
        Material material = Material.valueOf(this.material);
        ItemStack itemStack = new ItemStack(material);

        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta == null) {
            Log.error(String.format("No item meta when creating %s item.", this.id));
            return;
        }
        itemMeta.setDisplayName(this.name);
        itemStack.setItemMeta(itemMeta);

        this.instance = new LoreItem(itemStack);
        this.addLore();
    }

    private void addLore() {
        if (this.lore == null || this.lore.isEmpty()) {
            return;
        }
        
        LoreBlock loreBlock = new LoreBlock(LoreType.DESCRIPTION, this.lore);
        LoreComponent loreComponent = new LoreComponent(loreBlock);
        this.instance.addComponent(loreComponent);
    }
}
