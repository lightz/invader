package ca.lightz.invasion.io.config;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ConfigLoader {
    public String path;

    public ConfigLoader(String path) {
        this.path = path;
    }

    public <T> T loadConfig(Class<T> clazz, String filename) {
        Yaml yaml = new Yaml(new CustomClassLoaderConstructor(clazz.getClassLoader()));

        String filePath = this.path + "/" + filename + ".yml";

        InputStream input = null;
        try {
            input = new FileInputStream(new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return yaml.loadAs(input, clazz);
    }
}
