package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.constructor.SiegePlayerBuilder;
import ca.lightz.invasion.io.database.source.DataSource;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class PlayerRepository extends SQLRepository {
    private static final String TABLE_NAME = "Player";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Player` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `uuid` varchar(500) NOT NULL," +
            "  `perks` text NOT NULL," +
            "  `lastUnit` varchar(500) NOT NULL DEFAULT 'Swordsman'," +
            "  PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final SiegePlayerBuilder siegePlayerBuilder;

    private Map<UUID, SiegePlayer> players;

    public PlayerRepository(SiegePlayerBuilder playerBuilder) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.siegePlayerBuilder = playerBuilder;
        this.players = new HashMap<>(32);
    }

    public SiegePlayer getPlayer(Player player, DataSource dataSource) {
        UUID uuid = player.getUniqueId();
        if (this.players.containsKey(uuid)) {
            return this.players.get(uuid);
        }

        this.load(uuid, dataSource);
        if (this.players.containsKey(uuid)) {
            return this.players.get(uuid);
        }

        this.insertNewPlayer(uuid, dataSource);
        SiegePlayer newPlayer = this.siegePlayerBuilder.buildNewPlayer(player);
        this.players.put(uuid, newPlayer);
        return newPlayer;
    }

    private void load(UUID uuid, DataSource dataSource) {
        String stringUUID = uuid.toString();
        String query = this.getSelectQuery(stringUUID);
        dataSource.executeQuery(query, this);
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                String stringUUID = resultSet.getString("uuid");
                String rawPerks = resultSet.getString("perks");
                String lastUnit = resultSet.getString("lastUnit");

                UUID uuid = UUID.fromString(stringUUID);
                SiegePlayer loadedPlayer = this.siegePlayerBuilder.build(uuid, rawPerks, lastUnit);
                this.players.put(uuid, loadedPlayer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getSelectQuery(String uuid) {
        return "SELECT * FROM " + this.tableName + " WHERE `uuid`='" + uuid + "';";
    }

    private void insertNewPlayer(UUID uuid, DataSource dataSource) {
        String query = "INSERT INTO `player`(`uuid`, `perks`) VALUES ('%s', '');";
        query = String.format(query, uuid.toString());

        dataSource.executeUpdate(query);
    }

    public void saveAll(DataSource dataSource) {
        if (this.players.isEmpty()) {
            return;
        }

        for (SiegePlayer player : this.players.values()) {
            dataSource.executeUpdate(player.getSaveQuery());
        }
    }

    public void cleanPlayers() {
        Collection<SiegePlayer> onlinePlayers = new ArrayList<>(this.players.size());
        for (Map.Entry<UUID, SiegePlayer> entry : this.players.entrySet()) {
            if (Bukkit.getPlayer(entry.getKey()) != null) {
                onlinePlayers.add(entry.getValue());
            }
        }

        int capacity = Math.min(Bukkit.getMaxPlayers(), onlinePlayers.size() * 2);
        this.players = new HashMap<>(capacity + 1, 1); //Refresh HashMap to avoid memory leak
        for (SiegePlayer siegePlayer : onlinePlayers) {
            this.players.put(siegePlayer.getUUID(), siegePlayer);
        }
    }
}
