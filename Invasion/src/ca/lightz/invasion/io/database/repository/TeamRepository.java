package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.components.TeamColor;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class TeamRepository extends SQLRepository {
    private static final String TABLE_NAME = "Team";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Team` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `name` varchar(32) NOT NULL," +
            "  `primary_color` varchar(32) NOT NULL DEFAULT 'RED'," +
            "  `secondary_color` varchar(32) NOT NULL DEFAULT 'BLACK'," +
            "  `chat_color` varchar(32) NOT NULL DEFAULT 'BLUE'," +
            "  PRIMARY KEY (`id`)," +
            "  UNIQUE KEY `name` (`name`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private Map<Integer, Team> teams = new HashMap<>();

    public TeamRepository() {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                this.buildATeam(resultSet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void buildATeam(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String rawPrimaryColor = resultSet.getString("primary_color");
        String rawSecondaryColor = resultSet.getString("secondary_color");
        String rawChatColor = resultSet.getString("chat_color");

        DyeColor primaryColor = DyeColor.valueOf(rawPrimaryColor);
        DyeColor secondaryColor = DyeColor.valueOf(rawSecondaryColor);
        ChatColor chatColor = ChatColor.valueOf(rawChatColor);
        TeamColor teamColor = new TeamColor(primaryColor, secondaryColor, chatColor);

        Team team = new Team(id, name, teamColor);
        this.teams.put(id, team);
    }

    public Team get(int startingTeamId) {
        return this.teams.get(startingTeamId);
    }
}
