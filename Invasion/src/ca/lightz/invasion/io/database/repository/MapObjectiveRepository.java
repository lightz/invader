package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.objective.container.ObjectiveContainer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MapObjectiveRepository extends SQLRepository {
    private static final String TABLE_NAME = "Map_objective";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Map_objective` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `map_id` int(11) NOT NULL," +
            "  `objective_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `map_id` (`map_id`)," +
            "  KEY `objective_id` (`objective_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final Map<Integer, ObjectiveContainer> mapObjectives = new HashMap<>(64);
    private final ObjectiveRepository objectiveRepository;

    public MapObjectiveRepository(ObjectiveRepository objectiveRepository) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.objectiveRepository = objectiveRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                int mapId = resultSet.getInt("map_id");
                int objectiveId = resultSet.getInt("objective_id");
                Objective objective = this.objectiveRepository.get(objectiveId);

                this.add(mapId, objective);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void add(int mapId, Objective objective) {
        this.mapObjectives.putIfAbsent(mapId, new ObjectiveContainer());
        this.mapObjectives.get(mapId).add(objective);
    }

    public ObjectiveContainer get(int mapId) {
        return this.mapObjectives.get(mapId);
    }
}
