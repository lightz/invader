package ca.lightz.invasion.io.database.repository;

public class ObjectiveFlagZoneRepository extends ObjectiveZoneRepository {
    private static final String TABLE_NAME = "Objective_flag_zone";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `objective_flag_zone` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `objective_id` int(11) NOT NULL," +
            "  `zone_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `objective_id` (`objective_id`)," +
            "  KEY `zone_id` (`zone_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    public ObjectiveFlagZoneRepository(ZoneRepository zoneRepository) {
        super(zoneRepository, TABLE_NAME, TABLE_CREATE_SCRIPT);
    }
}