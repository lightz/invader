package ca.lightz.invasion.io.database.repository;


import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.objective.control.Control;
import ca.lightz.invasion.game.objective.flag.FlagScanner;
import ca.lightz.invasion.game.spawn.container.TeamSpawn;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.zone.FlagZone;
import ca.lightz.invasion.game.zone.Zone;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ObjectiveRepository extends SQLRepository {
    private static final String TABLE_NAME = "Objective";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Objective` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `name` varchar(32) NOT NULL," +
            "  `is_locked` tinyint(1) NOT NULL DEFAULT '0'," +
            "  `lock_on_capture` tinyint(1) NOT NULL DEFAULT '0'," +
            "  `control` int(11) NOT NULL," +
            "  `starting_team_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `starting_team_id` (`starting_team_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final Map<Integer, Objective> objectives = new HashMap<>(64);
    private final TeamRepository teamRepository;
    private final ObjectiveSpawnRepository spawnRepository;
    private final ObjectiveZoneRepository objectiveZoneRepository;
    private final ObjectiveZoneRepository objectiveFlagZoneRepository;

    public ObjectiveRepository(TeamRepository teamRepository, ObjectiveSpawnRepository spawnRepository, ObjectiveZoneRepository objectiveZoneRepository, ObjectiveZoneRepository objectiveFlagZoneRepository) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.teamRepository = teamRepository;
        this.spawnRepository = spawnRepository;
        this.objectiveZoneRepository = objectiveZoneRepository;
        this.objectiveFlagZoneRepository = objectiveFlagZoneRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        FlagScanner flagScanner = new FlagScanner();

        while (resultSet.next()) {
            try {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");

                Control control = this.buildControl(resultSet);
                TeamSpawn spawns = this.spawnRepository.get(id);
                Zone zone = this.objectiveZoneRepository.get(id);
                FlagZone flagZone = new FlagZone(this.objectiveFlagZoneRepository.get(id), flagScanner);
                Objective objective = new Objective(name, control, spawns, zone, flagZone);
                control.setObjective(objective);
                this.objectives.put(id, objective);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Control buildControl(ResultSet resultSet) throws SQLException {
        int controlValue = resultSet.getInt("control");
        boolean isLocked = resultSet.getBoolean("is_locked");
        boolean lockOnCapture = resultSet.getBoolean("lock_on_capture");

        int startingTeamId = resultSet.getInt("starting_team_id");
        Team team = this.teamRepository.get(startingTeamId);
        return new Control(controlValue, isLocked, lockOnCapture, team);
    }

    public Objective get(int objectiveId) {
        return this.objectives.get(objectiveId);
    }
}
