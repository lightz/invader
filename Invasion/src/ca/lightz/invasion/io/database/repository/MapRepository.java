package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.indicator.WoolMapScanner;
import ca.lightz.invasion.game.map.SiegeMap;
import ca.lightz.invasion.game.objective.container.ObjectiveContainer;
import ca.lightz.invasion.game.spawn.SpawnRoom;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.game.zone.Zone;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapRepository extends SQLRepository {
    private static final String TABLE_NAME = "Map";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Map` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `name` varchar(64) NOT NULL," +
            "  `world_name` varchar(64) NOT NULL," +
            "  `spawn_room` int(11) NOT NULL," +
            "  `wool_map_zone_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  UNIQUE KEY `name` (`name`,`world_name`)," +
            "  UNIQUE KEY `spawn_room` (`spawn_room`)," +
            "  KEY `wool_map_zone_id` (`wool_map_zone_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private Map<Integer, SiegeMap> maps = new HashMap<>(8);
    private final SpawnRepository spawnRepository;
    private final MapObjectiveRepository mapObjectiveRepository;
    private final MapTeamRepository mapTeamRepository;
    private final ZoneRepository zoneRepository;

    public MapRepository(ZoneRepository zoneRepository, SpawnRepository spawnRepository, MapObjectiveRepository mapObjectiveRepository, MapTeamRepository mapTeamRepository) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.spawnRepository = spawnRepository;
        this.mapObjectiveRepository = mapObjectiveRepository;
        this.mapTeamRepository = mapTeamRepository;
        this.zoneRepository = zoneRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        WoolMapScanner woolMapScanner = new WoolMapScanner();

        while (resultSet.next()) {
            try {
                int id = resultSet.getInt("id");
                int woolMapZoneId = resultSet.getInt("wool_map_zone_id");
                String name = resultSet.getString("name");
                String worldName = resultSet.getString("world_name");

                int spawnRoomId = resultSet.getInt("spawn_room");
                SpawnRoom spawnRoom = new SpawnRoom(this.spawnRepository.get(spawnRoomId));

                TeamContainer teams = this.mapTeamRepository.get(id);
                ObjectiveContainer objectives = this.mapObjectiveRepository.get(id);
                Zone zone = this.zoneRepository.get(woolMapZoneId);

                SiegeMap map = new SiegeMap(name, worldName, spawnRoom, zone, objectives, teams, woolMapScanner);
                this.maps.put(id, map);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<SiegeMap> getAll() {
        return new ArrayList<>(this.maps.values());
    }

    public SiegeMap get(int mapId) {
        return this.maps.get(mapId);
    }
}