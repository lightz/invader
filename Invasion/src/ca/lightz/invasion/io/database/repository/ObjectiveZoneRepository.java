package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.zone.ComposedZone;
import ca.lightz.invasion.game.zone.Zone;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectiveZoneRepository extends SQLRepository {
    private static final String DEFAULT_TABLE_NAME = "Objective_zone";
    private static final String DEFAULT_TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Objective_zone` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `objective_id` int(11) NOT NULL," +
            "  `zone_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `objective_id` (`objective_id`)," +
            "  KEY `zone_id` (`zone_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private static final int DEFAULT_AMOUNT_ZONES_PER_OBJECT = 1;

    private final Map<Integer, List<Zone>> objectiveZones = new HashMap<>(64);
    private final ZoneRepository zoneRepository;

    public ObjectiveZoneRepository(ZoneRepository zoneRepository) {
        this(zoneRepository, DEFAULT_TABLE_NAME, DEFAULT_TABLE_CREATE_SCRIPT);
    }

    protected ObjectiveZoneRepository(ZoneRepository zoneRepository, String tableName, String tableCreationScript) {
        super(tableName, tableCreationScript);
        this.zoneRepository = zoneRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                int zoneId = resultSet.getInt("zone_id");
                int objectiveId = resultSet.getInt("objective_id");
                Zone zone = this.zoneRepository.get(zoneId);

                this.add(objectiveId, zone);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void add(int objectiveId, Zone zone) {
        this.objectiveZones.putIfAbsent(objectiveId, new ArrayList<>(DEFAULT_AMOUNT_ZONES_PER_OBJECT));
        this.objectiveZones.get(objectiveId).add(zone);
    }

    public Zone get(int objectiveId) {
        List<Zone> zones = this.objectiveZones.getOrDefault(objectiveId, new ArrayList<>(DEFAULT_AMOUNT_ZONES_PER_OBJECT));
        return zones.size() == 1 ? zones.get(0) : new ComposedZone(zones);
    }
}