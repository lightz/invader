package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.spawn.Spawn;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.io.database.source.DataSource;
import org.bukkit.Location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SpawnRepository extends SQLRepository {
    private static final String TABLE_NAME = "Spawn";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Spawn` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `world` varchar(64) NOT NULL," +
            "  `x` double NOT NULL," +
            "  `y` double NOT NULL," +
            "  `z` double NOT NULL," +
            "  `pitch` double NOT NULL," +
            "  `yaw` double NOT NULL," +
            "  PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private Map<Integer, Spawn> spawns = new HashMap<>(512);

    public SpawnRepository() {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                this.buildASpawnPoint(resultSet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void buildASpawnPoint(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");

        String worldName = resultSet.getString("world");
        double x = resultSet.getDouble("x");
        double y = resultSet.getDouble("y");
        double z = resultSet.getDouble("z");
        float pitch = (float) resultSet.getDouble("pitch");
        float yaw = (float) resultSet.getDouble("yaw");
        WorldPosition position = new WorldPosition(worldName, x, y, z, pitch, yaw);

        Spawn spawn = new Spawn(id, position);
        this.spawns.put(id, spawn);
    }

    public Spawn get(int spawnId) {
        return this.spawns.get(spawnId);
    }

    public void updateSpawn(Location location, int id, DataSource dataSource) {
        String query = "UPDATE `spawn` SET `world`=?,`x`=?,`y`=?,`z`=?,`pitch`=?,`yaw`=? WHERE `id`=?;";
        dataSource.executeUpdate(query, location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw(), id);
    }
}
