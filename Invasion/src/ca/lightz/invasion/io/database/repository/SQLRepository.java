package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.io.database.source.DataSource;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class SQLRepository implements DatabaseDataLoader {
    protected final String tableName;
    protected final String tableCreationScript;

    public SQLRepository(String tableName, String tableCreationScript) {
        this.tableName = tableName;
        this.tableCreationScript = tableCreationScript;
    }

    public void loadAll(DataSource dataSource) {
        this.createTable(dataSource);

        String query = this.getSelectAllQuery();
        dataSource.executeQuery(query, this);
    }

    private void createTable(DataSource dataSource) {
        if (!dataSource.hasTable(this.tableName)) {
            dataSource.execute(this.tableCreationScript);
            Log.info(String.format("Created table %s.", this.tableName));
        }
    }

    protected String getSelectAllQuery() {
        return "SELECT * FROM " + this.tableName + ";";
    }

    public abstract void loaded(ResultSet resultSet) throws SQLException;
}
