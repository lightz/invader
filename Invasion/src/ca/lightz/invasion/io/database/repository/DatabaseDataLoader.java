package ca.lightz.invasion.io.database.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface DatabaseDataLoader {

    void loaded(ResultSet resultSet) throws SQLException;
}
