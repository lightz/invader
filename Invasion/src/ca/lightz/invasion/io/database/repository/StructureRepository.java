package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.structure.SiegeStructure;
import ca.lightz.invasion.game.structure.constructor.StructureFactory;
import ca.lightz.invasion.game.structure.constructor.StructureType;
import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.position.WorldPosition;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StructureRepository extends SQLRepository {
    private static final String TABLE_NAME = "Structure";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Structure` (\n" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `type` enum('GATE','BATTLE_RAM') NOT NULL," +
            "  `world` varchar(64) NOT NULL," +
            "  `x` int(11) NOT NULL," +
            "  `y` int(11) NOT NULL," +
            "  `z` int(11) NOT NULL," +
            "  `data` varchar(512) NOT NULL," +
            "  `map_id` int(11) NOT NULL," +
            "  `team_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `map_id` (`map_id`)," +
            "  KEY `team_id` (`team_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final MapRepository mapRepository;

    public StructureRepository(MapRepository mapRepository) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.mapRepository = mapRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        StructureFactory structureFactory = new StructureFactory();

        while (resultSet.next()) {
            try {
                int mapId = resultSet.getInt("map_id");
                int teamId = resultSet.getInt("team_id");
                StructureType type = StructureType.valueOf(resultSet.getString("type").toUpperCase());
                String data = resultSet.getString("data");

                String worldName = resultSet.getString("world");
                double x = resultSet.getDouble("x");
                double y = resultSet.getDouble("y");
                double z = resultSet.getDouble("z");
                WorldPosition position = new WorldPosition(worldName, x, y, z);

                DataMapper dataMapper = DataMapper.deserialize(data);
                SiegeStructure structure = structureFactory.build(type, dataMapper, teamId, position);
                this.mapRepository.get(mapId).addStructure(structure);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
