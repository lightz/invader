package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamContainer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MapTeamRepository extends SQLRepository {
    private static final String TABLE_NAME = "Map_team";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Map_team` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `team_id` int(11) NOT NULL," +
            "  `map_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `team_id` (`team_id`)," +
            "  KEY `map_id` (`map_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final Map<Integer, TeamContainer> mapTeams = new HashMap<>();
    private final TeamRepository teamRepository;

    public MapTeamRepository(TeamRepository teamRepository) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.teamRepository = teamRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                int mapId = resultSet.getInt("map_id");
                int teamId = resultSet.getInt("team_id");
                Team team = this.teamRepository.get(teamId);

                this.add(mapId, team);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void add(int mapId, Team team) {
        this.mapTeams.putIfAbsent(mapId, new TeamContainer());
        this.mapTeams.get(mapId).add(team);
    }

    public TeamContainer get(int mapId) {
        return this.mapTeams.get(mapId);
    }
}
