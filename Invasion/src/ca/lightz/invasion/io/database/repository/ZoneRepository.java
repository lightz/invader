package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.zone.Zone;
import ca.lightz.invasion.game.zone.ZoneFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ZoneRepository extends SQLRepository {
    private static final String TABLE_NAME = "Zone";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Zone` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `height` double NOT NULL," +
            "  `radius` int(11) NOT NULL," +
            "  `world` varchar(64) NOT NULL," +
            "  `x` int(11) NOT NULL," +
            "  `y` int(11) NOT NULL," +
            "  `z` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final Map<Integer, Zone> zones = new HashMap<>(64);

    public ZoneRepository() {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                this.buildAZone(resultSet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void buildAZone(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String serialized = resultSet.getString("data");
        String figure = resultSet.getString("figure");

        String worldName = resultSet.getString("world");
        double x = resultSet.getDouble("x");
        double y = resultSet.getDouble("y");
        double z = resultSet.getDouble("z");
        WorldPosition position = new WorldPosition(worldName, x, y, z);

        Zone zone = ZoneFactory.build(position, figure, serialized);
        this.zones.put(id, zone);
    }

    public Zone get(int zoneId) {
        return this.zones.get(zoneId);
    }
}
