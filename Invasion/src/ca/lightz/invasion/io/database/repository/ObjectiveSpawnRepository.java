package ca.lightz.invasion.io.database.repository;

import ca.lightz.invasion.game.spawn.Spawn;
import ca.lightz.invasion.game.spawn.container.TeamSpawn;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ObjectiveSpawnRepository extends SQLRepository {
    private static final String TABLE_NAME = "Team_spawn";
    private static final String TABLE_CREATE_SCRIPT = "CREATE TABLE IF NOT EXISTS `Team_spawn` (" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT," +
            "  `team_id` int(11) NOT NULL," +
            "  `spawn_id` int(11) NOT NULL," +
            "  `objective_id` int(11) NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  KEY `team_id` (`team_id`)," +
            "  KEY `spawn_id` (`spawn_id`)," +
            "  KEY `objective_id` (`objective_id`)" +
            ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    private final Map<Integer, TeamSpawn> objectiveTeamSpawns = new HashMap<>(256);
    private final SpawnRepository spawnRepository;

    public ObjectiveSpawnRepository(SpawnRepository spawnRepository) {
        super(TABLE_NAME, TABLE_CREATE_SCRIPT);
        this.spawnRepository = spawnRepository;
    }

    @Override
    public void loaded(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            try {
                int teamId = resultSet.getInt("team_id");
                int spawnId = resultSet.getInt("spawn_id");
                int objectiveId = resultSet.getInt("objective_id");
                Spawn spawn = this.spawnRepository.get(spawnId);

                this.add(objectiveId, teamId, spawn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void add(int objectiveId, int teamId, Spawn spawn) {
        this.objectiveTeamSpawns.putIfAbsent(objectiveId, new TeamSpawn());
        this.objectiveTeamSpawns.get(objectiveId).add(spawn, teamId);
    }

    public TeamSpawn get(int id) {
        return this.objectiveTeamSpawns.get(id);
    }
}