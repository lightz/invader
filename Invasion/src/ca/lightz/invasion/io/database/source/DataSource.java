package ca.lightz.invasion.io.database.source;

import ca.lightz.invasion.io.database.repository.DatabaseDataLoader;

public interface DataSource {
    void execute(String query, Object... params);

    void executeUpdate(String query, Object... params);

    void executeQuery(String query, DatabaseDataLoader loader, Object... params);

    boolean hasTable(String name);
}
