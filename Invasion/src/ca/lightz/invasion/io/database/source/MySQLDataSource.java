package ca.lightz.invasion.io.database.source;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.io.database.repository.DatabaseDataLoader;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLDataSource implements DataSource {
    private static final String[] TABLE_TYPES = {"TABLE"};

    private HikariDataSource dataSource;

    public MySQLDataSource(Plugin plugin) {
        FileConfiguration config = plugin.getConfig();
        HikariConfig hikariConfig = new HikariConfig();

        String hostname = config.getString("mysql.hostname");
        String port = config.getString("mysql.port");
        String databaseName = config.getString("mysql.database");
        String dsn = "jdbc:mysql://" + hostname + ":" + port + "/" + databaseName + "?verifyServerCertificate=false&useSSL=true";
        hikariConfig.setJdbcUrl(dsn);

        hikariConfig.setUsername(config.getString("mysql.username"));
        hikariConfig.setPassword(config.getString("mysql.password"));

        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        this.dataSource = new HikariDataSource(hikariConfig);
    }

    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }

    @Override
    public void execute(String query, Object... params) {
        try (Connection connection = this.getConnection(); PreparedStatement statement = this.buildPreparedStatement(connection, query, params)) {
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void executeUpdate(String query, Object... params) {
        try (Connection connection = this.getConnection(); PreparedStatement statement = this.buildPreparedStatement(connection, query, params)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void executeQuery(String query, DatabaseDataLoader loader, Object... params) {
        try (Connection connection = this.getConnection();
             PreparedStatement statement = this.buildPreparedStatement(connection, query, params);
             ResultSet result = statement.executeQuery()) {

            loader.loaded(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement buildPreparedStatement(Connection connection, String query, Object... params) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query);

        int index = 1;
        for (Object param : params) {
            if (param instanceof String) {
                preparedStatement.setString(index, (String) param);
            } else if (param instanceof Integer) {
                preparedStatement.setInt(index, (int) param);
            } else if (param instanceof Double) {
                preparedStatement.setDouble(index, (double) param);
            } else if (param instanceof Long) {
                preparedStatement.setLong(index, (long) param);
            } else {
                Log.error("Cannot parse " + param + " for database update query.");
            }
            index++;
        }

        return preparedStatement;
    }

    @Override
    public boolean hasTable(String name) {
        try (Connection connection = this.getConnection(); ResultSet result = this.queryForTable(connection, name)) {
            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private ResultSet queryForTable(Connection connection, String name) throws SQLException {
        return connection.getMetaData().getTables(null, null, name, TABLE_TYPES);
    }
}
