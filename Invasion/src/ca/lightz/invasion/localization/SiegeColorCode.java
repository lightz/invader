package ca.lightz.invasion.localization;

import org.bukkit.ChatColor;

public enum SiegeColorCode {
    WARNING(ChatColor.YELLOW),
    SPAWN(ChatColor.AQUA),
    ERROR(ChatColor.RED),
    BUFF(ChatColor.GOLD),
    DEBUFF(ChatColor.RED),
    MISS(ChatColor.RED),
    SUCCESS(ChatColor.GREEN),
    CAPTURING(ChatColor.GREEN),
    ATTACKED(ChatColor.RED),
    NEUTRAL(ChatColor.GRAY),
    SHINY(ChatColor.GOLD),
    TIPS(ChatColor.GOLD),
    INFO(ChatColor.GRAY),
    GLOBAL_CHAT(ChatColor.GREEN),
    TEAM_CHAT(ChatColor.YELLOW),
    DESCRIPTION(ChatColor.AQUA);

    private final ChatColor chatColor;

    SiegeColorCode(ChatColor chatColor) {
        this.chatColor = chatColor;
    }

    @Override
    public String toString() {
        return this.chatColor.toString();
    }
}
