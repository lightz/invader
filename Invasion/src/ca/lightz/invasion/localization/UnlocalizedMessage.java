package ca.lightz.invasion.localization;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class UnlocalizedMessage {
    public final MessageId id;
    public final Object[] args;

    public UnlocalizedMessage(MessageId id, Object... args) {
        this.id = id;
        this.args = args;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!super.equals(other)) {
            return false;
        }
        if (!(other instanceof UnlocalizedMessage)) {
            return false;
        }

        UnlocalizedMessage otherMessage = (UnlocalizedMessage) other;
        return new EqualsBuilder().
                append(this.id, otherMessage.id).
                append(this.args, otherMessage.args).
                isEquals();
    }

    @Override
    public int hashCode() {
        int superHashCode = super.hashCode();

        return new HashCodeBuilder(43, 599).
                append(this.id).
                append(this.args).
                append(superHashCode).
                toHashCode();
    }
}
