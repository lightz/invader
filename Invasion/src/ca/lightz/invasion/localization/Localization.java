package ca.lightz.invasion.localization;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Localization {
    private static final Map<String, String> MESSAGES = new HashMap<>();

    public static void loadFiles(YamlConfiguration... configurations) {
        for (YamlConfiguration configuration : configurations) {
            Configuration root = configuration.getRoot();
            for (String key : root.getKeys(false)) {
                MESSAGES.put(key, root.getString(key));
            }
        }
    }

    public String localize(UnlocalizedMessage message) {
        return localize(message.id, message.args);
    }

    public String localize(MessageId id, Object... args) {
        for (int i = 0; i < args.length; i++) {
            Object object = args[i];
            if (object instanceof UnlocalizedMessage) {
                UnlocalizedMessage nested = (UnlocalizedMessage) object;
                args[i] = localize(nested.id, nested.args);
            }
        }

        return localize(id.getName(), args);
    }

    private String localize(String toLocalize, Object... args) {
        String text = MESSAGES.get(toLocalize);
        MessageFormat message = new MessageFormat(text, Locale.ENGLISH);
        return message.format(args);
    }
}
