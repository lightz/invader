package ca.lightz.invasion.localization;

import ca.lightz.invasion.game.player.PlayerHolder;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.connorlinfoot.titleapi.TitleAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;

public class Messenger {
    private static Localization LOCALIZATION;

    public static void setLocalization(Localization localization) {
        LOCALIZATION = localization;
    }

    public static void send(PlayerHolder playerHolder, Collection<UnlocalizedMessage> messages) {
        for (UnlocalizedMessage message : messages) {
            send(playerHolder, message);
        }
    }

    public static void send(PlayerHolder player, UnlocalizedMessage message) {
        send(player.player, message.id, message.args);
    }

    public static void send(Player player, UnlocalizedMessage message) {
        send(player, message.id, message.args);
    }

    public static void send(PlayerHolder player, MessageId messageId, Object... args) {
        send(player.player, messageId, args);
    }

    public static void send(Player player, MessageId messageId, Object... args) {
        if (player == null) {
            return;
        }
        String localized = localize(messageId, args);
        player.sendMessage(localized);
    }

    public static void sendToActionBar(PlayerHolder player, MessageId messageId, Object... args) {
        sendToActionBar(player.player, messageId, args);
    }

    public static void sendToActionBar(Player player, MessageId messageId, Object... args) {
        if (player == null) {
            return;
        }
        String localized = localize(messageId, args);
        ActionBarAPI.sendActionBar(player, localized);
    }

    private static String localize(MessageId messageId, Object... args) {
        return LOCALIZATION.localize(messageId, args);
    }

    public static void sendTitle(Player player, String message, String subMessage) {
        TitleAPI.sendTitle(player, 10, 60, 10, message, subMessage);
    }

    public static void sendTitle(String message, String subMessage) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendTitle(player, message, subMessage);
        }
    }
}
