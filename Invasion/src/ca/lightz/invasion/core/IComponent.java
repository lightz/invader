package ca.lightz.invasion.core;

public interface IComponent {
    default String getComponentName() {
        return this.getClass().getSimpleName();
    }
}
