package ca.lightz.invasion.core.lore;

import ca.lightz.invasion.core.utility.priority.IPriority;
import org.bukkit.ChatColor;

public enum LoreType implements IPriority {
    TITLE(5, ChatColor.DARK_AQUA),
    DESCRIPTION(25, ChatColor.WHITE),
    LORE(50, ChatColor.GRAY),
    TIP(75, ChatColor.YELLOW),
    HOW_TO_USE(100, ChatColor.GOLD);

    private final int priority;
    private final ChatColor defaultColor;

    LoreType(int priority, ChatColor defaultColor) {
        this.priority = priority;
        this.defaultColor = defaultColor;
    }

    @Override
    public int getPriority() {
        return this.priority;
    }

    public ChatColor getDefaultColor() {
        return this.defaultColor;
    }
}
