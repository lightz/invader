package ca.lightz.invasion.core.lore;

import ca.lightz.invasion.core.utility.priority.IPriority;
import ca.lightz.invasion.core.utility.priority.PriorityComparator;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class LoreBlock implements IPriority {
    private static final PriorityComparator PRIORITY_COMPARATOR = PriorityComparator.INSTANCE;

    private LoreType category;
    private LinkedList<String> text;
    private List<LoreBlock> subBlocks = new LinkedList<>();

    public LoreBlock(String... text) {
        this(LoreType.TITLE, text);
    }

    public LoreBlock(LoreType category, String... text) {
        this.category = category;
        this.setText(text);
    }

    public LoreBlock(LoreType category, Iterable<String> text) {
        this.category = category;

        this.text = new LinkedList<>();
        for (String line : text) {
            this.text.add(category.getDefaultColor() + line);
        }
    }

    public void setText(String... text) {
        this.text = new LinkedList<>();
        for (String line : text) {
            this.text.add(category.getDefaultColor() + line);
        }
    }

    public LoreBlock addBlock(LoreType type, String... text) {
        LoreBlock subBlock = new LoreBlock(type, text);
        this.addBlock(subBlock);
        return this;
    }

    public LoreBlock addBlock(LoreBlock subBlock) {
        this.subBlocks.add(subBlock);
        return this;
    }

    public LinkedList<String> getSortedLore(Set<LoreType> display) {
        LinkedList<String> lines = new LinkedList<>();
        if (display.isEmpty() || display.contains(this.category)) {
            lines.addAll(this.text);
        }

        this.subBlocks.sort(PRIORITY_COMPARATOR);
        for (LoreBlock subBlock : this.subBlocks) {
            lines.addAll(subBlock.getSortedLore(display));
        }
        return lines;
    }

    @Override
    public int getPriority() {
        return this.category.getPriority();
    }

    public void clear() {
        this.text = new LinkedList<>();
        this.subBlocks = new LinkedList<>();
    }
}
