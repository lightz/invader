package ca.lightz.invasion.core.lore;

import com.mysql.jdbc.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Lore {
    public static final int MAX_CHARACTERS_PER_LINE = 50;

    public static String buildSpacedLine(String firstSection, String secondSection) {
        int length = firstSection.length() + secondSection.length();
        int numberOfSpaces = MAX_CHARACTERS_PER_LINE - length;
        numberOfSpaces += numberOfSpaces / 5;

        StringBuilder builder = new StringBuilder(MAX_CHARACTERS_PER_LINE);
        builder.append(firstSection);
        for (int i = 0; i < numberOfSpaces; i++) {
            builder.append(" ");
        }
        builder.append(secondSection);
        return builder.toString();
    }

    public static List<String> formatLore(List<String> input) {
        List<String> loreStrings = new LinkedList<>();
        for (String line : input) {
            formatLore(loreStrings, line);
        }
        return loreStrings;
    }

    public static List<String> formatLore(String input) {
        return formatLore(new LinkedList<>(), input);
    }

    public static List<String> formatLore(List<String> loreStrings, String input) {
        String[] lore = toLore(input);
        loreStrings.addAll(Arrays.asList(lore));
        return loreStrings;
    }

    public static String[] toLore(String input) {
        if (StringUtils.isNullOrEmpty(input)) {
            return new String[0];
        }

        int numberInvisibleCharacters = input.length() - ChatColor.stripColor(input).length();

        String wrappedText = WordUtils.wrap(input, MAX_CHARACTERS_PER_LINE + numberInvisibleCharacters);
        String[] lines = wrappedText.split(System.lineSeparator());

        for (int i = 1; i < lines.length; i++) {
            String lastColor = ChatColor.getLastColors(lines[i - 1]);
            lines[i] = lastColor + lines[i];
        }

        return lines;
    }

    public static ItemStack addLoreFormatted(ItemStack stack, String... input) {
        for (String str : input) {
            addLoreFormatted(stack, str);
        }
        return stack;
    }

    public static ItemStack addLoreFormatted(ItemStack stack, Collection<String> input) {
        for (String str : input) {
            addLoreFormatted(stack, str);
        }
        return stack;
    }

    public static ItemStack addLoreFormatted(ItemStack stack, String input) {
        ItemMeta itemMeta = stack.getItemMeta();
        if (itemMeta == null) {
            return stack;
        }

        addLoreFormatted(itemMeta, input);
        stack.setItemMeta(itemMeta);
        return stack;
    }

    public static void addLoreFormatted(ItemMeta itemMeta, String input) {
        LinkedList<String> formattedLore = new LinkedList<>();
        if (itemMeta.hasLore()) {
            formattedLore.addAll(itemMeta.getLore());
        }
        formattedLore.addAll(Arrays.asList(Lore.toLore(input)));
        itemMeta.setLore(formattedLore);
    }

    public static void replaceOrAdd(ItemMeta itemMeta, String description, String key) {
        if (!itemMeta.hasLore()) {
            Lore.addLoreFormatted(itemMeta, description);
            return;
        }

        List<String> lore = itemMeta.getLore();
        int index = Lore.getKeyIndex(lore, key);
        if (index >= 0) {
            //noinspection ConstantConditions
            lore.set(index, description);
            itemMeta.setLore(lore);
        } else {
            Lore.addLoreFormatted(itemMeta, description);
        }
    }

    private static int getKeyIndex(List<String> lore, String key) {
        if (lore == null) {
            return -1;
        }

        for (int i = 0; i < lore.size(); i++) {
            String line = lore.get(i);
            if (line != null && line.contains(key)) {
                return i;
            }
        }
        return -1;
    }

    public static void removeAllLoreLinesWith(ItemMeta itemMeta, String key) {
        if (itemMeta == null || !itemMeta.hasLore()) {
            return;
        }

        List<String> lore = itemMeta.getLore();
        if (lore == null) {
            return;
        }

        lore.removeIf(line -> line.contains(key));
        itemMeta.setLore(lore);
    }
}
