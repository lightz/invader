package ca.lightz.invasion.core.lore;

import ca.lightz.invasion.core.Components;
import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.item.component.ILoreHolder;
import ca.lightz.invasion.core.utility.priority.PriorityComparator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class LoreStorage {
    private static final PriorityComparator PRIORITY_COMPARATOR = PriorityComparator.INSTANCE;

    private Set<LoreType> toDisplay = new HashSet<>(1);
    private LinkedList<LoreBlock> loreBlocks = new LinkedList<>();

    public LoreStorage() {
    }

    public <T extends IComponent> LoreStorage(Components<T> itemEffects) {
        for (ILoreHolder loreHolder : itemEffects.getAllItemComponentsOfType(ILoreHolder.class)) {
            LoreBlock loreBlock = loreHolder.getLoreBlock();
            if (loreBlock != null) {
                this.loreBlocks.add(loreBlock);
            }
        }
    }

    public LinkedList<String> buildLore() {
        this.loreBlocks.sort(PRIORITY_COMPARATOR);

        LinkedList<String> lines = new LinkedList<>();
        Iterator<LoreBlock> loreBlockIterator = this.loreBlocks.iterator();

        while (loreBlockIterator.hasNext()) {
            LoreBlock loreBlock = loreBlockIterator.next();
            LinkedList<String> loreBlockLines = loreBlock.getSortedLore(this.toDisplay);

            for (String line : loreBlockLines) {
                lines.addAll(Lore.formatLore(line));
            }

            if (loreBlockIterator.hasNext()) {
                //If next, add space? need a rule to split category and/or effect?
            }
        }

        return lines;
    }
}
