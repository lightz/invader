package ca.lightz.invasion.core.gui.input;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.gui.click.UIClick;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.utility.Coordinate;

import java.util.Map;

public class InputSlot extends UIElement {
    protected Runnable callback;

    public InputSlot(Runnable callback) {
        super();
        this.allowPlacement = true;
        this.callback = callback;
    }

    @Override
    public void click(UIClick click, Coordinate offset) {
        if (this.callback != null) {
            this.callback.run();
        }
    }

    @Override
    public void buildGUIItems(Map<Coordinate, LoreItem> mapping) {
    }
}
