package ca.lightz.invasion.core.gui.button;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.gui.click.UIClick;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.lore.LoreBlock;
import ca.lightz.invasion.core.lore.LoreType;
import ca.lightz.invasion.core.utility.Coordinate;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Button extends UIElement {
    protected final Map<ClickType, Consumer<UIClick>> callbacks = new HashMap<>(ClickType.values().length / 4);
    protected Material visual = Material.STICK;
    protected String displayName = "TBD";
    protected LoreBlock description;

    public Button() {
        super();
    }

    public Button(Consumer<UIClick> callback, ClickType... clickTypes) {
        this();
        this.setFunctionality(callback, clickTypes);
    }

    public void setFunctionality(Consumer<UIClick> callback, ClickType... clickTypes) {
        for (ClickType clickType : clickTypes) {
            this.callbacks.put(clickType, callback);
        }
    }

    public void setVisual(Material visual, String displayName, String... lore) {
        this.visual = visual;
        this.displayName = displayName;
        this.description = new LoreBlock(LoreType.DESCRIPTION, lore);
    }

    public void addLore(String... lore) {
        this.description.addBlock(LoreType.DESCRIPTION, lore);
    }

    public void addLore(List<String> lore) {
        this.description.addBlock(new LoreBlock(LoreType.DESCRIPTION, lore));
    }

    public LoreItem getItem() {
        return this.buildBase(this.visual, this.displayName, this.description);
    }

    @Override
    public void click(UIClick click, Coordinate offset) {
        InventoryClickEvent event = click.getEvent();
        ClickType clickType = event.getClick();
        Consumer<UIClick> callback = this.callbacks.get(clickType);
        if (callback != null) {
            callback.accept(click);
        }
    }

    @Override
    public void buildGUIItems(Map<Coordinate, LoreItem> mapping) {
        LoreItem button = this.getItem();
        mapping.put(new Coordinate(0, 0), button);
    }
}

