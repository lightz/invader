package ca.lightz.invasion.core.gui.position.orientation;

import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public interface Orientation {

    Coordinate getCoordinate(Dimension dimension, int currentIndex);
}
