package ca.lightz.invasion.core.gui.position.anchoring;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

class BottomCenter implements Anchor {

    @Override
    public Coordinate getCoordinateIn(Dimension dimension, UIElement uiElement) {
        int width = dimension.getWidth();
        int x = (width - uiElement.getWidth()) / 2;
        return new Coordinate(x, (dimension.getHeight() - uiElement.getHeight()));
    }
}
