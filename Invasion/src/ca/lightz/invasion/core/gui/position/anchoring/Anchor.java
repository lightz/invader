package ca.lightz.invasion.core.gui.position.anchoring;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public interface Anchor {
    Coordinate getCoordinateIn(Dimension dimension, UIElement uiElement);
}
