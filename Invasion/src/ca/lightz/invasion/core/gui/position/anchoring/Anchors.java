package ca.lightz.invasion.core.gui.position.anchoring;

public class Anchors {
    public static final Anchor TOP_LEFT = new TopLeft();
    public static final Anchor TOP_CENTER = new TopCenter();
    public static final Anchor TOP_RIGHT = new TopRight();
    public static final Anchor CENTER = new Center();
    public static final Anchor BOTTOM_CENTER = new BottomCenter();
}
