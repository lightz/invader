package ca.lightz.invasion.core.gui.position.orientation;

import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public class Vertical implements Orientation {

    @Override
    public Coordinate getCoordinate(Dimension dimension, int currentIndex) {
        int height = dimension.getHeight();
        int x = currentIndex / height;
        int y = currentIndex % height;
        return new Coordinate(x, y);
    }
}