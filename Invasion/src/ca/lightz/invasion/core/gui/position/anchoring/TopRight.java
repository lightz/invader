package ca.lightz.invasion.core.gui.position.anchoring;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

class TopRight implements Anchor {

    @Override
    public Coordinate getCoordinateIn(Dimension dimension, UIElement uiElement) {
        return new Coordinate(dimension.getWidth() - uiElement.getWidth(), 0);
    }
}
