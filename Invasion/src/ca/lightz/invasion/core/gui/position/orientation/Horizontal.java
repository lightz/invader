package ca.lightz.invasion.core.gui.position.orientation;

import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public class Horizontal implements Orientation {

    @Override
    public Coordinate getCoordinate(Dimension dimension, int currentIndex) {
        int width = dimension.getWidth();
        int y = currentIndex / width;
        int x = currentIndex % width;
        return new Coordinate(x, y);
    }
}
