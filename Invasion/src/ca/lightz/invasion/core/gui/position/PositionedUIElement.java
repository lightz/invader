package ca.lightz.invasion.core.gui.position;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public interface PositionedUIElement {

    UIElement getUIElement();

    Coordinate getCoordinate(Dimension dimension);
}
