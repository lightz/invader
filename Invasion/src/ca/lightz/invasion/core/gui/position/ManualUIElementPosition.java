package ca.lightz.invasion.core.gui.position;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public class ManualUIElementPosition implements PositionedUIElement {
    protected UIElement uiElement;
    protected Coordinate coordinate;

    public ManualUIElementPosition(UIElement element, Coordinate coordinate) {
        this.uiElement = element;
        this.coordinate = coordinate;
    }


    @Override
    public UIElement getUIElement() {
        return this.uiElement;
    }

    @Override
    public Coordinate getCoordinate(Dimension dimension) {
        return this.coordinate;
    }
}
