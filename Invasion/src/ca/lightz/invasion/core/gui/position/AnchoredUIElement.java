package ca.lightz.invasion.core.gui.position;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.gui.position.anchoring.Anchor;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

public class AnchoredUIElement implements PositionedUIElement {
    protected UIElement uiElement;
    protected Anchor anchor;

    public AnchoredUIElement(UIElement element, Anchor anchor) {
        this.uiElement = element;
        this.anchor = anchor;
    }

    @Override
    public UIElement getUIElement() {
        return this.uiElement;
    }

    @Override
    public Coordinate getCoordinate(Dimension dimension) {
        return this.anchor.getCoordinateIn(dimension, this.uiElement);
    }
}
