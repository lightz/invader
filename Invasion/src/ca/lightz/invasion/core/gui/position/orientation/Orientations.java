package ca.lightz.invasion.core.gui.position.orientation;

public class Orientations {
    public static final Orientation HORIZONTAL = new Horizontal();
    public static final Orientation VERTICAL = new Vertical();
}
