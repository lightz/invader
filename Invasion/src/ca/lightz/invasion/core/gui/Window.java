package ca.lightz.invasion.core.gui;

import ca.lightz.invasion.Invasion;
import ca.lightz.invasion.core.gui.button.Button;
import ca.lightz.invasion.core.gui.click.UIClick;
import ca.lightz.invasion.core.gui.container.Fillable;
import ca.lightz.invasion.core.gui.container.Panel;
import ca.lightz.invasion.core.gui.container.UIContainer;
import ca.lightz.invasion.core.gui.position.anchoring.Anchor;
import ca.lightz.invasion.core.inventory.MultiInventory;
import ca.lightz.invasion.core.inventory.item.ItemWrapperFactory;
import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import ca.lightz.invasion.core.inventory.item.quantity.LeftOver;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class Window implements InventoryHolder, Fillable, UIContainer {
    public static final int INVENTORY_WIDTH = 9;
    protected static final ItemStack AIR = new ItemStack(Material.AIR);

    protected static final BukkitScheduler BUKKIT_SCHEDULER = Bukkit.getScheduler();
    protected static final Plugin PLUGIN_INSTANCE = Invasion.getPlugin(); //TODO update this to make it plugin agnostic
    protected static int DELAY_FOR_INVENTORY_UPDATE = 0;

    protected String title;
    protected Panel mainPanel;

    protected Inventory inventory;

    public Window(String title, int height) {
        this.title = title;
        this.mainPanel = new Panel(new Dimension(INVENTORY_WIDTH, height));
    }

    protected void setHeight(int height) {
        if (height == this.mainPanel.dimension.getHeight()) {
            return;
        }

        this.mainPanel.setDimension(new Dimension(INVENTORY_WIDTH, height));
    }

    public void updateVisual() {
        this.drawInventory();
        BUKKIT_SCHEDULER.scheduleSyncDelayedTask(PLUGIN_INSTANCE, this::updateInventoryOfViewers, DELAY_FOR_INVENTORY_UPDATE);
    }

    @Override
    public void addUIElement(UIElement element, Coordinate cornerCoordinate) {
        this.mainPanel.addUIElement(element, cornerCoordinate);
    }

    @Override
    public void addUIElement(UIElement element, Anchor anchor) {
        this.mainPanel.addUIElement(element, anchor);
    }

    public void onInventoryClose(InventoryCloseEvent event) {
        List<ItemStack> itemsToGiveBack = this.getItemsToGiveBack();
        if (itemsToGiveBack.isEmpty()) {
            return;
        }

        Player player = (Player) event.getPlayer();
        MultiInventory playerInventory = new MultiInventory(player);
        Location playerLocation = player.getLocation();

        for (ItemStack itemStack : itemsToGiveBack) {
            ItemWrapper itemWrapper = ItemWrapperFactory.wrap(itemStack);
            LeftOver leftOver = playerInventory.addItem(new ItemQuantity(itemWrapper, itemStack.getAmount()));

            leftOver.dropAllAt(playerLocation);
        }
    }

    protected List<ItemStack> getItemsToGiveBack() {
        List<ItemStack> items = new ArrayList<>();

        int slotIndex = -1;
        for (ItemStack content : this.inventory.getContents()) {
            slotIndex++;
            if (content == null || content.getType() == Material.AIR) {
                continue;
            }

            Coordinate coordinate = this.getCoordinateFromSlot(slotIndex);
            UIElement element = this.mainPanel.getAt(coordinate);
            if (element == null || element.allowsPlacement()) {
                items.add(content);
            }
        }

        return items;
    }

    public boolean isUIItem(int slotIndex) {
        Coordinate coordinate = this.getCoordinateFromSlot(slotIndex);
        UIElement element = this.mainPanel.getAt(coordinate);
        if (element == null || element.allowsPlacement()) {
            return false;
        }
        return true;
    }

    public void onInventoryDrag(InventoryDragEvent event) {
        if (this.mainPanel.allowsPlacement()) {
            this.triggerInputUpdate();
            return;
        }

        if (this.affectsAtLeastOneWindowSlot(event.getRawSlots())) {
            event.setCancelled(true);
        }
    }

    protected boolean affectsAtLeastOneWindowSlot(Collection<Integer> rawSlots) {
        for (int rawSlot : rawSlots) {
            if (rawSlot >= this.mainPanel.getSurfaceSize()) {
                return true;
            }
        }
        return false;
    }

    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getRawSlot() >= this.mainPanel.getSurfaceSize()) {
            this.handleBottomInventoryClick(event);
        } else {
            this.handleTopInventoryClick(event);
        }
    }

    protected void handleBottomInventoryClick(InventoryClickEvent event) {
        InventoryAction action = event.getAction();
        if (action == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
            if (this.mainPanel.allowsPlacement()) {
                this.triggerInputUpdate();
            } else {
                event.setCancelled(true);
            }
        }
    }

    protected void triggerInputUpdate() {
        BUKKIT_SCHEDULER.scheduleSyncDelayedTask(PLUGIN_INSTANCE, this::windowInputUpdate, DELAY_FOR_INVENTORY_UPDATE);
    }

    protected void windowInputUpdate() {
    }

    protected void handleTopInventoryClick(InventoryClickEvent event) {
        Coordinate coordinate = this.getCoordinateFromSlot(event.getSlot());
        UIElement element = this.mainPanel.getAt(coordinate);
        if (element == null) {
            this.handleClickOnEmptySpace(event);
            return;
        }

        if (!element.allowsPlacement()) {
            event.setCancelled(true);
        }
        this.mainPanel.click(new UIClick(event, element), coordinate);
    }

    protected void handleClickOnEmptySpace(Cancellable event) {
        if (this.mainPanel.allowsPlacement()) {
            this.triggerInputUpdate();
        } else {
            event.setCancelled(true);
        }
    }

    protected Coordinate getCoordinateFromSlot(int slot) {
        int y = slot / INVENTORY_WIDTH;
        int x = slot - (y * INVENTORY_WIDTH);
        return new Coordinate(x, y);
    }

    @Override
    public Inventory getInventory() {
        return this.drawInventory();
    }

    protected Inventory drawInventory() {
        if (this.inventory == null) {
            this.inventory = Bukkit.createInventory(this, this.mainPanel.getSurfaceSize(), this.title);
        }

        for (Map.Entry<Coordinate, LoreItem> entry : this.mainPanel.getItems().entrySet()) {
            Coordinate coordinate = entry.getKey();
            int slot = this.getSlotFromCoordinate(coordinate);

            LoreItem loreItem = entry.getValue();
            ItemStack itemStack = loreItem != null ? entry.getValue().spawn() : AIR;

            this.inventory.setItem(slot, itemStack);
        }
        return this.inventory;
    }

    protected void updateInventoryOfViewers() {
        if (this.inventory == null) {
            return;
        }

        for (HumanEntity viewer : this.inventory.getViewers()) {
            if (viewer instanceof Player) {
                ((Player) viewer).updateInventory();
            }
        }
    }

    protected int getSlotFromCoordinate(Coordinate coordinate) {
        return (coordinate.getY() * INVENTORY_WIDTH) + coordinate.getX();
    }

    @Override
    public void setFill(Button filling) {
        this.mainPanel.setFill(filling);
    }
}
