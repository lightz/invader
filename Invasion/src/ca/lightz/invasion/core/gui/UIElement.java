package ca.lightz.invasion.core.gui;

import ca.lightz.invasion.core.gui.click.Clickable;
import ca.lightz.invasion.core.gui.click.UIClick;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.item.component.LoreComponent;
import ca.lightz.invasion.core.lore.LoreBlock;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public abstract class UIElement implements Clickable {
    protected Dimension dimension;
    protected boolean allowPlacement = false;

    public UIElement() {
        this(new Dimension(1, 1));
    }

    public UIElement(Dimension dimension) {
        this.setDimension(dimension);
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public int getHeight() {
        return this.dimension.getHeight();
    }

    public int getWidth() {
        return this.dimension.getWidth();
    }

    public int getSurfaceSize() {
        return this.dimension.getSurfaceSize();
    }

    public boolean allowsPlacement() {
        return this.allowPlacement;
    }

    public void setAllowPlacement(boolean allowPlacement) {
        this.allowPlacement = allowPlacement;
    }

    @Override
    public abstract void click(UIClick click, Coordinate offset);

    public Map<Coordinate, LoreItem> getItems() {
        Map<Coordinate, LoreItem> items = new HashMap<>(this.getSurfaceSize());
        this.buildGUIItems(items);
        return items;
    }

    public abstract void buildGUIItems(Map<Coordinate, LoreItem> mapping);

    protected LoreItem buildBase(Material material, String displayName, LoreBlock loreBlock) {
        ItemStack item = new ItemStack(material);
        LoreItem loreItem = new LoreItem(item);
        loreItem.setDisplayName(displayName);

        LoreComponent lore = new LoreComponent(loreBlock);
        loreItem.addComponent(lore);

        return loreItem;
    }
}
