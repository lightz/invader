package ca.lightz.invasion.core.gui.event;

import ca.lightz.invasion.core.gui.Window;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

public class GuiListener implements Listener {

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        InventoryView inventoryView = event.getView();
        Inventory topInventory = inventoryView.getTopInventory();
        if (this.isRegularInventory(topInventory) || topInventory.getHolder() == null) {
            return;
        }

        Window window = (Window) topInventory.getHolder();
        window.onInventoryDrag(event);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        InventoryView inventoryView = event.getView();
        Inventory topInventory = inventoryView.getTopInventory();
        if (this.isRegularInventory(topInventory) || topInventory.getHolder() == null) {
            return;
        }

        Window window = (Window) topInventory.getHolder();
        window.onInventoryClick(event);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        InventoryView inventoryView = event.getView();
        Inventory topInventory = inventoryView.getTopInventory();
        if (this.isRegularInventory(topInventory) || topInventory.getHolder() == null) {
            return;
        }

        Window window = (Window) topInventory.getHolder();
        window.onInventoryClose(event);
    }

    private boolean isRegularInventory(Inventory inventory) {
        return inventory == null || !(inventory.getHolder() instanceof Window);
    }
}
