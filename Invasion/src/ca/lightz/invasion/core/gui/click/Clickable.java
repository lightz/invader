package ca.lightz.invasion.core.gui.click;

import ca.lightz.invasion.core.utility.Coordinate;

public interface Clickable {
    void click(UIClick click, Coordinate coordinate);
}
