package ca.lightz.invasion.core.gui.click;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.utility.KeyValuePair;
import org.bukkit.event.inventory.InventoryClickEvent;

public class UIClick extends KeyValuePair<InventoryClickEvent, UIElement> {

    public UIClick(InventoryClickEvent key, UIElement value) {
        super(key, value);
    }

    public InventoryClickEvent getEvent() {
        return this.key;
    }

    public UIElement getClickedUIElement() {
        return this.value;
    }
}
