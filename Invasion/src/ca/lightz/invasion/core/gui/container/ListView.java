package ca.lightz.invasion.core.gui.container;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.gui.button.Button;
import ca.lightz.invasion.core.gui.click.UIClick;
import ca.lightz.invasion.core.gui.position.orientation.Orientation;
import ca.lightz.invasion.core.gui.position.orientation.Orientations;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ListView extends UIElement implements Fillable, Clearable {
    //TODO if any button is bigger than 1x1 this class will derp
    protected List<Button> buttons = new LinkedList<>();
    protected Map<Coordinate, Button> buttonsMapping;

    protected Orientation orientation;
    protected Button filling;

    protected Runnable refreshCallback;

    protected int maximumNumberOfPaginationButtons = 2;
    protected int numberOfPages;
    protected int currentPageIndex;

    public ListView(Runnable refreshCallback, Dimension dimension) {
        this(refreshCallback, dimension, Orientations.HORIZONTAL);
    }

    public ListView(Runnable refreshCallback, Dimension dimension, Orientation orientation) {
        super(dimension);
        this.orientation = orientation;
        this.refreshCallback = refreshCallback;
    }

    @Override
    public void setFill(Button filling) {
        this.filling = filling;
    }

    public void addButton(Button button) {
        this.buttons.add(button);
        this.buttonsMapping = null;
        this.currentPageIndex = -1;
    }

    @Override
    public void click(UIClick click, Coordinate offset) {
        Button button = this.buttonsMapping != null ? this.buttonsMapping.get(offset) : null;
        if (button != null) {
            UIClick subClick = new UIClick(click.getEvent(), button);
            button.click(subClick, new Coordinate(0, 0)); //TODO If button is bigger than 1x1 it won't work
        }
    }

    @Override
    public void buildGUIItems(Map<Coordinate, LoreItem> mapping) {
        this.setupMapping();
        this.setUpNumberOfPages();
        int index = -1;

        for (Button button : this.getButtons()) {
            index++;
            if (button == null) {
                continue;
            }

            Coordinate cornerCoordinate = this.orientation.getCoordinate(this.dimension, index);
            Map<Coordinate, LoreItem> buttonMapping = button.getItems();
            for (Map.Entry<Coordinate, LoreItem> entry : buttonMapping.entrySet()) {
                Coordinate buttonCoordinate = entry.getKey().add(cornerCoordinate);

                this.buttonsMapping.put(buttonCoordinate, button);
                mapping.put(buttonCoordinate, entry.getValue());
            }
        }

        this.fill(mapping);
    }

    protected void setupMapping() {
        int maximumSizePerPage = this.dimension.getSurfaceSize();
        int size = this.filling == null ? Math.min(this.buttons.size(), maximumSizePerPage) : maximumSizePerPage;
        this.buttonsMapping = new HashMap<>(size);
    }

    protected void setUpNumberOfPages() {
        int numberOfButtons = this.buttons.size();
        int maximumSizePerPage = this.dimension.getSurfaceSize();

        this.numberOfPages = 1;
        if (numberOfButtons > maximumSizePerPage) {
            int numberPaginationButtons = this.maximumNumberOfPaginationButtons;
            this.numberOfPages++;

            int maximumElementsOnTheTwoCoverPages = (maximumSizePerPage * 2) - numberPaginationButtons;
            if (numberOfButtons > maximumElementsOnTheTwoCoverPages) {
                int overflowingButtons = numberOfButtons - maximumElementsOnTheTwoCoverPages;

                int maximumElementsOnMiddlePages = maximumSizePerPage - numberPaginationButtons;
                this.numberOfPages += overflowingButtons / maximumElementsOnMiddlePages;
                if (overflowingButtons % maximumElementsOnMiddlePages > 0) {
                    this.numberOfPages++;
                }
            }
        }

        this.currentPageIndex = Math.max(0, this.currentPageIndex);
    }

    protected LinkedList<Button> getButtons() {
        int maximumSize = this.dimension.getSurfaceSize();

        int firstPageOffset = Math.min(1, this.currentPageIndex);
        int offsetIndexFromFirstPage = firstPageOffset * (maximumSize - (this.maximumNumberOfPaginationButtons / 2));
        int offset = offsetIndexFromFirstPage + ((this.currentPageIndex - firstPageOffset) * (maximumSize - this.maximumNumberOfPaginationButtons));

        LinkedList<Button> paginationButtons = this.getPaginationButtons();
        LinkedList<Button> buttonsToDisplay = new LinkedList<>();

        int numberOfUseableSpace = maximumSize - paginationButtons.size();
        int endIndex = Math.min(offset + numberOfUseableSpace, this.buttons.size());
        for (int i = offset; i < endIndex; i++) {
            Button button = this.buttons.get(i);
            buttonsToDisplay.add(button);
        }

        if (this.hasMultiplePages() && this.isOnLastPage()) {
            int numberOfEmptySpaces = numberOfUseableSpace - buttonsToDisplay.size();
            for (int i = 0; i < numberOfEmptySpaces; i++) {
                buttonsToDisplay.add(this.filling); //This is to make sure that navigation buttons are always at the end on the last page
            }
        }

        buttonsToDisplay.addAll(paginationButtons);
        return buttonsToDisplay;
    }

    protected LinkedList<Button> getPaginationButtons() {
        LinkedList<Button> paginationButtons = new LinkedList<>();
        if (!this.isOnFirstPage()) {
            Button previousPageButton = new Button(this::previousPage, ClickType.LEFT, ClickType.RIGHT);
            previousPageButton.setVisual(Material.REDSTONE, String.format("%sPrevious Page (%d/%d)", ChatColor.WHITE, this.currentPageIndex, this.numberOfPages), ChatColor.GOLD + "<Click to go to the previous page>");
            paginationButtons.addFirst(previousPageButton);
        }

        if (this.hasMultiplePages() && !this.isOnLastPage()) {
            Button nextPageButton = new Button(this::nextPage, ClickType.LEFT, ClickType.RIGHT);
            nextPageButton.setVisual(Material.HOPPER, String.format("%sNext Page (%d/%d)", ChatColor.WHITE, (this.currentPageIndex + 2), this.numberOfPages), ChatColor.GOLD + "<Click to go to the next page>");
            paginationButtons.addLast(nextPageButton);
        }

        return paginationButtons;
    }

    protected boolean isOnFirstPage() {
        return this.currentPageIndex <= 0;
    }

    protected boolean hasMultiplePages() {
        return this.numberOfPages > 1;
    }

    protected boolean isOnLastPage() {
        return (this.currentPageIndex + 1) == this.numberOfPages;
    }

    public void previousPage(UIClick click) {
        this.currentPageIndex--;
        this.refresh();
    }

    public void nextPage(UIClick click) {
        this.currentPageIndex++;
        this.refresh();
    }

    protected void fill(Map<Coordinate, LoreItem> mapping) {
        LoreItem fillingItem = this.filling != null ? this.filling.getItem() : null;
        for (int x = 0; x < this.dimension.getWidth(); x++) {
            for (int y = 0; y < this.dimension.getHeight(); y++) {
                mapping.putIfAbsent(new Coordinate(x, y), fillingItem);
            }
        }
    }

    protected void refresh() {
        if (this.refreshCallback != null) {
            this.refreshCallback.run();
        }
    }

    @Override
    public void clear() {
        this.buttons = new LinkedList<>();
        this.buttonsMapping = null;
        this.currentPageIndex = -1;
    }
}
