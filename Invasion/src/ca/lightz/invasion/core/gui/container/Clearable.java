package ca.lightz.invasion.core.gui.container;

public interface Clearable {

    void clear();
}
