package ca.lightz.invasion.core.gui.container;

import ca.lightz.invasion.core.gui.button.Button;

public interface Fillable {

    void setFill(Button filling);
}
