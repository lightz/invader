package ca.lightz.invasion.core.gui.container;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.gui.position.anchoring.Anchor;
import ca.lightz.invasion.core.utility.Coordinate;

public interface UIContainer {

    void addUIElement(UIElement element, Coordinate coordinate);

    void addUIElement(UIElement element, Anchor anchor);
}
