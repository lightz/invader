package ca.lightz.invasion.core.gui.container;

import ca.lightz.invasion.core.gui.UIElement;
import ca.lightz.invasion.core.gui.button.Button;
import ca.lightz.invasion.core.gui.click.UIClick;
import ca.lightz.invasion.core.gui.position.AnchoredUIElement;
import ca.lightz.invasion.core.gui.position.ManualUIElementPosition;
import ca.lightz.invasion.core.gui.position.PositionedUIElement;
import ca.lightz.invasion.core.gui.position.anchoring.Anchor;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.core.utility.Coordinate;
import ca.lightz.invasion.core.utility.Dimension;

import java.util.*;

public class Panel extends UIElement implements Fillable, UIContainer {
    protected LinkedList<PositionedUIElement> elements = new LinkedList<>();
    protected Map<Coordinate, UIElement> elementsCorner;
    protected Map<Coordinate, Coordinate> mapping;

    protected Button filling;

    public Panel(Dimension dimension) {
        super(dimension);
    }

    public void setDimension(Dimension dimension) {
        super.setDimension(dimension);
        this.elementsCorner = null;
        this.mapping = null;
    }

    @Override
    public void addUIElement(UIElement element, Coordinate coordinate) {
        PositionedUIElement uiElementPosition = new ManualUIElementPosition(element, coordinate);
        this.addUIElement(uiElementPosition);
    }

    @Override
    public void addUIElement(UIElement element, Anchor anchor) {
        PositionedUIElement uiElementPosition = new AnchoredUIElement(element, anchor);
        this.addUIElement(uiElementPosition);
    }

    protected void addUIElement(PositionedUIElement positionedUIElement) {
        this.elements.addLast(positionedUIElement);
        if (positionedUIElement.getUIElement().allowsPlacement()) {
            this.allowPlacement = true;
        }
    }

    @Override
    public void click(UIClick click, Coordinate coordinate) {
        Coordinate cornerCoordinate = this.mapping.get(coordinate);
        if (cornerCoordinate == null) {
            return;
        }
        UIElement element = this.elementsCorner.get(cornerCoordinate);
        if (element == null) {
            return;
        }

        UIClick subClick = new UIClick(click.getEvent(), element);
        element.click(subClick, coordinate.subtract(cornerCoordinate));
    }

    @Override
    public void buildGUIItems(Map<Coordinate, LoreItem> mapping) {
        if (this.elementsCorner == null) {
            this.positionElements();
        }

        for (Map.Entry<Coordinate, UIElement> entry : this.elementsCorner.entrySet()) {
            Coordinate corner = entry.getKey();
            UIElement element = entry.getValue();

            for (Map.Entry<Coordinate, LoreItem> partEntry : element.getItems().entrySet()) {
                Coordinate partCoordinate = partEntry.getKey().add(corner);
                mapping.put(partCoordinate, partEntry.getValue());
            }
        }
    }

    protected void positionElements() {
        this.elementsCorner = new HashMap<>(this.elements.size());
        this.mapping = new HashMap<>(this.getSurfaceSize());

        for (PositionedUIElement positionedUIElement : this.elements) {
            Coordinate cornerCoordinate = positionedUIElement.getCoordinate(this.dimension);
            UIElement element = positionedUIElement.getUIElement();
            this.elementsCorner.put(cornerCoordinate, element);
            this.generateMapping(cornerCoordinate, element);
        }

        this.fill();
    }

    protected void generateMapping(Coordinate cornerCoordinate, UIElement element) {
        for (int x = 0; x < element.getWidth(); x++) {
            for (int y = 0; y < element.getHeight(); y++) {
                Coordinate coordinate = cornerCoordinate.add(x, y);
                this.mapping.put(coordinate, cornerCoordinate);
            }
        }
    }

    protected void fill() {
        if (this.filling == null) {
            return;
        }

        for (int x = 0; x < this.dimension.getWidth(); x++) {
            for (int y = 0; y < this.dimension.getHeight(); y++) {
                Coordinate coordinate = new Coordinate(x, y);
                if (!this.mapping.containsKey(coordinate)) {
                    this.elementsCorner.put(coordinate, this.filling);
                    this.generateMapping(coordinate, this.filling);
                }
            }
        }
    }

    public List<Coordinate> getAllOccupiedCoordinatesOf(UIElement uiElement) {
        Coordinate corner = this.getCornerOf(uiElement);
        if (corner == null) {
            return new ArrayList<>(1);
        }

        List<Coordinate> coordinates = new ArrayList<>(uiElement.getSurfaceSize());
        for (Map.Entry<Coordinate, Coordinate> entry : this.mapping.entrySet()) {
            if (corner == entry.getValue()) {
                coordinates.add(entry.getKey());
            }
        }
        return coordinates;
    }

    protected Coordinate getCornerOf(UIElement uiElement) {
        for (Map.Entry<Coordinate, UIElement> entry : this.elementsCorner.entrySet()) {
            if (uiElement == entry.getValue()) {
                return entry.getKey();
            }
        }
        return null;
    }

    public UIElement getAt(Coordinate coordinate) {
        Coordinate corner = this.mapping.get(coordinate);
        if (corner == null) {
            return null;
        }
        return this.elementsCorner.get(corner);
    }

    @Override
    public void setFill(Button filling) {
        this.filling = filling;
    }
}

