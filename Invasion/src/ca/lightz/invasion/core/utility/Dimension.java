package ca.lightz.invasion.core.utility;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Dimension {
    private int width;
    private int height;
    private int surface;

    public Dimension(int width, int height) {
        this.width = width;
        this.height = height;
        this.surface = this.height * this.width;
    }

    public Dimension add(Dimension other) {
        return new Dimension(this.width + other.width, this.height + other.height);
    }

    public Dimension subtract(Dimension other) {
        return new Dimension(this.width - other.width, this.height - other.height);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getSurfaceSize() {
        return this.surface;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Dimension)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        Dimension otherCoordinate = (Dimension) object;
        return new EqualsBuilder().
                append(this.width, otherCoordinate.width).
                append(this.height, otherCoordinate.height).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(3, 37). // two randomly chosen prime numbers
                append(this.width).
                append(this.height).
                toHashCode();
    }
}
