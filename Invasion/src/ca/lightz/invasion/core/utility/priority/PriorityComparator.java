package ca.lightz.invasion.core.utility.priority;

import java.util.Comparator;

public class PriorityComparator implements Comparator<IPriority> {
    public static final PriorityComparator INSTANCE = new PriorityComparator();

    @Override
    public int compare(IPriority first, IPriority second) {
        return Integer.compare(first.getPriority(), second.getPriority());
    }
}
