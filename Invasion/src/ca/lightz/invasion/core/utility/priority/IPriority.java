package ca.lightz.invasion.core.utility.priority;

public interface IPriority {
    int getPriority();
}
