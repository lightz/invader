package ca.lightz.invasion.core.utility;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Coordinate {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordinate add(int x, int y) {
        return new Coordinate(this.x + x, this.y + y);
    }

    public Coordinate add(Coordinate other) {
        return new Coordinate(this.x + other.x, this.y + other.y);
    }

    public Coordinate subtract(Coordinate other) {
        return new Coordinate(this.x - other.x, this.y - other.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Coordinate)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        Coordinate otherCoordinate = (Coordinate) object;
        return new EqualsBuilder().
                append(this.x, otherCoordinate.x).
                append(this.y, otherCoordinate.y).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(3, 31). // two randomly chosen prime numbers
                append(this.x).
                append(this.y).
                toHashCode();
    }

    @Override
    public String toString() {
        return String.format("%d:%d", this.x, this.y);
    }
}
