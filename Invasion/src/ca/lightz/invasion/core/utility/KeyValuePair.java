package ca.lightz.invasion.core.utility;

public class KeyValuePair<K, V> {
    public K key;
    public V value;

    public KeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
