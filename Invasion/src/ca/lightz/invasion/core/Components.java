package ca.lightz.invasion.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class Components<T extends IComponent> implements Iterable<T> {
    private final LinkedHashMap<String, T> components;

    public Components() {
        this.components = new LinkedHashMap<>(1);
    }

    public Components(Iterable<T> components) {
        this.components = new LinkedHashMap<>(4);

        for (T component : components) {
            this.components.put(component.getComponentName(), component);
        }
    }

    public <K extends IComponent> boolean hasComponent(Class<K> wantedClass) {
        return this.components.containsKey(wantedClass.getSimpleName());
    }

    public <K extends IComponent> ArrayList<K> getAllItemComponentsOfType(Class<K> wantedClass) {
        ArrayList<K> wantedComponents = new ArrayList<>(1);
        for (IComponent component : this.components.values()) {
            if (wantedClass.isInstance(component)) {
                wantedComponents.add(wantedClass.cast(component));
            }
        }
        return wantedComponents;
    }

    public void addComponent(T component) {
        if (component != null) {
            this.components.put(component.getComponentName(), component);
        }
    }

    public void addComponent(Iterable<T> components) {
        for (T component : components) {
            this.components.put(component.getComponentName(), component);
        }
    }

    @Override
    public Iterator<T> iterator() {
        return this.components.values().iterator();
    }

    public boolean isEmpty() {
        return this.components.isEmpty();
    }

    public int size() {
        return this.components.size();
    }
}
