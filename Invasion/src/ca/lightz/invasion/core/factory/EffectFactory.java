package ca.lightz.invasion.core.factory;

import ca.lightz.invasion.core.Components;
import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.core.item.component.INBTSerializable;
import ca.lightz.invasion.core.item.serialize.NBTStorage;
import com.mysql.jdbc.StringUtils;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Constructor;
import java.util.*;

public class EffectFactory {
    private static final String EFFECT_LIST_KEY = "effect_list";
    private static final String SPLITTER = ";";

    private final Plugin plugin;

    private final List<String> effectPackages = new LinkedList<>();
    private final Map<String, Constructor<?>> effectsMapping = new HashMap<>();

    public EffectFactory(Plugin plugin, String... effectPackages) {
        this.plugin = plugin;
        Collections.addAll(this.effectPackages, effectPackages);
    }

    public void registerClassPackage(String classPackage) {
        this.effectPackages.add(classPackage);
    }

    public NBTStorage compactEffectsToNBT(Components<IComponent> container) {
        NBTStorage storage = new NBTStorage(this.plugin);

        StringBuilder itemEffectIDs = new StringBuilder();
        for (INBTSerializable serializable : container.getAllItemComponentsOfType(INBTSerializable.class)) {
            serializable.serializeIDsToNBT(storage);
            itemEffectIDs.append(serializable.getComponentName()).append(SPLITTER);
        }

        String serializedIDs = itemEffectIDs.toString();
        if (!StringUtils.isNullOrEmpty(serializedIDs)) {
            storage.setToNBT(EFFECT_LIST_KEY, serializedIDs);
        }

        return storage;
    }

    public Components<IComponent> unpackEffectsFromNBT(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta == null) {
            return new Components<>();
        }

        NBTStorage storage = new NBTStorage(this.plugin, itemMeta.getPersistentDataContainer());
        return this.deserializeItemEffects(storage);
    }

    private Components<IComponent> deserializeItemEffects(NBTStorage storage) {
        LinkedList<IComponent> effects = new LinkedList<>();
        String unformattedEffectList = storage.getStringFromNBT(EFFECT_LIST_KEY);

        if (StringUtils.isNullOrEmpty(unformattedEffectList)) {
            return new Components<>();
        }

        String[] classNames = unformattedEffectList.split(SPLITTER);
        for (String className : classNames) {
            Optional<IComponent> optionalItemEffect = this.buildComponentFromClassName(className);

            if (optionalItemEffect.isPresent()) {
                IComponent itemEffect = optionalItemEffect.get();
                if (itemEffect instanceof INBTSerializable) {
                    ((INBTSerializable) itemEffect).loadDataFromNBT(storage);
                }
                effects.add(itemEffect);
            }
        }

        return new Components<>(effects);
    }

    private Optional<IComponent> buildComponentFromClassName(String simpleClassName) {
        Constructor<?> constructor = this.getItemEffectConstructor(simpleClassName);
        if (constructor == null) {
            return Optional.empty();
        }

        try {
            IComponent itemComponent = (IComponent) constructor.newInstance();
            return Optional.of(itemComponent);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private Constructor<?> getItemEffectConstructor(String simpleClassName) {
        Constructor<?> constructor = this.effectsMapping.get(simpleClassName);
        if (constructor != null) {
            return constructor;
        }
        return this.findConstructorForItemEffect(simpleClassName);
    }

    private Constructor<?> findConstructorForItemEffect(String simpleClassName) {
        for (String effectPackage : this.effectPackages) {
            String className = effectPackage + "." + simpleClassName;
            try {
                Class<?> someClass = Class.forName(className);
                Constructor<?> constructor = someClass.getDeclaredConstructor();

                Log.debug(String.format("Mapped %s with the package %s. (%s)", simpleClassName, effectPackage, className));
                this.effectsMapping.put(simpleClassName, constructor);

                return constructor;
            } catch (Exception ignore) {
            }
        }

        Log.error(String.format("Couldn't map %s to an ItemComponent", simpleClassName));
        return null;
    }

    public IComponent buildItemEffect(String className) {
        Optional<IComponent> optionalItemEffect = this.buildComponentFromClassName(className);
        return optionalItemEffect.orElse(null);
    }

    public <T extends IComponent> T buildItemEffect(Class<T> wantedClass) {
        Optional<IComponent> optionalItemEffect = this.buildComponentFromClassName(wantedClass.getSimpleName());
        return optionalItemEffect.map(wantedClass::cast).orElse(null);
    }
}
