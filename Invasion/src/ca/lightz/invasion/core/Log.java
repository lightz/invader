package ca.lightz.invasion.core;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class Log {
    private static Logger LOGGER;

    public static void init(JavaPlugin plugin) {
        LOGGER = plugin.getLogger();
    }

    public static void info(String message) {
        LOGGER.info(message);
    }

    public static void debug(String message) {
        info("[DEBUG] " + message);
    }

    public static void warning(String message) {
        LOGGER.warning(message);
    }

    public static void error(String message) {
        LOGGER.severe(message);
    }
}
