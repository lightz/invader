package ca.lightz.invasion.core.item;

import ca.lightz.invasion.core.Components;
import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.factory.EffectFactory;
import ca.lightz.invasion.core.item.serialize.NBTStorage;
import ca.lightz.invasion.core.lore.LoreStorage;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Objects;

public class LoreItem implements ILoreItem {
    public static EffectFactory EFFECT_FACTORY;

    private ItemBase base;
    private int amount;
    private String name;

    private Components<IComponent> effects;

    public LoreItem(LoreItem toClone) {
        this.base = new ItemBase(toClone.base);
        this.amount = toClone.amount;
        this.name = toClone.name;

        this.effects = new Components<>(toClone.effects);
    }

    public LoreItem(ItemStack itemStack) {
        ItemStack clone = itemStack.clone();
        this.base = new ItemBase(clone);
        this.amount = clone.getAmount();
        this.effects = EFFECT_FACTORY.unpackEffectsFromNBT(clone);
    }

    public ItemStack spawn() {
        return this.spawn(this.amount);
    }

    public ItemStack spawn(int amount) {
        NBTStorage storage = EFFECT_FACTORY.compactEffectsToNBT(this.effects);
        return this.base.buildBase(storage, amount, this.getLore(), this.name);
    }

    public LoreStorage getLore() {
        return new LoreStorage(this.effects); //TODO rebuild it only if something changed
    }

    //region Effects

    public <T extends IComponent> boolean hasComponent(Class<T> wantedClass) {
        return this.effects.hasComponent(wantedClass);
    }

    public <T extends IComponent> ArrayList<T> getAllItemComponentsOfType(Class<T> wantedClass) {
        return this.effects.getAllItemComponentsOfType(wantedClass);
    }

    public void addComponent(IComponent component) {
        this.effects.addComponent(component);
    }

    public void addComponent(Iterable<IComponent> components) {
        this.effects.addComponent(components);
    }

    //endregion

    public String getDisplayName() {
        if (StringUtils.isBlank(this.name)) {
            return this.base.getItemMeta().getDisplayName();
        }
        return this.name;
    }

    public Material getMaterial() {
        return this.base.getType();
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setDisplayName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.amount, this.name, this.effects.size());
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof LoreItem)) {
            return false;
        }

        LoreItem loreItem = (LoreItem) other;
        if (this.amount != loreItem.amount) {
            return false;
        }
        if (this.effects.size() != loreItem.effects.size()) {
            return false;
        }
        return Objects.equals(this.name, loreItem.name);
    }
}
