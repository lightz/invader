package ca.lightz.invasion.core.item;

import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.lore.LoreStorage;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public interface ILoreItem {

    ItemStack spawn();

    ItemStack spawn(int amount);

    LoreStorage getLore();

    //region Effects

    <T extends IComponent> boolean hasComponent(Class<T> wantedClass);

    <T extends IComponent> ArrayList<T> getAllItemComponentsOfType(Class<T> wantedClass);

    void addComponent(IComponent component);

    void addComponent(Iterable<IComponent> components);

    //endregion

    String getDisplayName();

    Material getMaterial();

    int getAmount();

    void setAmount(int amount);

    void setDisplayName(String name);
}
