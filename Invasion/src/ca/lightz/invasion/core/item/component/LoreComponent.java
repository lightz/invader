package ca.lightz.invasion.core.item.component;

import ca.lightz.invasion.core.lore.LoreBlock;

public class LoreComponent implements ILoreHolder {
    protected LoreBlock loreBlock;

    public LoreComponent() {
        this(new LoreBlock());
    }

    public LoreComponent(LoreBlock loreBlock) {
        this.loreBlock = loreBlock;
    }

    @Override
    public LoreBlock getLoreBlock() {
        return this.loreBlock;
    }
}
