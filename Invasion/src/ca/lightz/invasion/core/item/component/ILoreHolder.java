package ca.lightz.invasion.core.item.component;

import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.lore.LoreBlock;

public interface ILoreHolder extends IComponent {
    LoreBlock getLoreBlock();
}
