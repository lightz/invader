package ca.lightz.invasion.core.item.component;

import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.item.serialize.NBTStorage;

public interface INBTSerializable extends IComponent {
    void serializeIDsToNBT(NBTStorage storage);

    void loadDataFromNBT(NBTStorage storage);
}
