package ca.lightz.invasion.core.item;

import ca.lightz.invasion.core.item.serialize.NBTStorage;
import ca.lightz.invasion.core.lore.LoreStorage;
import com.mysql.jdbc.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

import java.lang.reflect.Field;
import java.util.LinkedList;

public class ItemBase {
    private static final String FIELD_TO_MAKE_PUBLIC = "persistentDataContainer";
    private ItemMeta itemMeta;
    private Material material;

    public ItemBase(ItemBase toClone) {
        this.itemMeta = toClone.itemMeta.clone();
        this.material = toClone.material;
    }

    public ItemBase(ItemStack itemStack) {
        this.itemMeta = itemStack.getItemMeta();
        this.material = itemStack.getType();
    }

    public ItemMeta getItemMeta() {
        return this.itemMeta.clone();
    }

    public Material getType() {
        return this.material;
    }

    public ItemStack buildBase(NBTStorage storage, int amount, LoreStorage loreStorage, String displayName) {
        ItemStack itemStack = new ItemStack(this.material, amount);
        if (this.itemMeta == null) {
            return itemStack;
        }

        ItemMeta itemMeta = this.itemMeta.clone();
        this.patchItemMetaWithCustomTagContainer(itemMeta, storage.getContainer());
        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

        if (!StringUtils.isNullOrEmpty(displayName)) {
            itemMeta.setDisplayName(ChatColor.WHITE + displayName);
        }

        LinkedList<String> lore = loreStorage.buildLore();
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    private void patchItemMetaWithCustomTagContainer(ItemMeta itemMetaToPatch, PersistentDataContainer container) {
        try {
            Field field = this.getField();
            field.setAccessible(true);
            field.set(itemMetaToPatch, container);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private Field getField() throws NoSuchFieldException {
        try {
            return this.itemMeta.getClass().getDeclaredField(FIELD_TO_MAKE_PUBLIC);
        } catch (NoSuchFieldException ignore) {
            try {
                return this.itemMeta.getClass().getSuperclass().getDeclaredField(FIELD_TO_MAKE_PUBLIC);
            } catch (NoSuchFieldException ex) {
                ex.printStackTrace();
            }
        }
        throw new NoSuchFieldException("No such field as persistentDataContainer.");
    }
}
