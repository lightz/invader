package ca.lightz.invasion.core.item.serialize;

import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_15_R1.persistence.CraftPersistentDataContainer;
import org.bukkit.craftbukkit.v1_15_R1.persistence.CraftPersistentDataTypeRegistry;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

public class NBTStorage {
    private static final Map<String, NamespacedKey> KEYS = new HashMap<>();

    private final Plugin plugin;
    private PersistentDataContainer container;

    public NBTStorage(Plugin plugin) {
        this(plugin, new CraftPersistentDataContainer(new CraftPersistentDataTypeRegistry()));
    }

    public NBTStorage(Plugin plugin, PersistentDataContainer container) {
        this.container = container;
        this.plugin = plugin;
    }

    public PersistentDataContainer getContainer() {
        return this.container;
    }

    public int getFromNBTOrDefault(String key, int defaultValue) {
        NamespacedKey namespacedKey = this.getKey(key);
        return this.container.getOrDefault(namespacedKey, PersistentDataType.INTEGER, defaultValue);
    }

    public boolean getFromNBTOrDefault(String key, boolean defaultValue) {
        NamespacedKey namespacedKey = this.getKey(key);
        Byte value = this.container.get(namespacedKey, PersistentDataType.BYTE);
        return value == null ? defaultValue : (value == 1);
    }

    public String getStringFromNBT(String key) {
        NamespacedKey namespacedKey = this.getKey(key);
        return this.container.get(namespacedKey, PersistentDataType.STRING);
    }

    public void setToNBT(String key, String value) {
        NamespacedKey namespacedKey = this.getKey(key);
        this.container.set(namespacedKey, PersistentDataType.STRING, value);
    }

    public void setToNBT(String key, int value) {
        NamespacedKey namespacedKey = this.getKey(key);
        this.container.set(namespacedKey, PersistentDataType.INTEGER, value);
    }

    public void setToNBT(String key, boolean value) {
        NamespacedKey namespacedKey = this.getKey(key);
        byte bool = (byte) (value ? 1 : 0);
        this.container.set(namespacedKey, PersistentDataType.BYTE, bool);
    }

    private NamespacedKey getKey(String key) {
        return KEYS.computeIfAbsent(key, k -> new NamespacedKey(this.plugin, k.toLowerCase()));
    }
}
