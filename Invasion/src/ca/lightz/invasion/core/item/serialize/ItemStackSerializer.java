package ca.lightz.invasion.core.item.serialize;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.v1_15_R1.CommandException;
import net.minecraft.server.v1_15_R1.MojangsonParser;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class ItemStackSerializer {

    public static ItemStack deserializeItemStack(String json) {
        if (json == null || json.equals("empty")) {
            return null;
        }

        try {
            NBTTagCompound nbtTagCompound = MojangsonParser.parse(json);
            net.minecraft.server.v1_15_R1.ItemStack craftItemStack = net.minecraft.server.v1_15_R1.ItemStack.a(nbtTagCompound);
            return CraftItemStack.asBukkitCopy(craftItemStack);
        } catch (CommandException | CommandSyntaxException exception) {
            exception.printStackTrace();
        }

        return null;
    }

    public static String serializeItemStack(ItemStack itemStack) {
        if (itemStack == null) {
            return "empty";
        }

        net.minecraft.server.v1_15_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = new NBTTagCompound();
        nmsItemStack.save(nbtTagCompound);

        return nbtTagCompound.toString();
    }
}
