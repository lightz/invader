package ca.lightz.invasion.core.item;

import ca.lightz.invasion.core.IComponent;
import ca.lightz.invasion.core.lore.LoreStorage;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Objects;

public class EmptyItem implements ILoreItem {

    public ItemStack spawn() {
        return new ItemStack(Material.AIR);
    }

    public ItemStack spawn(int amount) {
        return this.spawn();
    }

    public LoreStorage getLore() {
        return new LoreStorage(); //TODO rebuild it only if something changed
    }

    //region Effects

    public <T extends IComponent> boolean hasComponent(Class<T> wantedClass) {
        return false;
    }

    public <T extends IComponent> ArrayList<T> getAllItemComponentsOfType(Class<T> wantedClass) {
        return new ArrayList<>();
    }

    public void addComponent(IComponent component) {
    }

    public void addComponent(Iterable<IComponent> components) {
    }

    //endregion

    public String getDisplayName() {
        return "";
    }

    public Material getMaterial() {
        return Material.AIR;
    }

    public int getAmount() {
        return 1;
    }

    public void setAmount(int amount) {
    }

    public void setDisplayName(String name) {
    }

    @Override
    public int hashCode() {
        return Objects.hash(Material.AIR);
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        return other instanceof EmptyItem;
    }
}
