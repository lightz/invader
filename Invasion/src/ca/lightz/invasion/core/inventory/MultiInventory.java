
package ca.lightz.invasion.core.inventory;

import ca.lightz.invasion.core.inventory.filter.Filter;
import ca.lightz.invasion.core.inventory.item.ItemWrapperFactory;
import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import ca.lightz.invasion.core.inventory.item.quantity.LeftOver;
import ca.lightz.invasion.core.inventory.item.quantity.NoItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class MultiInventory {
    private final LinkedList<Inventory> inventories = new LinkedList<>();

    public MultiInventory() {
    }

    public MultiInventory(InventoryHolder inventoryHolder) {
        this(inventoryHolder.getInventory());
    }

    public MultiInventory(Inventory inventory) {
        this.addInventory(inventory);
    }

    public void addInventory(Inventory inv) {
        this.inventories.add(inv);
    }

    public boolean setItem(int i, ItemStack item) {
        int index = 0;

        for (Inventory inventory : this.inventories) {
            if (i < index + inventory.getSize()) {
                inventory.setItem(i - index, item);
                return true;
            }
            index += inventory.getSize();
        }
        return false;
    }

    public LeftOver addItems(Collection<ItemQuantity> items) {
        LeftOver leftOver = new LeftOver();

        for (ItemQuantity itemQuantity : items) {
            leftOver.addLeftOver(this.addItem(itemQuantity));
        }

        return leftOver;
    }

    public LeftOver addItem(ItemQuantity item) {
        List<ItemStack> itemStacks = item.spawn();
        return this.addItem(itemStacks);
    }

    public LeftOver addItem(List<ItemStack> itemStacks) {
        LeftOver totalLeftOvers = new LeftOver();

        for (ItemStack itemStack : itemStacks) {
            LeftOver leftOver = this.addItem(itemStack);
            totalLeftOvers.addLeftOver(leftOver);
        }
        return totalLeftOvers;
    }

    public LeftOver addItem(ItemStack itemStack) {
        int leftOverAmount = 0;
        for (Inventory inv : this.inventories) {
            leftOverAmount = 0;
            for (ItemStack leftOver : inv.addItem(itemStack).values()) {
                leftOverAmount += leftOver.getAmount();
            }

            if (leftOverAmount == 0) {
                break;
            }

            if (leftOverAmount != itemStack.getAmount()) {
                itemStack.setAmount(leftOverAmount); // Some items were deposited, update the items count and try again.
            }
        }

        if (leftOverAmount > 0) {
            return new LeftOver(new ItemQuantity(ItemWrapperFactory.wrap(itemStack), leftOverAmount));
        }

        return new LeftOver(new NoItem());
    }

    public boolean remove(ItemQuantity itemQuantity) {
        Collection<ItemInvPair> toBeRemoved = this.findItemsToBeRemoved(itemQuantity.item, itemQuantity.quantity, itemQuantity.quantity);
        if (toBeRemoved.isEmpty()) {
            return false;
        }

        for (ItemInvPair invPair : toBeRemoved) {
            this.removeItemFromInventory(invPair);
        }

        this.updateIfHolderIsPlayer(toBeRemoved);
        return true;
    }

    public int removeAll(ItemWrapper item) {
        return this.removeUpToMaximumItems(item, Integer.MAX_VALUE);
    }

    public int removeUpToMaximumItems(ItemWrapper item, int maximumToConsume) {
        Collection<ItemInvPair> toBeRemoved = this.findItemsToBeRemoved(item, maximumToConsume, 1);
        int count = 0;
        for (ItemInvPair invPair : toBeRemoved) {
            count += this.removeItemFromInventory(invPair);
        }

        this.updateIfHolderIsPlayer(toBeRemoved);
        return count;
    }

    private void updateIfHolderIsPlayer(Collection<ItemInvPair> invPairs) {
        Map<UUID, Player> playersToUpdate = new HashMap<>();

        for (ItemInvPair invPair : invPairs) {
            if (invPair.inventory.getHolder() instanceof Player) {
                Player player = (Player) invPair.inventory.getHolder();
                playersToUpdate.put(player.getUniqueId(), player);
            }
        }

        for (Player player : playersToUpdate.values()) {
            player.updateInventory();
        }
    }

    private Collection<ItemInvPair> findItemsToBeRemoved(ItemWrapper itemWrapper, int count, int minimumValid) {
        if (count <= 0) {
            return new ArrayList<>();
        }

        Collection<ItemInvPair> toBeRemoved = new ArrayList<>();
        for (Inventory inv : this.inventories) {
            for (ItemStack item : inv.getContents()) {
                if (item == null || item.getType() == Material.AIR) {
                    continue;
                }

                ItemQuantity itemQuantity = itemWrapper.getWhatToRemove(item, count);
                if (itemQuantity.quantity <= 0) {
                    continue;
                }

                ItemInvPair itemInvPair = new ItemInvPair(inv, itemQuantity);
                toBeRemoved.add(itemInvPair);
                count -= (itemQuantity.quantity * itemQuantity.valuePerItem);

                if (count <= 0) {
                    return toBeRemoved;
                }
            }
        }
        if (count < minimumValid) {
            return new ArrayList<>();
        }
        return toBeRemoved;
    }

    private int removeItemFromInventory(ItemInvPair itemInvPair) {
        int removed = 0;
        int leftToRemove = itemInvPair.amount;

        ItemStack[] contents = itemInvPair.inventory.getContents();
        for (int i = 0; i < contents.length; i++) {
            ItemStack stack = contents[i];
            if (!itemInvPair.item.isValid(stack)) {
                continue;
            }

            int stackAmount = stack.getAmount();
            if (stackAmount > leftToRemove) {
                stack.setAmount(stackAmount - leftToRemove); // We've got more than enough in this stack. Reduce it's size.
                removed += leftToRemove * itemInvPair.valuePerItem;
                leftToRemove = 0; // We've got it all.
            } else {
                removed += stackAmount * itemInvPair.valuePerItem;
                leftToRemove -= stackAmount;
                contents[i] = null;
            }

            if (leftToRemove == 0) {
                break; // We've removed everything. No need to continue.
            }
        }
        itemInvPair.inventory.setContents(contents);
        return removed;
    }

    public int getFreeSpaceInStorageFor(ItemWrapper wrap, int maximumNeeded) {
        int maxStackSize = wrap.getMaxStackSize();
        int freeSpace = 0;

        for (Inventory inv : this.inventories) {
            for (ItemStack item : inv.getStorageContents()) {
                if (item == null || item.getType() == Material.AIR) {
                    freeSpace += maxStackSize;
                } else if (wrap.isValid(item)) {
                    freeSpace += wrap.getMaxStackSize() - item.getAmount();
                }
                if (freeSpace >= maximumNeeded) {
                    return freeSpace;
                }
            }
        }
        return freeSpace;
    }

    public boolean hasSpaceForInStorage(final Collection<ItemQuantity> items) {
        int freeSlots = 0;

        List<ItemQuantity> copy = new LinkedList<>();
        for (ItemQuantity item : items) {
            copy.add(item.copy());
        }

        for (Inventory inv : this.inventories) {
            for (ItemStack item : inv.getStorageContents()) {
                if (item == null || item.getType() == Material.AIR) {
                    freeSlots++;
                    continue;
                }

                Iterator<ItemQuantity> iterator = copy.iterator();
                while (iterator.hasNext()) {
                    ItemQuantity itemQuantity = iterator.next();
                    if (!itemQuantity.item.isValid(item)) {
                        continue;
                    }
                    int placeLeft = itemQuantity.item.getMaxStackSize() - item.getAmount();
                    int left = itemQuantity.quantity - placeLeft;
                    if (left <= 0) {
                        iterator.remove();
                    } else {
                        itemQuantity.quantity = left;
                    }
                    break; //We calculated what can go in this slot
                }

                if (copy.isEmpty()) {
                    return true; //we were able to find a slot for every items
                }
            }
        }
        if (freeSlots <= 0) {
            return false;
        }

        Iterator<ItemQuantity> iterator = items.iterator();
        while (iterator.hasNext() && freeSlots > 0) {
            ItemQuantity itemQuantity = iterator.next();
            iterator.remove();
            int stackSize = itemQuantity.item.getMaxStackSize();
            int slotsNeeded = (int) Math.ceil(itemQuantity.quantity / (double) stackSize);
            freeSlots -= slotsNeeded;
        }

        return freeSlots >= 0;
    }

    public List<ItemStack> findItemsWith(Filter<ItemStack> filter) {
        List<ItemStack> items = new ArrayList<>();
        for (Inventory inv : this.inventories) {
            for (ItemStack item : inv.getContents()) {
                if (filter.isValid(item)) {
                    items.add(item);
                }
            }
        }
        return items;
    }

    public boolean contains(ItemQuantity itemQuantity) {
        int count = 0;
        for (Inventory inv : this.inventories) {
            for (ItemStack item : inv.getContents()) {
                if (!itemQuantity.item.isValid(item)) {
                    continue;
                }

                ItemQuantity quantityAvailable = itemQuantity.item.getWhatToRemove(item, item.getAmount());
                count += (quantityAvailable.quantity * quantityAvailable.valuePerItem);
                if (count >= itemQuantity.quantity) {
                    return true;
                }
            }
        }
        return false;
    }

    public ItemStack[] getStorageContent() {
        int size = this.inventories.stream().mapToInt(inventory -> inventory.getContents().length).sum();
        ItemStack[] content = new ItemStack[size];

        int globalIndex = 0;
        for (Inventory inventory : this.inventories) {
            for (int inventoryIndex = 0; inventoryIndex < inventory.getContents().length; inventoryIndex++) {
                content[globalIndex] = inventory.getContents()[inventoryIndex];
                globalIndex++;
            }
        }
        return content;
    }

    public int countOccupiedSlots() {
        int count = 0;
        for (Inventory inv : this.inventories) {
            for (ItemStack item : inv.getContents()) {
                if (this.isAnItem(item)) {
                    count++;
                }
            }
        }
        return count;
    }

    public List<Inventory> getInventories() {
        return this.inventories;
    }

    private boolean isAnItem(ItemStack itemStack) {
        return itemStack != null && itemStack.getType() != Material.AIR;
    }
}