
package ca.lightz.invasion.core.inventory;

import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import org.bukkit.inventory.Inventory;

class ItemInvPair {
    public final Inventory inventory;
    public final ItemWrapper item;
    public final int amount;
    public final int valuePerItem;

    public ItemInvPair(Inventory inventory, ItemQuantity item) {
        this.inventory = inventory;
        this.item = item.item;
        this.amount = item.quantity;
        this.valuePerItem = item.valuePerItem;
    }
}