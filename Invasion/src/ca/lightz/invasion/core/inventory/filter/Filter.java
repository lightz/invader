package ca.lightz.invasion.core.inventory.filter;

public interface Filter<T> {
    boolean isValid(T toCheck);
}
