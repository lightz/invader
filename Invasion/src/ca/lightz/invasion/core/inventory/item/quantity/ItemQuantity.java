package ca.lightz.invasion.core.inventory.item.quantity;

import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ItemQuantity {
    public final ItemWrapper item;
    public int quantity;
    public int valuePerItem = 1;

    public ItemQuantity(ItemWrapper item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public List<ItemStack> spawn() {
        List<ItemStack> itemStacks = this.item.spawn();
        for (ItemStack itemStack : itemStacks) {
            itemStack.setAmount(this.quantity);
        }
        return itemStacks;
    }

    public void setValuePerItem(int value) {
        this.valuePerItem = value;
    }

    public ItemQuantity copy() {
        return new ItemQuantity(this.item, this.quantity);
    }

    public boolean isEmpty() {
        return this.quantity <= 0;
    }

    public String getDisplayName() {
        return this.item.getDisplayName();
    }
}
