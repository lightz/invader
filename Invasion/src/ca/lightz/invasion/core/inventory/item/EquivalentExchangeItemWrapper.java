package ca.lightz.invasion.core.inventory.item;

import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class EquivalentExchangeItemWrapper implements ItemWrapper {
    private final ItemWrapper itemWrapper;
    private final int value;

    public EquivalentExchangeItemWrapper(ItemWrapper itemWrapper, int value) {
        this.itemWrapper = itemWrapper;
        this.value = value;
    }

    @Override
    public boolean isValid(ItemStack itemStack) {
        return this.itemWrapper.isValid(itemStack);
    }

    @Override
    public ItemQuantity getWhatToRemove(ItemStack itemStack, int count) {
        int maxCountToRemove = (int) Math.ceil((double) count / (double) this.value);
        ItemQuantity itemQuantity = this.itemWrapper.getWhatToRemove(itemStack, maxCountToRemove);
        itemQuantity.setValuePerItem(this.value);
        return itemQuantity;
    }

    @Override
    public List<ItemStack> spawn() {
        return this.itemWrapper.spawn();
    }

    @Override
    public int getMaxStackSize() {
        return this.itemWrapper.getMaxStackSize();
    }

    @Override
    public String getDisplayName() {
        return this.itemWrapper.getDisplayName();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof EquivalentExchangeItemWrapper)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        EquivalentExchangeItemWrapper other = (EquivalentExchangeItemWrapper) object;
        return this.value == other.value && this.itemWrapper.equals(other.itemWrapper);
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(3, 11);
        hashCodeBuilder.append(this.value);
        hashCodeBuilder.append(this.itemWrapper.hashCode());
        return hashCodeBuilder.toHashCode();
    }
}
