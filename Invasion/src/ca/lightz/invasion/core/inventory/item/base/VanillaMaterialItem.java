package ca.lightz.invasion.core.inventory.item.base;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class VanillaMaterialItem implements ItemWrapper {
    private final Material material;

    public VanillaMaterialItem(Material material) {
        this.material = material;
    }

    @Override
    public boolean isValid(ItemStack itemStack) {
        return itemStack != null && this.material == itemStack.getType(); //TODO check for custom item
    }

    @Override
    public List<ItemStack> spawn() {
        List<ItemStack> items = new ArrayList<>();
        items.add(new ItemStack(this.material));
        return items;
    }

    @Override
    public int getMaxStackSize() {
        return this.material.getMaxStackSize();
    }

    @Override
    public String getDisplayName() {
        ItemStack itemStack = this.spawn().get(0);
        return itemStack.hasItemMeta() ? itemStack.getItemMeta().getDisplayName() : this.material.name();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof VanillaMaterialItem)) {
            return false;
        }
        if (object == this) {
            return true;
        }


        VanillaMaterialItem other = (VanillaMaterialItem) object;
        return this.material == other.material;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(11, 41). // two randomly chosen prime numbers
                append(this.material).
                toHashCode();
    }

    public Material getMaterial() {
        return this.material;
    }
}