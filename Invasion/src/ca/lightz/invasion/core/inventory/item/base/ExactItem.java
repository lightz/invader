package ca.lightz.invasion.core.inventory.item.base;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ExactItem implements ItemWrapper {
    private final ItemStack item;

    public ExactItem(ItemStack itemStack) {
        this.item = itemStack.clone();
    }

    @Override
    public boolean isValid(ItemStack itemStack) {
        return this.item.isSimilar(itemStack);
    }

    @Override
    public List<ItemStack> spawn() {
        List<ItemStack> items = new ArrayList<>();
        items.add(this.item.clone());
        return items;
    }

    @Override
    public int getMaxStackSize() {
        return this.item.getMaxStackSize();
    }

    @Override
    public String getDisplayName() {
        return this.item.getItemMeta() != null ? this.item.getItemMeta().getDisplayName() : this.item.getType().name();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ExactItem)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        ExactItem other = (ExactItem) object;
        return this.item.equals(other.item);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(11, 41). // two randomly chosen prime numbers
                append(this.item.hashCode()).
                toHashCode();
    }
}
