package ca.lightz.invasion.core.inventory.item.base;

import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface ItemWrapper {

    boolean isValid(ItemStack itemStack);

    default ItemQuantity getWhatToRemove(ItemStack itemStack, int count) {
        if (this.isValid(itemStack)) {
            int countToRemove = Math.min(count, itemStack.getAmount());
            return new ItemQuantity(this, countToRemove);
        }
        return new ItemQuantity(this, 0);
    }

    boolean equals(Object object);

    int hashCode();

    List<ItemStack> spawn();

    int getMaxStackSize();

    String getDisplayName();
}
