package ca.lightz.invasion.core.inventory.item.quantity;

import ca.lightz.invasion.core.inventory.item.ItemWrapperFactory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class NoItem extends ItemQuantity {

    public NoItem() {
        super(ItemWrapperFactory.empty(), 0);
    }

    @Override
    public List<ItemStack> spawn() {
        return new ArrayList<>();
    }

    @Override
    public NoItem copy() {
        return new NoItem();
    }
}
