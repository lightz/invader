package ca.lightz.invasion.core.inventory.item;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SimilarItemWrapper implements ItemWrapper {
    private final LinkedList<ItemWrapper> items = new LinkedList<>();
    private int maxStackSize = Integer.MAX_VALUE;

    public SimilarItemWrapper(ItemWrapper... items) {
        for (ItemWrapper item : items) {
            this.add(item);
        }
    }

    public void add(ItemWrapper itemWrapper) {
        this.items.addLast(itemWrapper);
        if (this.maxStackSize > itemWrapper.getMaxStackSize()) {
            this.maxStackSize = itemWrapper.getMaxStackSize();
        }
    }

    @Override
    public boolean isValid(ItemStack itemStack) {
        for (ItemWrapper item : this.items) {
            if (item.isValid(itemStack)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public ItemQuantity getWhatToRemove(ItemStack itemStack, int count) {
        for (ItemWrapper item : this.items) {
            if (!item.isValid(itemStack)) {
                continue;
            }
            return item.getWhatToRemove(itemStack, count);
        }

        return null;
    }

    @Override
    public List<ItemStack> spawn() {
        Log.error("Spawning SimilarItemWrapper, this probably should not happen.");
        List<ItemStack> items = new ArrayList<>();
        for (ItemWrapper itemWrapper : this.items) {
            items.addAll(itemWrapper.spawn());
        }
        return items;
    }

    @Override
    public int getMaxStackSize() {
        return this.maxStackSize;
    }

    @Override
    public String getDisplayName() {
        Log.error("Tried to display a SimilarItemWrapper, which isn't possible.");
        return "Should not be seeing this, contact an admin.";
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SimilarItemWrapper)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        SimilarItemWrapper other = (SimilarItemWrapper) object;
        if (this.items.size() != other.items.size()) {
            return false;
        }
        if (this.maxStackSize != other.maxStackSize) {
            return false;
        }

        for (ItemWrapper itemWrapper : this.items) {
            if (!other.items.contains(itemWrapper)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(3, 7);
        hashCodeBuilder.append(this.maxStackSize);
        for (ItemWrapper itemWrapper : this.items) {
            hashCodeBuilder.append(itemWrapper.hashCode());
        }
        return hashCodeBuilder.toHashCode();
    }
}
