package ca.lightz.invasion.core.inventory.item;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.core.inventory.item.base.ExactItem;
import ca.lightz.invasion.core.inventory.item.base.ItemWrapper;
import ca.lightz.invasion.core.inventory.item.base.VanillaMaterialItem;
import ca.lightz.invasion.core.inventory.item.quantity.ItemQuantity;
import ca.lightz.invasion.core.inventory.item.quantity.NoItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemWrapperFactory {

    public static ItemQuantity wrap(String data, int amount) {
        return new ItemQuantity(wrap(data), amount);
    }

    public static ItemWrapper wrap(String data) {
        String uppercase = data.toUpperCase();
        if (Material.getMaterial(uppercase) != null) {
            return new VanillaMaterialItem(Material.valueOf(uppercase));
        }

        Log.error("ItemWrapperFactory::wrap Invalid id <" + data + ">");
        return null;
    }

    public static ItemQuantity wrap(ItemStack itemStack, int amount) {
        if (itemStack == null) {
            Log.error("ItemWrapperFactory::wrap the item is null.");
            return new NoItem();
        }
        return new ItemQuantity(wrap(itemStack), amount);
    }

    public static ItemWrapper wrap(ItemStack itemStack) {
        if (itemStack == null) {
            Log.error("ItemWrapperFactory::wrap the item is null.");
            return empty();
        }
        return new ExactItem(itemStack);
    }

    public static ItemQuantity wrap(Material material, int amount) {
        return new ItemQuantity(wrap(material), amount);
    }

    public static ItemWrapper wrap(Material material) {
        return new VanillaMaterialItem(material);
    }

    public static ItemWrapper empty() {
        return new VanillaMaterialItem(Material.AIR);
    }
}
