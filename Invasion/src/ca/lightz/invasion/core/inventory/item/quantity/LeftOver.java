package ca.lightz.invasion.core.inventory.item.quantity;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class LeftOver {
    private List<ItemQuantity> leftOvers = new ArrayList<>();

    public LeftOver() {
    }

    public LeftOver(ItemQuantity itemQuantity) {
        this.addLeftOver(itemQuantity);
    }

    public void addLeftOver(ItemQuantity itemQuantity) {
        this.leftOvers.add(itemQuantity);
    }

    public void addLeftOver(LeftOver otherLeftOver) {
        for (ItemQuantity itemQuantity : otherLeftOver.leftOvers) {
            if (!itemQuantity.isEmpty()) {
                this.leftOvers.add(itemQuantity);
            }
        }
    }

    public boolean hasLeftOvers() {
        for (ItemQuantity itemQuantity : this.leftOvers) {
            if (!itemQuantity.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public List<ItemQuantity> getLeftOvers() {
        return this.leftOvers;
    }

    public void dropAllAt(Location location) {
        if (!this.hasLeftOvers()) {
            return;
        }

        World world = location.getWorld();
        if (world == null) {
            return;
        }

        for (ItemQuantity itemQuantity : this.getLeftOvers()) {
            for (ItemStack item : itemQuantity.spawn()) {
                world.dropItem(location, item);
            }
        }
    }
}
