package ca.lightz.invasion;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.core.factory.EffectFactory;
import ca.lightz.invasion.core.item.LoreItem;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.channel.ChannelManager;
import ca.lightz.invasion.game.command.DebugCommand;
import ca.lightz.invasion.game.command.admin.ClassLockCommand;
import ca.lightz.invasion.game.command.admin.ClassUnlockCommand;
import ca.lightz.invasion.game.command.admin.MapSkipCommand;
import ca.lightz.invasion.game.command.admin.ObjectiveCommand;
import ca.lightz.invasion.game.command.builder.DoorCommand;
import ca.lightz.invasion.game.command.builder.FastSpawnCommand;
import ca.lightz.invasion.game.command.builder.SpawnTestCommand;
import ca.lightz.invasion.game.command.builder.UpdateSpawnCommand;
import ca.lightz.invasion.game.command.player.ChangeClassCommand;
import ca.lightz.invasion.game.command.player.RespawnCommand;
import ca.lightz.invasion.game.command.player.SeeMapRotationCommand;
import ca.lightz.invasion.game.command.player.channel.GlobalChatCommand;
import ca.lightz.invasion.game.command.player.channel.TeamChatCommand;
import ca.lightz.invasion.game.listener.*;
import ca.lightz.invasion.game.listener.combat.CombatEvent;
import ca.lightz.invasion.game.objective.listener.ObjectiveListener;
import ca.lightz.invasion.io.config.ConfigLoader;
import ca.lightz.invasion.io.config.GearConfig;
import ca.lightz.invasion.localization.Localization;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Invasion extends JavaPlugin {
    public static EffectFactory EFFECT_FACTORY;
    private static Invasion PLUGIN;
    private Game game;
    private final boolean mapBuildingMode = true;

    @Override
    public void onEnable() {
        PLUGIN = this;

        Log.init(this);
        this.setUpEffectFactory();

        if (!this.getDataFolder().exists()) {
            boolean created = this.getDataFolder().mkdirs();
            if (!created) {
                Log.warning("Couldn't create data folders.");
            }
        }

        YamlConfiguration messages = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "Message.yml"));
        YamlConfiguration deathMessages = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "DeathMessage.yml"));
        Localization.loadFiles(messages, deathMessages);
        ChannelManager channelManager = new ChannelManager();

        String configPath = this.getDataFolder().getAbsolutePath();
        ConfigLoader loader = new ConfigLoader(configPath);
        GearConfig gearConfig = loader.loadConfig(GearConfig.class, "Gear");

        if (!this.mapBuildingMode) {
            this.game = new Game(this, channelManager);
            this.registerEventListeners(channelManager);
            this.getLogger().info("Loaded!");
        } else {
            this.getLogger().info("Build mode!");
        }

        registerCommands(channelManager);
    }

    private void setUpEffectFactory() {
        EFFECT_FACTORY = new EffectFactory(this);
        EFFECT_FACTORY.registerClassPackage("ca.lightz.invasion.game.component");
        LoreItem.EFFECT_FACTORY = EFFECT_FACTORY;
    }

    private void registerEventListeners(ChannelManager channelManager) {
        this.registerEvents(this, new CombatEvent(this.game), new PlayerEvent(this.game),
                new GameLogin(this.game), new ServerMessage(this.game), new MapEvent(), new ChatListener(channelManager, this.game),
                new ObjectiveListener(this.game));
    }

    private void registerCommands(ChannelManager channelManager) {
        this.setCommandExecutor(this.getCommand("class"), new ChangeClassCommand(this.game), "Change Class Command loaded.");
        this.setCommandExecutor(this.getCommand("debug"), new DebugCommand(this.game), "Debug Command loaded.");
        this.setCommandExecutor(this.getCommand("respawn"), new RespawnCommand(), "Respawn Command loaded.");
        this.setCommandExecutor(this.getCommand("map"), new SeeMapRotationCommand(this.game), "See Map Rotation Command loaded.");
        this.setCommandExecutor(this.getCommand("s"), new FastSpawnCommand(), "Fast Spawn (Build) Command loaded.");
        this.setCommandExecutor(this.getCommand("updatespawn"), new UpdateSpawnCommand(this.game), "Update Spawn (Build) Command loaded.");
        this.setCommandExecutor(this.getCommand("d"), new DoorCommand(), "Door (Build) Command loaded.");
        this.setCommandExecutor(this.getCommand("spawntest"), new SpawnTestCommand(this.game), "Spawn Test (In-game Build) Command loaded.");
        this.setCommandExecutor(this.getCommand("mapskip"), new MapSkipCommand(this.game), "Map skip (Admin) Command loaded.");
        this.setCommandExecutor(this.getCommand("classunlock"), new ClassUnlockCommand(this.game), "Class Unlock (Shop) Command loaded.");
        this.setCommandExecutor(this.getCommand("classlock"), new ClassLockCommand(this.game), "Class Lock (Shop) Command loaded.");
        this.setCommandExecutor(this.getCommand("g"), new GlobalChatCommand(channelManager, this.game), "Global Chat (Chat) Command loaded.");
        this.setCommandExecutor(this.getCommand("t"), new TeamChatCommand(channelManager, this.game), "Team Chat (Chat) Command loaded.");
        this.setCommandExecutor(this.getCommand("objective"), new ObjectiveCommand(this.game), "Objective Command loaded.");
    }

    private void setCommandExecutor(PluginCommand command, CommandExecutor executor, String logMessage) {
        if (command != null) {
            command.setExecutor(executor);
            this.getLogger().info(logMessage);
        }
    }

    private void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
        }
    }

    @Override
    public void onDisable() {
        if (this.game != null) {
            this.game.closeServer();
        }
        PLUGIN = null;
    }

    public static Invasion getPlugin() {
        return PLUGIN;
    }
}