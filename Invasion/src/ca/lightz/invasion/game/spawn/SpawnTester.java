package ca.lightz.invasion.game.spawn;

import ca.lightz.invasion.game.spawn.container.SpawnList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class SpawnTester extends BukkitRunnable {
    private final SpawnList spawnList;
    private final UUID uuid;

    public SpawnTester(UUID uuid, SpawnList spawnList) {
        this.uuid = uuid;
        this.spawnList = new SpawnList(spawnList);
    }

    @Override
    public void run() {
        Spawn spawn = this.spawnList.getRandomSpawn();
        Player player = Bukkit.getPlayer(this.uuid);

        if (player == null || spawn == null) {
            this.cancel();
            return;
        }

        spawn.teleport(player);
        this.spawnList.remove(spawn);
        player.sendMessage("Teleported to spawn with id " + spawn.getId());

        if (this.spawnList.isEmpty()) {
            this.cancel();
        }
    }
}
