package ca.lightz.invasion.game.spawn;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.entity.Player;

public class SpawnRoom extends Spawn {

    public SpawnRoom(Spawn spawn) {
        super(spawn);
    }

    @Override
    protected void setSpawnParameters(PlayerHolder playerHolder) {
        playerHolder.siegePlayer.respawn();
    }

    public void sendTo(Player player) {
        this.teleport(player);
    }
    
    public WorldPosition getPosition() {
        return this.getSafePosition();
    }
}
