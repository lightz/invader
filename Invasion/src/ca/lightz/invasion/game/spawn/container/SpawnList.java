package ca.lightz.invasion.game.spawn.container;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.spawn.Spawn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class SpawnList {
    private static final Random RANDOM = new Random();
    private final List<Spawn> spawns = new ArrayList<>();

    public SpawnList() {
    }

    public SpawnList(SpawnList toCopy) {
        this.add(toCopy);
    }

    public void spawn(PlayerHolder playerHolder) {
        Spawn spawn = this.getRandomSpawn();
        spawn.spawn(playerHolder);
    }

    public void add(Spawn spawn) {
        this.spawns.add(spawn);
    }

    public void add(SpawnList spawnList) {
        this.spawns.addAll(spawnList.spawns);
    }

    public Spawn getRandomSpawn() {
        int index = RANDOM.nextInt(this.spawns.size());
        return this.spawns.get(index);
    }

    public void remove(Spawn spawnToRemove) {
        int indexToDelete = spawnToRemove.getId();

        Iterator<Spawn> iterator = this.spawns.iterator();
        while (iterator.hasNext()) {
            Spawn spawn = iterator.next();
            if (spawn.getId() == indexToDelete) {
                iterator.remove();
                break;
            }
        }
    }

    public boolean isEmpty() {
        return this.spawns.isEmpty();
    }
}
