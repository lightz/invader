package ca.lightz.invasion.game.spawn.container;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.spawn.Spawn;
import ca.lightz.invasion.game.team.Team;

import java.util.HashMap;
import java.util.Map;

public class TeamSpawn {
    private static final int DEFAULT_NUMBER_TEAMS_PER_MAP = 2;
    private final Map<Integer, SpawnList> spawns = new HashMap<>(DEFAULT_NUMBER_TEAMS_PER_MAP + 1, 1);

    public void add(Spawn spawn, int ownerId) {
        this.spawns.putIfAbsent(ownerId, new SpawnList());
        this.spawns.get(ownerId).add(spawn);
    }

    public void spawn(PlayerHolder playerHolder) {
        Team team = playerHolder.siegePlayer.getTeam();
        SpawnList spawns = this.spawns.get(team.getId());
        spawns.spawn(playerHolder);
    }

    public SpawnList getSpawns() {
        SpawnList allSpawns = new SpawnList();
        for (SpawnList teamSpawns : this.spawns.values()) {
            allSpawns.add(teamSpawns);
        }
        return allSpawns;
    }
}
