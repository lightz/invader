package ca.lightz.invasion.game.spawn;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

public class Spawn {
    private static final int SPAWN_PROTECTION_TICK_DURATION = 40;

    private final int id;
    private WorldPosition position;
    private WorldPosition safePosition;

    public Spawn(Spawn spawn) {
        this(spawn.id, new WorldPosition(spawn.position));
    }

    public Spawn(int id, WorldPosition position) {
        this.id = id;
        this.position = position;
        this.safePosition = this.position.add(0, 1, 0);
    }

    public void spawn(PlayerHolder playerHolder) {
        this.teleport(playerHolder.player);
        this.setSpawnParameters(playerHolder);
    }

    protected void teleport(Player player) {
        WorldPosition spawnPosition = this.getSafePosition();
        player.teleport(spawnPosition.getLocation());
    }

    protected WorldPosition getSafePosition() {
        Block spawnBlock = this.position.getBlock();
        if (spawnBlock.getType().isSolid()) {
            return this.safePosition;
        }
        return this.position;
    }

    protected void setSpawnParameters(PlayerHolder playerHolder) {
        playerHolder.siegePlayer.spawn();
        PotionUtil.applyDodge(playerHolder.player, SPAWN_PROTECTION_TICK_DURATION);
    }

    public int getId() {
        return this.id;
    }
}
