package ca.lightz.invasion.game.indicator;

import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.DyeColor;

public class DualColorWoolIndicator {
    private final WoolIndicatorContainer primaryColorIndicators;
    private final WoolIndicatorContainer secondaryColorIndicators;

    public DualColorWoolIndicator() {
        this.primaryColorIndicators = new WoolIndicatorContainer();
        this.secondaryColorIndicators = new WoolIndicatorContainer();
    }

    public void addPrimary(WorldPosition position) {
        this.primaryColorIndicators.add(position);
    }

    public void addSecondary(WorldPosition position) {
        this.secondaryColorIndicators.add(position);
    }

    public void setColors(DyeColor primaryColor, DyeColor secondaryColor) {
        this.primaryColorIndicators.setColor(primaryColor);
        this.secondaryColorIndicators.setColor(secondaryColor);
    }

    public void remove() {
        this.primaryColorIndicators.delete();
        this.secondaryColorIndicators.delete();
    }

    public void generate(DyeColor primary, DyeColor secondary) {
        this.primaryColorIndicators.generate(primary);
        this.secondaryColorIndicators.generate(secondary);
    }
}
