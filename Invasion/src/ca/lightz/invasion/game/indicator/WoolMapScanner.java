package ca.lightz.invasion.game.indicator;

import ca.lightz.invasion.game.objective.container.ObjectiveContainer;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.zone.Zone;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;

import java.util.HashMap;
import java.util.Map;

public class WoolMapScanner {

    public void bindWool(Zone zone, ObjectiveContainer objectives) {
        Map<String, WoolIndicatorContainer> objectivesWool = this.scan(zone);
        for (Map.Entry<String, WoolIndicatorContainer> entry : objectivesWool.entrySet()) {
            objectives.setVisual(entry.getKey(), entry.getValue());
        }
    }

    private Map<String, WoolIndicatorContainer> scan(Zone zone) {
        Map<String, WoolIndicatorContainer> objectivesWool = new HashMap<>();

        for (Block block : zone) {
            if (!this.isSign(block)) {
                continue;
            }

            Sign sign = (Sign) block.getState();
            if (!this.isObjectiveSign(sign)) {
                continue;
            }

            Block support = this.getSupport(sign, block);
            if (!Tag.WOOL.isTagged(support.getType())) {
                continue;
            }

            String name = sign.getLine(1);
            WoolIndicatorContainer woolMapContainer;
            if (objectivesWool.containsKey(name)) {
                woolMapContainer = objectivesWool.get(name);
            } else {
                woolMapContainer = new WoolIndicatorContainer();
                objectivesWool.put(name, woolMapContainer);
            }

            WorldPosition indicator = new WorldPosition(support.getLocation());
            woolMapContainer.add(indicator);
        }

        return objectivesWool;
    }

    private Block getSupport(Sign sign, Block signBlock) {
        org.bukkit.material.Sign materialSign = (org.bukkit.material.Sign) sign.getData();
        BlockFace attachedFace = materialSign.getAttachedFace();
        return signBlock.getRelative(attachedFace);
    }

    private boolean isSign(Block block) {
        return block.getState() instanceof Sign;
    }

    private boolean isObjectiveSign(Sign sign) {
        return sign.getLine(0).contains("Spawn");
    }
}
