package ca.lightz.invasion.game.indicator;

import ca.lightz.invasion.game.util.block.BlockChanger;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.material.Wool;

import java.util.HashSet;
import java.util.Set;

public class WoolIndicatorContainer {
    private final Set<WorldPosition> indicators;
    private DyeColor lastColor;

    public WoolIndicatorContainer() {
        this.indicators = new HashSet<>();
    }

    public void add(WorldPosition position) {
        this.indicators.add(position);
    }

    public void addAll(WoolIndicatorContainer linkedWoolMap) {
        this.indicators.addAll(linkedWoolMap.indicators);
    }

    public void setColor(DyeColor color) {
        if (this.lastColor != color) {
            this.lastColor = color;
        }

        for (WorldPosition position : this.indicators) {
            Block block = position.getBlock();
            this.setColor(block, color);
        }
    }

    public void delete() {
        for (WorldPosition position : this.indicators) {
            Block block = position.getBlock();
            if (Tag.WOOL.isTagged(block.getType())) {
                BlockChanger.setBlock(block, Material.AIR);
            }
        }
    }

    public void generate(DyeColor color) {
        for (WorldPosition position : this.indicators) {
            Block block = position.getBlock();
            BlockChanger.setBlock(block, Material.WHITE_WOOL);
            this.setColor(block, color);
        }
    }

    private void setColor(Block block, DyeColor color) {
        if (!Tag.WOOL.isTagged(block.getType())) {
            return;
        }

        BlockState state = block.getState();
        Wool blockData = (Wool) state.getData();
        blockData.setColor(color);
        state.setData(blockData);
        state.update(true, false);
    }
}
