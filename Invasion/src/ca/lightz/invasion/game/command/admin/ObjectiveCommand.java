package ca.lightz.invasion.game.command.admin;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.map.SiegeMap;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.team.Team;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ObjectiveCommand implements CommandExecutor {

    private final Game game;

    public ObjectiveCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        if (!sender.isOp()) {
            return false;
        }

        if (args.length < 1) {
//            Messenger.send(new UnlocalizedMessage(MessageId.));MessageId
        }

        if (args[0].equalsIgnoreCase("change")) {
            if (args.length < 2) {
                //No objective name
                sender.sendMessage("No objective name given");
                return false;
            }
            Objective objective = game.getMap().getObjective(args[1]);
            if (objective == null) {
                sender.sendMessage("No objective by that name.");
                return false;
            }

            Team team;
            if (args.length < 3) {
                team = game.getPlayer(((Player) sender).getUniqueId()).getTeam();
            } else {
                team = game.getMap().getTeams().get(args[2]);
            }

            if (team == null) {
                //No team by that name probably
                sender.sendMessage("No Team by that name.");
                return false;
            }
            objective.takeControl(team);
        }

        if (args[0].equalsIgnoreCase("list")) {

        }

        SiegeMap map = game.getMap();


        return false;
    }
}
