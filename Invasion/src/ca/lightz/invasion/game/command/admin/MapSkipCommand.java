package ca.lightz.invasion.game.command.admin;

import ca.lightz.invasion.game.Game;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MapSkipCommand implements CommandExecutor {
    private final Game game;

    public MapSkipCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        boolean canEnd;
        if (!(sender instanceof Player)) {
            canEnd = true;
        } else {
            Player player = (Player) sender;
            canEnd = player.isOp();
        }

        if (canEnd) {
            this.game.endGame();
            return true;
        }
        return false;
    }
}