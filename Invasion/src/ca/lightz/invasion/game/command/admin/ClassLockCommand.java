package ca.lightz.invasion.game.command.admin;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.player.perk.PerkType;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ClassLockCommand implements CommandExecutor {
    private final Game game;

    public ClassLockCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof ConsoleCommandSender)) {
            return false;
        }

        if (args.length < 2) {
            String info = args.length == 1 ? args[0] : "";
            Log.warning("Wasn't able to process Class Lock. " + info);
            return false;
        }

        String playerName = args[0];
        Player player = Bukkit.getPlayer(playerName);
        String perkName = args[1].toUpperCase();
        PerkType perkType = null;
        try {
            perkType = PerkType.valueOf(perkName);
        } catch (Exception ignored) {
        }

        if (player == null || perkType == null) {
            Log.warning("Wasn't able to process Class Lock for " + playerName + " / " + perkName + ".");
            return false;
        }

        this.game.removePerk(player, perkType);
        return true;
    }
}