package ca.lightz.invasion.game.command.builder;

import ca.lightz.invasion.Invasion;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.map.SiegeMap;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.spawn.SpawnTester;
import ca.lightz.invasion.game.spawn.container.SpawnList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnTestCommand implements CommandExecutor {
    private static final int DELAY = 100;
    private final Game game;

    public SpawnTestCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        if (!player.isOp()) {
            return false;
        }

        if (args.length < 1) {
            player.sendMessage("Need to specify the objective name.");
            return false;
        }

        StringBuilder concatName = new StringBuilder();
        for (String arg : args) {
            concatName.append(" ").append(arg);
        }

        String objectiveName = concatName.toString().trim();
        SiegeMap map = this.game.getMap();
        Objective objective = map.getObjective(objectiveName);
        if (objective == null) {
            player.sendMessage("Objective not found.");
            return false;
        }

        SpawnList spawns = objective.getAllSpawns();
        SpawnTester spawnTester = new SpawnTester(player.getUniqueId(), spawns);
        spawnTester.runTaskTimer(Invasion.getPlugin(), 0, DELAY);

        return true;
    }
}