package ca.lightz.invasion.game.command.builder;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public class DoorCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        if (args.length < 1) {
            return false;
        }

        Player player = (Player) sender;
        if (!player.isOp()) {
            return false;
        }

        LocalSession session = WorldEdit.getInstance().getSessionManager().get(BukkitAdapter.adapt(player));
        Region region;
        try {
            region = session.getSelection(BukkitAdapter.adapt(player.getWorld()));
        } catch (IncompleteRegionException e) {
            player.sendMessage(e.getMessage());
            return false;
        }

        World world = player.getWorld();
        BlockVector3 min = region.getMinimumPoint();
        BlockVector3 max = region.getMaximumPoint();

        player.sendMessage("Building door from " + min + " to " + max);

        List<BlockState> blocks = new LinkedList<>();

        for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
            for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
                    Block block = world.getBlockAt(x, y, z);
                    if (block.getType() == Material.AIR) {
                        continue;
                    }

                    blocks.add(block.getState());
                }
            }
        }

        for (BlockState state : blocks) {
            Material material = state.getType();
            BlockData blockData = state.getBlockData();

            Block block = state.getBlock();
            block.setType(Material.OAK_SIGN);
            for (int i = 0; i < args.length; i++) {
                String line = args[i];
                Sign sign = (Sign) block.getState();
                sign.setLine(i, line + ":" + material + ":" + blockData);
                sign.update();
            }
            FallingBlock fallingBlock = block.getWorld().spawnFallingBlock(block.getLocation().add(0.5, 0.5, 0.5), blockData);
            fallingBlock.setGlowing(true);
        }

        return true;
    }
}
