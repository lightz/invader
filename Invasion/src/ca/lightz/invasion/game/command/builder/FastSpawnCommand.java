package ca.lightz.invasion.game.command.builder;

import ca.lightz.invasion.core.Log;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FastSpawnCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        if (!player.isOp()) {
            return false;
        }

        Location location = player.getLocation();

        String query = "INSERT INTO `128945_sql`.`spawn` (`world`, `x`, `y`, `z`, `pitch`, `yaw`) VALUES ('%s', '%f', '%f', '%f', '%f', '%f')";
        String formatted = String.format(query, location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
        Log.info(formatted);

        String objQuery = "INSERT INTO `128945_sql`.`zone` (`world`, `x`, `y`, `z`) VALUES ('%s', '%d', '%d', '%d')";
        String objFormatted = String.format(objQuery, location.getWorld().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
        Log.info(objFormatted);

        return true;
    }
}
