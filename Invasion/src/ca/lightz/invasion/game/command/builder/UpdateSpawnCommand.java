package ca.lightz.invasion.game.command.builder;

import ca.lightz.invasion.game.Game;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UpdateSpawnCommand implements CommandExecutor {
    private Game game;

    public UpdateSpawnCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        if (!player.isOp()) {
            return false;
        }

        if (args.length < 1) {
            player.sendMessage("Need to specify the spawn id.");
            return false;
        }

        try {
            int id = Integer.parseInt(args[0]);
            this.game.updateSpawn(player, id);
            player.sendMessage("Updated " + id);
        } catch (Exception ignore) {
            player.sendMessage("Need to specify the spawn id. (Number)");
        }

        return true;
    }
}