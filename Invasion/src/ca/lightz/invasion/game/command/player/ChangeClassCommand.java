package ca.lightz.invasion.game.command.player;

import ca.lightz.invasion.game.Game;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChangeClassCommand implements CommandExecutor {
    private final Game game;

    public ChangeClassCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;

        StringBuilder className = new StringBuilder();

        String delimiter = "";
        for (String arg : args) {
            className.append(delimiter).append(arg);
            delimiter = " ";
        }

        this.game.changeUnit(player, className.toString());
        return true;
    }
}
