package ca.lightz.invasion.game.command.player.channel;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.channel.ChannelManager;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamChatCommand implements CommandExecutor {
    private final ChannelManager channelManager;
    private final Game game;

    public TeamChatCommand(ChannelManager channelManager, Game game) {
        this.channelManager = channelManager;
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;
        SiegePlayer siegePlayer = this.game.getPlayer(player.getUniqueId());
        PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);
        if (args.length == 0) {
            this.channelManager.setPlayerWritingToTeam(playerHolder);
            return true;
        }

        StringBuilder message = new StringBuilder();
        String delimiter = "";
        for (String arg : args) {
            message.append(delimiter).append(arg);
            delimiter = " ";
        }

        this.channelManager.sendToTeamChat(playerHolder, message.toString());
        return true;
    }
}
