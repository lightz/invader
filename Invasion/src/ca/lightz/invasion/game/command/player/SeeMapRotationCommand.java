package ca.lightz.invasion.game.command.player;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.map.SiegeMap;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Queue;

public class SeeMapRotationCommand implements CommandExecutor {
    private Game game;

    public SeeMapRotationCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Queue<SiegeMap> mapRotation = this.game.getMapRotation();
        String currentMapName = this.game.getCurrentMapName().toString();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(currentMapName);
        for (SiegeMap map : mapRotation) {
            stringBuilder.append(" => ").append(map.getName());
        }

        sender.sendMessage(stringBuilder.toString());
        return true;
    }
}