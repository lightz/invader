package ca.lightz.invasion.game.command;

import ca.lightz.invasion.Invasion;
import ca.lightz.invasion.experimental.pathing.display.DrawPath;
import ca.lightz.invasion.experimental.pathing.display.Path;
import ca.lightz.invasion.experimental.pathing.navmesh.AStarPathinfinder;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugCommand implements CommandExecutor {
    Game game;
    AStarPathinfinder pathinfinder = new AStarPathinfinder();

    public DebugCommand(Game game) {
        this.game = game;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;

        if (!player.isOp()) {
            return false;
        }

        if (args.length < 1) {
            sender.sendMessage("Need a sub command..");
            return false;
        }
        if (args[0].equalsIgnoreCase("layer")) {
            pathinfinder.showLayer(player.getWorld(), player.getLocation().getBlockY());
        } else if (args[0].equalsIgnoreCase("nei")) {
            pathinfinder.showNeighbor(new WorldPosition(player.getLocation()));
        } else if (args[0].equalsIgnoreCase("object")) {
            Objective t = game.getMap().getObjective(args[1]);
        } else if (args[1].equalsIgnoreCase("path")) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Invasion.getPlugin(), () -> {
                Player playerTarget = Bukkit.getPlayer(args[0]);
                Player player1 = Bukkit.getPlayer(sender.getName());
                if (playerTarget == null || player1 == null) {
                    return;
                }

                WorldPosition start = new WorldPosition(player1.getLocation());
                WorldPosition end = new WorldPosition(playerTarget.getLocation());
                Location endLocation = end.getLocation();
                endLocation.getWorld().spawnParticle(Particle.BLOCK_DUST, start.getLocation(), 0);

                endLocation.getWorld().spawnParticle(Particle.NOTE, endLocation, 0);
                endLocation.getWorld().spawnParticle(Particle.NOTE, endLocation, 0);
                endLocation.getWorld().spawnParticle(Particle.NOTE, endLocation, 0);
                endLocation.getWorld().spawnParticle(Particle.NOTE, endLocation, 0);

                Path path = pathinfinder.findPath(start, end);
                DrawPath drawPath = new DrawPath(path);
                drawPath.createPath();
            }, 10L);
        }

        return true;
    }
}