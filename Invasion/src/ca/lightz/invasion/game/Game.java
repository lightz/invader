package ca.lightz.invasion.game;

import ca.lightz.invasion.Invasion;
import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.channel.ChannelManager;
import ca.lightz.invasion.game.hologram.InfoHologram;
import ca.lightz.invasion.game.listener.GameLogin;
import ca.lightz.invasion.game.listener.ServerMessage;
import ca.lightz.invasion.game.map.SiegeMap;
import ca.lightz.invasion.game.map.UpdateTick;
import ca.lightz.invasion.game.objective.ObjectiveMessage;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.container.SiegePlayerContainer;
import ca.lightz.invasion.game.player.hider.PlayerHider;
import ca.lightz.invasion.game.player.hider.SpawnRoomHider;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.unit.UnitFactory;
import ca.lightz.invasion.game.unit.flamewarden.firepit.FirePitContainer;
import ca.lightz.invasion.game.util.ScoreboardUtil;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.io.GameLoader;
import ca.lightz.invasion.localization.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class Game {
    private final Queue<SiegeMap> mapRotation = new LinkedList<>();
    private List<SiegeMap> maps = new ArrayList<>();
    private SiegeMap currentMap;

    private final GameLoader gameLoader;
    private final SpawnRoomHider spawnRoomHider;
    private final SiegePlayerContainer siegesPlayers = new SiegePlayerContainer();

    private final DamagingEntityCache<Projectile> arrowCache;
    private final UnitFactory unitFactory;

    private final ChannelManager channelManager;
    private final ScoreboardUtil scoreboardUtil;

    public Game(Plugin plugin, ChannelManager channelManager) {
        PlayerHider playerHider = new PlayerHider(plugin);
        this.channelManager = channelManager;
        this.spawnRoomHider = new SpawnRoomHider(playerHider);
        this.arrowCache = new DamagingEntityCache<>();
        this.unitFactory = new UnitFactory(this.arrowCache);

        Localization localization = new Localization();
        Messenger.setLocalization(localization);
        ItemBuilder.setLocalization(localization);
        InfoHologram.setLocalization(localization);

        this.gameLoader = new GameLoader(this, plugin);
        this.scoreboardUtil = new ScoreboardUtil(this);
    }

    public void closeServer() {
        this.gameLoader.saveAllPlayers();
    }

    public void setMaps(List<SiegeMap> maps) {
        this.maps = maps;
        if (this.maps.isEmpty()) {
            Log.warning("No maps loaded, disabling most of the plugin.");
            return;
        }
        this.startGame();
        ServerMessage.setServerReady();
    }

    private void startGame() {
        this.resetMapRotation();
        SiegeMap nextMap = this.mapRotation.poll();
        if (this.currentMap != null) {
            //We make sure that we never play the same map twice in a row (after refilling map list)
            if (nextMap.getName().equals(this.currentMap.getName())) {
                this.mapRotation.add(nextMap);
                nextMap = this.mapRotation.poll();
            }
        }

        SiegeMap oldMap = this.currentMap;
        this.currentMap = nextMap;

        UpdateTick updateTick = new UpdateTick(this, this.currentMap);
        int captureTick = this.currentMap.getCaptureTick();
        updateTick.runTaskTimer(Invasion.getPlugin(), captureTick, captureTick);

        this.channelManager.startNewGame(this.currentMap.getTeams());
        this.splitAllPlayers();
        this.clearMap(oldMap);
        Messenger.sendTitle(ChatColor.BOLD + "" + ChatColor.RED + this.currentMap.getName().toString(), "");
    }

    private void clearMap(SiegeMap map) {
        if (map != null) {
            map.setOver();
            map.unloadWorld();
        }
    }

    public void endGame() {
        this.sendEndGameMessage();
        this.currentMap.clear();
        this.arrowCache.clear();
        FirePitContainer.getInstance().clear();
        this.gameLoader.saveAllPlayers();
        this.gameLoader.cleanPlayers();
        this.siegesPlayers.clean();
        this.startGame();
        this.scoreboardUtil.sendAll(this.siegesPlayers);
    }

    private void sendEndGameMessage() {
        Collection<UnlocalizedMessage> messages = this.currentMap.getEndingMessage();

        for (Player player : Bukkit.getOnlinePlayers()) {
            UUID uuid = player.getUniqueId();
            SiegePlayer siegePlayer = this.siegesPlayers.get(uuid);
            PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);
            Messenger.send(playerHolder, messages);
        }
    }

    public void sendTimeLeft(int timeLeft) {
        int minutes = timeLeft / 60;
        int seconds = timeLeft % 60;
        String secondsFormatted = seconds < 10 ? "0" + seconds : Integer.toString(seconds);

        for (Player player : Bukkit.getOnlinePlayers()) {
            UUID uuid = player.getUniqueId();
            SiegePlayer siegePlayer = this.siegesPlayers.get(uuid);
            PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);
            Messenger.sendToActionBar(playerHolder, MessageId.TIME_LEFT, minutes, secondsFormatted);
        }
    }

    private void splitAllPlayers() {
        for (SiegePlayer siegePlayer : this.siegesPlayers) {
            Player player = Bukkit.getPlayer(siegePlayer.getUUID());
            if (player == null) {
                continue;
            }

            PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);
            this.addPlayerToTeam(playerHolder);
            this.currentMap.respawn(playerHolder);
            this.spawnRoomHider.enterSpawnRoom(player, this.siegesPlayers);
        }
    }

    private void addPlayerToTeam(PlayerHolder playerHolder) {
        this.currentMap.addPlayer(playerHolder);
    }

    private void resetMapRotation() {
        if (this.mapRotation.isEmpty()) {
            Collections.shuffle(this.maps);
            this.mapRotation.addAll(this.maps);
        }
    }

    public Queue<SiegeMap> getMapRotation() {
        return this.mapRotation;
    }

    public void sendToSpawn(Player player) {
        this.currentMap.teleportOnJoin(player);
        this.spawnRoomHider.enterSpawnRoom(player, this.siegesPlayers);
    }

    public void addPlayer(Player player) {
        PlayerHolder playerHolder = this.playerJoined(player);
        this.addPlayerToTeam(playerHolder);
        this.channelManager.setPlayerWritingToGlobal(playerHolder);
        this.channelManager.addPlayerAsListenerToGlobal(playerHolder.siegePlayer);
    }

    private PlayerHolder playerJoined(Player player) {
        UUID uuid = player.getUniqueId();
        SiegePlayer siegePlayer;
        if (this.siegesPlayers.contains(uuid)) {
            siegePlayer = this.siegesPlayers.get(uuid);
        } else {
            siegePlayer = this.gameLoader.get(player);
            this.siegesPlayers.add(siegePlayer);
        }

        return new PlayerHolder(player, siegePlayer);
    }

    public void removePlayer(Player player) {
        UUID uuid = player.getUniqueId();
        if (!this.siegesPlayers.contains(uuid)) {
            return; //Not there, might have been kicked?
        }

        SiegePlayer siegePlayer = this.siegesPlayers.get(uuid);
        siegePlayer.removeFromTeam(player);
        this.channelManager.removeFromAllChannels(siegePlayer);
    }

    public void respawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        AttributeInstance attribute = player.getAttribute(Attribute.GENERIC_MAX_HEALTH);
        player.setHealth(attribute.getValue());
        player.setFoodLevel(GameLogin.STARTING_FOOD);
        player.setSaturation(GameLogin.STARTING_SATURATION);

        SiegePlayer siegePlayer = this.getPlayer(player.getUniqueId());
        PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);
        this.currentMap.respawn(event, playerHolder);
        this.spawnRoomHider.enterSpawnRoom(player, this.siegesPlayers);
    }

    public void spawn(Player player, String objectiveName) {
        SiegePlayer siegePlayer = this.getPlayer(player.getUniqueId());
        if (!siegePlayer.isInSpawnRoom()) {
            return;
        }
        PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);
        this.currentMap.spawn(playerHolder, objectiveName);
        this.spawnRoomHider.exitSpawnRoom(player, this.siegesPlayers);
    }

    public void refill(Player player) {
        this.getPlayer(player.getUniqueId()).refill(player);
    }

    public SiegePlayer getPlayer(UUID uuid) {
        return this.siegesPlayers.get(uuid);
    }

    public boolean isInSpawnRoom(Player player) {
        return this.getPlayer(player.getUniqueId()).isInSpawnRoom();
    }

    public String getCurrentMapName() {
        return this.currentMap.getName();
    }

    public Unit getUnit(Player player) {
        return this.getPlayer(player.getUniqueId()).getUnit();
    }

    public DamagingEntityCache<Projectile> getArrowCache() {
        return this.arrowCache;
    }

    public void changeUnit(Player player, String unitName) {
        SiegePlayer siegePlayer = this.getPlayer(player.getUniqueId());
        this.changeUnit(siegePlayer, unitName);
        this.scoreboardUtil.send(player);
    }

    public void changeUnit(SiegePlayer player, String unitName) {
        if (!player.isInSpawnRoom()) {
            Messenger.send(player.getPlayer(), MessageId.CANNOT_CHANGE_UNIT_OUT_OF_SPAWN, SiegeColorCode.WARNING);
            return;
        }

        this.unitFactory.setUnit(player, unitName);
    }

    public void sendUnitDescription(Player player, String unitName) {
        UnlocalizedMessage description = this.unitFactory.getDescription(unitName);
        Messenger.send(player, description);
    }

    public void breakBlock(BlockBreakEvent event) {
        Player player = event.getPlayer();
        SiegePlayer siegePlayer = this.getPlayer(player.getUniqueId());
        PlayerHolder playerHolder = new PlayerHolder(player, siegePlayer);

        this.currentMap.damageStructure(event, playerHolder);
    }

    public SiegeMap getMap() {
        return this.currentMap;
    }

    public void sendObjectiveMessage(ObjectiveMessage message) {
        this.currentMap.send(message);
    }

    public void unlockPerk(Player player, PerkType perkType) {
        SiegePlayer siegePlayer = this.getPlayer(player.getUniqueId());
        siegePlayer.unlockPerk(perkType);

        Messenger.send(player, MessageId.THANK_YOU_PURCHASE, SiegeColorCode.SHINY);
    }

    public void removePerk(Player player, PerkType perkType) {
        SiegePlayer siegePlayer = this.getPlayer(player.getUniqueId());
        siegePlayer.removePerk(perkType);
    }

    public void updateSpawn(Player player, int spawnId) {
        Location location = player.getLocation();
        this.gameLoader.updateSpawn(location, spawnId);
    }

    public void sendScoreboard(Player player) {
        this.scoreboardUtil.send(player);
    }

    public void sendScoreboard() {
        this.scoreboardUtil.sendAll(siegesPlayers);
    }
}
