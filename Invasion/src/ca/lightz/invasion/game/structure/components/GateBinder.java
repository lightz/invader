package ca.lightz.invasion.game.structure.components;

import ca.lightz.invasion.game.structure.gate.Gate;

import java.util.Collection;

public interface GateBinder {
    void bind(Collection<Gate> gates);
}
