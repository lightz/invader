package ca.lightz.invasion.game.structure.components;

import ca.lightz.invasion.game.player.PlayerHolder;
import org.bukkit.block.Block;

public interface Destroyable {
    boolean breakBlock(Block block, PlayerHolder playerHolder);

    boolean isDestructibleBlock(Block block);

    boolean isPartOf(Block block);
}
