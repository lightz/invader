package ca.lightz.invasion.game.structure.components;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.LinkedHashMap;
import java.util.Map;

public class HitPoints {
    private static final Map<Integer, Double> SCALE = new LinkedHashMap<>();
    private static final int DEFAULT_SCALE = 25;

    static {
        SCALE.put(5, 0.25);
        SCALE.put(15, 0.5);
        SCALE.put(25, 1.0);
        SCALE.put(35, 1.25);
        SCALE.put(50, 1.75);
    }

    private int startingHitpoints;
    private int current;
    private int initialMaximum;
    private int currentMaximum;
    private int currentScale = DEFAULT_SCALE;

    public HitPoints(int current, int maximum) {
        this.current = current;
        this.startingHitpoints = this.current;
        this.initialMaximum = maximum;
        this.currentMaximum = maximum;
    }

    public int getMaximumHitpoints() {
        return this.currentMaximum;
    }

    public int getHitpoints() {
        return this.current;
    }

    public boolean isDestroyed() {
        return this.current <= 0;
    }

    public void damage(int amount) {
        this.current -= amount;
    }

    public void reset() {
        this.current = this.startingHitpoints;
        this.currentMaximum = this.initialMaximum;
        this.currentScale = DEFAULT_SCALE;
    }

    public void scale(int teamSize) {
        double rate = 1;

        for (Map.Entry<Integer, Double> entry : SCALE.entrySet()) {
            if (teamSize <= entry.getKey()) {
                rate = entry.getValue();
                if (this.currentScale == entry.getKey()) {
                    return;//if its the same scale we are not updating
                }
                this.currentScale = entry.getKey();
                break;
            }
        }

        double ratioDamaged = this.current / this.currentMaximum;
        this.currentMaximum = (int) Math.ceil(this.initialMaximum * rate);
        this.current = (int) Math.ceil(this.currentMaximum * ratioDamaged); //Ceiling to avoid going from 1 to <0.5 and rounding down.*/
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof HitPoints)) {
            return false;
        }
        if (other == this) {
            return true;
        }

        HitPoints otherHitpoints = (HitPoints) other;
        return new EqualsBuilder().
                append(this.startingHitpoints, otherHitpoints.startingHitpoints).
                append(this.current, otherHitpoints.current).
                append(this.initialMaximum, otherHitpoints.initialMaximum).
                append(this.currentMaximum, otherHitpoints.currentMaximum).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(101, 163).
                append(this.current).
                append(this.startingHitpoints).
                append(this.initialMaximum).
                append(this.currentMaximum).
                toHashCode();
    }
}
