package ca.lightz.invasion.game.structure.constructor;

public enum StructureType {
    GATE, BATTLE_RAM
}
