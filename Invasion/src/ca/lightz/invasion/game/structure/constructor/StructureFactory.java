package ca.lightz.invasion.game.structure.constructor;

import ca.lightz.invasion.game.structure.SiegeStructure;
import ca.lightz.invasion.game.structure.batteringram.BatteringRam;
import ca.lightz.invasion.game.structure.gate.Gate;
import ca.lightz.invasion.game.structure.gate.GateScanner;
import ca.lightz.invasion.game.structure.gate.WeakGate;
import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.position.WorldPosition;

public class StructureFactory {
    private final GateScanner gateScanner;

    public StructureFactory() {
        this.gateScanner = new GateScanner();
    }

    public SiegeStructure build(StructureType type, DataMapper dataMapper, int teamId, WorldPosition position) {
        switch (type) {
            case BATTLE_RAM:
                return new BatteringRam(position, dataMapper, teamId);
            case GATE:
                return buildGate(position, dataMapper, teamId);
            default:
                throw new RuntimeException("Invalid structure type : " + type + ".");
        }
    }

    private Gate buildGate(WorldPosition position, DataMapper dataMapper, int teamId) {
        if (dataMapper.getBoolean("destroy_manually")) {
            return new WeakGate(position, this.gateScanner, dataMapper, teamId);
        }
        return new Gate(position, this.gateScanner, dataMapper, teamId);
    }
}
