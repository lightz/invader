package ca.lightz.invasion.game.structure.gate;

public enum GateState {
    OPEN("O"),
    CLOSED("C"),
    DESTROYED("D");

    private final String identifier;

    GateState(String identifier) {
        this.identifier = identifier;
    }

    public static GateState getEnum(String name) {
        if (name.length() == 1) {
            for (GateState state : GateState.values()) {
                if (state.identifier.equalsIgnoreCase(name)) {
                    return state;
                }
            }
        }
        return GateState.valueOf(name);
    }
}
