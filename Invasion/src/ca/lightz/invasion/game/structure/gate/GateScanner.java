package ca.lightz.invasion.game.structure.gate;

import ca.lightz.invasion.game.util.block.SiegeBlockState;
import ca.lightz.invasion.game.util.block.SiegeBlockStateList;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.zone.Zone;
import ca.lightz.invasion.game.zone.ZoneFactory;
import com.google.common.base.Strings;
import com.mysql.jdbc.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class GateScanner {

    public Map<GateState, SiegeBlockStateList> scan(WorldPosition position, Vector scanZoneSize) {
        Zone toScan = this.generateZone(position, scanZoneSize);

        Map<GateState, SiegeBlockStateList> blocks = this.generateEmpty();
        for (Block block : toScan) {
            BlockState blockState = block.getState();
            if (!(blockState instanceof Sign)) {
                continue;
            }

            Sign sign = (Sign) blockState;
            for (String line : sign.getLines()) {
                if (Strings.isNullOrEmpty(line)) {
                    continue;
                }
                String[] rawData = line.split(":");
                String rawBlockData = "";
                if (rawData.length > 3) {
                    rawBlockData = rawData[3];
                    rawBlockData = rawBlockData.substring(rawBlockData.indexOf("["), rawBlockData.length() - 1);
                }

                GateState gateState;
                BlockData blockData;
                try {
                    Material material = Material.valueOf(rawData[1]);
                    blockData = StringUtils.isNullOrEmpty(rawBlockData) ? Bukkit.createBlockData(material) : Bukkit.createBlockData(material, rawBlockData);

                    gateState = GateState.getEnum(rawData[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }

                SiegeBlockState siegeBlockState = new SiegeBlockState(new WorldPosition(block.getLocation()), blockData);
                blocks.get(gateState).add(siegeBlockState);
            }
        }

        return blocks;
    }

    private Map<GateState, SiegeBlockStateList> generateEmpty() {
        Map<GateState, SiegeBlockStateList> blocks = new HashMap<>();

        for (GateState state : GateState.values()) {
            blocks.put(state, new SiegeBlockStateList());
        }

        return blocks;
    }

    private Zone generateZone(WorldPosition center, Vector scanZoneSize) {
        Block corner = center.getBlock().getRelative(-scanZoneSize.getBlockX(), 0, -scanZoneSize.getBlockZ());
        WorldPosition position = new WorldPosition(corner.getLocation());

        String zoneData = "xLength,Integer,%d;height,Integer,%d;zLength,Integer,%d";
        zoneData = String.format(zoneData, scanZoneSize.getBlockX() * 2 + 1, scanZoneSize.getBlockY() * 2, scanZoneSize.getBlockZ() * 2 + 1);

        return ZoneFactory.build(position, "RectangularPrism", zoneData);
    }
}
