package ca.lightz.invasion.game.structure.gate;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.WeaponType;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.inventory.ItemStack;

public class WeakGate extends Gate {
    private static final int MANUAL_DAMAGE_AMOUNT = 1;

    public WeakGate(WorldPosition positionToScan, GateScanner scanner, DataMapper dataMapper, int owner) {
        super(positionToScan, scanner, dataMapper, owner);
    }

    @Override
    public void update(TeamContainer teams) {
        if (this.isDestroyed()) {
            return;
        }

        Team team = teams.getSmallestTeamExcept(this.getOwnerId());
        int teamSize = team.size();
        this.hitpoints.scale(teamSize);
        this.updateHologramMessage();
    }

    @Override
    protected void damageByHand(PlayerHolder playerHolder) {
        this.damage(MANUAL_DAMAGE_AMOUNT);
    }

    @Override
    public ItemStack getTool(PlayerHolder playerHolder) {
        if (!this.isDestroyed() && this.canDamage(playerHolder.siegePlayer)) {
            return Armory.getWeapon(WeaponType.AXE);
        }
        return super.getTool(playerHolder);
    }
}
