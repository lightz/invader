package ca.lightz.invasion.game.structure.gate;

import ca.lightz.invasion.game.hologram.InfoHologram;
import ca.lightz.invasion.game.hologram.SelectiveHologram;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.structure.SiegeStructure;
import ca.lightz.invasion.game.structure.components.Destroyable;
import ca.lightz.invasion.game.structure.components.HitPoints;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.block.SiegeBlockState;
import ca.lightz.invasion.game.util.block.SiegeBlockStateList;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import java.util.Map;

public class Gate extends SiegeStructure implements Destroyable {
    private static final int RAM_DAMAGE_AMOUNT = 25;

    private Map<GateState, SiegeBlockStateList> blocks;
    private GateState startingState;
    private GateState currentState;
    protected HitPoints hitpoints;

    private final WorldPosition positionToScan;
    private Vector scanZoneSize;
    private final GateScanner scanner;

    private InfoHologram hologram;

    public Gate(WorldPosition positionToScan, GateScanner scanner, DataMapper dataMapper, int owner) {
        super(dataMapper, owner);
        this.positionToScan = positionToScan;
        this.scanner = scanner;
    }

    @Override
    public void init() {
        this.hitpoints.reset();
        this.blocks = this.scanner.scan(this.positionToScan, this.scanZoneSize);
        this.setState(this.startingState);
    }

    @Override
    public void update(TeamContainer teams) {
    }

    @Override
    public void setHolograms(TeamContainer teams) {
        this.hologram = new SelectiveHologram(this.getCenteredLocation());
        this.updateHologramMessage();

        for (Team team : teams) {
            team.addHologram(this.hologram);
        }
    }

    protected void updateHologramMessage() {
        this.hologram.setText(new UnlocalizedMessage(MessageId.GATE_HEALTH_NOTIFICATION, SiegeColorCode.CAPTURING, SiegeColorCode.SHINY, this.hitpoints.getHitpoints(), this.hitpoints.getMaximumHitpoints()));
    }

    private Location getCenteredLocation() {
        return this.blocks.get(GateState.CLOSED).getCenteredLocation();
    }

    private void setState(GateState newState) {
        if (this.currentState == GateState.DESTROYED) {
            return;
        }

        for (SiegeBlockState blockState : this.getBlocks()) {
            blockState.setAir();
        }
        this.currentState = newState;
        for (SiegeBlockState blockState : this.getBlocks()) {
            blockState.apply();
        }
    }

    @Override
    public boolean isDestructibleBlock(Block block) {
        WorldPosition position = new WorldPosition(block.getLocation());
        SiegeBlockStateList blocks = this.getBlocks();
        return blocks.contains(position);
    }

    @Override
    public boolean isPartOf(Block block) {
        WorldPosition position = new WorldPosition(block.getLocation());

        for (SiegeBlockStateList state : this.blocks.values()) {
            if (state.contains(position)) {
                return true;
            }
        }

        return false;
    }

    public boolean breakBlock(Block block, PlayerHolder playerHolder) {
        if (!this.isDestructibleBlock(block)) {
            return false;
        }

        if (this.canDamage(playerHolder.siegePlayer)) {
            this.damageByHand(playerHolder);
        }
        this.sendStatus(playerHolder);

        return true;
    }

    private SiegeBlockStateList getBlocks() {
        return this.blocks.get(this.currentState);
    }

    protected void damageByHand(PlayerHolder playerHolder) {
        Messenger.send(playerHolder.player, MessageId.CANNOT_DAMAGE_THIS_GATE, SiegeColorCode.WARNING);
    }

    public void ramDamage() {
        this.damage(RAM_DAMAGE_AMOUNT);
    }

    protected void damage(int amount) {
        this.hitpoints.damage(amount);
        if (this.isDestroyed()) {
            this.hologram.hide();
            this.setState(GateState.DESTROYED);
        } else {
            this.updateHologramMessage();
        }
    }

    protected boolean canDamage(SiegePlayer siegePlayer) {
        return siegePlayer.getTeam().getId() != this.getOwnerId();
    }

    private void sendStatus(PlayerHolder playerHolder) {
        if (this.isDestroyed()) {
            Messenger.sendToActionBar(playerHolder.player, MessageId.GATE_DESTROYED, SiegeColorCode.WARNING);
        } else {
            Messenger.sendToActionBar(playerHolder.player, MessageId.GATE_HEALTH_NOTIFICATION,
                    SiegeColorCode.CAPTURING, SiegeColorCode.SHINY, this.hitpoints.getHitpoints(),
                    this.hitpoints.getMaximumHitpoints());
        }
    }

    @Override
    protected void deserialize(DataMapper dataMapper) {
        int maxHitpoints = dataMapper.getInt("max_hitpoints");
        Integer rawHitpoints = dataMapper.getInt("hitpoints");
        int amountHitpoints = rawHitpoints != null ? rawHitpoints : maxHitpoints;

        this.hitpoints = new HitPoints(amountHitpoints, maxHitpoints);
        this.currentState = GateState.CLOSED;
        this.startingState = this.currentState;

        this.buildZoneSize(dataMapper);
    }

    private void buildZoneSize(DataMapper dataMapper) {
        Integer xScanZoneSize = dataMapper.getInt("x_scan_size");
        int xSize = xScanZoneSize != null ? xScanZoneSize : 10;

        Integer yScanZoneSize = dataMapper.getInt("y_scan_size");
        int ySize = yScanZoneSize != null ? yScanZoneSize : 10;

        Integer zScanZoneSize = dataMapper.getInt("z_scan_size");
        int zSize = zScanZoneSize != null ? zScanZoneSize : 10;

        this.scanZoneSize = new Vector(xSize, ySize, zSize);
    }

    public boolean isDestroyed() {
        return this.hitpoints.isDestroyed();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gate)) {
            return false;
        }
        if (other == this) {
            return true;
        }

        Gate otherGate = (Gate) other;
        return new EqualsBuilder().
                append(this.startingState, otherGate.startingState).
                append(this.currentState, otherGate.currentState).
                append(this.hitpoints, otherGate.hitpoints).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(43, 47).
                append(this.positionToScan).
                append(this.startingState).
                append(this.currentState).
                append(this.hitpoints).
                toHashCode();
    }
}
