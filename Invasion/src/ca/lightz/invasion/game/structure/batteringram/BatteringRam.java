package ca.lightz.invasion.game.structure.batteringram;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.effect.sound.SoundSender;
import ca.lightz.invasion.game.hologram.InfoHologram;
import ca.lightz.invasion.game.hologram.SelectiveHologram;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.structure.SiegeStructure;
import ca.lightz.invasion.game.structure.batteringram.structure.BatteringRamStructure;
import ca.lightz.invasion.game.structure.components.GateBinder;
import ca.lightz.invasion.game.structure.gate.Gate;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.Collection;

public class BatteringRam extends SiegeStructure implements GateBinder {
    private static final double PERCENTAGE_PLAYERS_TO_ACTIVATE_RAM = 0.2;
    private static final int DISTANCE_FROM_GATE = 3;

    private BatteringRamStructure structure;
    private final WorldPosition origin;
    private BlockFace direction;
    private Gate gate;

    private InfoHologram hologram;

    private boolean isDisabled = false;

    public BatteringRam(WorldPosition origin, DataMapper dataMapper, int owner) {
        super(dataMapper, owner);
        this.origin = origin;
    }

    @Override
    protected void deserialize(DataMapper dataMapper) {
        this.direction = BlockFace.valueOf(dataMapper.getString("direction").toUpperCase());
    }

    @Override
    public void init() {
        this.structure = new BatteringRamStructure(this, this.origin, this.direction);
        this.isDisabled = false;
    }

    @Override
    public void bind(Collection<Gate> gates) {
        Block block = this.origin.getBlock().getRelative(this.direction, DISTANCE_FROM_GATE);
        for (Gate gate : gates) {
            if (gate.isPartOf(block)) {
                this.gate = gate;
                break;
            }
        }

        if (this.gate == null) {
            Log.warning("Battering Ram couldn't find its gate.");
        }
    }

    @Override
    public void update(TeamContainer teams) {
        if (this.isDisabled) {
            return;
        }

        if (this.isGateDestroyed()) {
            this.clean();
            return;
        }

        Team team = teams.get(this.getOwnerId());
        int requiredAmountPlayers = this.getRequiredAmountPlayers(team);

        this.structure.update(requiredAmountPlayers);
        this.move(team);
    }

    @Override
    public void setHolograms(TeamContainer teams) {
        this.hologram = new SelectiveHologram(this.getCenteredLocation());
        this.hologram.setText(new UnlocalizedMessage(MessageId.BATTERING_RAM_HOW_TO, SiegeColorCode.TIPS));

        Team owner = teams.get(this.getOwnerId());
        owner.addHologram(this.hologram);
    }

    private Location getCenteredLocation() {
        return this.structure.getCenteredLocation().add(0, 1.5, 0);
    }

    private int getRequiredAmountPlayers(Team owner) {
        int teamPlayers = owner.getActivePlayers().size();
        int requiredPlayers = (int) Math.floor(teamPlayers * PERCENTAGE_PLAYERS_TO_ACTIVATE_RAM);
        requiredPlayers = Math.max(requiredPlayers, 1);
        return Math.min(requiredPlayers, 4);
    }

    private void move(Team team) {
        if (this.canMove(team)) {
            this.structure.move();
        } else {
            this.structure.reset();
        }
    }

    private boolean canMove(Team team) {
        Collection<WorldPosition> pressurePlates = this.structure.getPressurePlates();

        for (PlayerHolder playerHolder : team.getActivePlayers()) {
            WorldPosition playerPosition = new WorldPosition(playerHolder.player.getLocation());

            pressurePlates.removeIf(playerPosition::isSameBlockPosition);
            if (pressurePlates.isEmpty()) {
                return true;
            }
        }

        return false;
    }

    public void damageGate() {
        this.gate.ramDamage();
        this.playDamageEffects();
        if (this.isGateDestroyed()) {
            this.clean();
        }
    }

    private void playDamageEffects() {
        Location soundLocation = this.origin.getBlock().getRelative(this.direction, 2).getRelative(BlockFace.UP).getLocation();
        SoundSender.playSound(soundLocation, Sound.BLOCK_ANVIL_LAND);
    }

    private void clean() {
        if (!this.isDisabled) {
            this.isDisabled = true;
            this.structure.reset();
            this.structure.clearPressurePlates();
            this.hologram.hide();
        }
    }

    private boolean isGateDestroyed() {
        return this.gate == null || this.gate.isDestroyed();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BatteringRam)) {
            return false;
        }
        if (other == this) {
            return true;
        }

        BatteringRam otherRam = (BatteringRam) other;
        return new EqualsBuilder().
                append(this.isDisabled, otherRam.isDisabled).
                append(this.origin, otherRam.origin).
                append(this.direction, otherRam.direction).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(41, 43).
                append(this.isDisabled).
                append(this.origin).
                append(this.direction).
                toHashCode();
    }
}
