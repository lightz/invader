package ca.lightz.invasion.game.structure.batteringram.structure;

import ca.lightz.invasion.game.structure.batteringram.BatteringRam;
import ca.lightz.invasion.game.util.block.BlockChanger;
import ca.lightz.invasion.game.util.block.MovingStructure;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Axis;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Orientable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.bukkit.Material.AIR;

public class BatteringRamStructure extends MovingStructure {
    private final BatteringRam batteringRam;

    private final WorldPosition origin;
    private final BlockFace direction;
    private final BlockFace buildDirection;
    private final BlockFace left;
    private final BlockFace right;
    private final Axis logDirection;

    private final Collection<WorldPosition> pressurePlates;
    private int currentRequiredPlayers;

    private double height = 0;
    private double distance = 0;
    private boolean movingForward;

    public BatteringRamStructure(BatteringRam batteringRam, WorldPosition origin, BlockFace direction) {
        super();
        this.batteringRam = batteringRam;
        this.origin = origin;
        this.direction = direction;
        this.movingForward = true;
        this.buildDirection = this.getInverse(this.direction);
        this.logDirection = this.getLogDirection();
        this.left = this.getLeft();
        this.right = this.getInverse(this.left);
        this.pressurePlates = new ArrayList<>();
        this.currentRequiredPlayers = 1;
        this.build();
    }

    public void update(int requiredPlayers) {
        //If the ram is moving do not reset it
        if (this.currentRequiredPlayers == requiredPlayers || this.isMoving()) {
            return;
        }
        this.currentRequiredPlayers = requiredPlayers;
        this.reset();
    }

    @Override
    public void reset() {
        super.reset();
        this.build();
        this.height = 0;
        this.movingForward = true;
        this.distance = 0;
    }

    private void build() {
        Block head = this.origin.getBlock();
        BlockChanger.setBlock(head, Material.IRON_BLOCK);
        this.add(head);

        this.addWoodSection(head);
        this.addArms(head);
    }

    private void addWoodSection(Block head) {
        int length = 2 * ((int) Math.ceil(this.currentRequiredPlayers / 2.0));
        length = Math.min(length, 4);

        Block current = head;
        for (int i = 0; i < length; i++) {
            current = current.getRelative(this.buildDirection);

            BlockChanger.setBlock(current, Material.OAK_LOG);
            BlockData blockData = current.getBlockData();
            ((Orientable) blockData).setAxis(this.logDirection);
            current.setBlockData(blockData);

            this.add(current);
        }
    }

    private void addArms(Block head) {
        this.pressurePlates.clear();
        Block midSection = head.getRelative(this.buildDirection);
        this.buildArms(midSection, this.left);

        if (this.currentRequiredPlayers >= 2) {
            this.buildArms(midSection, this.right);

            if (this.currentRequiredPlayers >= 3) {
                Block endSection = midSection.getRelative(this.buildDirection, 2);
                this.buildArms(endSection, this.left);

                if (this.currentRequiredPlayers >= 4) {
                    this.buildArms(endSection, this.right);
                }
            }
        }
    }

    private void buildArms(Block attachedTo, BlockFace direction) {
        Block block = attachedTo.getRelative(direction);
        BlockChanger.setBlock(block, Material.OAK_FENCE);
        this.add(block);

        this.buildPressurePlate(block.getRelative(this.buildDirection));
    }

    private void buildPressurePlate(Block block) {
        if (this.pressurePlates.size() >= this.currentRequiredPlayers) {
            return;
        }
        BlockChanger.setBlock(block, Material.STONE_PRESSURE_PLATE);
        this.pressurePlates.add(new WorldPosition(block.getLocation()));
    }

    private BlockFace getLeft() {
        switch (this.direction) {
            case NORTH:
                return BlockFace.WEST;
            case SOUTH:
                return BlockFace.EAST;
            case EAST:
                return BlockFace.NORTH;
            case WEST:
            default:
                return BlockFace.SOUTH;
        }
    }

    private BlockFace getInverse(BlockFace direction) {
        switch (direction) {
            case NORTH:
                return BlockFace.SOUTH;
            case SOUTH:
                return BlockFace.NORTH;
            case EAST:
                return BlockFace.WEST;
            case WEST:
            default:
                return BlockFace.EAST;
        }
    }

    private Axis getLogDirection() {
        switch (this.direction) {
            case NORTH:
            case SOUTH:
                return Axis.Z;
            case EAST:
            case WEST:
            default:
                return Axis.X;
        }
    }

    public Collection<WorldPosition> getPressurePlates() {
        //Make a copy
        return this.pressurePlates.stream().map(position -> (new WorldPosition(position))).collect(Collectors.toCollection(ArrayList::new));
    }

    private boolean isMoving() {
        return this.origin.getBlock().getType() == AIR;
    }

    public void move() {
        if (this.height < 1) {
            this.raise();
            return;
        }

        if (this.movingForward) {
            this.distance++;
            this.move(this.direction);
            if (this.distance >= 2) {
                this.batteringRam.damageGate();
                this.movingForward = false;
            }
        } else {
            this.distance--;
            this.move(this.buildDirection);
            if (this.distance <= 0) {
                this.movingForward = true;
            }
        }
    }

    private void raise() {
        //If the ram raises too fast its more optimal for players to reset it instead of waiting for it to move back
        this.height += 0.5;
        if (this.height >= 1) {
            this.move(BlockFace.UP);
        }
    }

    public void clearPressurePlates() {
        for (WorldPosition position : this.pressurePlates) {
            position.getBlock().setType(AIR);
        }
    }
}
