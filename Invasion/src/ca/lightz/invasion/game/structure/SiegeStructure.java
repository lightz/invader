package ca.lightz.invasion.game.structure;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.game.util.DataMapper;
import org.bukkit.inventory.ItemStack;

public abstract class SiegeStructure {
    private final int owner;

    public SiegeStructure(DataMapper dataMapper, int owner) {
        this.owner = owner;
        this.deserialize(dataMapper);
    }

    protected int getOwnerId() {
        return this.owner;
    }

    protected abstract void deserialize(DataMapper dataMapper);

    public abstract void init();

    public abstract void update(TeamContainer teams);

    public ItemStack getTool(PlayerHolder playerHolder) {
        return null;
    }

    public abstract void setHolograms(TeamContainer teams);

    @Override
    public abstract boolean equals(Object other);

    @Override
    public abstract int hashCode();
}
