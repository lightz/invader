package ca.lightz.invasion.game.structure.container;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.structure.SiegeStructure;
import ca.lightz.invasion.game.structure.components.GateBinder;
import ca.lightz.invasion.game.structure.gate.Gate;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import javax.annotation.Nonnull;
import java.util.*;

public class StructureContainer implements Iterable<SiegeStructure> {
    private final Set<SiegeStructure> structures;

    public StructureContainer() {
        this.structures = new HashSet<>();
    }

    public void generate() {
        this.structures.forEach(SiegeStructure::init);
        this.bindBattleRam();
    }

    private void bindBattleRam() {
        Collection<Gate> gates = this.getStructuresByClass(Gate.class);
        for (GateBinder gateBinder : this.getStructuresByClass(GateBinder.class)) {
            gateBinder.bind(gates);
        }
    }

    @Override
    @Nonnull
    public Iterator<SiegeStructure> iterator() {
        return this.structures.iterator();
    }

    public void add(SiegeStructure structure) {
        this.structures.add(structure);
    }

    private <T> Collection<T> getStructuresByClass(Class<T> clazz) {
        Collection<T> filter = new ArrayList<>();
        for (SiegeStructure structure : this.structures) {
            if (clazz.isInstance(structure)) {
                filter.add(clazz.cast(structure));
            }
        }
        return filter;
    }

    public void giveTools(PlayerHolder playerHolder) {
        HashMap<String, ItemStack> tools = new HashMap<>();
        for (SiegeStructure structure : this) {
            ItemStack tool = structure.getTool(playerHolder);
            if (tool != null) {
                tools.put(tool.getItemMeta().getDisplayName(), tool);
            }
        }

        if (tools.isEmpty()) {
            return;
        }
        PlayerInventory inventory = playerHolder.player.getInventory();
        tools.values().forEach(inventory::addItem);
        playerHolder.player.updateInventory();
    }
}
