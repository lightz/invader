package ca.lightz.invasion.game.player.perk;

public enum PerkType {
    ARBALIST, BERSERKER, DEFENDER, FIELD_ENGINEER, MARKSMAN, SENTINEL, SWORDMASTER, NONE, ALL
}
