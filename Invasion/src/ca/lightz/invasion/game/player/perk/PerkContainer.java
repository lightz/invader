package ca.lightz.invasion.game.player.perk;

import ca.lightz.invasion.core.Log;
import com.google.common.base.Strings;

import java.util.HashSet;
import java.util.Set;

public class PerkContainer {
    private static final String DELIMITER = ":";
    private final Set<PerkType> perks = new HashSet<>();

    public boolean contains(PerkType perkType) {
        return this.perks.contains(perkType);
    }

    public void add(PerkType perkType) {
        this.perks.add(perkType);
    }

    public boolean remove(PerkType perkType) {
        return this.perks.remove(perkType);
    }

    public void deserialize(String rawPerks) {
        String[] rawSplitPerks = rawPerks.split(DELIMITER);

        for (String perkName1 : rawSplitPerks) {
            if (Strings.isNullOrEmpty(perkName1)) {
                continue;
            }
            String perkName = perkName1.toUpperCase();
            try {
                PerkType perkType = PerkType.valueOf(perkName);
                this.perks.add(perkType);
            } catch (Exception exception) {
                Log.warning(perkName + " doesn't exist.");
            }
        }
    }

    public String serialize() {
        StringBuilder stringBuilder = new StringBuilder();
        for (PerkType perkType : this.perks) {
            stringBuilder.append(perkType.toString()).append(DELIMITER);
        }
        return stringBuilder.toString();
    }
}