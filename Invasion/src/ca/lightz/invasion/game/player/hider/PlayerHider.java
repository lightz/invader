package ca.lightz.invasion.game.player.hider;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.comphenix.protocol.PacketType.Play.Server.*;

public class PlayerHider {
    private static final PacketType[] ENTITY_PACKETS = {ENTITY_EQUIPMENT, BED, ANIMATION, NAMED_ENTITY_SPAWN, COLLECT, ENTITY_VELOCITY, REL_ENTITY_MOVE, ENTITY_LOOK, REL_ENTITY_MOVE_LOOK, ENTITY_TELEPORT, ENTITY_HEAD_ROTATION, ENTITY_STATUS, ATTACH_ENTITY, ENTITY_METADATA, ENTITY_EFFECT, REMOVE_ENTITY_EFFECT, BLOCK_BREAK_ANIMATION};
    private final Map<Integer, Set<Integer>> observerEntityMap = new ConcurrentHashMap<>();

    public void hidePlayer(Player a, Player b) {
        hideEntity(a, b);
    }

    public void showPlayer(Player a, Player b) {
        showEntity(a, b);
    }

    private final ProtocolManager manager;
    // Listeners
    private final PacketAdapter protocolListener;

    /**
     * Construct a new entity hider.
     *
     * @param plugin - the plugin that controls this entity hider.
     */
    public PlayerHider(Plugin plugin) {
        //noinspection ResultOfMethodCallIgnored
        Preconditions.checkNotNull(plugin, "plugin cannot be NULL.");

        // Save policy
        this.manager = ProtocolLibrary.getProtocolManager();

        // Register events and packet listener
        Bukkit.getServer().getPluginManager().registerEvents(constructBukkit(), plugin);
        this.manager.addPacketListener(protocolListener = constructProtocol(plugin));
    }

    /**
     * Construct the packet listener that will be used to intercept every entity-related packet.
     *
     * @param plugin - the parent plugin.
     * @return The packet listener.
     */
    private PacketAdapter constructProtocol(Plugin plugin) {
        return new PacketAdapter(plugin, ENTITY_PACKETS) {
            @Override
            public void onPacketSending(PacketEvent event) {
                int entityID = event.getPacket().getIntegers().read(0);

                // See if this packet should be cancelled
                if (!isVisible(event.getPlayer(), entityID)) {
                    event.setCancelled(true);
                }
            }
        };
    }

    /**
     * Determine if a given entity is visible for a particular observer.
     *
     * @param observer - the observer player.
     * @param entityID -  ID of the entity that we are testing for visibility.
     * @return TRUE if the entity is visible, FALSE otherwise.
     */
    private boolean isVisible(Player observer, int entityID) {
        // We are using a black list, presence means not being able to see
        Set<Integer> targets = this.observerEntityMap.get(observer.getEntityId());
        return targets == null || !targets.contains(entityID);
    }

    /**
     * Construct the Bukkit event listener.
     *
     * @return Our listener.
     */
    private Listener constructBukkit() {
        return new Listener() {

            @EventHandler
            public void onPlayerQuit(PlayerQuitEvent e) {
                removePlayer(e.getPlayer());
            }
        };
    }

    /**
     * Invoked when a player logs out.
     *
     * @param player - the player that jused logged out.
     */
    private void removePlayer(Player player) {
        int playerId = player.getEntityId();
        this.observerEntityMap.remove(playerId);
        for (Set<Integer> targets : this.observerEntityMap.values()) {
            targets.remove(playerId);
        }
    }

    /**
     * Allow the observer to see an entity that was previously hidden.
     *
     * @param observer - the observer.
     * @param entity   - the entity to show.
     * @return TRUE if the entity was hidden before, FALSE otherwise.
     */
    private boolean showEntity(Player observer, Entity entity) {
        validate(observer, entity);
        boolean hiddenBefore = this.setVisible(observer, entity.getEntityId());

        // Resend packets
        if (this.manager != null && hiddenBefore) {
            this.manager.updateEntity(entity, Collections.singletonList(observer));
        }
        return hiddenBefore;
    }

    /**
     * Prevent the observer from seeing a given entity.
     *
     * @param observer - the player observer.
     * @param entity   - the entity to hide.
     * @return TRUE if the entity was previously visible, FALSE otherwise.
     */
    private boolean hideEntity(Player observer, Entity entity) {
        validate(observer, entity);
        boolean visibleBefore = setInvisible(observer, entity.getEntityId());

        if (visibleBefore) {
            PacketContainer destroyEntity = new PacketContainer(ENTITY_DESTROY);
            destroyEntity.getIntegerArrays().write(0, new int[]{entity.getEntityId()});

            // Make the entity disappear
            try {
                this.manager.sendServerPacket(observer, destroyEntity);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Cannot send server packet.", e);
            }
        }
        return visibleBefore;
    }

    /**
     * Remove the given link between observer and entity entry from the table.
     *
     * @param observer - the player observer.
     * @param entityID - ID of the entity.
     * @return TRUE if they were not present, FALSE otherwise.
     */
    private boolean setInvisible(Player observer, int entityID) {
        int observerId = observer.getEntityId();
        if (this.observerEntityMap.containsKey(observerId)) {
            Set<Integer> targets = this.observerEntityMap.get(entityID);
            if (targets.contains(entityID)) {
                return false;
            }
            targets.add(entityID);
            this.observerEntityMap.put(observerId, targets);
        }
        return true;
    }

    /**
     * Remove the given link between observer and entity entry from the table.
     *
     * @param observer - the player observer.
     * @param entityID - ID of the entity.
     * @return TRUE if they were not present, FALSE otherwise.
     */
    private boolean setVisible(Player observer, int entityID) {
        Set<Integer> observerTargets = this.observerEntityMap.get(observer.getEntityId());
        return observerTargets != null && observerTargets.remove(entityID);
    }

    /**
     * Determine if the given entity has been hidden from an observer.
     * <p/>
     * Note that the entity may very well be occluded or out of range from the perspective
     * of the observer. This method simply checks if an entity has been completely hidden
     * for that observer.
     *
     * @param observer - the observer.
     * @param entity   - the entity that may be hidden.
     * @return TRUE if the player may see the entity, FALSE if the entity has been hidden.
     */
    public final boolean canSee(Player observer, Entity entity) {
        validate(observer, entity);

        return isVisible(observer, entity.getEntityId());
    }

    // For validating the input parameters
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void validate(Player observer, Entity entity) {
        Preconditions.checkNotNull(observer, "observer cannot be NULL.");
        Preconditions.checkNotNull(entity, "entity cannot be NULL.");
    }
}
