package ca.lightz.invasion.game.player.hider;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.container.SiegePlayerContainer;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SpawnRoomHider {
    private final PlayerHider playerHider;

    public SpawnRoomHider(PlayerHider playerHider) {
        this.playerHider = playerHider;
    }

    public void exitSpawnRoom(Player player, SiegePlayerContainer allPlayers) {
        UUID uuid = player.getUniqueId();

        for (SiegePlayer siegePlayer : allPlayers) {
            if (siegePlayer.getUUID().equals(uuid)) {
                continue;
            }

            if (siegePlayer.isInSpawnRoom()) {
                continue;
            }

            Player otherPlayer = siegePlayer.getPlayer();
            if (otherPlayer == null) {
                continue;
            }
            this.playerHider.showPlayer(player, otherPlayer);
            this.playerHider.showPlayer(otherPlayer, player);
        }
    }

    public void enterSpawnRoom(Player player, SiegePlayerContainer allPlayers) {
        UUID uuid = player.getUniqueId();

        for (SiegePlayer siegePlayer : allPlayers) {
            if (siegePlayer.getUUID().equals(uuid)) {
                continue;
            }
            Player otherPlayer = siegePlayer.getPlayer();
            if (otherPlayer == null) {
                continue;
            }

            this.playerHider.hidePlayer(player, otherPlayer);
            this.playerHider.hidePlayer(otherPlayer, player);
        }
    }
}
