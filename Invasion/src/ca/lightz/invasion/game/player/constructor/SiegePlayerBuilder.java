package ca.lightz.invasion.game.player.constructor;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.player.SiegePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SiegePlayerBuilder {
    private static final String DEFAULT_UNIT = "Soldier";
    private final Game game;

    public SiegePlayerBuilder(Game game) {
        this.game = game;
    }

    public SiegePlayer buildNewPlayer(Player player) {
        SiegePlayer siegePlayer = new SiegePlayer(player.getUniqueId());
        this.game.changeUnit(siegePlayer, DEFAULT_UNIT);//TODO Load from DB
        return siegePlayer;
    }

    public SiegePlayer build(UUID uuid, String rawPerks, String lastUnit) {
        SiegePlayer siegePlayer = new SiegePlayer(uuid, rawPerks);
        this.game.changeUnit(siegePlayer, lastUnit);
        if (this.playerHasNoUnit(siegePlayer)) {
            this.game.changeUnit(siegePlayer, DEFAULT_UNIT);
        }
        return siegePlayer;
    }

    private boolean playerHasNoUnit(SiegePlayer siegePlayer) {
        return siegePlayer.getUnit() == null;
    }
}
