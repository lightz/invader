package ca.lightz.invasion.game.player;

import org.bukkit.entity.Player;

import java.util.UUID;

public class PlayerHolder {
    public final Player player;
    public final SiegePlayer siegePlayer;

    public PlayerHolder(Player player, SiegePlayer siegePlayer) {
        this.player = player;
        this.siegePlayer = siegePlayer;
    }

    public UUID getUUID() {
        return this.siegePlayer.getUUID();
    }
}
