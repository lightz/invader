package ca.lightz.invasion.game.player;

import ca.lightz.invasion.game.player.perk.PerkContainer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SiegePlayer {
    private final UUID uuid;
    private Team team;
    private Unit unit;
    private boolean isInSpawnRoom;
    private final PerkContainer perks;

    public SiegePlayer(UUID uuid) {
        this.uuid = uuid;
        this.isInSpawnRoom = true;
        this.perks = new PerkContainer();
    }

    public SiegePlayer(UUID uuid, String rawPerks) {
        this(uuid);
        this.perks.deserialize(rawPerks);
    }

    public String getSaveQuery() {
        String query = "UPDATE `player` SET `perks`='%s',`lastUnit`='%s' WHERE `uuid`='%s';";
        return String.format(query, this.perks.serialize(), this.unit.getName(), this.uuid.toString());
    }

    public UUID getUUID() {
        return this.uuid;
    }

    public Team getTeam() {
        return this.team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public boolean inSameTeam(SiegePlayer owner) {
        return this.team.equals(owner.team);
    }

    public void refill(Player player) {
        this.getUnit().refillInventory(player);
    }

    public void respawn() {
        this.isInSpawnRoom = true;
        this.giveUnitKit();
    }

    public void spawn() {
        this.isInSpawnRoom = false;
        this.giveUnitKit();
    }

    private void giveUnitKit() {
        Player player = this.getPlayer();
        this.giveUnitKit(player);
        if (this.team != null) {
            this.team.setWoolColor(player);
        }
    }

    private void giveUnitKit(Player player) {
        this.getUnit().setPlayerInventory(player);
    }

    public boolean isInSpawnRoom() {
        return this.isInSpawnRoom;
    }

    public void removeFromTeam(Player player) {
        this.team.removePlayer(new PlayerHolder(player, this));
    }

    public Unit getUnit() {
        return this.unit;
    }

    public void setUnit(Unit newUnit) {
        if (this.unit != null && this.unit.getName().equalsIgnoreCase(newUnit.getName())) {
            return;
        }

        Player player = this.getPlayer();
        if (!this.canSelectUnit(newUnit)) {
            Messenger.send(player, MessageId.CLASS_NOT_OWNED, SiegeColorCode.WARNING, SiegeColorCode.SHINY, newUnit.getName(), "http://www.civilizationcraft.net/shop");
            return;
        }

        this.unit = newUnit;
        this.giveUnitKit();
        Messenger.send(player, MessageId.SET_CLASS, SiegeColorCode.SUCCESS, SiegeColorCode.SHINY, this.unit.getName());
    }

    private boolean canSelectUnit(Unit unit) {
        PerkType perk = unit.getNeededPerk();
        return perk == PerkType.NONE || this.perks.contains(perk) || this.hasPerkForAllClasses();
    }

    private boolean hasPerkForAllClasses() {
        return this.perks.contains(PerkType.ALL);
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(this.uuid);
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SiegePlayer)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        SiegePlayer otherPlayer = (SiegePlayer) object;
        return this.uuid.equals(otherPlayer.uuid);
    }

    @Override
    public int hashCode() {
        // two randomly chosen prime numbers
        return new HashCodeBuilder(13, 37).append(this.uuid).toHashCode();
    }

    public void unlockPerk(PerkType perkType) {
        this.perks.add(perkType);
    }

    public void removePerk(PerkType perkType) {
        this.perks.remove(perkType);
    }
}
