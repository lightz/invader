package ca.lightz.invasion.game.player.container;

import ca.lightz.invasion.game.player.SiegePlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class SiegePlayerContainer implements Iterable<SiegePlayer> {
    private final Map<UUID, SiegePlayer> players = new HashMap<>();

    public boolean contains(UUID uuid) {
        return this.players.containsKey(uuid);
    }

    public SiegePlayer get(UUID uuid) {
        return this.players.get(uuid);
    }

    public void add(SiegePlayer siegePlayer) {
        this.players.put(siegePlayer.getUUID(), siegePlayer);
    }

    public int size() {
        return this.players.size();
    }

    public SiegePlayer remove(UUID uuid) {
        return this.players.remove(uuid);
    }

    public void clear() {
        this.players.clear();
    }

    public void clean() {
        Iterator<SiegePlayer> iterator = this.players.values().iterator();
        while (iterator.hasNext()) {
            SiegePlayer siegePlayer = iterator.next();
            Player player = Bukkit.getPlayer(siegePlayer.getUUID());
            if (player == null) {
                iterator.remove();
            }
        }
    }

    @Override
    @Nonnull
    public Iterator<SiegePlayer> iterator() {
        return this.players.values().iterator();
    }

    public boolean isEmpty() {
        return this.players.isEmpty();
    }
}
