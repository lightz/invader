package ca.lightz.invasion.game.listener;

import ca.lightz.invasion.game.Game;
import org.bukkit.event.Listener;

public abstract class GameEvent implements Listener {
    protected final Game game;

    public GameEvent(Game game) {
        this.game = game;
    }
}
