package ca.lightz.invasion.game.listener;

import ca.lightz.invasion.game.Game;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;


public class ServerMessage implements Listener {
    private static boolean SERVER_IS_READY = false;
    private final Game game;

    public static void setServerReady() {
        SERVER_IS_READY = true;
    }

    public ServerMessage(Game game) {
        this.game = game;
    }

    @EventHandler(ignoreCancelled = true)
    public void playerPingServer(ServerListPingEvent event) {
        if (ServerMessage.SERVER_IS_READY) {
            String mapName = this.game.getCurrentMapName();
            event.setMotd(ChatColor.GOLD + "Siege! Current map : " + ChatColor.BLUE + mapName);
        } else {
            event.setMotd(ChatColor.GOLD + "Siege's server is currently restarting, please wait a bit. :)");
        }
    }
}
