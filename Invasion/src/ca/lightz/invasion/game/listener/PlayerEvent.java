package ca.lightz.invasion.game.listener;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.unit.flamewarden.firepit.FirePitContainer;
import ca.lightz.invasion.game.util.block.BlockUtil;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.Ladder;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashSet;
import java.util.Set;

import static org.bukkit.event.EventPriority.LOW;

public class PlayerEvent extends GameEvent {
    private static final Set<SlotType> DISABLED_INVENTORY_SLOT = new HashSet<>();
    private static final Set<BlockFace> SIGN_DIRECTION = new HashSet<>();
    private static final Set<Material> TRANSPARENT = new HashSet<>();

    static {
        TRANSPARENT.add(Material.AIR);

        DISABLED_INVENTORY_SLOT.add(SlotType.ARMOR);
        DISABLED_INVENTORY_SLOT.add(SlotType.CRAFTING);
        DISABLED_INVENTORY_SLOT.add(SlotType.FUEL);
        DISABLED_INVENTORY_SLOT.add(SlotType.RESULT);

        SIGN_DIRECTION.add(BlockFace.EAST);
        SIGN_DIRECTION.add(BlockFace.SOUTH);
        SIGN_DIRECTION.add(BlockFace.NORTH);
        SIGN_DIRECTION.add(BlockFace.WEST);
        SIGN_DIRECTION.add(BlockFace.UP);
    }

    public PlayerEvent(Game game) {
        super(game);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        this.game.respawn(event);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent itemEvent) {
        itemEvent.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerPickupItem(EntityPickupItemEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        if (this.game.isInSpawnRoom(player) || (event.getItem().getType() != EntityType.ARROW && this.hasBow(player))) {
            event.setCancelled(true);
        }
    }

    private boolean hasBow(Player player) {
        return this.hasItem(player, Material.BOW);
    }

    private boolean hasItem(Player player, Material material) {
        Inventory inventory = player.getInventory();
        for (ItemStack itemStack : inventory.getContents()) {
            if (itemStack == null) {
                continue;
            }
            if (itemStack.getType() == material) {
                return true;
            }
        }
        return false;
    }

    @EventHandler(priority = LOW, ignoreCancelled = true)
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        if (this.game.isInSpawnRoom(player)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerBedEnterEvent(PlayerBedEnterEvent event) {
        event.setCancelled(true);
    }


    @EventHandler(ignoreCancelled = true)
    public void onPlayerTryToBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (this.game.isInSpawnRoom(player)) {
            event.setCancelled(true);
            return;
        }

        event.setDropItems(false);
        Block block = event.getBlock();
        block.getDrops().clear();
        switch (block.getType()) {
            case CAULDRON:
            case LADDER:
                this.game.getUnit(player).onBlockBreak(event);
                break;
            default:
                event.setCancelled(true);
                break;
        }

        this.game.breakBlock(event);
    }

    @EventHandler(ignoreCancelled = true)
    public void playerPlaceBlock(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (this.game.isInSpawnRoom(player)) {
            event.setCancelled(true);
            return;
        }

        Material replaced = event.getBlockReplacedState().getType();
        if (this.isSpaceTakenByNonSolid(replaced, event.getBlockPlaced().getLocation())) {
            //No need for error messages when placed on item frame/painting
            event.setCancelled(true);
            return;
        }

        if (BlockUtil.isLiquid(event.getBlockReplacedState())) {
            Messenger.send(player, MessageId.ERROR_CANNOT_PLACE_IN_WATER, SiegeColorCode.ERROR);
            event.setCancelled(true);
            return;
        }

        if (!this.isValidLadderPlacement(event.getBlockPlaced(), event.getBlockAgainst())) {
            Messenger.send(player, MessageId.ERROR_CANNOT_SUPPORT_LADDER, SiegeColorCode.WARNING);
            event.setCancelled(true);
            return;
        }

        this.game.getUnit(player).onBlockPlace(event);
    }

    private boolean isSpaceTakenByNonSolid(Material replaced, Location location) {
        if (replaced == Material.PAINTING) {
            return true;
        }

        World world = location.getWorld();
        for (Entity entity : world.getNearbyEntities(location, 1, 1, 1)) {
            if (entity instanceof Hanging) {
                return true;
            }
        }

        return false;
    }

    private boolean isValidLadderPlacement(Block ladder, Block support) {
        if (ladder.getType() != Material.LADDER) {
            return true;
        }

        Ladder ladderPlaced = (Ladder) ladder.getBlockData();
        BlockFace attached = ladderPlaced.getFacing();

        return BlockUtil.isValidLadderSupport(attached, support);
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent event) {
        if (DISABLED_INVENTORY_SLOT.contains(event.getSlotType())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void playerTryToCraft(CraftItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onKill(PlayerDeathEvent event) {
        SiegePlayer siegePlayer = this.game.getPlayer(event.getEntity().getUniqueId());
        FirePitContainer.getInstance().removeFirePit(siegePlayer);
        //TODO send custom death message
        event.setDeathMessage("");
    }

    @EventHandler(ignoreCancelled = true)
    public void onReceiveExperience(PlayerExpChangeEvent event) {
        event.setAmount(0);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBowShoot(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        if (this.game.isInSpawnRoom(player)) {
            event.setCancelled(true);
            player.updateInventory();
            return;
        }

        double force = event.getForce();
        if (force < 0.75) {
            if (force >= 0.25) {
                Messenger.send(player, MessageId.ERROR_BOW_NOT_CHARGED_ENOUGH, SiegeColorCode.WARNING);
            }
            event.setCancelled(true);
            player.updateInventory();
            return;
        }

        this.game.getUnit(player).onBowShoot(event);
    }

    @EventHandler(priority = LOW, ignoreCancelled = true)
    public void onPlayerInteractWithEntity(PlayerInteractEntityEvent event) {
        Entity clickedEntity = event.getRightClicked();
        if (!this.isValidEntity(clickedEntity)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = LOW, ignoreCancelled = true)
    public void onPlayerInteractWithEntity(PlayerArmorStandManipulateEvent event) {
        event.setCancelled(true);
    }

    private boolean isValidEntity(Entity entity) {
        return (entity instanceof LivingEntity && !(entity instanceof ArmorStand)) || entity instanceof Vehicle;
    }


    /**
     * Represents an event that is called when a player interacts with an object or air,
     * potentially fired once for each hand. The hand can be determined using getHand().
     * This event will fire as cancelled if the vanilla behavior is to do nothing (e.g interacting with air)
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        SiegePlayer siegePlayer = this.game.getPlayer(event.getPlayer().getUniqueId());
        if (siegePlayer.isInSpawnRoom()) {
            this.onInteractInSpawnRoom(event);
            return;
        }

        Action eventAction = event.getAction();
        if (eventAction == Action.LEFT_CLICK_BLOCK || eventAction == Action.RIGHT_CLICK_BLOCK) {
            this.onPlayerBlockClick(event);
        }

        siegePlayer.getUnit().interact(event);
    }

    private void onInteractInSpawnRoom(PlayerInteractEvent event) {
        Action action = event.getAction();
        Player player = event.getPlayer();
        Block block = null;
        if (action == Action.LEFT_CLICK_AIR || action == Action.RIGHT_CLICK_AIR) {
            block = this.getClickedBlock(player);
        } else if (action == Action.LEFT_CLICK_BLOCK || action == Action.RIGHT_CLICK_BLOCK) {
            if (action == Action.RIGHT_CLICK_BLOCK && this.hasBlockInHands(player)) {
                event.setCancelled(true); //Prevent placing blocks when spawning
            }
            block = event.getClickedBlock();
        }

        if (block == null) {
            return;
        }

        Sign sign = this.getSign(block);
        if (sign == null) {
            return;
        }

        this.clickOnSign(player, sign, action);
    }

    private void clickOnSign(Player player, Sign sign, Action action) {
        String signId = ChatColor.stripColor(sign.getLine(0).trim().toUpperCase());
        switch (signId) {
            case "SPAWN":
                this.game.spawn(player, sign.getLine(1));
                break;
            case "CLASS":
                String unitName = sign.getLine(1);
                if (action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR) {
                    this.game.sendUnitDescription(player, unitName);
                } else {
                    this.changeClass(player, unitName);
                }
                break;
            default:
                break;
        }
    }

    private void changeClass(Player player, String className) {
        this.game.changeUnit(player, className);
    }

    private Sign getSign(Block block) {
        Material blockType = block.getType();
        if (block.getState() instanceof Sign) {
            return (Sign) block.getState();
        } else if (Tag.WOOL.isTagged(blockType)) {
            return this.getSignAroundWool(block);
        }
        return null;
    }

    private Sign getSignAroundWool(Block wool) {
        for (BlockFace face : SIGN_DIRECTION) {
            Block block = wool.getRelative(face);
            if (block.getState() instanceof Sign) {
                return (Sign) block.getState();
            }
        }
        return null;
    }

    private Block getClickedBlock(Player player) {
        for (Block block : player.getLastTwoTargetBlocks(TRANSPARENT, 30)) {
            Material blockType = block.getType();
            BlockState state = block.getState();
            if (state instanceof Sign || Tag.WOOL.isTagged(blockType)) {
                return block;
            } else if (blockType.isSolid()) {
                break;
            }
        }
        return null;
    }

    private void onPlayerBlockClick(PlayerInteractEvent event) {
        Block clickedBlock = event.getClickedBlock();
        Player player = event.getPlayer();

        if (this.cancelInteractEvent(event)) {
            event.setCancelled(true);
        }

        switch (clickedBlock.getType()) {
            case CAULDRON:
                PlayerHolder playerHolder = new PlayerHolder(player, this.game.getPlayer(player.getUniqueId()));
                FirePitContainer.getInstance().interactWithFirePit(clickedBlock, playerHolder);
                break;
            case ENDER_CHEST:
                this.game.refill(player);
                break;
            default:
                break;
        }
    }

    private boolean cancelInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && this.isPlacingBlock(player)) {
            return false;
        }

        Block clickedBlock = event.getClickedBlock();
        return BlockUtil.isDisabledInteractiveBlock(clickedBlock);
    }

    private boolean isPlacingBlock(Player player) {
        return player.isSneaking() && this.hasBlockInHands(player);
    }

    private boolean hasBlockInHands(Player player) {
        PlayerInventory inventory = player.getInventory();
        return this.isBlock(inventory.getItemInMainHand()) || this.isBlock(inventory.getItemInOffHand());
    }

    private boolean isBlock(ItemStack itemStack) {
        return itemStack != null && (itemStack.getType().isBlock() || itemStack.getType() == Material.CAULDRON);
    }
}