package ca.lightz.invasion.game.listener;

import ca.lightz.invasion.game.Game;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

public class GameLogin extends GameEvent {
    public static final int STARTING_SATURATION = 5;
    public static final int STARTING_FOOD = 20;

    public GameLogin(Game game) {
        super(game);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void playerLogin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        this.playerConnect(player);
    }

    private void playerConnect(Player player) {
        player.getInventory().setHeldItemSlot(0); //Must be set after the player is logged
        this.game.addPlayer(player);
        this.game.sendToSpawn(player);
        this.game.sendScoreboard(player);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void playerSpawnOnJoin(PlayerSpawnLocationEvent event) {
        this.clearPlayerData(event.getPlayer());
        this.game.sendToSpawn(event.getPlayer());
    }

    private void clearPlayerData(Player player) {
        player.getEnderChest().clear();
        player.getInventory().clear();
        AttributeInstance attribute = player.getAttribute(Attribute.GENERIC_MAX_HEALTH);
        player.setHealth(attribute.getValue());
        player.setFoodLevel(STARTING_FOOD);
        player.setSaturation(STARTING_SATURATION);
        player.setFireTicks(0);
        player.setVelocity(new Vector());
        player.setFlying(false);
        player.setExp(0);
        player.setNoDamageTicks(0);
        player.setLevel(0);
        player.setFallDistance(0);
    }

    @EventHandler(ignoreCancelled = true)
    public void playerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        this.game.removePlayer(player);
    }

    @EventHandler(ignoreCancelled = true)
    public void playerQuit(PlayerKickEvent event) {
        Player player = event.getPlayer();
        this.game.removePlayer(player);
    }
}
