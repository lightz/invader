package ca.lightz.invasion.game.listener;

import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.unit.flamewarden.firepit.FirePitContainer;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.Collection;

public class MapEvent implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onCauldronFill(CauldronLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void preventMapDamage(PlayerInteractEvent event) {
        if (event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.FARMLAND) {
            event.setCancelled(true);
            return;
        }

        if (event.getAction() != Action.RIGHT_CLICK_BLOCK || event.getItem() == null) {
            return;
        }

        Block block = event.getClickedBlock();
        Material material = block.getType();
        if (Tag.LOGS.isTagged(material)) {
            if (Armory.AXES.contains(event.getItem().getType())) {
                event.setCancelled(true);
            }
        } else if (material == Material.GRASS_BLOCK) {
            if (Armory.SHOVELS.contains(event.getItem().getType())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void preventDeathDrop(EntityDeathEvent event) {
        event.getDrops().clear();
        event.setDroppedExp(0);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void preventSpawnerSpawn(SpawnerSpawnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void preventMobSpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason() != CreatureSpawnEvent.SpawnReason.CUSTOM) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void preventItemDropOnEntityExplosion(EntityExplodeEvent event) {
        this.processExplosion(event.blockList());
        event.setYield(0);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void preventItemDropOnBlockExplosion(BlockExplodeEvent event) {
        this.processExplosion(event.blockList());
        event.setYield(0);
    }

    private void processExplosion(Collection<Block> blocks) {
        FirePitContainer firePitContainer = FirePitContainer.getInstance();
        blocks.forEach(firePitContainer::destroyFirePit);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onThunderChange(ThunderChangeEvent event) {
        event.setCancelled(true);
    }
}
