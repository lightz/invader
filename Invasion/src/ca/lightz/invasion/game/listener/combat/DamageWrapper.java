package ca.lightz.invasion.game.listener.combat;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageWrapper {
    public final Player damager;
    public final Entity defender;
    public int damage;
    public int armor;
    public final boolean isProjectile;
    public final EntityDamageEvent.DamageCause cause;

    public DamageWrapper(Player damager, Entity damagingEntity, Entity defender, EntityDamageEvent.DamageCause cause, int damage, int armor) {
        this.damager = damager;
        this.isProjectile = damagingEntity instanceof Projectile;
        this.defender = defender;
        this.cause = cause;
        this.damage = damage;
        this.armor = armor;
    }

    public int getEffectiveDamage() {
        return this.damage - this.armor;
    }
}
