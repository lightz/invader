package ca.lightz.invasion.game.listener.combat;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.listener.GameEvent;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.util.item.SiegeAttributeExtractor;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.*;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

import java.util.HashSet;
import java.util.Set;

public class CombatEvent extends GameEvent {
    public static final PotionEffectType DODGE_POTION_EFFECT = PotionEffectType.DAMAGE_RESISTANCE;
    private static final Set<EntityDamageEvent.DamageCause> PASSIVE_DAMAGE_DODGE = new HashSet<>();
    private static final Set<EntityDamageEvent.DamageCause> ACTIVE_DAMAGE_DODGE = new HashSet<>();
    private static final int IMMUNE_TICKS = 2;
    private DamagingEntityCache<Projectile> arrowCache;

    static {
        ACTIVE_DAMAGE_DODGE.add(EntityDamageEvent.DamageCause.ENTITY_ATTACK);
        ACTIVE_DAMAGE_DODGE.add(EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK);
        ACTIVE_DAMAGE_DODGE.add(EntityDamageEvent.DamageCause.PROJECTILE);
        PASSIVE_DAMAGE_DODGE.add(EntityDamageEvent.DamageCause.FALL);
    }

    public CombatEvent(Game game) {
        super(game);
        this.arrowCache = this.game.getArrowCache();
    }

    @EventHandler(ignoreCancelled = true)
    public void onGenericReceiveDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (!(entity instanceof Damageable)) {
            return;
        }
        this.reduceFallDamage(event);

        if (!(entity instanceof Player)) {
            return;
        }
        Player defender = (Player) entity;
        //We do not prevent projectile since we want to remove the projectile to avoid bouncing effect
        //In order to do that we must wait for the EntityDamagedByEntity event
        if (event.getCause() != EntityDamageEvent.DamageCause.PROJECTILE && this.game.isInSpawnRoom(defender)) {
            event.setCancelled(true);
            return;
        }

        this.onReceiveDamage(defender, event.getCause(), event);
    }

    private void reduceFallDamage(EntityDamageEvent event) {
        if (event.getCause() != EntityDamageEvent.DamageCause.FALL) {
            return;
        }
        Block floor = event.getEntity().getLocation().getBlock().getRelative(BlockFace.DOWN);
        if (floor.getType() == Material.HAY_BLOCK) {
            event.setDamage(event.getDamage() * 0.5);
        }
    }

    private void onReceiveDamage(Player defender, EntityDamageEvent.DamageCause cause, Cancellable event) {
        if (this.canDodgePassiveDamage(defender, cause)) {
            Messenger.send(defender, MessageId.DODGED_DAMAGE, SiegeColorCode.SUCCESS);
            event.setCancelled(true);
        }
    }

    private boolean canDodgePassiveDamage(Player player, EntityDamageEvent.DamageCause cause) {
        return PASSIVE_DAMAGE_DODGE.contains(cause) && this.isInDodge(player);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        Entity damaged = event.getEntity();
        Entity damager = event.getDamager();
        if (!(damaged instanceof Damageable) || damaged instanceof ArmorStand) {
            this.cancelDamageEvent(event, damager);
            return;
        }

        if (damaged instanceof LivingEntity && this.hasNoDamageTicksLeft((LivingEntity) damaged)) {
            this.cancelDamageEvent(event, damager);
            return;
        }

        if (!this.isValidDamager(damager)) {
            if (damager instanceof Explosive) {
                return;
            }
            Log.warning("Damaged by an invalid entity : " + event.getDamager().getType());
            return;
        }

        event.setDamage(0);
        Player attacker = this.getPlayer(damager);
        if (this.game.isInSpawnRoom(attacker)) {
            this.cancelDamageEvent(event, damager);
            return;
        }

        DamageWrapper damageWrapper = this.getDamageWrapper(event, attacker);
        if (damageWrapper.damage <= 0) {
            this.cancelDamageEvent(event, damager);
            return;
        }

        SiegePlayer siegeAttacker = this.game.getPlayer(attacker.getUniqueId());
        if (damaged instanceof Player) {
            Player defender = (Player) event.getEntity();

            if (this.game.isInSpawnRoom(defender)) {
                this.cancelDamageEvent(event, damager);
                return;
            }

            SiegePlayer siegeDefender = this.game.getPlayer(defender.getUniqueId());
            if (siegeAttacker.inSameTeam(siegeDefender)) {
                this.cancelDamageEvent(event, damager);
                return;
            }
            this.onReceiveAttack(damageWrapper, damager, defender, event);
        }
        siegeAttacker.getUnit().onDealDamage(damageWrapper, event);
        this.dealDamage(damaged, damageWrapper);
    }

    private boolean hasNoDamageTicksLeft(LivingEntity livingEntity) {
        return livingEntity.getNoDamageTicks() > 0;
    }

    private void onReceiveAttack(DamageWrapper damageWrapper, Entity damager, Player defender, Cancellable event) {
        if (this.canDodgeAttack(defender, damageWrapper.cause)) {
            Messenger.send(defender, MessageId.DODGED_DAMAGE_FROM_PLAYER, SiegeColorCode.SUCCESS, SiegeColorCode.SHINY, damageWrapper.damager.getName());
            Messenger.send(damageWrapper.damager, MessageId.TARGET_DODGED_DAMAGE, SiegeColorCode.MISS, SiegeColorCode.SHINY, defender.getName());
            this.cancelDamageEvent(event, damager);
        }
    }

    private void cancelDamageEvent(Cancellable event, Entity damager) {
        event.setCancelled(true);
        if (damager instanceof Projectile) {
            damager.remove();
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onTakingFire(EntityCombustByEntityEvent event) {
        Entity entity = event.getEntity();
        if (!(entity instanceof Player)) {
            return;
        }

        Player damaged = (Player) entity;
        if (this.game.isInSpawnRoom(damaged) || cancelFireDamage(damaged, event)) {
            event.setCancelled(true);
        }
    }

    private boolean cancelFireDamage(Player damaged, EntityCombustByEntityEvent event) {
        Entity combuster = event.getCombuster();
        if (!(combuster instanceof Projectile)) {
            return false;
        }

        if (this.isInDodge(damaged)) {
            return true;
        }

        Projectile projectile = (Projectile) combuster;
        ProjectileSource source = projectile.getShooter();
        if (!(source instanceof Player)) {
            return false;
        }

        SiegePlayer siegeAttacker = this.game.getPlayer(((Player) source).getUniqueId());
        SiegePlayer siegeDefender = this.game.getPlayer(damaged.getUniqueId());
        return siegeAttacker.inSameTeam(siegeDefender);
    }

    private boolean canDodgeAttack(Player player, EntityDamageEvent.DamageCause cause) {
        return ACTIVE_DAMAGE_DODGE.contains(cause) && this.isInDodge(player);
    }

    private boolean isInDodge(Player player) {
        return player.hasPotionEffect(DODGE_POTION_EFFECT);
    }

    private DamageWrapper getDamageWrapper(EntityDamageByEntityEvent event, Player attacker) {
        Entity defender = event.getEntity();
        Entity damager = event.getDamager();
        int damage = this.getDamage(damager);
        int armor = this.getArmor(defender);
        return new DamageWrapper(attacker, damager, defender, event.getCause(), damage, armor);
    }

    private Player getPlayer(Entity entity) {
        if (entity instanceof Player) {
            return (Player) entity;
        }
        return (Player) ((Projectile) entity).getShooter();
    }

    private void dealDamage(Entity damaged, DamageWrapper damageWrapper) {
        Damageable damageable = (Damageable) damaged;
        int reducedDamage = damageWrapper.getEffectiveDamage();
        double healthLeft = Math.max(0, damageable.getHealth() - reducedDamage);
        damageable.setHealth(healthLeft);

        if (healthLeft > 0 && damaged instanceof LivingEntity) {
            LivingEntity damagedLivingEntity = (LivingEntity) damaged;
            damagedLivingEntity.setNoDamageTicks(IMMUNE_TICKS);
        } else if (healthLeft == 0) {
            deathMessage(damaged, damageWrapper);
        }
    }

    private void deathMessage(Entity damaged, DamageWrapper damageWrapper) {
        if (damaged instanceof Player) {
            Player damagedPlayer = (Player) damaged;
            Messenger.send(damageWrapper.damager, MessageId.TEMP_KILL_MESSAGE, damagedPlayer.getName());
            Messenger.send(damagedPlayer, MessageId.TEMP_DEATH_MESSAGE, damageWrapper.damager.getName());
        }
    }

    private int getArmor(Entity defender) {
        if (defender instanceof Player) {
            return this.getPlayerArmor((Player) defender);
        }
        return 0;
    }

    private int getDamage(Entity entity) {
        if (entity instanceof Player) {
            return this.getPlayerDamage((Player) entity);
        }
        if (entity instanceof Projectile) {
            return this.getProjectileDamage((Projectile) entity);
        }
        return 0;
    }

    private int getProjectileDamage(Projectile projectile) {
        return this.arrowCache.getDamage(projectile);
    }

    private int getPlayerDamage(Player player) {
        ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand.getType() == Material.BOW) {
            return 0;
        }

        return SiegeAttributeExtractor.getDamage(itemInHand);
    }

    private int getPlayerArmor(Player player) {
        PlayerInventory inventory = player.getInventory();
        for (ItemStack itemStack : inventory.getArmorContents()) {
            int armor = SiegeAttributeExtractor.getArmor(itemStack);
            if (armor > 0) {
                return armor;
            }
        }
        return 0;
    }

    private boolean isValidDamager(Entity entity) {
        return entity instanceof Player || (entity instanceof Projectile && ((Projectile) entity).getShooter() instanceof Player);
    }
}
