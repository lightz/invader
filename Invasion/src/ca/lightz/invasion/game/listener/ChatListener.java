package ca.lightz.invasion.game.listener;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.channel.ChannelManager;
import ca.lightz.invasion.game.player.SiegePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {
    private Game game;
    private ChannelManager channelManager;

    public ChatListener(ChannelManager channelManager, Game game) {
        this.channelManager = channelManager;
        this.game = game;
    }

    @EventHandler
    public void sendChatMessage(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        SiegePlayer siegePlayer = this.game.getPlayer(player.getUniqueId());

        this.channelManager.sendChatMessage(event, siegePlayer);
    }
}
