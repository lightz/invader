package ca.lightz.invasion.game.objective.control;

import ca.lightz.invasion.game.map.SiegeMap;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.objective.event.ObjectiveAttackEvent;
import ca.lightz.invasion.game.objective.event.ObjectiveDefenseEvent;
import ca.lightz.invasion.game.objective.event.ObjectiveOwnershipChangeEvent;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamPlayerContainer;
import org.bukkit.Bukkit;

import java.util.Collection;

public class Control {
    private static final int POINT_PER_TICK = SiegeMap.CAPTURE_TICK / 20;
    private static final double MINIMUM_PERCENTAGE_CONTROL_TO_SPAWN = 0.5;

    private final Team startingTeam;
    private final boolean wasLockedAtStart;
    private final int startingControl;

    private Team team;
    private int current;
    private final int maximum;

    private Objective objective;
    private boolean isLocked;
    private final boolean lockOnCapture;

    public Control(int controlValue, boolean isLocked, boolean lockOnCapture, Team team) {
        this.team = team;
        this.maximum = controlValue;
        this.isLocked = isLocked;
        this.lockOnCapture = lockOnCapture;
        this.current = this.team.isNeutral() ? 0 : this.maximum;

        this.startingTeam = team;
        this.wasLockedAtStart = isLocked;
        this.startingControl = this.current;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }

    public void captureTick(Collection<TeamPlayerContainer> teamsPlayers) {
        TeamPlayerContainer bestSquad = this.getCapturingTeam(teamsPlayers);
        if (bestSquad == null || bestSquad.isEmpty() || !this.canCapture(bestSquad.getTeam())) {
            return;
        }
        this.capture(bestSquad);
    }

    private void capture(TeamPlayerContainer squad) {
        if (!this.team.equals(squad.getTeam())) {
            this.attack(squad);
        } else {
            this.defend(squad);
        }
    }

    private void defend(TeamPlayerContainer defendingSquad) {
        int ticks = defendingSquad.size() * POINT_PER_TICK;
        this.increaseTicks(ticks);

        ObjectiveDefenseEvent event = new ObjectiveDefenseEvent(this.objective);
        Bukkit.getPluginManager().callEvent(event);
    }

    private void increaseTicks(int ticks) {
        if (this.isLocked()) {
            return;
        }
        this.current += ticks;
        this.current = Math.min(this.current, this.maximum);
    }

    private void attack(TeamPlayerContainer capturingSquad) {
        int ticks = capturingSquad.size() * POINT_PER_TICK;
        this.reduceTicks(capturingSquad.getTeam(), ticks);
    }

    private void reduceTicks(Team capturingTeam, int ticks) {
        if (ticks <= this.current) {
            this.current -= ticks;
            this.announce(capturingTeam);
            return;
        }

        ticks -= this.current;
        this.current = 0;
        this.changeOwnership(capturingTeam);
        this.increaseTicks(ticks);
    }

    private void announce(Team capturingTeam) {
        ObjectiveAttackEvent event = new ObjectiveAttackEvent(this.objective, this.current, this.maximum, capturingTeam, this.team);
        Bukkit.getPluginManager().callEvent(event);
    }

    public void changeOwnership(Team newTeam) {
        this.team = newTeam;
        if (this.lockOnCapture) {
            this.isLocked = true;
            this.current = this.maximum;
        }
        ObjectiveOwnershipChangeEvent event = new ObjectiveOwnershipChangeEvent(this.objective, newTeam, this.team);
        Bukkit.getPluginManager().callEvent(event);
    }

    private TeamPlayerContainer getCapturingTeam(Collection<TeamPlayerContainer> teamsPlayers) {
        TeamPlayerContainer capturingTeam = null;
        int best = 0;

        for (TeamPlayerContainer playerContainer : teamsPlayers) {
            int teamSize = playerContainer.size();
            if (teamSize > best) {
                best = teamSize;
                capturingTeam = playerContainer;
            } else if (teamSize == best) {
                capturingTeam = null; //If two teams have the same amount of players capturing -> Stalemate
            }
        }

        return capturingTeam;
    }

    private boolean canCapture(Team capturingTeam) {
        return !this.isFull() || !this.team.equals(capturingTeam);
    }

    private boolean isFull() {
        return this.current >= this.maximum;
    }

    public boolean isLocked() {
        return this.isLocked;
    }

    public boolean isControlling(Team teamToCheck) {
        return this.team.equals(teamToCheck);
    }

    public boolean canSpawn() {
        return (this.current / (double) this.maximum) > MINIMUM_PERCENTAGE_CONTROL_TO_SPAWN;
    }

    public Team getControllingTeam() {
        return this.team;
    }

    public void reset() {
        this.team = this.startingTeam;
        this.isLocked = this.wasLockedAtStart;
        this.current = this.startingControl;
    }

    public boolean isLockedOnStart() {
        return this.wasLockedAtStart;
    }

    public int getPercentage() {
        return (int) Math.ceil(this.current / ((double) this.maximum) * 100);
    }

    public int getMaximum() {
        return this.maximum;
    }

    public int getCurrent() {
        return this.current;
    }
}
