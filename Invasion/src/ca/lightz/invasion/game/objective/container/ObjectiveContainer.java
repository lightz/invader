package ca.lightz.invasion.game.objective.container;

import ca.lightz.invasion.game.indicator.WoolIndicatorContainer;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamPlayerContainer;

import javax.annotation.Nonnull;
import java.util.*;

public class ObjectiveContainer implements Iterable<Objective> {
    private final Map<String, Objective> objectives;

    public ObjectiveContainer() {
        this.objectives = new HashMap<>();
    }

    public void add(Objective objective) {
        this.objectives.put(objective.getName(), objective);
    }

    public void resetAll() {
        this.objectives.values().forEach(Objective::reset);
    }

    public void setVisual(String objectiveName, WoolIndicatorContainer woolIndicatorContainer) {
        if (!this.objectives.containsKey(objectiveName)) {
            return;
        }

        Objective objective = this.objectives.get(objectiveName);
        objective.setVisual(woolIndicatorContainer);
    }

    public boolean spawn(PlayerHolder playerHolder, String objectiveName) {
        if (!this.objectives.containsKey(objectiveName)) {
            return false;
        }

        Objective objective = this.objectives.get(objectiveName);
        return objective.spawn(playerHolder);
    }

    public void captureTick(Collection<TeamPlayerContainer> teamsPlayers) {
        for (Objective objective : this.objectives.values()) {
            objective.captureTick(teamsPlayers);
        }
    }

    public boolean isTeamDominating(int numberOfTeams) {
        Set<Team> teamsAbleToSpawn = this.getTeamsAbleToSpawn();
        return numberOfTeams > teamsAbleToSpawn.size();
    }

    private Set<Team> getTeamsAbleToSpawn() {
        Set<Team> teamsAbleToSpawn = new HashSet<>();

        for (Objective objective : this.objectives.values()) {
            if (!objective.canSpawn()) {
                continue;
            }

            Team controllingTeam = objective.getControllingTeam();
            if (controllingTeam.isNeutral()) {
                continue;
            }
            teamsAbleToSpawn.add(controllingTeam);
        }

        return teamsAbleToSpawn;
    }

    public Objective getObjective(String objectiveName) {
        return this.objectives.get(objectiveName);
    }

    public int size() {
        return this.objectives.size();
    }

    @Nonnull
    @Override
    public Iterator<Objective> iterator() {
        return this.objectives.values().iterator();
    }
}
