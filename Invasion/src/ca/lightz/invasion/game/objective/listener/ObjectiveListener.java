package ca.lightz.invasion.game.objective.listener;

import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.objective.ObjectiveMessage;
import ca.lightz.invasion.game.objective.event.ObjectiveAttackEvent;
import ca.lightz.invasion.game.objective.event.ObjectiveDefenseEvent;
import ca.lightz.invasion.game.objective.event.ObjectiveOwnershipChangeEvent;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.components.TeamMessage;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Map;

public class ObjectiveListener implements Listener {
    private static final double NUMBER_MESSAGE_DURING_CAPTURE = 4;
    private final Game game;

    private final Map<String, Integer> objectiveLastAnnouncedStep = new HashMap<>();

    public ObjectiveListener(Game game) {
        this.game = game;
    }

    @EventHandler
    public void onObjectiveAttack(ObjectiveDefenseEvent event) {
        Objective objective = event.getObjective();
        this.setLastAnnouncedStep(objective.getName(), Integer.MAX_VALUE);
    }

    @EventHandler
    public void onObjectiveAttack(ObjectiveAttackEvent event) {
        Objective objective = event.getObjective();
        Team capturingTeam = event.getCapturingTeam();
        Team defendingTeam = event.getDefendingTeam();
        int current = event.getCurrent();
        int maximum = event.getMaximum();
        this.messageOnObjectiveCapturing(objective, current, maximum, capturingTeam, defendingTeam);
    }

    private void messageOnObjectiveCapturing(Objective objective, int current, int maximum, Team capturingTeam, Team defendingTeam) {
        String objectiveName = objective.getName();

        double step = maximum / NUMBER_MESSAGE_DURING_CAPTURE;
        int currentStep = (int) Math.ceil(current / step);
        int lastAnnounced = this.getLastAnnouncedStep(objectiveName);
        if (currentStep == lastAnnounced) {
            return;
        }

        this.setLastAnnouncedStep(objectiveName, currentStep);

        int capturingId = capturingTeam.getId();
        TeamMessage capturingMessage = new TeamMessage(capturingId, new UnlocalizedMessage(MessageId.ATTACKING_OBJECTIVE_PROGRESS, SiegeColorCode.CAPTURING, SiegeColorCode.SHINY, objectiveName, defendingTeam.getChatColor(), current, maximum));

        int defendingId = defendingTeam.getId();
        TeamMessage defendingMessage = new TeamMessage(defendingId, new UnlocalizedMessage(MessageId.OBJECTIVE_ATTACKED, SiegeColorCode.ATTACKED, SiegeColorCode.SHINY, capturingTeam.getName(), objectiveName, defendingTeam.getChatColor(), current, maximum));

        UnlocalizedMessage neutral = new UnlocalizedMessage(MessageId.OTHER_OBJECTIVE_ATTACKED, SiegeColorCode.NEUTRAL, SiegeColorCode.SHINY, capturingTeam.getName(), objectiveName);

        ObjectiveMessage message = new ObjectiveMessage(defendingMessage, capturingMessage, neutral);
        this.messageTeams(message);
    }

    private int getLastAnnouncedStep(String objectiveName) {
        return this.objectiveLastAnnouncedStep.getOrDefault(objectiveName, Integer.MAX_VALUE);
    }

    private void setLastAnnouncedStep(String objectiveName, int step) {
        this.objectiveLastAnnouncedStep.put(objectiveName, step);
    }

    @EventHandler
    public void onObjectiveCapture(ObjectiveOwnershipChangeEvent event) {
        Objective objective = event.getObjective();
        Team newTeam = event.getNewTeam();
        Team oldTeam = event.getOldTeam();
        this.game.sendScoreboard();
        this.messageOnObjectiveCapture(objective, newTeam, oldTeam);
    }

    private void messageOnObjectiveCapture(Objective objective, Team newTeam, Team oldTeam) {
        String objectiveName = objective.getName();
        int capturingId = newTeam.getId();
        TeamMessage capturingMessage = new TeamMessage(capturingId, new UnlocalizedMessage(MessageId.CAPTURED_OBJECTIVE, SiegeColorCode.CAPTURING, SiegeColorCode.SHINY, objectiveName));

        int defendingId = oldTeam.getId();
        TeamMessage defendingMessage = new TeamMessage(defendingId, new UnlocalizedMessage(MessageId.OBJECTIVE_LOST, SiegeColorCode.ATTACKED, newTeam.getChatColor(), newTeam.getName(), SiegeColorCode.SHINY, objectiveName));

        UnlocalizedMessage neutral = new UnlocalizedMessage(MessageId.NEUTRAL_OBJECTIVE_CHANGED_OWNERSHIP, SiegeColorCode.NEUTRAL, SiegeColorCode.SHINY, newTeam.getName(), objectiveName);

        ObjectiveMessage message = new ObjectiveMessage(defendingMessage, capturingMessage, neutral);
        this.messageTeams(message);
    }

    private void messageTeams(ObjectiveMessage message) {
        this.game.sendObjectiveMessage(message);
    }
}
