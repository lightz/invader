package ca.lightz.invasion.game.objective.flag;

import ca.lightz.invasion.game.objective.control.Control;
import ca.lightz.invasion.game.indicator.DualColorWoolIndicator;
import ca.lightz.invasion.game.team.Team;
import org.bukkit.DyeColor;

import java.util.Map;

public class Flag {
    private final Map<Integer, DualColorWoolIndicator> phases;
    private DyeColor primary;
    private DyeColor secondary;

    private int lastPercentage;

    public Flag(Map<Integer, DualColorWoolIndicator> phases) {
        this.phases = phases;
    }

    public void setState(Control control) {
        Team team = control.getControllingTeam();
        this.setColor(team.getPrimaryColor(), team.getSecondaryColor());
        this.setCaptureStage(control.getPercentage());
    }

    private void setColor(DyeColor primary, DyeColor secondary) {
        if (this.primary == primary && this.secondary == secondary) {
            return;
        }

        this.primary = primary;
        this.secondary = secondary;
        for (DualColorWoolIndicator phase : this.phases.values()) {
            phase.setColors(this.primary, this.secondary);
        }
    }

    private void setCaptureStage(int percent) {
        if (this.lastPercentage == percent) {
            return;
        }

        for (Map.Entry<Integer, DualColorWoolIndicator> entry : this.phases.entrySet()) {
            if (entry.getKey() > percent) {
                entry.getValue().remove();
            } else {
                entry.getValue().generate(this.primary, this.secondary);
            }
        }
        this.lastPercentage = percent;
    }
}
