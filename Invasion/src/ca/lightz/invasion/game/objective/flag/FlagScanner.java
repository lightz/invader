package ca.lightz.invasion.game.objective.flag;

import ca.lightz.invasion.game.indicator.DualColorWoolIndicator;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.zone.Zone;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.ShulkerBox;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class FlagScanner {
    private static final Map<Material, Integer> PHASES = new HashMap<>();
    private static final Map<Material, Boolean> PRIMARY = new HashMap<>();

    static {
        PHASES.put(Material.WHITE_SHULKER_BOX, 25);
        PHASES.put(Material.YELLOW_SHULKER_BOX, 25);
        PHASES.put(Material.ORANGE_SHULKER_BOX, 50);
        PHASES.put(Material.LIME_SHULKER_BOX, 50);
        PHASES.put(Material.MAGENTA_SHULKER_BOX, 75);
        PHASES.put(Material.PINK_SHULKER_BOX, 75);
        PHASES.put(Material.LIGHT_BLUE_SHULKER_BOX, 100);
        PHASES.put(Material.GRAY_SHULKER_BOX, 100);

        PRIMARY.put(Material.WHITE_SHULKER_BOX, true);
        PRIMARY.put(Material.YELLOW_SHULKER_BOX, false);
        PRIMARY.put(Material.ORANGE_SHULKER_BOX, true);
        PRIMARY.put(Material.LIME_SHULKER_BOX, false);
        PRIMARY.put(Material.MAGENTA_SHULKER_BOX, true);
        PRIMARY.put(Material.PINK_SHULKER_BOX, false);
        PRIMARY.put(Material.LIGHT_BLUE_SHULKER_BOX, true);
        PRIMARY.put(Material.GRAY_SHULKER_BOX, false);
    }

    public Flag scan(Zone zone) {
        Map<Integer, DualColorWoolIndicator> phases = this.generateEmpty();

        for (Block block : zone) {
            if (!(block.getState() instanceof ShulkerBox)) {
                continue;
            }
            Material material = block.getType();
            Integer phase = PHASES.get(material);
            if (phase == null) {
                continue;
            }

            block.setType(Material.AIR);

            WorldPosition position = new WorldPosition(block.getLocation());
            if (PRIMARY.get(material)) {
                phases.get(phase).addPrimary(position);
            } else {
                phases.get(phase).addSecondary(position);
            }
        }

        return new Flag(phases);
    }

    private Map<Integer, DualColorWoolIndicator> generateEmpty() {
        Map<Integer, DualColorWoolIndicator> phases = new LinkedHashMap<>();
        for (int i = 25; i <= 100; i += 25) {
            phases.put(i, new DualColorWoolIndicator());
        }
        return phases;
    }
}
