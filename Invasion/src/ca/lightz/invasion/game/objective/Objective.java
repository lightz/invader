package ca.lightz.invasion.game.objective;

import ca.lightz.invasion.game.hologram.InfoHologram;
import ca.lightz.invasion.game.indicator.WoolIndicatorContainer;
import ca.lightz.invasion.game.objective.control.Control;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.spawn.container.SpawnList;
import ca.lightz.invasion.game.spawn.container.TeamSpawn;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamPlayerContainer;
import ca.lightz.invasion.game.util.block.BlockChanger;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.zone.FlagZone;
import ca.lightz.invasion.game.zone.Zone;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;

public class Objective {
    private final String name;
    private final Control control;
    private final Zone zone;
    private final TeamSpawn spawnManager;
    private final FlagZone flagZone;

    private WoolIndicatorContainer woolIndicators;
    private InfoHologram infoHologram;

    public Objective(String name, Control control, TeamSpawn spawn, Zone zone, FlagZone flagZone) {
        this.name = name;
        this.control = control;
        this.spawnManager = spawn;
        this.zone = zone;
        this.flagZone = flagZone;
        this.woolIndicators = new WoolIndicatorContainer();
    }

    public void setVisual(WoolIndicatorContainer linkedWoolMap) {
        this.woolIndicators.addAll(linkedWoolMap);
        this.flagZone.setFlagState(this.control);
        this.drawFloorOutlines();
        if (!this.isLockedOnStart()) {
            this.infoHologram = new InfoHologram(this.zone.getCenteredLocation().add(0, 2.5, 0));
            this.updateHologram();
        }
    }

    private void drawFloorOutlines() {
        if (this.isLocked()) {
            return;
        }

        for (Block block : this.zone.getFloorOutlines()) {
            BlockChanger.setBlock(block, Material.WHITE_WOOL);
            WorldPosition position = new WorldPosition(block.getLocation());
            this.woolIndicators.add(position);
        }
    }

    private void updateHologram() {
        if (this.infoHologram == null) {
            return;
        }
        UnlocalizedMessage objectiveName = new UnlocalizedMessage(MessageId.OBJECTIVE_HOLOGRAM_NAME, this.control.getControllingTeam().getChatColor(), this.name);
        if (this.isLocked()) {
            this.infoHologram.setText(objectiveName);
        } else {
            this.infoHologram.setText(objectiveName, new UnlocalizedMessage(MessageId.OBJECTIVE_HOLOGRAM_INFO, SiegeColorCode.NEUTRAL, this.control.getCurrent(), this.control.getMaximum()));
        }
    }

    public void captureTick(Collection<TeamPlayerContainer> teamsPlayers) {
        if (this.isLocked()) {
            return;
        }

        Collection<TeamPlayerContainer> inRangeTeams = this.removeOutOfRangePlayers(teamsPlayers);
        this.control.captureTick(inRangeTeams);
        this.refreshLinkedBlocks();
        this.updateHologram();
    }

    private Collection<TeamPlayerContainer> removeOutOfRangePlayers(Collection<TeamPlayerContainer> teamsPlayers) {
        Collection<TeamPlayerContainer> inRangeTeams = new ArrayList<>(teamsPlayers.size());
        for (TeamPlayerContainer teamPlayerContainer : teamsPlayers) {
            TeamPlayerContainer team = this.removeOutOfRangePlayers(teamPlayerContainer);
            if (!team.isEmpty()) {
                inRangeTeams.add(team);
            }
        }
        return inRangeTeams;
    }

    private TeamPlayerContainer removeOutOfRangePlayers(TeamPlayerContainer teamPlayers) {
        TeamPlayerContainer teamContainer = new TeamPlayerContainer(teamPlayers.getTeam());

        for (PlayerHolder playerHolder : teamPlayers) {
            if (playerHolder.siegePlayer.isInSpawnRoom() || this.cannotCapture(playerHolder.player)) {
                continue;
            }
            if (this.isInRange(playerHolder.player)) {
                teamContainer.add(playerHolder);
            }
        }

        return teamContainer;
    }

    private boolean cannotCapture(Player player) {
        return player.getGameMode() == GameMode.CREATIVE || player.getGameMode() == GameMode.SPECTATOR;
    }

    private boolean isInRange(Player player) {
        Location headLocation = player.getLocation();
        Location legsLocation = player.getEyeLocation();

        return this.zone.isAnyInside(headLocation, legsLocation);
    }

    private void refreshLinkedBlocks() {
        DyeColor woolColor = this.control.getControllingTeam().getPrimaryColor();
        this.woolIndicators.setColor(woolColor);
        this.flagZone.setFlagState(this.control);
    }

    private boolean isLocked() {
        return this.control.isLocked();
    }

    public boolean spawn(PlayerHolder playerHolder) {
        Team team = playerHolder.siegePlayer.getTeam();
        if (!this.control.isControlling(team)) {
            Messenger.send(playerHolder, MessageId.ERROR_CANNOT_SPAWN_HERE, SiegeColorCode.ERROR);
            return false;
        }

        if (!this.canSpawn()) {
            Messenger.send(playerHolder, MessageId.ERROR_LOOSING_SPAWN, SiegeColorCode.ERROR);
            return false;
        }

        this.spawnManager.spawn(playerHolder);
        Messenger.send(playerHolder, MessageId.SPAWNED_AT, SiegeColorCode.SPAWN, SiegeColorCode.SHINY, this.name);
        return true;
    }

    public boolean canSpawn() {
        return this.control.canSpawn();
    }

    public SpawnList getAllSpawns() {
        return this.spawnManager.getSpawns();
    }

    public void reset() {
        this.woolIndicators = new WoolIndicatorContainer();
        this.control.reset();
        this.flagZone.reset();
        if (this.infoHologram != null) {
            this.infoHologram.hide();
            this.infoHologram = null;
        }
    }

    public String getName() {
        return this.name;
    }

    public Team getControllingTeam() {
        return this.control.getControllingTeam();
    }

    public void takeControl(Team team) {
        this.control.changeOwnership(team);
        refreshLinkedBlocks();
    }

    public boolean isLockedOnStart() {
        return this.control.isLockedOnStart();
    }
}
