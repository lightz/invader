package ca.lightz.invasion.game.objective.event;

import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.team.Team;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ObjectiveAttackEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private final Objective objective;
    private final Team capturingTeam;
    private final Team defendingTeam;
    private final int current;
    private final int maximum;

    public ObjectiveAttackEvent(Objective objective, int current, int maximum, Team capturingTeam, Team defendingTeam) {
        this.objective = objective;
        this.capturingTeam = capturingTeam;
        this.defendingTeam = defendingTeam;
        this.current = current;
        this.maximum = maximum;
    }

    public Objective getObjective() {
        return this.objective;
    }

    public Team getCapturingTeam() {
        return this.capturingTeam;
    }

    public Team getDefendingTeam() {
        return this.defendingTeam;
    }

    public int getCurrent() {
        return this.current;
    }

    public int getMaximum() {
        return this.maximum;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
