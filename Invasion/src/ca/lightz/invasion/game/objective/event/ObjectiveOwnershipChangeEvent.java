package ca.lightz.invasion.game.objective.event;

import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.team.Team;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ObjectiveOwnershipChangeEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private final Objective objective;
    private final Team newTeam;
    private final Team oldTeam;

    public ObjectiveOwnershipChangeEvent(Objective objective, Team newTeam, Team oldTeam) {
        this.objective = objective;
        this.newTeam = newTeam;
        this.oldTeam = oldTeam;
    }

    public Objective getObjective() {
        return this.objective;
    }

    public Team getNewTeam() {
        return this.newTeam;
    }

    public Team getOldTeam() {
        return this.oldTeam;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
