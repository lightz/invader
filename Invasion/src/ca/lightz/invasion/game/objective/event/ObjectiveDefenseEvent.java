package ca.lightz.invasion.game.objective.event;

import ca.lightz.invasion.game.objective.Objective;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ObjectiveDefenseEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private final Objective objective;

    public ObjectiveDefenseEvent(Objective objective) {
        this.objective = objective;
    }

    public Objective getObjective() {
        return this.objective;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
