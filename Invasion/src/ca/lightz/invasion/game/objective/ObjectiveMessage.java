package ca.lightz.invasion.game.objective;

import ca.lightz.invasion.game.team.components.TeamMessage;
import ca.lightz.invasion.localization.UnlocalizedMessage;

public class ObjectiveMessage {
    public final TeamMessage defendingTeam;
    public final TeamMessage attackingTeam;
    public final UnlocalizedMessage otherTeams;

    public ObjectiveMessage(TeamMessage defendingTeam, TeamMessage attackingTeam, UnlocalizedMessage otherTeams) {
        this.defendingTeam = defendingTeam;
        this.attackingTeam = attackingTeam;
        this.otherTeams = otherTeams;
    }
}
