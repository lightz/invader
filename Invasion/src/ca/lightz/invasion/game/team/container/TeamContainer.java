package ca.lightz.invasion.game.team.container;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.team.Team;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class TeamContainer implements Iterable<Team> {
    private final Map<Integer, Team> teams = new HashMap<>(3, 1);
    private int numberPlayerTeams = 0;

    public void addPlayer(PlayerHolder playerHolder) {
        Team team = this.getLeastPopulatedTeam();
        team.addPlayer(playerHolder);
    }

    private Team getLeastPopulatedTeam() {
        if (this.teams.isEmpty()) {
            throw new RuntimeException("No team found!");
        }

        List<Team> leastPopulatedTeams = new ArrayList<>();
        int smallest = Integer.MAX_VALUE;
        for (Team team : this.teams.values()) {
            if (team.isNeutral()) {
                continue;
            }
            int size = team.size();
            if (size < smallest) {
                smallest = size;
                leastPopulatedTeams.clear();
                leastPopulatedTeams.add(team);
            } else if (size == smallest) {
                leastPopulatedTeams.add(team);
            }
        }

        Collections.shuffle(leastPopulatedTeams);
        return leastPopulatedTeams.get(0);
    }

    public Stream<Team> stream() {
        return StreamSupport.stream(this.spliterator(), false);
    }

    @Override
    @Nonnull
    public Iterator<Team> iterator() {
        return this.teams.values().iterator();
    }

    @Override
    public Spliterator<Team> spliterator() {
        return this.teams.values().spliterator();
    }

    public void add(Team team) {
        this.teams.put(team.getId(), team);
        if (!team.isNeutral()) {
            this.numberPlayerTeams++;
        }
    }

    public Team get(int teamId) {
        return this.teams.get(teamId);
    }

    public Team get(String name) {
        for (Team team : teams.values()) {
            if (team.getName().equalsIgnoreCase(name)) {
                return team;
            }
        }
        return null;
    }

    public Team getSmallestTeamExcept(int exceptTeamId) {
        if (this.teams.isEmpty()) {
            throw new RuntimeException("No team found!");
        }

        List<Team> leastPopulatedTeams = new ArrayList<>();
        int smallest = Integer.MAX_VALUE;
        for (Team team : this.teams.values()) {
            if (team.isNeutral() || team.getId() == exceptTeamId) {
                continue;
            }
            int size = team.size();
            if (size < smallest) {
                smallest = size;
                leastPopulatedTeams.clear();
                leastPopulatedTeams.add(team);
            } else if (size == smallest) {
                leastPopulatedTeams.add(team);
            }
        }

        return leastPopulatedTeams.get(0);
    }

    public int getNumberPlayerTeams() {
        return this.numberPlayerTeams;
    }
}
