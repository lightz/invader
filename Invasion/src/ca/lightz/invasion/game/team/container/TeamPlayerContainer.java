package ca.lightz.invasion.game.team.container;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.team.Team;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class TeamPlayerContainer implements Iterable<PlayerHolder> {
    private final Team team;
    private final Map<UUID, PlayerHolder> players = new HashMap<>();

    public TeamPlayerContainer(Team team) {
        this.team = team;
    }

    public void add(PlayerHolder playerHolder) {
        this.players.put(playerHolder.getUUID(), playerHolder);
    }

    @Override
    @Nonnull
    public Iterator<PlayerHolder> iterator() {
        return this.players.values().iterator();
    }

    public Team getTeam() {
        return this.team;
    }

    public boolean isEmpty() {
        return this.players.isEmpty();
    }

    public int size() {
        return this.players.size();
    }
}
