package ca.lightz.invasion.game.team.components;

import ca.lightz.invasion.localization.UnlocalizedMessage;

public class TeamMessage {
    public final int teamId;
    public final UnlocalizedMessage message;

    public TeamMessage(int teamId, UnlocalizedMessage message) {
        this.teamId = teamId;
        this.message = message;
    }
}
