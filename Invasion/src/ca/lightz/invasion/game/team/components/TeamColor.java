package ca.lightz.invasion.game.team.components;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

public class TeamColor {
    public final DyeColor primaryColor;
    public final DyeColor secondaryColor;
    public final ChatColor chatColor;

    public TeamColor(DyeColor primaryColor, DyeColor secondaryColor, ChatColor chatColor) {
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.chatColor = chatColor;
    }
}
