package ca.lightz.invasion.game.team;

import ca.lightz.invasion.game.hologram.InfoHologram;
import ca.lightz.invasion.game.hologram.TeamHologramContainer;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.container.SiegePlayerContainer;
import ca.lightz.invasion.game.team.components.TeamColor;
import ca.lightz.invasion.game.team.container.TeamPlayerContainer;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;

import java.util.Iterator;
import java.util.UUID;

public class Team {
    private final int id;
    private final String name;
    private final String acronym;
    private final TeamColor color;
    private final SiegePlayerContainer players = new SiegePlayerContainer();

    private final TeamHologramContainer holograms = new TeamHologramContainer();

    public Team(int teamId, String name, TeamColor teamColor) {
        this.id = teamId;
        this.name = name;
        this.acronym = this.name.substring(0, 1);
        this.color = teamColor;
    }

    public TeamPlayerContainer getActivePlayers() {
        TeamPlayerContainer teamPlayers = new TeamPlayerContainer(this);

        Iterator<SiegePlayer> iterator = this.players.iterator();
        while (iterator.hasNext()) {
            SiegePlayer siegePlayer = iterator.next();
            UUID uuid = siegePlayer.getUUID();
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                teamPlayers.add(new PlayerHolder(player, siegePlayer));
            } else {
                iterator.remove();
            }
        }

        return teamPlayers;
    }

    public int getId() {
        return this.id;
    }

    public int size() {
        return this.players.size();
    }

    public void addPlayer(PlayerHolder playerHolder) {
        this.players.add(playerHolder.siegePlayer);
        playerHolder.siegePlayer.setTeam(this);

        this.setChatName(playerHolder.player);
        this.setWoolColor(playerHolder.player);
        this.holograms.addPlayer(playerHolder.player);
        Messenger.send(playerHolder, MessageId.JOINED_TEAM, SiegeColorCode.SUCCESS, this.color.chatColor, this.name);
    }

    public void addHologram(InfoHologram hologram) {
        this.holograms.add(hologram);
        for (SiegePlayer siegePlayer : this.players) {
            this.holograms.addPlayer(siegePlayer.getPlayer());
        }
    }

    public void setWoolColor(Player player) {
        Wool wool = new Wool(this.color.primaryColor);
        ItemStack woolHead = wool.toItemStack(1);
        player.getInventory().setHelmet(woolHead);
    }

    private void setChatName(Player player) {
        String playerName = ChatColor.stripColor(player.getName());
        player.setDisplayName(this.color.chatColor + playerName + this.getAcronym() + ChatColor.RESET);
    }

    private String getAcronym() {
        return " [" + this.acronym + "]";
    }

    public void removePlayer(PlayerHolder playerHolder) {
        this.players.remove(playerHolder.siegePlayer.getUUID());
        this.holograms.removePlayer(playerHolder.player);
        this.clearPlayerName(playerHolder.player);
    }

    private void clearPlayerName(Player player) {
        if (player == null) {
            return;
        }
        String displayName = player.getDisplayName();
        displayName = displayName.replace(this.getAcronym(), "");
        player.setDisplayName(displayName);
    }

    public boolean isNeutral() {
        return this.id == 0;
    }

    public void clear() {
        this.players.clear();
        this.holograms.clear();
    }

    public String getName() {
        return this.name;
    }

    public void send(UnlocalizedMessage message) {
        for (SiegePlayer siegePlayer : this.players) {
            Player player = siegePlayer.getPlayer();
            if (player != null) {
                Messenger.send(player, message);
            }
        }
    }

    public ChatColor getChatColor() {
        return this.color.chatColor;
    }

    public DyeColor getPrimaryColor() {
        return this.color.primaryColor;
    }

    public DyeColor getSecondaryColor() {
        return this.color.secondaryColor;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Team)) {
            return false;
        }

        return this.getId() == ((Team) other).getId();
    }

    @Override
    public int hashCode() {
        // two randomly chosen prime numbers
        return new HashCodeBuilder(7, 31)
                .append(this.id)
                .toHashCode();
    }
}
