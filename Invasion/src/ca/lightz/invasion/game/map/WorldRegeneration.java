package ca.lightz.invasion.game.map;

import ca.lightz.invasion.core.Log;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class WorldRegeneration {
    private static final String BACK_UP_NAME = "_backup";

    public static void regen(String worldName) {
        WorldRegeneration.copyWorld(worldName + BACK_UP_NAME, worldName);
        World world = loadWorld(worldName); //Then we load the world so we can play in it
        cleanEntities(world, LivingEntity.class);
        cleanEntities(world, Item.class);
        world.setDifficulty(Difficulty.EASY);
        Log.info("Regenerated " + world.getName());
    }

    private static void cleanEntities(World world, Class<? extends Entity> clazz) {
        for (Entity entity : world.getEntitiesByClass(clazz)) {
            if (entity instanceof Player) {
                continue;
            }
            entity.remove();
        }
    }

    private static void copyWorld(String worldToCopy, String copyToHere) {
        // The world to copy
        File sourceFolder = new File(Bukkit.getWorldContainer(), worldToCopy);

        // The world to overwrite when copying
        unloadWorld(copyToHere); //We make sure the world is unloaded
        File targetFolder = new File(Bukkit.getWorldContainer(), copyToHere);

        copyWorld(sourceFolder, targetFolder);
    }

    private static void copyWorld(File source, File target) {
        Set<String> ignore = new HashSet<>(Arrays.asList("uid.dat", "session.dat"));
        if (ignore.contains(source.getName())) {
            return;
        }

        try {
            if (source.isDirectory()) {
                if (!target.exists()) {
                    if (!target.mkdirs()) {
                        throw new RuntimeException("Couldn't make directory " + target.getName() + " to copy the map.");
                    }
                }

                String files[] = source.list();
                if (files != null) {
                    for (String file : files) {
                        File srcFile = new File(source, file);
                        File destFile = new File(target, file);
                        copyWorld(srcFile, destFile);
                    }
                }
            } else {
                InputStream in = new FileInputStream(source);
                OutputStream out = new FileOutputStream(target);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in.close();
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void unloadWorld(String world) {
        Bukkit.getServer().unloadWorld(world, false);
    }

    private static World loadWorld(String name) {
        World world = Bukkit.getServer().getWorld(name);
        if (world == null) {
            WorldCreator worldCreator = new WorldCreator(name);
            World createdWorld = Bukkit.getServer().createWorld(worldCreator);
            Log.info("Created " + createdWorld);
            return createdWorld;
        }
        Log.info("Loaded " + world.getName());
        return world;
    }

    public static void disableSaving(String worldName) {
        World world = Bukkit.getServer().getWorld(worldName);
        if (world != null) {
            world.setAutoSave(false);
        }
    }
}
