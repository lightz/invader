package ca.lightz.invasion.game.map;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.indicator.WoolMapScanner;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.objective.ObjectiveMessage;
import ca.lightz.invasion.game.objective.container.ObjectiveContainer;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.spawn.SpawnRoom;
import ca.lightz.invasion.game.structure.SiegeStructure;
import ca.lightz.invasion.game.structure.components.Destroyable;
import ca.lightz.invasion.game.structure.container.StructureContainer;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.game.team.container.TeamPlayerContainer;
import ca.lightz.invasion.game.util.Counter;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.zone.Zone;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.*;
import java.util.stream.Collectors;

public class SiegeMap {
    public static final int CAPTURE_TICK = 20;
    private static final int DEFAULT_TIME_DURATION_IN_SECONDS = 15 * 60;
    private static final int UPDATE_TIME_LEFT = 30;

    private final String name;
    private final String worldName;
    private final ObjectiveContainer objectives;
    private final StructureContainer structures;
    private final TeamContainer teams;
    private final SpawnRoom spawnRoom;

    private final Zone woolMapZone;
    private final WoolMapScanner woolMapScanner;

    private boolean isOver = false;
    private final Counter secondsCounter;

    public SiegeMap(String name, String worldName, SpawnRoom spawnRoom, Zone woolMapZone, ObjectiveContainer objectives, TeamContainer teams, WoolMapScanner mapScanner) {
        this.name = name;
        this.worldName = worldName;
        this.objectives = objectives;
        this.teams = teams;
        this.spawnRoom = spawnRoom;
        this.woolMapZone = woolMapZone;
        this.woolMapScanner = mapScanner;
        this.structures = new StructureContainer();
        this.secondsCounter = new Counter(DEFAULT_TIME_DURATION_IN_SECONDS);
    }

    public void startMap() {
        Log.info("Starting " + this.name);
        WorldRegeneration.regen(this.worldName);
        WorldRegeneration.disableSaving(this.worldName);
        this.secondsCounter.reset();
        this.resetObjectives();
        this.generateWoolMapLink();
        this.generateStructures();
        this.isOver = false;
    }

    private void resetObjectives() {
        this.objectives.resetAll();
    }

    private void generateWoolMapLink() {
        this.woolMapScanner.bindWool(this.woolMapZone, this.objectives);
    }

    private void generateStructures() {
        this.structures.generate();

        for (SiegeStructure siegeStructure : this.structures) {
            siegeStructure.setHolograms(this.teams);
        }
    }

    public void damageStructure(BlockBreakEvent event, PlayerHolder playerHolder) {
        Block block = event.getBlock();
        for (SiegeStructure structure : this.structures) {
            if (!(structure instanceof Destroyable)) {
                continue;
            }

            boolean isPartOfStructure = ((Destroyable) structure).breakBlock(block, playerHolder);
            if (isPartOfStructure) {
                event.setCancelled(true);
                return;
            }
        }
    }

    public void addPlayer(PlayerHolder playerHolder) {
        this.teams.addPlayer(playerHolder);
        this.respawn(playerHolder);
    }

    public void respawn(PlayerRespawnEvent event, PlayerHolder playerHolder) {
        WorldPosition position = this.spawnRoom.getPosition();
        event.setRespawnLocation(position.getLocation());
        this.respawn(playerHolder);
    }

    public void respawn(PlayerHolder playerHolder) {
        this.spawnRoom.spawn(playerHolder);
    }

    public void spawn(PlayerHolder playerHolder, String objectiveName) {
        boolean spawned = this.objectives.spawn(playerHolder, objectiveName);
        if (spawned) {
            this.structures.giveTools(playerHolder);
        }
    }

    public void updateTick(Game game) {
        this.captureTick();
        this.checkForWinner(game);
        if (!this.isOver()) {
            this.updateStructures();
        }
    }

    private void captureTick() {
        Collection<TeamPlayerContainer> teamsPlayers = this.getTeamsPlayers();
        this.objectives.captureTick(teamsPlayers);
    }

    public void send(ObjectiveMessage message) {
        if (message == null) {
            return;
        }
        for (Team team : this.teams) {
            int teamId = team.getId();
            if (teamId == message.attackingTeam.teamId) {
                team.send(message.attackingTeam.message);
            } else if (teamId == message.defendingTeam.teamId) {
                team.send(message.defendingTeam.message);
            } else {
                team.send(message.otherTeams);
            }
        }
    }

    private void updateStructures() {
        for (SiegeStructure structure : this.structures) {
            structure.update(this.teams);
        }
    }

    private Collection<TeamPlayerContainer> getTeamsPlayers() {
        return this.teams.stream()
                .map(Team::getActivePlayers)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private void checkForWinner(Game game) {
        boolean hasNoTimeLeft = this.secondsCounter.increment();
        if (hasNoTimeLeft || this.isOver()) {
            this.setOver();
            game.endGame();
        } else {
            this.sendTimeLeft(game);
        }
    }

    private void sendTimeLeft(Game game) {
        int timeLeft = this.secondsCounter.getMaximum() - this.secondsCounter.getValue();
        if (timeLeft % UPDATE_TIME_LEFT == 0) {
            game.sendTimeLeft(timeLeft);
        }
    }

    public void setOver() {
        this.isOver = true;
    }

    public boolean isOver() {
        return this.isOver || this.objectives.isTeamDominating(this.teams.getNumberPlayerTeams());
    }

    public String getName() {
        return this.name;
    }

    public void clear() {
        for (Team team : this.teams) {
            team.clear();
        }
    }

    public void addStructure(SiegeStructure structure) {
        this.structures.add(structure);
    }

    public void teleportOnJoin(Player player) {
        this.spawnRoom.sendTo(player);
    }

    public Objective getObjective(String objectiveName) {
        return this.objectives.getObjective(objectiveName);
    }

    public Iterator<Objective> getObjectives() {
        return this.objectives.iterator();
    }

    public Collection<UnlocalizedMessage> getEndingMessage() {
        Map<Team, Integer> objectivesControlled = this.getEndGameState();
        Collection<UnlocalizedMessage> messages = new ArrayList<>();

        if (objectivesControlled.size() == 1) {
            Team team = objectivesControlled.keySet().iterator().next();
            messages.add(new UnlocalizedMessage(MessageId.TEAM_DOMINATED, team.getChatColor(), team.getName(), SiegeColorCode.SUCCESS));
        } else {
            for (Map.Entry<Team, Integer> entry : objectivesControlled.entrySet()) {
                Team team = entry.getKey();
                messages.add(new UnlocalizedMessage(MessageId.TEAM_OBJECTIVE_SCORE, team.getChatColor(), team.getName(), SiegeColorCode.SUCCESS, SiegeColorCode.SHINY, entry.getValue()));
            }
        }

        return messages;
    }

    private Map<Team, Integer> getEndGameState() {
        Map<Team, Integer> objectivesControlled = new HashMap<>();

        for (Objective objective : this.objectives) {
            if (objective.isLockedOnStart()) {
                continue;
            }
            Team controllingTeam = objective.getControllingTeam();
            objectivesControlled.merge(controllingTeam, 1, Integer::sum);
        }

        return objectivesControlled;
    }

    public TeamContainer getTeams() {
        return this.teams;
    }

    public void unloadWorld() {
        WorldRegeneration.unloadWorld(this.worldName);
    }

    public int getCaptureTick() {
        return CAPTURE_TICK;
    }
}
