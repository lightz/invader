package ca.lightz.invasion.game.map;

import ca.lightz.invasion.game.Game;
import org.bukkit.scheduler.BukkitRunnable;

public class UpdateTick extends BukkitRunnable {
    private final Game game;
    private final SiegeMap map;

    public UpdateTick(Game game, SiegeMap map) {
        this.game = game;

        this.map = map;
        this.map.startMap();
    }

    @Override
    public void run() {
        if (this.map.isOver()) {
            this.cancel();
            return;
        }

        this.map.updateTick(this.game);
    }
}
