package ca.lightz.invasion.game.effect.particle;

import org.bukkit.Color;

public class RGBColor {
    private static final double SCALE = 255.0;
    private static final double ZERO_FIX = 0.000001;  //Bug where value cannot be 0 otherwise it freaks
    private int red;
    private int green;
    private int blue;

    public RGBColor(Color color) {
        this(color.getRed(), color.getGreen(), color.getBlue());
    }

    public RGBColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public RGBColor() {
        super();
    }

    public double getParticleRed() {
        if (this.red <= 0) {
            return ZERO_FIX;
        }
        return this.red / SCALE;
    }

    public double getParticleGreen() {
        if (this.green <= 0) {
            return ZERO_FIX;
        }
        return this.green / SCALE;
    }

    public double getParticleBlue() {
        if (this.blue <= 0) {
            return ZERO_FIX;
        }
        return this.blue / SCALE;
    }
}
