package ca.lightz.invasion.game.effect.particle;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.Collection;

public class ParticleSender {
    private static final int ZERO_COUNT_FOR_COLORED = 0;
    private static final double FULL_BRIGHTNESS = 1;

    public static void sendColoredParticle(Player player, Collection<Location> locations, RGBColor color) {
        for (Location location : locations) {
            player.spawnParticle(Particle.REDSTONE, location, ZERO_COUNT_FOR_COLORED, color.getParticleRed(), color.getParticleGreen(), color.getParticleBlue(), FULL_BRIGHTNESS);
        }
    }

    public static void spawnAtWithNoSpeed(Location at, Particle particle, double offset, int count) {
        World world = at.getWorld();
        world.spawnParticle(particle, at, count, offset, offset, offset, 0);
    }

    public static void spawnAt(Location at, Particle particle, int count, double offset, Object data) {
        World world = at.getWorld();
        world.spawnParticle(particle, at, count, offset, offset, offset, data);
    }
}
