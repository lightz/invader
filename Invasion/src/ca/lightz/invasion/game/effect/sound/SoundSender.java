package ca.lightz.invasion.game.effect.sound;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.World;

public class SoundSender {

    public static void playSound(Location location, Sound sound) {
        World world = location.getWorld();
        world.playSound(location, sound, SoundCategory.PLAYERS, 10, 1);
    }
}
