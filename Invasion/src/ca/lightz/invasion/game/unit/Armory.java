package ca.lightz.invasion.game.unit;

import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;

public class Armory {
    private static final int DAGGER_DAMAGE = 9;
    private static final int SHORT_SWORD_DAMAGE = 11;
    private static final int LONG_SWORD_DAMAGE = 13;

    private static final int AXE_DAMAGE = 9;
    private static final int BATTLE_AXE_DAMAGE = 10;
    private static final int HALBERD_DAMAGE = 12;

    private static final int MACE_DAMAGE = 10;
    private static final int HAMMER_DAMAGE = 11;

    private static final int CROSSBOW_DAMAGE = 14;
    private static final int LONG_BOW_DAMAGE = 15;

    public static final Set<Material> AXES;
    public static final Set<Material> SHOVELS;

    static {
        AXES = new HashSet<>();
        AXES.add(Material.WOODEN_AXE);
        AXES.add(Material.STONE_AXE);
        AXES.add(Material.IRON_AXE);
        AXES.add(Material.GOLDEN_AXE);
        AXES.add(Material.DIAMOND_AXE);

        SHOVELS = new HashSet<>();
        SHOVELS.add(Material.WOODEN_SHOVEL);
        SHOVELS.add(Material.STONE_SHOVEL);
        SHOVELS.add(Material.IRON_SHOVEL);
        SHOVELS.add(Material.GOLDEN_SHOVEL);
        SHOVELS.add(Material.DIAMOND_SHOVEL);
    }

    public static ItemStack getWeapon(WeaponType weapon) {
        switch (weapon) {
            case CROSSBOW:
                return ItemBuilder.buildWeapon(Material.CROSSBOW, "Crossbow", CROSSBOW_DAMAGE);
            case SHORT_SWORD:
                return ItemBuilder.buildWeapon(Material.STONE_SWORD, "Short Sword", SHORT_SWORD_DAMAGE);
            case LONG_BOW:
                return ItemBuilder.buildWeapon(Material.BOW, "Long Bow", LONG_BOW_DAMAGE);
            case BATTLE_AXE:
                return ItemBuilder.buildWeapon(Material.IRON_AXE, "Battle Axe", BATTLE_AXE_DAMAGE);
            case MACE:
                return ItemBuilder.buildWeapon(Material.IRON_SHOVEL, "Mace", MACE_DAMAGE);
            case HAMMER:
                return ItemBuilder.buildWeapon(Material.GOLDEN_SHOVEL, "Hammer", HAMMER_DAMAGE);
            case LONG_SWORD:
                return ItemBuilder.buildWeapon(Material.IRON_SWORD, "Long Sword", LONG_SWORD_DAMAGE);
            case HALBERD:
                return ItemBuilder.buildWeapon(Material.DIAMOND_AXE, "Halberd", HALBERD_DAMAGE);
            case AXE:
                return ItemBuilder.buildWeapon(Material.STONE_AXE, "Axe - Door Breaker", AXE_DAMAGE);
            default:
            case DAGGER:
                return ItemBuilder.buildWeapon(Material.WOODEN_SWORD, "Dagger", DAGGER_DAMAGE);
        }
    }
}
