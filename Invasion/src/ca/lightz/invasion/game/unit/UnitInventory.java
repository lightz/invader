package ca.lightz.invasion.game.unit;

import ca.lightz.invasion.game.util.inventory.InventoryUtil;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Collection;
import java.util.LinkedList;

public abstract class UnitInventory {
    protected static final int DEFAULT_STEAKS = 8;
    protected static final int DEFAULT_LADDERS = 4;

    private final ItemStack chest;
    private final ItemStack pants;
    private final ItemStack boots;
    private final ItemStack offHand;

    protected final Collection<ItemStack> inventory = new LinkedList<>();

    protected UnitInventory() {
        this(DEFAULT_STEAKS, DEFAULT_LADDERS);
    }

    protected UnitInventory(int numberSteaks, int numberLadders) {
        this.chest = this.getChest();
        this.pants = this.getPants();
        this.boots = this.getBoots();
        this.offHand = this.getOffHand();
        this.setInventory();

        if (numberSteaks > 0) {
            ItemStack steaks = ItemBuilder.build(Material.COOKED_BEEF, numberSteaks);
            this.inventory.add(steaks);
        }

        if (numberLadders > 0) {
            ItemStack ladders = ItemBuilder.build(Material.LADDER, numberLadders);
            this.inventory.add(ladders);
        }
    }

    protected abstract ItemStack getChest();

    protected abstract ItemStack getPants();

    protected abstract ItemStack getBoots();

    protected abstract ItemStack getOffHand();

    protected abstract void setInventory();

    public void setPlayerInventory(PlayerInventory playerInventory) {
        playerInventory.setItemInOffHand(this.offHand);
        playerInventory.setChestplate(this.chest);
        playerInventory.setLeggings(this.pants);
        playerInventory.setBoots(this.boots);

        this.inventory.forEach(playerInventory::addItem);
    }

    protected boolean refill(PlayerInventory playerInventory) {
        boolean changedItem = false;

        for (ItemStack item : this.inventory) {
            if (this.add(item, playerInventory)) {
                changedItem = true;
                break;
            }
        }
        if (!changedItem && this.offHand != null) {
            changedItem = this.add(this.offHand, playerInventory);
        }

        return changedItem;
    }

    private boolean add(ItemStack itemStack, PlayerInventory inventory) {
        int currentAmount = InventoryUtil.count(inventory, itemStack.getType());
        int difference = itemStack.getAmount() - currentAmount;
        if (difference <= 0) {
            return false;
        }

        ItemStack newItem = itemStack.clone();
        newItem.setAmount(difference);
        inventory.addItem(newItem);
        return true;
    }
}
