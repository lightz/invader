package ca.lightz.invasion.game.unit.arbalist;

import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ArbalistInventory extends UnitInventory {
    private static final int ARMOR = 7;
    private static final int ARROWS = 24;

    public ArbalistInventory() {
        super();
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildArmor(Material.CHAINMAIL_CHESTPLATE, ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildArmor(Material.CHAINMAIL_LEGGINGS, 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildArmor(Material.CHAINMAIL_BOOTS, 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return ItemBuilder.build(Material.ARROW, ARROWS);
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.CROSSBOW));
        this.inventory.add(Armory.getWeapon(WeaponType.DAGGER));
    }
}
