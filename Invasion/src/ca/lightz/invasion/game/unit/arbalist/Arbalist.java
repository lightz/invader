package ca.lightz.invasion.game.unit.arbalist;

import ca.lightz.invasion.game.listener.combat.DamageWrapper;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Arbalist extends Unit {
    public static final int CHAIN_MAIL_EXTRA_DAMAGE = 2;

    public Arbalist(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new ArbalistInventory(), arrowCache, PerkType.ARBALIST);
    }

    @Override
    public void onDealDamage(DamageWrapper damageWrapper, Cancellable event) {
        if (!damageWrapper.isProjectile) {
            return;
        }
        if (this.isWearingChainMail(damageWrapper.defender)) {
            damageWrapper.damage += CHAIN_MAIL_EXTRA_DAMAGE;
        }
    }

    private boolean isWearingChainMail(Entity defender) {
        if (defender instanceof InventoryHolder) {
            Inventory inventory = ((InventoryHolder) defender).getInventory();
            if (inventory instanceof PlayerInventory) {
                return this.containsChainMail(((PlayerInventory) inventory).getArmorContents());
            }
        }
        return false;
    }

    private boolean containsChainMail(ItemStack[] items) {
        for (ItemStack item : items) {
            switch (item.getType()) {
                case CHAINMAIL_BOOTS:
                case CHAINMAIL_CHESTPLATE:
                case CHAINMAIL_LEGGINGS:
                case CHAINMAIL_HELMET:
                    return true;
                default:
                    break;
            }
        }
        return false;
    }

    @Override
    public String getName() {
        return "Arbalist";
    }
}
