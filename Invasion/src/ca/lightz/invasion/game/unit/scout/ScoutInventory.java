package ca.lightz.invasion.game.unit.scout;

import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ScoutInventory extends UnitInventory {
    private static final int ARMOR = 6;

    private static final int LADDER = (int) (DEFAULT_LADDERS * 1.5);

    private static final int RGB_GREEN_COLOR = 4943898;
    private static final int RGB_BASIC_COLOR = 10511680;

    public ScoutInventory() {
        super(DEFAULT_STEAKS, LADDER);
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_CHESTPLATE, Color.fromRGB(RGB_GREEN_COLOR), ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_LEGGINGS, Color.fromRGB(RGB_BASIC_COLOR), 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_BOOTS, Color.fromRGB(RGB_GREEN_COLOR), 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return null;
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.LONG_SWORD));
    }
}
