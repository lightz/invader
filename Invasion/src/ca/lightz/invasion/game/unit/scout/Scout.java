package ca.lightz.invasion.game.unit.scout;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.potion.PotionEffectType;

public class Scout extends Unit {
    private static final int SPEED_LEVEL = 0;

    public Scout(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new ScoutInventory(), arrowCache);
    }

    @Override
    public String getName() {
        return "Scout";
    }

    @Override
    protected void applyPermanentPotion(Player player) {
        super.applyPermanentPotion(player);
        PotionUtil.applyPotion(player, PotionEffectType.SPEED, SPEED_LEVEL, Integer.MAX_VALUE);
    }
}
