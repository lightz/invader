package ca.lightz.invasion.game.unit.soldier;

import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class SoldierInventory extends UnitInventory {
    private static final int ARMOR = 8;

    public SoldierInventory() {
        super();
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildArmor(Material.IRON_CHESTPLATE, ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildArmor(Material.IRON_LEGGINGS, 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildArmor(Material.IRON_BOOTS, 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return null;
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.LONG_SWORD));
    }
}
