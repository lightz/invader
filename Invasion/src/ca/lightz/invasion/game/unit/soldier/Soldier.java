package ca.lightz.invasion.game.unit.soldier;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Projectile;

public class Soldier extends Unit {

    public Soldier(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new SoldierInventory(), arrowCache);
    }

    @Override
    public String getName() {
        return "Soldier";
    }
}
