package ca.lightz.invasion.game.unit;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.arbalist.Arbalist;
import ca.lightz.invasion.game.unit.archer.Archer;
import ca.lightz.invasion.game.unit.berserker.Berserker;
import ca.lightz.invasion.game.unit.defender.Defender;
import ca.lightz.invasion.game.unit.fieldengineer.FieldEngineer;
import ca.lightz.invasion.game.unit.flamewarden.FlameWarden;
import ca.lightz.invasion.game.unit.marksman.Marksman;
import ca.lightz.invasion.game.unit.scout.Scout;
import ca.lightz.invasion.game.unit.sentinel.Sentinel;
import ca.lightz.invasion.game.unit.soldier.Soldier;
import ca.lightz.invasion.game.unit.swordmaster.Swordmaster;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.entity.Projectile;

public class UnitFactory {
    private final DamagingEntityCache<Projectile> arrowCache;

    public UnitFactory(DamagingEntityCache<Projectile> arrowCache) {
        this.arrowCache = arrowCache;
    }

    public void setUnit(SiegePlayer siegePlayer, String unitName) {
        Unit unit = this.getUnit(siegePlayer, unitName);
        siegePlayer.setUnit(unit);
    }

    private Unit getUnit(SiegePlayer player, String unitName) {
        switch (unitName.toUpperCase()) {
            case "ARCHER":
                return new Archer(player, this.arrowCache);
            case "BERSERKER":
                return new Berserker(player, this.arrowCache);
            case "SCOUT":
                return new Scout(player, this.arrowCache);
            case "ARBALIST":
                return new Arbalist(player, this.arrowCache);
            case "FLAME WARDEN":
                return new FlameWarden(player, this.arrowCache);
            case "FIELD ENGINEER":
                return new FieldEngineer(player, this.arrowCache);
            case "MARKSMAN":
                return new Marksman(player, this.arrowCache);
            case "SENTINEL":
                return new Sentinel(player, this.arrowCache);
            case "DEFENDER":
                return new Defender(player, this.arrowCache);
            case "SWORDMASTER":
                return new Swordmaster(player, this.arrowCache);
            case "SWORDSMAN":
            default:
                return new Soldier(player, this.arrowCache);
        }
    }

    public UnlocalizedMessage getDescription(String unitName) {
        switch (unitName.toUpperCase()) {
            case "ARCHER":
                return new UnlocalizedMessage(MessageId.ARCHER_DESCRIPTION, SiegeColorCode.DESCRIPTION);
            case "BERSERKER":
                return new UnlocalizedMessage(MessageId.BERSERK_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.BUFF, Berserker.HP_FOR_EXTRA_DAMAGE, Berserker.EXTRA_DAMAGE);
            case "SCOUT":
                return new UnlocalizedMessage(MessageId.SCOUT_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.BUFF);
            case "ARBALIST":
                return new UnlocalizedMessage(MessageId.ARBALIST_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.BUFF, Arbalist.CHAIN_MAIL_EXTRA_DAMAGE);
            case "FLAME WARDEN":
                return new UnlocalizedMessage(MessageId.FLAME_WARDEN_DESCRIPTION, SiegeColorCode.DESCRIPTION);
            case "FIELD ENGINEER":
                return new UnlocalizedMessage(MessageId.FIELD_ENGINEER_DESCRIPTION, SiegeColorCode.DESCRIPTION);
            case "MARKSMAN":
                return new UnlocalizedMessage(MessageId.MASKSMAN_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.BUFF, Marksman.EXTRA_ARROWS);
            case "SENTINEL":
                return new UnlocalizedMessage(MessageId.SENTINEL_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.DEBUFF);
            case "DEFENDER":
                return new UnlocalizedMessage(MessageId.DEFENDER_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.BUFF, Defender.POTION_DURATION_SECONDS);
            case "SWORDMASTER":
                return new UnlocalizedMessage(MessageId.SWORDMASTER_DESCRIPTION, SiegeColorCode.DESCRIPTION, SiegeColorCode.BUFF, Swordmaster.DODGE_BUFF_DURATION_SECONDS, Swordmaster.MINIMUM_FOOD_TO_DODGE, Swordmaster.FOOD_TO_DODGE);
            case "SWORDSMAN":
            default:
                return new UnlocalizedMessage(MessageId.SWORDSMAN_DESCRIPTION, SiegeColorCode.DESCRIPTION);
        }
    }
}
