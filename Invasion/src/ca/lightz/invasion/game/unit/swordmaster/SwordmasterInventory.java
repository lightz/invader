package ca.lightz.invasion.game.unit.swordmaster;

import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class SwordmasterInventory extends UnitInventory {
    private static final int ARMOR = 6;
    private static final int RGB_COLOR = 8421504;
    private static final int STEAKS = (int) (DEFAULT_STEAKS * 1.5);
    private static final int LADDER = (int) (DEFAULT_LADDERS * 0.5);

    public SwordmasterInventory() {
        super(STEAKS, LADDER);
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_CHESTPLATE, Color.fromRGB(RGB_COLOR), ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildArmor(Material.IRON_LEGGINGS, 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_BOOTS, Color.fromRGB(RGB_COLOR), 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return null;
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.LONG_SWORD));
    }
}
