package ca.lightz.invasion.game.unit.swordmaster;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class Swordmaster extends Unit {
    public static final int DODGE_BUFF_DURATION_SECONDS = 2;
    public static final int MINIMUM_FOOD_TO_DODGE = 15;
    public static final int FOOD_TO_DODGE = 3;
    private static final Set<Action> DODGE_ACTIONS = new HashSet<>();
    private static final int VELOCITY_BOOST_DODGE = 3;

    static {
        DODGE_ACTIONS.add(Action.RIGHT_CLICK_AIR);
        DODGE_ACTIONS.add(Action.RIGHT_CLICK_BLOCK);
    }

    public Swordmaster(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new SwordmasterInventory(), arrowCache, PerkType.SWORDMASTER);
    }

    @Override
    public void interact(PlayerInteractEvent event) {
        if (!DODGE_ACTIONS.contains(event.getAction()) || this.getOwner().isInSpawnRoom()) {
            return;
        }
        Player player = event.getPlayer();
        if (!this.canDodge(player)) {
            return;
        }

        this.reduceFood(player);
        this.applyDodgeVelocity(player);
        PotionUtil.applyDodge(player, (DODGE_BUFF_DURATION_SECONDS * 20));
    }

    private void applyDodgeVelocity(Player player) {
        Vector currentVelocity = player.getVelocity();
        Vector direction = player.getLocation().getDirection().multiply(VELOCITY_BOOST_DODGE);
        Vector newSpeed = new Vector(direction.getX(), currentVelocity.getY(), direction.getZ());
        player.setVelocity(newSpeed);
    }

    private void reduceFood(Player player) {
        int newFoodLevel = Math.max(player.getFoodLevel() - FOOD_TO_DODGE, 0);
        player.setFoodLevel(newFoodLevel);
    }

    private boolean canDodge(Player player) {
        return player.getFoodLevel() >= MINIMUM_FOOD_TO_DODGE && player.isOnGround() && this.isHoldingSword(player);
    }

    private boolean isHoldingSword(Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();
        return item != null && item.getType() == Material.IRON_SWORD;
    }

    @Override
    public String getName() {
        return "Swordmaster";
    }
}
