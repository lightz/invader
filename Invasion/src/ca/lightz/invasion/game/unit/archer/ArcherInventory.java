package ca.lightz.invasion.game.unit.archer;

import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ArcherInventory extends UnitInventory {
    private static final int ARMOR = 6;

    private static final int RGB_COLOR = 10184011;
    private static final int ARROWS = 32;

    public ArcherInventory() {
        super();
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_CHESTPLATE, Color.fromRGB(RGB_COLOR), ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_LEGGINGS, Color.fromRGB(RGB_COLOR), 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_BOOTS, Color.fromRGB(RGB_COLOR), 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return ItemBuilder.build(Material.ARROW, ARROWS);
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.LONG_BOW));
        this.inventory.add(Armory.getWeapon(WeaponType.SHORT_SWORD));
    }
}
