package ca.lightz.invasion.game.unit.archer;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import org.bukkit.entity.Projectile;

public class Archer extends Unit {

    public Archer(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new ArcherInventory(), arrowCache);
    }

    @Override
    public String getName() {
        return "Archer";
    }
}
