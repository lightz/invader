package ca.lightz.invasion.game.unit.defender;

import ca.lightz.invasion.game.listener.combat.DamageWrapper;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Defender extends Unit {
    private static final int POTION_LEVEL = 2;
    public static final int POTION_DURATION_SECONDS = 2;

    public Defender(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new DefenderInventory(), arrowCache, PerkType.DEFENDER);
    }

    @Override
    public void onDealDamage(DamageWrapper damageWrapper, Cancellable event) {
        Player attacking = damageWrapper.damager;
        ItemStack mainHandItem = attacking.getInventory().getItemInMainHand();
        if (mainHandItem.getType() != Material.IRON_SHOVEL) {
            return;
        }

        Entity defender = damageWrapper.defender;
        PotionUtil.applyPotion(defender, PotionEffectType.SLOW, POTION_LEVEL, (POTION_DURATION_SECONDS * 20));
    }

    @Override
    public String getName() {
        return "Defender";
    }
}
