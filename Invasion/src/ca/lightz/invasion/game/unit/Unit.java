package ca.lightz.invasion.game.unit;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.listener.combat.DamageWrapper;
import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.flamewarden.firepit.FirePitContainer;
import ca.lightz.invasion.game.util.item.SiegeAttributeExtractor;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

public abstract class Unit {
    private final DamagingEntityCache<Projectile> arrowCache;
    private final UnitInventory inventory;
    private final SiegePlayer owner;
    private final PerkType neededPerk;

    protected Unit(SiegePlayer owner, UnitInventory inventory, DamagingEntityCache<Projectile> arrowCache) {
        this(owner, inventory, arrowCache, PerkType.NONE);
    }

    protected Unit(SiegePlayer owner, UnitInventory inventory, DamagingEntityCache<Projectile> arrowCache, PerkType neededPerk) {
        this.owner = owner;
        this.inventory = inventory;
        this.arrowCache = arrowCache;
        this.neededPerk = neededPerk;
    }

    public void onBowShoot(EntityShootBowEvent event) {
        Player player = (Player) event.getEntity();
        ItemStack bow = player.getInventory().getItemInMainHand();
        this.removeEnchant(bow, Material.BOW, Enchantment.ARROW_FIRE);

        Entity projectile = event.getProjectile();
        if (projectile instanceof Projectile) {
            this.addArrowToCache((Projectile) projectile, player);
        } else {
            Log.error("Shot an Entity which wasn't a projectile");
        }
    }

    protected void addArrowToCache(Projectile arrow, Player player) {
        int damage = SiegeAttributeExtractor.getDamage(player.getInventory().getItemInMainHand());
        this.arrowCache.addProjectile(arrow, damage);
    }

    private void removeEnchant(ItemStack itemStack, Material material, Enchantment enchantment) {
        if (itemStack == null || itemStack.getType() != material) {
            return;
        }

        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta != null) {
            itemMeta.removeEnchant(enchantment);
            itemStack.setItemMeta(itemMeta);
        }
    }

    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        switch (block.getType()) {
            case CAULDRON:
                Player player = event.getPlayer();
                PlayerHolder playerHolder = new PlayerHolder(player, this.getOwner());
                FirePitContainer.getInstance().breakFirePit(event.getBlock(), playerHolder);
                break;
            case LADDER:
                this.breakLadder(block, event.getPlayer());
                break;
        }
    }

    protected void breakLadder(Block block, Player player) {
    }

    public void onBlockPlace(BlockPlaceEvent event) {
    }

    protected SiegePlayer getOwner() {
        return this.owner;
    }

    public UnitInventory getInventory() {
        return this.inventory;
    }

    public void interact(PlayerInteractEvent event) {
    }

    public void onDealDamage(DamageWrapper damageWrapper, Cancellable event) {
    }

    public void setPlayerInventory(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();

        this.applyPermanentPotion(player);
        this.inventory.setPlayerInventory(playerInventory);
    }

    public void refillInventory(Player player) {
        boolean refilledSomething = this.inventory.refill(player.getInventory());
        if (refilledSomething) {
            Messenger.send(player, MessageId.REFILL_INVENTORY, SiegeColorCode.SUCCESS);
        }
    }

    protected void applyPermanentPotion(Player player) {
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
    }

    public abstract String getName();

    public PerkType getNeededPerk() {
        return this.neededPerk;
    }
}
