package ca.lightz.invasion.game.unit.berserker;

import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BerserkerInventory extends UnitInventory {
    private static final int ARMOR = 5;

    public BerserkerInventory() {
        super();
    }

    @Override
    protected ItemStack getChest() {
        return null;
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildArmor(Material.LEATHER_LEGGINGS, ARMOR);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildArmor(Material.LEATHER_BOOTS, 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return null;
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.BATTLE_AXE));
    }
}
