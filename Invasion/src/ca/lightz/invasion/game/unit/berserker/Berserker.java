package ca.lightz.invasion.game.unit.berserker;

import ca.lightz.invasion.game.listener.combat.DamageWrapper;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;

public class Berserker extends Unit {
    public static final double HP_FOR_EXTRA_DAMAGE = 3;
    public static final int EXTRA_DAMAGE = 1;

    public Berserker(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new BerserkerInventory(), arrowCache, PerkType.BERSERKER);
    }

    @Override
    public void onDealDamage(DamageWrapper damageWrapper, Cancellable event) {
        Player damager = damageWrapper.damager;
        double missingHealth = damager.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() - damager.getHealth();
        int extraDamage = ((int) Math.floor(missingHealth / HP_FOR_EXTRA_DAMAGE)) * EXTRA_DAMAGE;

        damageWrapper.damage += extraDamage;
    }

    @Override
    public String getName() {
        return "Berserker";
    }
}
