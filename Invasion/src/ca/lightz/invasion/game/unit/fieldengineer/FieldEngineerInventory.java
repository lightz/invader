package ca.lightz.invasion.game.unit.fieldengineer;

import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class FieldEngineerInventory extends UnitInventory {
    private static final int ARMOR = 7;


    private static final int RGB_COLOR = 2570988;

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_CHESTPLATE, Color.fromRGB(RGB_COLOR), ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_LEGGINGS, Color.fromRGB(RGB_COLOR), 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_BOOTS, Color.fromRGB(RGB_COLOR), 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return null;
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.HAMMER));
    }
}
