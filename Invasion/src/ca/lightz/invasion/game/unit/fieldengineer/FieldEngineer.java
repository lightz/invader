package ca.lightz.invasion.game.unit.fieldengineer;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.util.block.BlockChanger;
import ca.lightz.invasion.game.util.block.BlockUtil;
import ca.lightz.invasion.game.util.inventory.InventoryUtil;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.type.Ladder;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;

public class FieldEngineer extends Unit {

    public FieldEngineer(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new FieldEngineerInventory(), arrowCache, PerkType.FIELD_ENGINEER);
    }

    @Override
    public void onBlockPlace(BlockPlaceEvent event) {
        super.onBlockPlace(event);
        if (event.getBlockPlaced().getType() != Material.LADDER) {
            return;
        }

        Player player = event.getPlayer();
        this.placeAllLadders(player, event.getBlockPlaced(), event.getBlockAgainst());
    }

    private void placeAllLadders(Player player, Block ladderBlock, Block support) {
        Ladder startingLadderData = (Ladder) ladderBlock.getBlockData();
        BlockFace attachedFace = startingLadderData.getFacing();

        int numberLadderPlaced = 1;
        Block currentBlock = ladderBlock.getRelative(BlockFace.UP);
        Block attachedBlock = support.getRelative(BlockFace.UP);

        PlayerInventory inventory = player.getInventory();

        while (currentBlock.getType() == Material.AIR && BlockUtil.isValidLadderSupport(attachedFace, attachedBlock) && this.removeExtraLadder(inventory)) {
            BlockChanger.setBlock(currentBlock, Material.LADDER);
            BlockState state = currentBlock.getState();
            state.setBlockData(startingLadderData);
            state.update();

            currentBlock = currentBlock.getRelative(BlockFace.UP);
            attachedBlock = attachedBlock.getRelative(BlockFace.UP);

            numberLadderPlaced++;
        }

        if (numberLadderPlaced > 1) {
            Messenger.send(player, MessageId.FIELD_LADDER_PLACED, SiegeColorCode.INFO, numberLadderPlaced);
            player.updateInventory();
        }
    }

    private boolean removeExtraLadder(PlayerInventory playerInventory) {
        return InventoryUtil.remove(playerInventory, Material.LADDER);
    }

    @Override
    protected void breakLadder(Block ladder, Player player) {
        ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() != Material.GOLDEN_SHOVEL) {
            return;
        }

        player.getInventory().addItem(new ItemStack(Material.LADDER));
        player.updateInventory();
    }

    @Override
    public String getName() {
        return "Field Engineer";
    }

    @Override
    protected void applyPermanentPotion(Player player) {
        super.applyPermanentPotion(player);
        PotionUtil.applyPotion(player, PotionEffectType.SLOW_DIGGING, 1, Integer.MAX_VALUE);
    }
}
