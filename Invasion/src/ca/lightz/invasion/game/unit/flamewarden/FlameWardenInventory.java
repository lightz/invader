package ca.lightz.invasion.game.unit.flamewarden;

import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import ca.lightz.invasion.localization.MessageId;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Iterator;

public class FlameWardenInventory extends UnitInventory {
    private static final int ARMOR = 6;

    private static final int RGB_COLOR_RED = 16720942;
    private static final int RGB_COLOR_ORANGE = 15493400;
    private static final int ARROWS = 32;

    public FlameWardenInventory() {
        super();
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_CHESTPLATE, Color.fromRGB(RGB_COLOR_RED), ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_LEGGINGS, Color.fromRGB(RGB_COLOR_ORANGE), 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildLeatherArmor(Material.LEATHER_BOOTS, Color.fromRGB(RGB_COLOR_RED), 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return ItemBuilder.build(Material.ARROW, ARROWS);
    }

    @Override
    protected void setInventory() {
        this.inventory.add(Armory.getWeapon(WeaponType.LONG_BOW));
        this.inventory.add(Armory.getWeapon(WeaponType.DAGGER));
        this.inventory.add(this.getFirePit());
    }

    protected boolean refill(PlayerInventory playerInventory, boolean giveFirePit) {
        if (giveFirePit) {
            return this.refill(playerInventory);
        }

        ItemStack removedFirePit = this.removeFirePit();
        boolean result = this.refill(playerInventory);
        this.inventory.add(removedFirePit);
        return result;
    }

    private ItemStack removeFirePit() {
        Iterator<ItemStack> iterator = this.inventory.iterator();
        while (iterator.hasNext()) {
            ItemStack itemStack = iterator.next();
            if (itemStack.getType() == Material.CAULDRON) {
                iterator.remove();
                return itemStack;
            }
        }
        return null;
    }

    private ItemStack getFirePit() {
        ItemStack firePit = ItemBuilder.build(Material.CAULDRON, 1, "Fire Pit");
        return ItemBuilder.addLore(firePit, MessageId.FIRE_PIT_DESCRIPTION);
    }

    public void giveFirePit(Player player) {
        player.getInventory().addItem(this.getFirePit());
        player.updateInventory();
    }
}
