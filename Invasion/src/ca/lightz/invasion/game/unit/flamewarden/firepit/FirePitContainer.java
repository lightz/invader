package ca.lightz.invasion.game.unit.flamewarden.firepit;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.unit.flamewarden.FlameWarden;
import ca.lightz.invasion.game.util.block.BlockChanger;
import ca.lightz.invasion.game.util.position.WorldPosition;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FirePitContainer {
    private static final int MAX_DISTANCE_SQUARED_TO_GET_FIRE_PIT = 25;
    private static FirePitContainer instance;
    private final Map<SiegePlayer, WorldPosition> firePits = new HashMap<>();
    private final Map<WorldPosition, SiegePlayer> playerFirePits = new HashMap<>();
    private final Map<WorldPosition, UUID> linkedSpiders = new HashMap<>();

    public static FirePitContainer getInstance() {
        if (instance == null) {
            instance = new FirePitContainer();
        }
        return instance;
    }

    public void clear() {
        this.playerFirePits.keySet().forEach(this::removeEffect);
        this.firePits.clear();
        this.playerFirePits.clear();
    }

    public void buildFirePit(SiegePlayer owner, Block block) {
        Location location = block.getLocation();
        WorldPosition position = new WorldPosition(location);
        this.firePits.put(owner, position);
        this.playerFirePits.put(position, owner);
        this.createFireEffect(location, position);
    }

    private void createFireEffect(Location location, WorldPosition position) {
        World world = location.getWorld();
        Entity entity = world.spawnEntity(location.add(0.5, 0.5, 0.5), EntityType.CAVE_SPIDER);
        entity.setSilent(true);
        entity.setInvulnerable(true);
        entity.setFireTicks(Integer.MAX_VALUE);
        LivingEntity livingEntity = (LivingEntity) entity;
        livingEntity.setAI(false);
        livingEntity.setCollidable(false);
        livingEntity.setRemoveWhenFarAway(false);
        PotionUtil.applyPotion(livingEntity, PotionEffectType.INVISIBILITY, 1, Integer.MAX_VALUE);
        this.linkedSpiders.put(position, entity.getUniqueId());
    }

    public void interactWithFirePit(Block block, PlayerHolder playerHolder) {
        if (!this.isFirePit(block)) {
            return;
        }
        WorldPosition position = new WorldPosition(block.getLocation());
        SiegePlayer owner = this.playerFirePits.get(position);

        if (playerHolder.siegePlayer.inSameTeam(owner)) {
            this.sameTeamInteract(block, playerHolder);
        } else {
            this.removeFirePit(owner);
        }
    }

    private void sameTeamInteract(Block block, PlayerHolder playerHolder) {
        if (playerHolder.player.isSneaking()) {
            this.breakFirePit(block, playerHolder);
        } else {
            this.giveFlame(playerHolder);
        }
    }

    private void giveFlame(PlayerHolder playerHolder) {
        ItemStack itemInHand = playerHolder.player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() != Material.BOW || itemInHand.containsEnchantment(Enchantment.ARROW_FIRE)) {
            return;
        }

        itemInHand.addEnchantment(Enchantment.ARROW_FIRE, 1);
    }

    public void breakFirePit(Block block, PlayerHolder playerHolder) {
        if (!this.isFirePit(block)) {
            return;
        }
        Location location = block.getLocation();
        WorldPosition position = new WorldPosition(location);
        SiegePlayer owner = playerFirePits.get(position);

        if (playerHolder.siegePlayer.inSameTeam(owner)) {
            this.giveFirePitBack(location, owner);
        }

        this.removeFirePit(owner);
    }

    private void giveFirePitBack(Location location, SiegePlayer owner) {
        Player player = owner.getPlayer();
        double distance = player.getLocation().distanceSquared(location);
        if (distance > MAX_DISTANCE_SQUARED_TO_GET_FIRE_PIT) {
            return;
        }

        FlameWarden unit = (FlameWarden) owner.getUnit();
        unit.giveFirePit();
    }

    public boolean isFirePit(Block block) {
        WorldPosition position = new WorldPosition(block.getLocation());
        return this.playerFirePits.containsKey(position);
    }

    public boolean playerHasFirePit(SiegePlayer owner) {
        return this.firePits.containsKey(owner);
    }

    public void removeFirePit(SiegePlayer owner) {
        WorldPosition position = firePits.remove(owner);
        playerFirePits.remove(position);
        if (position != null && position.getBlock().getType() == Material.CAULDRON) {
            BlockChanger.setBlock(position.getBlock(), Material.AIR);
            this.removeEffect(position);
        }
    }

    public void destroyFirePit(Block block) {
        if (block.getType() != Material.CAULDRON) {
            return;
        }

        Location location = block.getLocation();
        WorldPosition position = new WorldPosition(location);
        SiegePlayer owner = this.playerFirePits.remove(position);
        this.firePits.remove(owner);
        this.removeEffect(position);
    }

    private void removeEffect(WorldPosition position) {
        if (position == null) {
            return;
        }
        UUID entityUUID = this.linkedSpiders.remove(position);
        position.getLocation().getWorld().getLivingEntities().stream().filter(entity -> entity.getUniqueId().equals(entityUUID)).forEach(Entity::remove);
    }
}
