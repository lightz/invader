package ca.lightz.invasion.game.unit.flamewarden;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.flamewarden.firepit.FirePitContainer;
import ca.lightz.invasion.game.util.block.BlockUtil;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.BlockPlaceEvent;

public class FlameWarden extends Unit {

    public FlameWarden(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new FlameWardenInventory(), arrowCache);
    }

    @Override
    public void onBlockPlace(BlockPlaceEvent event) {
        super.onBlockPlace(event);
        if (event.getBlockPlaced().getType() != Material.CAULDRON) {
            return;
        }
        if (this.isInvalidFirePitPlacement(event)) {
            event.setCancelled(true);
            return;
        }

        this.placeFirePit(event.getBlock());
    }

    private boolean isInvalidFirePitPlacement(BlockPlaceEvent event) {
        Block cauldronSupport = event.getBlockPlaced().getRelative(BlockFace.DOWN);
        if (!BlockUtil.isValidFloorSupport(cauldronSupport)) {
            Messenger.send(event.getPlayer(), MessageId.ERROR_FIRE_PIT_NOT_SUPPORTED, SiegeColorCode.ERROR);
            return true;  //event cancelled
        }

        return false;
    }

    private void placeFirePit(Block block) {
        FirePitContainer.getInstance().buildFirePit(this.getOwner(), block);
    }

    public void giveFirePit() {
        UnitInventory inventory = this.getInventory();
        FlameWardenInventory flameWardenInventory = (FlameWardenInventory) inventory;
        Player player = this.getOwner().getPlayer();
        flameWardenInventory.giveFirePit(player);
    }

    @Override
    public void refillInventory(Player player) {
        boolean firePitPlaced = FirePitContainer.getInstance().playerHasFirePit(this.getOwner());

        FlameWardenInventory flameWardenInventory = (FlameWardenInventory) this.getInventory();
        boolean refilledSomething = flameWardenInventory.refill(player.getInventory(), !firePitPlaced);
        if (refilledSomething) {
            Messenger.send(player, MessageId.REFILL_INVENTORY, SiegeColorCode.SUCCESS);
        }
    }

    @Override
    public String getName() {
        return "Flame Warden";
    }
}
