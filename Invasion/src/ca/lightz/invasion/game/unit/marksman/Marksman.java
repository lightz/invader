package ca.lightz.invasion.game.unit.marksman;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.util.inventory.InventoryUtil;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

public class Marksman extends Unit {
    public static final int EXTRA_ARROWS = 2;
    private static final double MINIMUM_FORCE_FOR_ABILITY = 1.0;
    private static final float VELOCITY_DROP = 0.2f;
    private static final double VELOCITY_SPREAD = 0.25D;

    public Marksman(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new MarksmanInventory(), arrowCache, PerkType.MARKSMAN);
    }

    @Override
    public void onBowShoot(EntityShootBowEvent event) {
        super.onBowShoot(event);
        if (!(event.getProjectile() instanceof Arrow) || event.getForce() < MINIMUM_FORCE_FOR_ABILITY) {
            return;
        }

        Player player = (Player) event.getEntity();
        Arrow arrow = (Arrow) event.getProjectile();

        boolean shotExtraArrow;
        if (player.isSneaking()) {
            shotExtraArrow = this.secondaryFiringAbility(player, arrow);
        } else {
            shotExtraArrow = this.primaryFiringAbility(player, arrow);
        }

        if (shotExtraArrow) {
            player.updateInventory();
        }
    }

    private boolean secondaryFiringAbility(Player player, Arrow arrow) {
        Vector velocity = arrow.getVelocity();
        boolean shotExtraArrow = false;

        for (int i = -1; i <= 1; i += 2) {
            if (!removeExtraArrow(player.getInventory())) {
                break;
            }

            Vector newVelocity = new Vector(velocity.getX() + (VELOCITY_SPREAD * i), velocity.getY(), velocity.getZ() + (VELOCITY_SPREAD * i));
            this.shoot(player, arrow, newVelocity);
            shotExtraArrow = true;
        }
        return shotExtraArrow;
    }

    private boolean primaryFiringAbility(Player player, Arrow arrow) {
        Vector velocity = arrow.getVelocity();
        boolean shotExtraArrow = false;

        for (int i = 1; i <= EXTRA_ARROWS; ++i) {
            if (!removeExtraArrow(player.getInventory())) {
                break;
            }

            double velocityDrop = 1 - (VELOCITY_DROP * i);
            double y = velocity.getY() * velocityDrop;
            Vector newVelocity = new Vector(velocity.getX(), y, velocity.getZ());
            this.shoot(player, arrow, newVelocity);
            shotExtraArrow = true;
        }
        return shotExtraArrow;
    }

    private void shoot(Player player, Arrow toCopy, Vector newVelocity) {
        Arrow arrowShot = player.launchProjectile(Arrow.class, newVelocity);
        arrowShot.setKnockbackStrength(arrowShot.getKnockbackStrength());
        arrowShot.setFallDistance(toCopy.getFallDistance());
        arrowShot.setFireTicks(toCopy.getFireTicks());
        this.addArrowToCache(arrowShot, player);
    }

    private boolean removeExtraArrow(PlayerInventory playerInventory) {
        return InventoryUtil.remove(playerInventory, Material.ARROW);
    }

    @Override
    public String getName() {
        return "Marksman";
    }
}
