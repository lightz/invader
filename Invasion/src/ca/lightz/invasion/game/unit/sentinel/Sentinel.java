package ca.lightz.invasion.game.unit.sentinel;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.perk.PerkType;
import ca.lightz.invasion.game.projectile.DamagingEntityCache;
import ca.lightz.invasion.game.unit.Unit;
import ca.lightz.invasion.game.util.potion.PotionUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Sentinel extends Unit {
    private static final int SLOW_DIGGING_LEVEL = 0;

    public Sentinel(SiegePlayer owner, DamagingEntityCache<Projectile> arrowCache) {
        super(owner, new SentinelInventory(), arrowCache, PerkType.SENTINEL);
    }

    @Override
    protected void breakLadder(Block ladder, Player player) {
        ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand.getType() != Material.DIAMOND_AXE) {
            return;
        }

        while (ladder.getRelative(BlockFace.UP).getType() == Material.LADDER) {
            ladder = ladder.getRelative(BlockFace.UP);
            ladder.breakNaturally();
        }
    }

    @Override
    public String getName() {
        return "Sentinel";
    }

    @Override
    protected void applyPermanentPotion(Player player) {
        super.applyPermanentPotion(player);
        PotionUtil.applyPotion(player, PotionEffectType.SLOW_DIGGING, SLOW_DIGGING_LEVEL, Integer.MAX_VALUE);
    }
}
