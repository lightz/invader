package ca.lightz.invasion.game.unit.sentinel;

import ca.lightz.invasion.game.unit.UnitInventory;
import ca.lightz.invasion.game.unit.Armory;
import ca.lightz.invasion.game.util.item.ItemBuilder;
import ca.lightz.invasion.game.util.item.WeaponType;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class SentinelInventory extends UnitInventory {
    private static final int ARMOR = 7;

    public SentinelInventory() {
        super();
    }

    @Override
    protected ItemStack getChest() {
        return ItemBuilder.buildArmor(Material.IRON_CHESTPLATE, ARMOR);
    }

    @Override
    protected ItemStack getPants() {
        return ItemBuilder.buildArmor(Material.DIAMOND_LEGGINGS, 0);
    }

    @Override
    protected ItemStack getBoots() {
        return ItemBuilder.buildArmor(Material.IRON_BOOTS, 0);
    }

    @Override
    protected ItemStack getOffHand() {
        return null;
    }

    @Override
    protected void setInventory() {
        ItemStack weapon = Armory.getWeapon(WeaponType.HALBERD);
        ItemBuilder.addEnchant(weapon, Enchantment.KNOCKBACK, 1);
        this.inventory.add(weapon);
    }
}
