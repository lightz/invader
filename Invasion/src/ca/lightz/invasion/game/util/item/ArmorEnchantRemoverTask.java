package ca.lightz.invasion.game.util.item;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class ArmorEnchantRemoverTask extends BukkitRunnable {
    private final UUID playerUUID;
    private final Enchantment enchantment;

    public ArmorEnchantRemoverTask(UUID playerUUID, Enchantment enchantment) {
        this.playerUUID = playerUUID;
        this.enchantment = enchantment;
    }

    @Override
    public void run() {
        Player player = Bukkit.getPlayer(this.playerUUID);
        if (player == null) {
            return;
        }

        PlayerInventory inventory = player.getInventory();

        for (ItemStack armor : inventory.getArmorContents()) {
            ItemChanger.removeEnchant(armor, this.enchantment);
        }
        player.updateInventory();
    }
}
