package ca.lightz.invasion.game.util.item;

import ca.lightz.invasion.localization.Localization;
import ca.lightz.invasion.localization.MessageId;
import com.google.common.base.Strings;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemBuilder {
    private static final int MAX_CHARACTERS_PER_LINE = 45;
    private static Localization LOCALIZATION;

    public static void setLocalization(Localization localization) {
        LOCALIZATION = localization;
    }

    public static ItemStack build(Material material) {
        return build(material, 1, "");
    }

    public static ItemStack build(Material material, int amount) {
        return build(material, amount, "");
    }

    public static ItemStack build(Material material, int amount, String name) {
        ItemStack itemStack = new ItemStack(material, amount);

        ItemMeta meta = itemStack.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

        if (!Strings.isNullOrEmpty(name)) {
            meta.setDisplayName(name);
        }

        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public static ItemStack addLore(ItemStack itemStack, MessageId... loreId) {
        String[] lore = new String[loreId.length];
        int index = 0;
        for (MessageId id : loreId) {
            lore[index] = LOCALIZATION.localize(id);
            index++;
        }

        return addLore(itemStack, lore);
    }

    private static ItemStack addLore(ItemStack itemStack, String... loreText) {
        if (loreText.length == 0) {
            return itemStack;
        }

        ItemMeta meta = itemStack.getItemMeta();
        List<String> currentLore = meta.getLore() != null ? meta.getLore() : new ArrayList<>();

        for (String loreLine : loreText) {
            String[] formatted = toFormattedLore(loreLine);
            currentLore.addAll(Arrays.asList(formatted));
        }

        meta.setLore(currentLore);
        itemStack.setItemMeta(meta);

        return itemStack;
    }

    private static String[] toFormattedLore(String input) {
        if (input == null || input.isEmpty()) {
            return new String[0];
        }

        String wrappedText = WordUtils.wrap(input, MAX_CHARACTERS_PER_LINE);
        String[] lines = wrappedText.split(System.lineSeparator());

        for (int i = 1; i < lines.length; i++) {
            String lastColor = ChatColor.getLastColors(lines[i - 1]);
            lines[i] = lastColor + lines[i];
        }

        return lines;
    }

    public static ItemStack buildLeatherArmor(Material material, Color color, int armor) {
        ItemStack armorItem = buildArmor(material, armor);

        LeatherArmorMeta armorPieceMeta = (LeatherArmorMeta) armorItem.getItemMeta();
        armorPieceMeta.setColor(color);
        armorItem.setItemMeta(armorPieceMeta);

        return armorItem;
    }

    public static ItemStack buildArmor(Material material, int armor) {
        ItemStack stack = build(material, 1, "");
        stack = SiegeAttributeExtractor.addAttribute(stack, SiegeAttribute.ARMOR, armor);
        return stack;
    }

    public static ItemStack buildWeapon(Material material, String name, int damage) {
        ItemStack stack = build(material, 1, name);
        stack = SiegeAttributeExtractor.addAttribute(stack, SiegeAttribute.DAMAGE, damage);
        return stack;
    }

    public static ItemStack addEnchant(ItemStack item, Enchantment enchant, int level) {
        item.addUnsafeEnchantment(enchant, level);
        return item;
    }
}
