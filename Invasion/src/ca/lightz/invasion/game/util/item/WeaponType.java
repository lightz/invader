package ca.lightz.invasion.game.util.item;

public enum WeaponType {
    CROSSBOW, DAGGER, SHORT_SWORD, LONG_BOW, BATTLE_AXE, MACE, HAMMER, LONG_SWORD, HALBERD, AXE
}
