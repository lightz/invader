package ca.lightz.invasion.game.util.item;

public enum SiegeAttribute {
    ARMOR,
    DAMAGE
}
