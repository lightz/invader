package ca.lightz.invasion.game.util.item;

import ca.lightz.invasion.Invasion;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

public class ItemChanger {

    public static void addEnchant(ItemStack itemStack, Enchantment enchantment, int level) {
        if (itemStack != null) {
            itemStack.addUnsafeEnchantment(enchantment, level);
        }
    }

    public static void removeEnchant(ItemStack itemStack, Enchantment enchantment) {
        if (itemStack != null) {
            itemStack.removeEnchantment(enchantment);
        }
    }

    public static void addProtectionToArmor(Player player, int tickDuration) {
        PlayerInventory playerInventory = player.getInventory();
        for (ItemStack armor : playerInventory.getArmorContents()) {
            ItemChanger.addEnchant(armor, Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        }
        BukkitRunnable enchantRemover = new ArmorEnchantRemoverTask(player.getUniqueId(), Enchantment.PROTECTION_ENVIRONMENTAL);
        enchantRemover.runTaskLater(Invasion.getPlugin(), tickDuration);
        player.updateInventory();
    }
}
