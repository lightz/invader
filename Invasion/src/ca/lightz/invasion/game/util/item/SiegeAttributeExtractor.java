package ca.lightz.invasion.game.util.item;

import ca.lightz.invasion.core.Log;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SiegeAttributeExtractor {
    private static final String DAMAGE_LORE_KEY = "Damage : ";
    private static final String COLORED_DAMAGE_LORE_KEY = ChatColor.GRAY + DAMAGE_LORE_KEY + ChatColor.RED;
    private static final String ARMOR_LORE_KEY = "Armor : ";
    private static final String COLORED_ARMOR_LORE_KEY = ChatColor.GRAY + ARMOR_LORE_KEY + ChatColor.BLUE;

    public static int getDamage(ItemStack itemStack) {
        return getValue(itemStack, SiegeAttribute.DAMAGE);
    }

    public static int getArmor(ItemStack itemStack) {
        return getValue(itemStack, SiegeAttribute.ARMOR);
    }

    private static int getValue(ItemStack itemStack, SiegeAttribute attribute) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta == null) {
            return 0;
        }

        List<String> lore = itemMeta.getLore();
        if (lore == null) {
            return 0;
        }

        String loreKey = getLoreKey(attribute);
        for (String line : lore) {
            line = ChatColor.stripColor(line);
            if (line.contains(loreKey)) {
                String data = line.replace(loreKey, "");
                return Integer.parseInt(data);
            }
        }

        return 0;
    }

    public static ItemStack addAttribute(ItemStack itemStack, SiegeAttribute attribute, int value) {
        if (value > 0) {
            String loreKey = getColoredLore(attribute);
            String lore = loreKey + value;
            addLore(itemStack, lore);
        }
        return itemStack;
    }

    private static String getColoredLore(SiegeAttribute attribute) {
        switch (attribute) {
            case ARMOR:
                return COLORED_ARMOR_LORE_KEY;
            case DAMAGE:
                return COLORED_DAMAGE_LORE_KEY;
            default:
                return "";
        }
    }

    private static String getLoreKey(SiegeAttribute attribute) {
        switch (attribute) {
            case ARMOR:
                return ARMOR_LORE_KEY;
            case DAMAGE:
                return DAMAGE_LORE_KEY;
            default:
                return "";
        }
    }

    private static void addLore(ItemStack itemStack, String... lines) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta == null) {
            Log.error("No item meta on an item, cannot add lore to it.");
            return;
        }

        List<String> lore = itemMeta.getLore();
        if (lore == null) {
            itemMeta.setLore(Arrays.asList(lines));
        } else {
            Collections.addAll(lore, lines);
            itemMeta.setLore(lore);
        }

        itemStack.setItemMeta(itemMeta);
    }
}
