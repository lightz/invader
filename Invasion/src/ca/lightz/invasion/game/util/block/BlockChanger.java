package ca.lightz.invasion.game.util.block;

import ca.lightz.invasion.game.unit.flamewarden.firepit.FirePitContainer;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class BlockChanger {

    public static void setBlock(Block block, Material material) {
        FirePitContainer.getInstance().destroyFirePit(block);
        block.setType(material);
    }
}
