package ca.lightz.invasion.game.util.block;

import ca.lightz.invasion.Invasion;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.ShulkerBox;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;

import java.util.HashSet;
import java.util.Set;

public class BlockUtil {
    private static final Set<Material> VALID_FLOOR_OUTLINE = new HashSet<>();
    private static final Set<Material> VALID_LADDER_SUPPORT = new HashSet<>();
    private static final Set<Material> DISABLED_INTERACTIVE_BLOCK = new HashSet<>();
    private static final Set<Material> LIQUID = new HashSet<>();

    static {
        VALID_FLOOR_OUTLINE.add(Material.COBBLESTONE);
        VALID_FLOOR_OUTLINE.add(Material.INFESTED_COBBLESTONE);
        VALID_FLOOR_OUTLINE.add(Material.STONE);
        VALID_FLOOR_OUTLINE.add(Material.INFESTED_STONE);
        VALID_FLOOR_OUTLINE.add(Material.SMOOTH_STONE);

        VALID_FLOOR_OUTLINE.add(Material.GRASS_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.GRASS_PATH);
        VALID_FLOOR_OUTLINE.add(Material.COARSE_DIRT);
        VALID_FLOOR_OUTLINE.add(Material.FARMLAND);
        VALID_FLOOR_OUTLINE.add(Material.DIRT);
        VALID_FLOOR_OUTLINE.add(Material.GRAVEL);
        VALID_FLOOR_OUTLINE.add(Material.MYCELIUM);
        VALID_FLOOR_OUTLINE.add(Material.BROWN_MUSHROOM_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.RED_MUSHROOM_BLOCK);

        VALID_FLOOR_OUTLINE.add(Material.BONE_BLOCK);

        VALID_FLOOR_OUTLINE.add(Material.ACACIA_PLANKS);
        VALID_FLOOR_OUTLINE.add(Material.BIRCH_PLANKS);
        VALID_FLOOR_OUTLINE.add(Material.DARK_OAK_PLANKS);
        VALID_FLOOR_OUTLINE.add(Material.JUNGLE_PLANKS);
        VALID_FLOOR_OUTLINE.add(Material.OAK_PLANKS);
        VALID_FLOOR_OUTLINE.add(Material.SPRUCE_PLANKS);

        VALID_FLOOR_OUTLINE.addAll(Tag.LOGS.getValues());
        VALID_FLOOR_OUTLINE.addAll(Tag.WOOL.getValues());
        VALID_FLOOR_OUTLINE.addAll(Tag.STONE_BRICKS.getValues());
        VALID_FLOOR_OUTLINE.addAll(Tag.SAND.getValues());

        VALID_FLOOR_OUTLINE.add(Material.GOLD_ORE);
        VALID_FLOOR_OUTLINE.add(Material.GOLD_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.IRON_ORE);
        VALID_FLOOR_OUTLINE.add(Material.IRON_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.COAL_ORE);
        VALID_FLOOR_OUTLINE.add(Material.COAL_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.LAPIS_ORE);
        VALID_FLOOR_OUTLINE.add(Material.LAPIS_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.DIAMOND_ORE);
        VALID_FLOOR_OUTLINE.add(Material.DIAMOND_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.EMERALD_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.EMERALD_ORE);
        VALID_FLOOR_OUTLINE.add(Material.REDSTONE_ORE);
        VALID_FLOOR_OUTLINE.add(Material.REDSTONE_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.OBSIDIAN);

        VALID_FLOOR_OUTLINE.add(Material.NETHER_QUARTZ_ORE);
        VALID_FLOOR_OUTLINE.add(Material.QUARTZ_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.CHISELED_QUARTZ_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.SMOOTH_QUARTZ);
        VALID_FLOOR_OUTLINE.add(Material.QUARTZ_PILLAR);

        VALID_FLOOR_OUTLINE.add(Material.SANDSTONE);
        VALID_FLOOR_OUTLINE.add(Material.SMOOTH_SANDSTONE);
        VALID_FLOOR_OUTLINE.add(Material.CHISELED_SANDSTONE);
        VALID_FLOOR_OUTLINE.add(Material.CUT_SANDSTONE);

        VALID_FLOOR_OUTLINE.add(Material.RED_SANDSTONE);
        VALID_FLOOR_OUTLINE.add(Material.SMOOTH_RED_SANDSTONE);
        VALID_FLOOR_OUTLINE.add(Material.CHISELED_RED_SANDSTONE);
        VALID_FLOOR_OUTLINE.add(Material.CUT_RED_SANDSTONE);

        VALID_FLOOR_OUTLINE.add(Material.BRICK);
        VALID_FLOOR_OUTLINE.add(Material.MOSSY_COBBLESTONE);
        VALID_FLOOR_OUTLINE.add(Material.INFESTED_CHISELED_STONE_BRICKS);
        VALID_FLOOR_OUTLINE.add(Material.INFESTED_CRACKED_STONE_BRICKS);
        VALID_FLOOR_OUTLINE.add(Material.INFESTED_MOSSY_STONE_BRICKS);
        VALID_FLOOR_OUTLINE.add(Material.INFESTED_STONE_BRICKS);

        VALID_FLOOR_OUTLINE.add(Material.CLAY);
        VALID_FLOOR_OUTLINE.add(Material.TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.BLACK_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.BLACK_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.BLUE_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.BLUE_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.BROWN_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.BROWN_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.CYAN_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.CYAN_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.GRAY_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.GRAY_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.GREEN_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.GREEN_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.LIGHT_BLUE_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.LIGHT_BLUE_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.LIME_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.LIME_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.MAGENTA_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.MAGENTA_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.ORANGE_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.ORANGE_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.PINK_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.PINK_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.PURPLE_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.PURPLE_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.RED_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.RED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.WHITE_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.WHITE_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.YELLOW_GLAZED_TERRACOTTA);
        VALID_FLOOR_OUTLINE.add(Material.YELLOW_TERRACOTTA);

        VALID_FLOOR_OUTLINE.add(Material.NETHERRACK);
        VALID_FLOOR_OUTLINE.add(Material.SOUL_SAND);
        VALID_FLOOR_OUTLINE.add(Material.GLOWSTONE); //NO?

        VALID_FLOOR_OUTLINE.add(Material.NETHER_BRICK);
        VALID_FLOOR_OUTLINE.add(Material.NETHER_BRICKS); //Which one is the block?
        VALID_FLOOR_OUTLINE.add(Material.NETHER_WART_BLOCK);

        VALID_FLOOR_OUTLINE.add(Material.END_STONE);
        VALID_FLOOR_OUTLINE.add(Material.END_STONE_BRICKS);

        VALID_FLOOR_OUTLINE.add(Material.PRISMARINE);
        VALID_FLOOR_OUTLINE.add(Material.PRISMARINE_BRICKS);
        VALID_FLOOR_OUTLINE.add(Material.DARK_PRISMARINE);

        VALID_FLOOR_OUTLINE.add(Material.BLUE_ICE);
        VALID_FLOOR_OUTLINE.add(Material.PACKED_ICE);

        VALID_FLOOR_OUTLINE.add(Material.PURPUR_BLOCK);
        VALID_FLOOR_OUTLINE.add(Material.PURPUR_PILLAR);

        VALID_FLOOR_OUTLINE.add(Material.BOOKSHELF);

        ///////////////////////////////////////////////

        DISABLED_INTERACTIVE_BLOCK.add(Material.ANVIL);
        DISABLED_INTERACTIVE_BLOCK.add(Material.BEACON);

        //DISABLED_INTERACTIVE_BLOCK.addAll(Tag.BEDS.getValues());
        DISABLED_INTERACTIVE_BLOCK.add(Material.BLACK_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.BLUE_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.BROWN_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.CYAN_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.GRAY_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.GREEN_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.LIGHT_BLUE_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.LIGHT_GRAY_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.LIME_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.MAGENTA_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.ORANGE_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.PINK_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.PURPLE_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.RED_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.WHITE_BED);
        DISABLED_INTERACTIVE_BLOCK.add(Material.YELLOW_BED);

        DISABLED_INTERACTIVE_BLOCK.add(Material.BREWING_STAND);
        DISABLED_INTERACTIVE_BLOCK.add(Material.CAKE);
        DISABLED_INTERACTIVE_BLOCK.add(Material.CHEST);
        DISABLED_INTERACTIVE_BLOCK.add(Material.COMPARATOR);
        DISABLED_INTERACTIVE_BLOCK.add(Material.DISPENSER);
        DISABLED_INTERACTIVE_BLOCK.add(Material.DROPPER);
        DISABLED_INTERACTIVE_BLOCK.add(Material.ENCHANTING_TABLE);
        DISABLED_INTERACTIVE_BLOCK.add(Material.ENDER_CHEST);
        DISABLED_INTERACTIVE_BLOCK.add(Material.FLOWER_POT);
        DISABLED_INTERACTIVE_BLOCK.add(Material.FURNACE);
        DISABLED_INTERACTIVE_BLOCK.add(Material.HOPPER);
        DISABLED_INTERACTIVE_BLOCK.add(Material.JUKEBOX);
        DISABLED_INTERACTIVE_BLOCK.add(Material.OBSERVER);
        DISABLED_INTERACTIVE_BLOCK.add(Material.TRAPPED_CHEST);
        DISABLED_INTERACTIVE_BLOCK.add(Material.CRAFTING_TABLE);

        LIQUID.add(Material.WATER);
        LIQUID.add(Material.LAVA);
    }

    public static boolean isLiquid(BlockState block) {
        return LIQUID.contains(block.getType()) || (block.getBlockData() instanceof Waterlogged && ((Waterlogged) block.getBlockData()).isWaterlogged());
    }

    public static boolean isValidFloorSupport(Block floor) {
        Material material = floor.getType();
        return material.isSolid() && (VALID_FLOOR_OUTLINE.contains(material) || isUpsideDownStairs(floor) || isUpperSlab(floor));
    }

    public static boolean isValidFloorOutline(Block floor) {
        Material material = floor.getType();
        return material.isSolid() && VALID_FLOOR_OUTLINE.contains(material) || isDoubleSlab(floor);
    }

    private static boolean isDoubleSlab(Block block) {
        if (!Tag.SLABS.isTagged(block.getType())) {
            return false;
        }
        BlockData blockData = block.getBlockData();
        Slab slab = (Slab) blockData;

        return slab.getType() == Slab.Type.DOUBLE;
    }

    private static boolean isUpperSlab(Block block) {
        if (!Tag.SLABS.isTagged(block.getType())) {
            return false;
        }
        BlockData blockData = block.getBlockData();
        Slab slab = (Slab) blockData;

        return slab.getType() == Slab.Type.DOUBLE || slab.getType() == Slab.Type.TOP;
    }

    private static boolean isUpsideDownStairs(Block block) {
        if (!Tag.STAIRS.isTagged(block.getType())) {
            return false;
        }
        BlockData blockData = block.getBlockData();
        Stairs stairs = (Stairs) blockData;
        return stairs.getHalf() == Bisected.Half.TOP;
    }

    public static boolean isValidLadderSupport(BlockFace attachedFace, Block support) {
        Material material = support.getType();
        return material.isSolid() && (!VALID_LADDER_SUPPORT.contains(material) || isAttachingToStairsBackSide(attachedFace, support));
    }

    private static boolean isAttachingToStairsBackSide(BlockFace attachedFace, Block support) {
        BlockData blockData = support.getBlockData();
        if (!(blockData instanceof Stairs)) {
            return false;
        }
        Stairs stairs = (Stairs) blockData;
        return stairs.getFacing().getOppositeFace() == attachedFace; //TODO doesn't work with corner stair
    }

    public static boolean isDisabledInteractiveBlock(Block block) {
        return DISABLED_INTERACTIVE_BLOCK.contains(block.getType()) || isShulkerBox(block.getState());
    }

    private static boolean isShulkerBox(BlockState state) {
        return state instanceof ShulkerBox;
    }
}
