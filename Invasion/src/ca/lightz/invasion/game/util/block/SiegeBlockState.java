package ca.lightz.invasion.game.util.block;

import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;

public class SiegeBlockState {
    private final WorldPosition position;
    private final BlockData blockData;

    public SiegeBlockState(WorldPosition position, BlockData blockData) {
        this.position = position;
        this.blockData = blockData;
    }

    public void apply() {
        Block block = this.getBlock();
        block.setBlockData(this.blockData, true);
    }

    public void setAir() {
        Block block = this.getBlock();
        BlockChanger.setBlock(block, Material.AIR);
    }

    private Block getBlock() {
        return this.position.getBlock();
    }

    public WorldPosition getPosition() {
        return this.position;
    }

    public boolean isAt(WorldPosition position) {
        return this.position.isSameBlockPosition(position);
    }
}
