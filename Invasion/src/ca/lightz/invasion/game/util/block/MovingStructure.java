package ca.lightz.invasion.game.util.block;

import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;
import java.util.List;

public class MovingStructure {
    private List<WorldPosition> blocks;

    public MovingStructure() {
        this.blocks = new ArrayList<>();
    }

    protected void move(BlockFace direction) {
        List<WorldPosition> positions = new ArrayList<>();
        SiegeBlockStateList nextStates = new SiegeBlockStateList();

        for (WorldPosition position : this.blocks) {
            Block block = position.getBlock();

            Block target = block.getRelative(direction);
            WorldPosition targetPosition = new WorldPosition(target.getLocation());
            SiegeBlockState nextState = new SiegeBlockState(targetPosition, block.getBlockData());
            nextStates.add(nextState);

            BlockChanger.setBlock(block, Material.AIR);
            positions.add(targetPosition);
        }

        for (SiegeBlockState state : nextStates) {
            state.apply();
        }

        this.blocks = positions;
    }

    protected void reset() {
        for (WorldPosition position : this.blocks) {
            position.getBlock().setType(Material.AIR);
        }
        this.blocks.clear();
    }

    protected void add(Block block) {
        this.blocks.add(new WorldPosition(block.getLocation()));
    }

    public Location getCenteredLocation() {
        double x = 0;
        double y = 0;
        double z = 0;

        for (WorldPosition position : this.blocks) {
            x += position.getX();
            y += position.getY();
            z += position.getZ();
        }

        x = (x / this.blocks.size()) + 0.5;
        y = (y / this.blocks.size()) + 0.5;
        z = (z / this.blocks.size()) + 0.5;

        return new Location(this.blocks.get(0).getWorld(), x, y, z);
    }
}
