package ca.lightz.invasion.game.util.block;

import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Location;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SiegeBlockStateList implements Iterable<SiegeBlockState> {
    private final List<SiegeBlockState> positions;

    public SiegeBlockStateList() {
        this.positions = new ArrayList<>();
    }

    public void add(SiegeBlockState blockState) {
        this.positions.add(blockState);
    }

    public boolean contains(WorldPosition position) {
        for (SiegeBlockState blockState : this.positions) {
            if (blockState.isAt(position)) {
                return true;
            }
        }
        return false;
    }

    public Location getCenteredLocation() {
        double x = 0;
        double y = 0;
        double z = 0;

        for (SiegeBlockState state : this.positions) {
            WorldPosition position = state.getPosition();
            x += position.getX();
            y += position.getY();
            z += position.getZ();
        }

        x = (x / this.positions.size()) + 0.5;
        y = (y / this.positions.size()) + 0.5;
        z = (z / this.positions.size()) + 0.5;

        return new Location(this.positions.get(0).getPosition().getWorld(), x, y, z);
    }

    @Override
    @Nonnull
    public Iterator<SiegeBlockState> iterator() {
        return this.positions.iterator();
    }
}
