package ca.lightz.invasion.game.util.potion;

import ca.lightz.invasion.game.listener.combat.CombatEvent;
import ca.lightz.invasion.game.util.item.ItemChanger;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionUtil {

    public static void applyDodge(LivingEntity livingEntity, int tickDuration) {
        applyPotion(livingEntity, CombatEvent.DODGE_POTION_EFFECT, 1, tickDuration);
        if (livingEntity instanceof Player) {
            ItemChanger.addProtectionToArmor((Player) livingEntity, tickDuration);
        }
    }

    public static void applyPotion(Entity entity, PotionEffectType potionEffectType, int level, int duration) {
        if (entity instanceof LivingEntity) {
            applyPotion((LivingEntity) entity, potionEffectType, level, duration);
        }
    }

    public static void applyPotion(LivingEntity livingEntity, PotionEffectType potionEffectType, int level, int duration) {
        PotionEffect potionEffect = new PotionEffect(potionEffectType, duration, level, false, false);
        livingEntity.addPotionEffect(potionEffect);
    }
}
