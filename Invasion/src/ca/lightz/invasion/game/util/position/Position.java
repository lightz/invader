package ca.lightz.invasion.game.util.position;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Position {
    protected double x;
    protected double y;
    protected double z;

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public int getBlockX() {
        return this.floor(this.x);
    }

    public int getBlockY() {
        return this.floor(this.y);
    }

    public int getBlockZ() {
        return this.floor(this.z);
    }

    private int floor(double value) {
        return (int) Math.floor(value);
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Position)) {
            return false;
        }

        Position otherPosition = (Position) other;
        return new EqualsBuilder().
                append(this.x, otherPosition.x).
                append(this.y, otherPosition.y).
                append(this.z, otherPosition.z).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 19).
                append(this.x).
                append(this.y).
                append(this.z).
                toHashCode();
    }

    @Override
    public String toString() {
        return this.x + ":" + this.y + ":" + this.z;
    }
}
