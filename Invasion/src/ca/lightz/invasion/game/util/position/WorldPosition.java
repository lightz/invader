package ca.lightz.invasion.game.util.position;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class WorldPosition extends Position {
    private final String worldName;
    private float pitch;
    private float yaw;
    private Location location;

    public WorldPosition(WorldPosition position) {
        this(position.worldName, position.getX(), position.getY(), position.getZ(), position.pitch, position.yaw);
    }

    public WorldPosition(Location location) {
        this(location.getWorld() == null ? "" : location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
    }

    public WorldPosition(String worldName, double x, double y, double z, float pitch, float yaw) {
        this(worldName, x, y, z);
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public WorldPosition(String worldName, double x, double y, double z) {
        super(x, y, z);
        this.worldName = worldName;
    }

    public WorldPosition add(int x, int y, int z) {
        return new WorldPosition(this.worldName, this.x + x, this.y + y, this.z + z, this.pitch, this.yaw);
    }

    public Location getLocation() {
        if (this.location == null) {
            this.location = new Location(this.getWorld(), this.getX(), this.getY(), this.getZ(), this.yaw, this.pitch);
        }
        return this.location;
    }

    public Block getBlock() {
        World world = this.getWorld();
        return world.getBlockAt(this.getBlockX(), this.getBlockY(), this.getBlockZ());
    }

    public String getWorldName() {
        return this.worldName;
    }

    public World getWorld() {
        return Bukkit.getWorld(this.worldName);
    }

    public float getPitch() {
        return this.pitch;
    }

    public float getYaw() {
        return this.yaw;
    }

    @Override
    public boolean equals(Object other) {
        if (!super.equals(other)) {
            return false;
        }
        if (!(other instanceof WorldPosition)) {
            return false;
        }
        if (other == this) {
            return true;
        }

        WorldPosition otherPosition = (WorldPosition) other;
        return new EqualsBuilder().
                append(this.pitch, otherPosition.pitch).
                append(this.yaw, otherPosition.yaw).
                isEquals();
    }

    @Override
    public int hashCode() {
        int superHashCode = super.hashCode();

        return new HashCodeBuilder(17, 19).
                append(this.worldName).
                append(this.pitch).
                append(this.yaw).
                append(superHashCode).
                toHashCode();
    }

    public boolean isSameBlockPosition(Position otherPosition) {
        return super.equals(otherPosition);
    }

    @Override
    public String toString() {
        return this.worldName + ":" + super.toString() + " pitch:" + this.pitch + " yaw:" + this.yaw;
    }
}
