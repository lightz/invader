package ca.lightz.invasion.game.util;

import com.google.common.base.Strings;

import java.util.HashMap;
import java.util.Map;

public class DataMapper {
    private final Map<String, Object> dataMapping = new HashMap<>();

    public void put(String key, Object value) {
        this.dataMapping.put(key, value);
    }

    public String getString(String key) {
        return (String) this.dataMapping.get(key);
    }

    public Integer getInt(String key) {
        return (Integer) this.dataMapping.get(key);
    }

    public Double getDouble(String key) {
        return (Double) this.dataMapping.get(key);
    }

    public Boolean getBoolean(String key) {
        return (Boolean) this.dataMapping.get(key);
    }

    public static DataMapper deserialize(String serialized) {
        DataMapper dataMapper = new DataMapper();
        if (Strings.isNullOrEmpty(serialized)) {
            return dataMapper;
        }

        String[] variables = serialized.split(";");
        for (String variable : variables) {
            String[] keyTypeValue = variable.split(",");

            String key = keyTypeValue[0];
            String className = keyTypeValue[1];
            String rawValue = (keyTypeValue.length < 3) ? "" : keyTypeValue[2]; //Make sure there's a value

            Object value = tryParseValue(className, rawValue);
            dataMapper.put(key, value);
        }

        return dataMapper;
    }

    private static Object tryParseValue(String className, String rawValue) {
        try {
            return parseValue(className, rawValue);
        } catch (Exception e) {
            e.printStackTrace();
            return rawValue;
        }
    }

    private static Object parseValue(String className, String rawValue) {
        switch (className) {
            case "Integer":
                return Integer.valueOf(rawValue);
            case "Boolean":
                return Boolean.valueOf(rawValue);
            case "Double":
                return Double.valueOf(rawValue);
            default:
                return rawValue;
        }
    }
}
