package ca.lightz.invasion.game.util;

import ca.lightz.invasion.core.Log;
import ca.lightz.invasion.game.Game;
import ca.lightz.invasion.game.map.SiegeMap;
import ca.lightz.invasion.game.objective.Objective;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.container.SiegePlayerContainer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.Iterator;

public class ScoreboardUtil {
    private Game game;

    public ScoreboardUtil(Game game) {
        this.game = game;
    }

    public void send(Player player) {
        ScoreboardManager manager = Bukkit.getScoreboardManager();

        Scoreboard board = manager.getNewScoreboard();
        org.bukkit.scoreboard.Objective objective = board.registerNewObjective("test", "dummy");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.BOLD + "" + ChatColor.DARK_RED + this.game.getMap().getName());

        Score score11 = objective.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Class:");
        score11.setScore(21);

        Score score10 = objective.getScore(ChatColor.AQUA + "" + ChatColor.BOLD + game.getPlayer(player.getUniqueId()).getUnit().getName());
        score10.setScore(20);

        Score score9 = objective.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Objectives");
        score9.setScore(19);

        Score score8 = objective.getScore(ChatColor.STRIKETHROUGH + "+-------------+");
        score8.setScore(18);

        SiegeMap map = this.game.getMap();
        Iterator<Objective> iterator = map.getObjectives();
        Objective obj;
        int x = 17;
        while (iterator.hasNext()) {
            obj = iterator.next();
            if (obj.isLockedOnStart()) {
                Log.info("Skipping Objective (Locked) : " + obj.getName());
                continue;
            }
            Score score = objective.getScore(obj.getControllingTeam().getChatColor() + obj.getName().toString());
            score.setScore(x);
            x--;
        }

        Scoreboard news = manager.getNewScoreboard();
        player.setScoreboard(news);

        player.setScoreboard(board);
    }

    public void sendAll(SiegePlayerContainer con) {
        Iterator<SiegePlayer> it = con.iterator();
        SiegePlayer player;
        while (it.hasNext()) {
            player = it.next();
            Player player1 = Bukkit.getPlayer(player.getUUID());
            send(player1);
        }
    }
}
