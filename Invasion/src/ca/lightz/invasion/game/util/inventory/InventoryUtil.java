package ca.lightz.invasion.game.util.inventory;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class InventoryUtil {

    public static boolean remove(PlayerInventory inventory, Material material) {
        ItemStack[] content = inventory.getContents();
        boolean removed = false;
        for (int i = 0; i < content.length; i++) {
            ItemStack item = content[i];
            if (item == null || item.getType() != material) {
                continue;
            }
            int amount = item.getAmount();
            if (amount <= 1) {
                content[i] = null;
            } else {
                item.setAmount(amount - 1);
            }
            removed = true;
            break;
        }

        inventory.setContents(content); //Fix for offhand/armor slot removal
        return removed;
    }

    public static int count(PlayerInventory inventory, Material material) {
        int count = 0;
        for (ItemStack item : inventory) {
            if (item == null || item.getType() != material) {
                continue;
            }
            count += item.getAmount();
        }
        return count;
    }
}
