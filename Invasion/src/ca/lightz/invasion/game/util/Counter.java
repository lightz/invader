package ca.lightz.invasion.game.util;

public class Counter {
    private final int maximum;
    private int current;

    public Counter(int maximum) {
        this.maximum = maximum;
        this.current = 0;
    }

    public boolean increment() {
        this.current++;
        if (this.current >= this.maximum) {
            this.reset();
            return true;
        }
        return false;
    }

    public void reset() {
        this.current = 0;
    }

    public int getValue() {
        return this.current;
    }

    public int getMaximum() {
        return this.maximum;
    }
}
