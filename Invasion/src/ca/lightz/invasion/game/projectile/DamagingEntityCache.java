package ca.lightz.invasion.game.projectile;

import org.bukkit.entity.Entity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DamagingEntityCache<E extends Entity> {
    private Map<UUID, Integer> entitiesUUID = new HashMap<>(64);

    public void addProjectile(E entity, int damage) {
        this.entitiesUUID.put(entity.getUniqueId(), damage);
    }

    public void clear() {
        this.entitiesUUID = new HashMap<>(64);
    } //We reset to avoid memory leak

    public int getDamage(E entity) {
        UUID uuid = entity.getUniqueId();
        return this.entitiesUUID.getOrDefault(uuid, 0);
    }
}
