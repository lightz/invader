package ca.lightz.invasion.game.channel;

import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.player.container.SiegePlayerContainer;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class GroupChannel extends Channel {
    private SiegePlayerContainer listeners;

    public GroupChannel(MessageId displayName, MessageId channelAcronym, SiegeColorCode channelColor) {
        super(displayName, channelAcronym, channelColor);
        this.listeners = new SiegePlayerContainer();
    }

    @Override
    public void send(String sender, String message) {
        for (SiegePlayer siegePlayer : this.listeners) {
            Player player = Bukkit.getPlayer(siegePlayer.getUUID());
            if (player == null) {
                continue;
            }
            UnlocalizedMessage unlocalizedMessage = new UnlocalizedMessage(MessageId.PLAYER_CHAT_MESSAGE, this.channelAcronym, ChatColor.GRAY, sender, Channel.MESSAGE_DELIMITER, this.channelColor, message);
            Messenger.send(player, unlocalizedMessage);
        }
    }

    @Override
    public String getId() {
        return this.channelAcronym.id.toString(); //TODO will break with multiple group channel
    }

    public void addListener(SiegePlayer player) {
        this.listeners.add(player);
    }

    public void removeListener(SiegePlayer player) {
        this.listeners.remove(player.getUUID());
    }

    public boolean isEmpty() {
        return this.listeners.isEmpty();
    }
}
