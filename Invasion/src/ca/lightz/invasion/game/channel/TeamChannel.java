package ca.lightz.invasion.game.channel;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.ChatColor;

public class TeamChannel extends Channel {
    private Team team;

    public TeamChannel(Team team) {
        super(MessageId.TEAM_CHANNEL, MessageId.TEAM_CHANNEL_ACRONYM, SiegeColorCode.TEAM_CHAT);
        this.team = team;
    }

    @Override
    public void send(String sender, String message) {
        for (PlayerHolder playerHolder : this.team.getActivePlayers()) {
            UnlocalizedMessage unlocalizedMessage = new UnlocalizedMessage(MessageId.PLAYER_CHAT_MESSAGE, this.channelAcronym, ChatColor.GRAY, sender, Channel.MESSAGE_DELIMITER, this.channelColor, message);
            Messenger.send(playerHolder, unlocalizedMessage);
        }
    }

    public boolean isSameTeam(Team team) {
        return this.team.equals(team);
    }

    @Override
    public String getId() {
        return "Team:" + this.team.getId();
    }
}
