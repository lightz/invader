package ca.lightz.invasion.game.channel;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.game.player.SiegePlayer;
import ca.lightz.invasion.game.team.Team;
import ca.lightz.invasion.game.team.container.TeamContainer;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ChannelManager implements EventListener {
    private final GroupChannel global = new GroupChannel(MessageId.GLOBAL_CHANNEL, MessageId.GLOBAL_CHANNEL_ACRONYM, SiegeColorCode.GLOBAL_CHAT);
    private Map<String, Channel> channels = new HashMap<>();
    private Map<SiegePlayer, Channel> playerChannels = new HashMap<>();

    public void startNewGame(TeamContainer teams) {
        this.clearTeamChannels();
        for (Team team : teams) {
            Channel channel = new TeamChannel(team);
            this.channels.put(channel.getId(), channel);
        }
    }

    private void clearTeamChannels() {
        Iterator<Map.Entry<String, Channel>> iterator = this.channels.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, Channel> entry = iterator.next();
            Channel channel = entry.getValue();
            if (channel instanceof TeamChannel) {
                iterator.remove();
            }
        }
    }

    public void addPlayerAsListenerToGlobal(SiegePlayer siegePlayer) {
        this.global.addListener(siegePlayer);
    }

    public void setPlayerWritingToGlobal(PlayerHolder playerHolder) {
        this.changeChannel(playerHolder, this.global);
    }

    public void setPlayerWritingToTeam(PlayerHolder playerHolder) {
        Channel teamChannel = this.getTeamChannel(playerHolder.siegePlayer);
        this.changeChannel(playerHolder, teamChannel);
    }

    private void changeChannel(PlayerHolder playerHolder, Channel newChannel) {
        Channel oldChannel = this.playerChannels.get(playerHolder.siegePlayer);
        this.playerChannels.put(playerHolder.siegePlayer, newChannel);
        if (!newChannel.equals(oldChannel)) {
            newChannel.sendJoinMessage(playerHolder);
        }
    }

    private Channel getTeamChannel(SiegePlayer player) {
        Team team = player.getTeam();
        for (Channel channel : this.channels.values()) {
            if (!(channel instanceof TeamChannel)) {
                continue;
            }
            if (((TeamChannel) channel).isSameTeam(team)) {
                return channel;
            }
        }
        return this.global;
    }

    public void removeFromAllChannels(SiegePlayer player) {
        this.playerChannels.remove(player);
        this.global.removeListener(player);
        for (Channel channel : this.channels.values()) {
            if (!(channel instanceof GroupChannel)) {
                continue;
            }
            ((GroupChannel) channel).removeListener(player);
        }
    }

    @EventHandler
    public void sendChatMessage(AsyncPlayerChatEvent event, SiegePlayer siegePlayer) {
        Player player = event.getPlayer();

        Channel channel = this.playerChannels.get(siegePlayer);
        String message = event.getMessage();
        channel.send(player.getName(), message);
        event.setCancelled(true);
    }

    public void sendToGlobal(PlayerHolder playerHolder, String message) {
        this.global.send(playerHolder.player.getName(), message);
    }

    public void sendToTeamChat(PlayerHolder playerHolder, String message) {
        Channel channel = this.getTeamChannel(playerHolder.siegePlayer);
        channel.send(playerHolder.player.getName(), message);
    }
}
