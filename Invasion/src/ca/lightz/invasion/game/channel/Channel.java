package ca.lightz.invasion.game.channel;

import ca.lightz.invasion.game.player.PlayerHolder;
import ca.lightz.invasion.localization.MessageId;
import ca.lightz.invasion.localization.Messenger;
import ca.lightz.invasion.localization.SiegeColorCode;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.ChatColor;

public abstract class Channel {
    protected static final String MESSAGE_DELIMITER = ":";
    private MessageId channelName;
    protected UnlocalizedMessage channelAcronym;
    protected SiegeColorCode channelColor;

    public Channel(MessageId displayName, MessageId channelAcronym, SiegeColorCode channelColor) {
        this.channelName = displayName;
        this.channelAcronym = new UnlocalizedMessage(channelAcronym, ChatColor.BOLD, ChatColor.DARK_GRAY, channelColor);
        this.channelColor = channelColor;
    }

    public abstract void send(String sender, String message);

    @Override
    public boolean equals(Object other) {
        if (!super.equals(other)) {
            return false;
        }
        if (!(other instanceof Channel)) {
            return false;
        }
        if (other == this) {
            return true;
        }

        Channel otherChannel = (Channel) other;
        return new EqualsBuilder().
                append(this.channelName, otherChannel.channelName).
                append(this.channelAcronym, otherChannel.channelAcronym).
                isEquals();
    }

    @Override
    public int hashCode() {
        int superHashCode = super.hashCode();

        return new HashCodeBuilder(43, 47).
                append(this.channelName).
                append(this.channelAcronym).
                append(superHashCode).
                toHashCode();
    }

    public abstract String getId();

    public void sendJoinMessage(PlayerHolder playerHolder) {
        Messenger.send(playerHolder, this.channelName);
    }
}
