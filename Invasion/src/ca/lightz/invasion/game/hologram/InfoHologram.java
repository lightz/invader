package ca.lightz.invasion.game.hologram;

import ca.lightz.invasion.Invasion;
import ca.lightz.invasion.localization.Localization;
import ca.lightz.invasion.localization.UnlocalizedMessage;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.LinkedList;

public class InfoHologram {
    private static Localization LOCALIZATION;

    protected Hologram hologram;
    private Location location;
    private LinkedList<UnlocalizedMessage> text = new LinkedList<>();

    public InfoHologram(Location location) {
        this.location = location;
        this.show();
    }

    public static void setLocalization(Localization localization) {
        LOCALIZATION = localization;
    }

    private void show() {
        if (this.hologram != null) {
            return;
        }

        this.hologram = HologramsAPI.createHologram(Invasion.getPlugin(), this.location);
        this.setVisibility();
        this.setHologramText();
    }

    protected void setVisibility() {
    }

    private void setHologramText() {
        if (this.hologram == null) {
            return;
        }
        this.hologram.clearLines();
        for (UnlocalizedMessage message : this.text) {
            String line = LOCALIZATION.localize(message);
            this.hologram.appendTextLine(line);
        }
    }

    public void addPlayer(Player player) {
    }

    public void removePlayer(Player player) {
    }

    public void hide() {
        this.hologram.delete();
        this.hologram = null;
    }

    public void setText(UnlocalizedMessage... text) {
        this.text.clear();
        for (UnlocalizedMessage line : text) {
            this.text.addLast(line);
        }
        this.setHologramText();
    }
}
