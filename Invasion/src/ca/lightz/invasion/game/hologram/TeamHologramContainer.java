package ca.lightz.invasion.game.hologram;

import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;

public class TeamHologramContainer implements Iterable<InfoHologram> {
    private final List<InfoHologram> holograms = new ArrayList<>();

    public void addPlayer(Player player) {
        if (player == null) {
            return;
        }
        for (InfoHologram hologram : this) {
            hologram.addPlayer(player);
        }
    }

    public void removePlayer(Player player) {
        for (InfoHologram hologram : this) {
            hologram.removePlayer(player);
        }
    }

    @Override
    @Nonnull
    public Iterator<InfoHologram> iterator() {
        return this.holograms.iterator();
    }

    @Override
    public Spliterator<InfoHologram> spliterator() {
        return this.holograms.spliterator();
    }

    public void add(InfoHologram hologram) {
        if (hologram != null) {
            this.holograms.add(hologram);
        }
    }

    public void clear() {
        this.holograms.clear();
    }
}
