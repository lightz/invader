package ca.lightz.invasion.game.hologram;

import ca.lightz.invasion.core.Log;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SelectiveHologram extends InfoHologram {
    private VisibilityManager visibilityManager;

    public SelectiveHologram(Location location) {
        super(location);
    }

    @Override
    protected void setVisibility() {
        this.visibilityManager = this.hologram.getVisibilityManager();
        this.visibilityManager.setVisibleByDefault(false);
    }

    @Override
    public void addPlayer(Player player) {
        if (this.hologram != null) {
            try {
                this.visibilityManager.showTo(player);
            } catch (Exception ignore) {
                Log.warning("Couldn't correctly set visibility for an hologram.");
            }
        }

    }

    @Override
    public void removePlayer(Player player) {
        if (player != null && this.hologram != null) {
            this.visibilityManager.hideTo(player);
        }
    }
}
