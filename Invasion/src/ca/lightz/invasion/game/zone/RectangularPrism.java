package ca.lightz.invasion.game.zone;

import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class RectangularPrism extends Zone {
    private final WorldPosition corner;
    private int height;
    private int xLength;
    private int zLength;

    public RectangularPrism(WorldPosition corner, DataMapper dataMapper) {
        this.corner = corner;
        this.deserialize(dataMapper);
    }

    @Override
    protected void deserialize(DataMapper dataMapper) {
        this.height = dataMapper.getInt("height");
        this.xLength = dataMapper.getInt("xLength");
        this.zLength = dataMapper.getInt("zLength");
    }

    @Override
    public boolean isAnyInside(Location... locations) {
        for (Location location : locations) {
            if (this.isInside(location)) {
                return true;
            }
        }
        return false;
    }

    private boolean isInside(Location location) {
        double y = location.getY() - this.corner.getY();
        if (y < 0 || y > this.height) {
            return false;
        }

        double x = location.getX() - this.corner.getX();
        if (x < 0 || x > this.xLength) {
            return false;
        }

        double z = location.getZ() - this.corner.getZ();
        return !(z < 0 || z > this.zLength);
    }

    @Override
    protected Collection<Block> getBlocks() {
        Collection<Block> blocks = new LinkedList<>();
        Block centerBlock = this.corner.getBlock();

        for (int x = 0; x < this.xLength; x++) {
            for (int z = 0; z < this.zLength; z++) {
                for (int y = 0; y < this.height; y++) {
                    Block block = centerBlock.getRelative(x, y, z);
                    blocks.add(block);
                }
            }
        }

        return blocks;
    }

    @Override
    public Location getCenteredLocation() {
        return this.corner.getLocation().add((this.xLength / 2) + 0.5, (this.height / 2) + 0.5, (this.zLength / 2) + 0.5);
    }

    @Override
    public Collection<Block> getFloorOutlines() {
        Collection<Block> floorOutlines = new ArrayList<>();

        Block cornerBlock = this.corner.getBlock();

        for (int x = 0; x < this.xLength; x++) {
            Block firstSideBlock = cornerBlock.getRelative(x, 0, 0);
            Block secondSideBlock = cornerBlock.getRelative(x, 0, (this.zLength - 1));

            Block firstSideFloorBlock = this.getFloorBlock(firstSideBlock);
            Block secondSideFloorBlock = this.getFloorBlock(secondSideBlock);
            if (firstSideFloorBlock != null) {
                floorOutlines.add(firstSideFloorBlock);
            }
            if (secondSideFloorBlock != null) {
                floorOutlines.add(secondSideFloorBlock);
            }
        }

        for (int z = 1; z < (this.zLength - 1); z++) {
            Block firstSideBlock = cornerBlock.getRelative(0, 0, z);
            Block secondSideBlock = cornerBlock.getRelative((this.xLength - 1), 0, z);

            Block firstSideFloorBlock = this.getFloorBlock(firstSideBlock);
            Block secondSideFloorBlock = this.getFloorBlock(secondSideBlock);
            if (firstSideFloorBlock != null) {
                floorOutlines.add(firstSideFloorBlock);
            }
            if (secondSideFloorBlock != null) {
                floorOutlines.add(secondSideFloorBlock);
            }
        }

        return floorOutlines;
    }
}
