package ca.lightz.invasion.game.zone;

import ca.lightz.invasion.game.objective.control.Control;
import ca.lightz.invasion.game.objective.flag.Flag;
import ca.lightz.invasion.game.objective.flag.FlagScanner;

public class FlagZone {
    private final Zone zone;
    private final FlagScanner scanner;
    private Flag flag;

    public FlagZone(Zone zone, FlagScanner scanner) {
        this.zone = zone;
        this.scanner = scanner;
    }

    private Flag getFlag() {
        if (this.flag == null) {
            this.flag = this.scanner.scan(this.zone);
        }
        return this.flag;
    }

    public void setFlagState(Control control) {
        this.getFlag().setState(control);
    }

    public void reset() {
        this.flag = null;
    }
}
