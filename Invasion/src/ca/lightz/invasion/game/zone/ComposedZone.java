package ca.lightz.invasion.game.zone;

import ca.lightz.invasion.game.util.DataMapper;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ComposedZone extends Zone {
    private final List<Zone> zones;

    public ComposedZone(List<Zone> zones) {
        this.zones = (zones == null) ? new ArrayList<>(1) : zones;
    }

    public void add(Zone zone) {
        this.zones.add(zone);
    }

    @Override
    public boolean isAnyInside(Location... locations) {
        for (Zone zone : this.zones) {
            if (zone.isAnyInside(locations)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void deserialize(DataMapper dataMapper) {
    }

    @Override
    public Collection<Block> getFloorOutlines() {
        Collection<Block> outlines = new ArrayList<>();

        for (Zone zone : this.zones) {
            outlines.addAll(zone.getFloorOutlines());
        }

        return outlines;
    }

    @Override
    protected Collection<Block> getBlocks() {
        Collection<Block> blocks = new ArrayList<>();
        for (Zone zone : this.zones) {
            blocks.addAll(zone.getBlocks());
        }

        return blocks;
    }

    @Override
    public Location getCenteredLocation() {
        double x = 0;
        double y = 0;
        double z = 0;

        for (Zone zone : this.zones) {
            Location center = zone.getCenteredLocation();
            x += center.getX();
            y += center.getY();
            z += center.getZ();
        }

        int size = this.zones.size();
        x /= size;
        y /= size;
        z /= size;

        return new Location(this.zones.get(0).getCenteredLocation().getWorld(), x, y, z);
    }
}
