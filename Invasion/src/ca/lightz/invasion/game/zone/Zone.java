package ca.lightz.invasion.game.zone;

import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.block.BlockUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Iterator;

public abstract class Zone implements Iterable<Block> {

    public abstract boolean isAnyInside(Location... locations);

    protected abstract void deserialize(DataMapper dataMapper);

    public abstract Collection<Block> getFloorOutlines();

    protected Block getFloorBlock(Block block) {
        for (int yDepth = 0; yDepth < 3; yDepth++) {
            //TODO change function since this will impact half-slab and stairs
            if (BlockUtil.isValidFloorOutline(block)) {
                return block;
            }
            block = block.getRelative(BlockFace.DOWN);
        }
        return null;
    }

    protected abstract Collection<Block> getBlocks();

    @Override
    @Nonnull
    public Iterator<Block> iterator() {
        return this.getBlocks().iterator();
    }

    public abstract Location getCenteredLocation();
}
