package ca.lightz.invasion.game.zone;

import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.position.WorldPosition;

public class ZoneFactory {

    public static Zone build(WorldPosition position, String figure, String data) {
        DataMapper dataMapper = DataMapper.deserialize(data);
        switch (figure) {
            case "RectangularPrism":
                return new RectangularPrism(position, dataMapper);
            default:
                return new Cylinder(position, dataMapper);
        }
    }
}
