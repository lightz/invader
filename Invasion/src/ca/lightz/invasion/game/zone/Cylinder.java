package ca.lightz.invasion.game.zone;

import ca.lightz.invasion.game.util.DataMapper;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class Cylinder extends Zone {
    private static final int[][] CIRCLE_PLOT_POINTS = {{1, 1}, {-1, 1}, {1, -1}, {-1, -1}};

    private final WorldPosition center;
    private int height;
    private int radius;
    private final int squaredRadius;

    public Cylinder(WorldPosition center, DataMapper dataMapper) {
        this.center = center;
        this.deserialize(dataMapper);
        this.squaredRadius = this.radius * this.radius;
    }

    @Override
    protected void deserialize(DataMapper dataMapper) {
        this.radius = dataMapper.getInt("radius");
        this.height = dataMapper.getInt("height");
    }

    @Override
    public boolean isAnyInside(Location... locations) {
        for (Location location : locations) {
            if (this.isInside(location)) {
                return true;
            }
        }
        return false;
    }

    private boolean isInside(Location location) {
        double y = location.getY() - this.center.getY();
        if (y < 0 || y > this.height) {
            return false;
        }

        double x = location.getX() - this.center.getX();
        double z = location.getZ() - this.center.getZ();
        double distance = x * x + z * z;

        return distance <= this.squaredRadius;
    }

    @Override
    protected Collection<Block> getBlocks() {
        Collection<Block> blocks = new LinkedList<>();
        Block centerBlock = this.center.getBlock();

        for (int x = -this.radius; x <= this.radius; x++) {
            for (int z = this.radius; z <= this.radius; z++) {
                double distanceSquared = x * x + z * z;
                if (distanceSquared > this.squaredRadius) {
                    continue;
                }

                for (int y = 0; y < this.height; y++) {
                    Block block = centerBlock.getRelative(x, y, z);
                    blocks.add(block);
                }
            }
        }

        return blocks;
    }

    @Override
    public Location getCenteredLocation() {
        return this.center.getLocation().add(0.5, 0.5, 0.5);
    }

    @Override
    public Collection<Block> getFloorOutlines() {
        return this.circleMidpoint();
    }

    private Collection<Block> circleMidpoint() {
        int x = 0;
        int z = this.radius;
        int p = 1 - this.radius;

        Block centerBlock = this.center.getBlock();
        Collection<Block> blocks = new ArrayList<>(this.circlePlotPoint(centerBlock, x, z));
        while (x < z) {
            x++;
            if (p < 0) {
                p += 2 * x + 1;
            } else {
                z--;
                p += 2 * (x - z) + 1;
            }
            blocks.addAll(this.circlePlotPoint(centerBlock, x, z));
        }

        return blocks;
    }

    private Collection<Block> circlePlotPoint(Block centerBlock, int x, int z) {
        Collection<Block> blocks = new ArrayList<>();
        for (int[] pair : CIRCLE_PLOT_POINTS) {
            Block block = this.getFloorBlock(centerBlock.getRelative(x * pair[0], 0, z * pair[1]));
            if (block != null) {
                blocks.add(block);
            }
        }

        for (int[] invertedPair : CIRCLE_PLOT_POINTS) {
            Block block = this.getFloorBlock(centerBlock.getRelative(z * invertedPair[0], 0, x * invertedPair[1]));
            if (block != null) {
                blocks.add(block);
            }
        }
        return blocks;
    }
}
