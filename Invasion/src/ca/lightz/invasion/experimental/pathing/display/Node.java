package ca.lightz.invasion.experimental.pathing.display;

import ca.lightz.invasion.game.util.position.WorldPosition;

public class Node {
    public final WorldPosition position;

    public Node(WorldPosition position) {
        this.position = position;
    }
}
