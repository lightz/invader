package ca.lightz.invasion.experimental.pathing.display;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.LinkedList;

public class Path implements Iterable<Node> {
    private LinkedList<Node> nodes = new LinkedList<>();

    public void addNode(Node node) {
        this.nodes.add(node);
    }


    public void addFirstNode(Node node) {
        this.nodes.addFirst(node);
    }

    @Nonnull
    @Override
    public Iterator<Node> iterator() {
        return this.nodes.iterator();
    }
}
