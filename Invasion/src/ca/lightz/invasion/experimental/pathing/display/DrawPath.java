package ca.lightz.invasion.experimental.pathing.display;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

import java.util.Iterator;

public class DrawPath {

    private Path path;

    public DrawPath(Path path) {
        this.path = path;
    }

    public void createPath() {
        Iterator<Node> nodes = this.path.iterator();
        Node startNode = null;
        Node nextNode = null;

        while (nodes.hasNext()) {
            if (startNode == null) {
                startNode = nodes.next();
                continue;
            }
            if (nextNode != null) {
                startNode = nextNode;
                nextNode = nodes.next();
            } else {
                nextNode = nodes.next();
            }

            Location start = startNode.position.getLocation();
            start.getWorld().spawnParticle(Particle.REDSTONE, start, 0);
            Location end = nextNode.position.getLocation();
            buildParticles(start, end);
        }
    }

    private void buildParticles(Location start, Location end) {
        Vector target = end.toVector();
        start.setDirection(target.subtract(start.toVector()));
        Vector increase = start.getDirection().normalize();
        Location current = start.clone();
        for (int counter = 0; counter < start.distance(end); counter++) {
            start.getWorld().spawnParticle(Particle.HEART, current, 360);
            current = current.add(increase);
        }
    }
}
