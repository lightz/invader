package ca.lightz.invasion.experimental.pathing.navmesh;

import ca.lightz.invasion.experimental.pathing.display.Node;
import ca.lightz.invasion.experimental.pathing.display.Path;
import ca.lightz.invasion.game.util.position.WorldPosition;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.material.Wool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class AStarPathinfinder {
    private static final DyeColor[] COLORS = DyeColor.values();
    private Map<String, List<SquareCell>> navMeshes = new HashMap<>();
    private int colorIndex = 0;

    public Path findPath(WorldPosition start, WorldPosition end) {
        String worldName = start.getWorldName();
        List<SquareCell> navMesh = this.getNavMesh(worldName);

        SquareCell startingCell = this.getCell(navMesh, start);
        SquareCell endingCell = this.getCell(navMesh, end);

        SquareCell finish = this.process(startingCell, endingCell);
        Path path = this.buildPath(finish, worldName);

        this.reset(navMesh);
        return path;
    }

    private void reset(List<SquareCell> navMesh) {
        for (SquareCell cell : navMesh) {
            cell.reset();
        }
    }

    private Path buildPath(SquareCell finish, String worldName) {
        Path path = new Path();

        while (finish != null) {
            Node node = new Node(new WorldPosition(worldName, finish.centerX, finish.y, finish.centerZ));
            path.addFirstNode(node);
            finish = finish.parent;
        }

        return path;
    }

    private List<SquareCell> getNavMesh(String worldName) {
        World world = Bukkit.getWorld(worldName);
        if (!this.navMeshes.containsKey(worldName)) {
            NavMeshFloorScannner scanner = new NavMeshFloorScannner();
            scanner.scan(world);
            this.navMeshes.put(worldName, scanner.getNavMesh());
        }

        return this.navMeshes.get(worldName);
    }

    private SquareCell getCell(List<SquareCell> navMesh, WorldPosition position) {
        WorldPosition above = new WorldPosition(position);
        above.setY(above.getY() + 1);
        for (SquareCell cell : navMesh) {
            if (cell.contains(position) || cell.contains(above)) {
                return cell;
            }
        }
        return null;
    }

    private SquareCell process(SquareCell start, SquareCell end) {
        if (start == null || end == null || start == end) {
            return null;
        }

        TreeSet<SquareCell> openList = new TreeSet<>(SquareCell.Comparators.ESTIMATE_COST);

        start.setWalkingCost(0);
        SquareCell current;
        openList.add(start);
        while (!openList.isEmpty()) {
            current = openList.pollFirst();
            if (current == end) {
                return current;
            }
            current.setChecked();
            for (SquareCell neighbor : current.neighbors) {
                if (neighbor.wasChecked()) {
                    continue;
                }
                double score = current.getWalkingCost() + current.manhattanDistance(neighbor);
                if (score >= (neighbor.getHeuristicScore(end) + neighbor.getWalkingCost())) {
                    continue; //This is not a better path
                }

                neighbor.parent = current;
                neighbor.setWalkingCost(score);
                if (!openList.contains(neighbor)) {
                    openList.add(neighbor);
                }
            }
        }
        return null;
    }

    public void showLayer(World world, int layer) {
        this.colorIndex = 0;
        List<SquareCell> navMesh = this.getNavMesh(world.getName());

        for (SquareCell cell : navMesh) {
            if (cell.y == layer) {
                this.draw(world, cell);
            }
        }
    }

    public void showNeighbor(WorldPosition start) {
        String worldName = start.getWorldName();
        World world = Bukkit.getWorld(worldName);
        List<SquareCell> navMesh = this.getNavMesh(worldName);
        SquareCell startingCell = this.getCell(navMesh, start);

        if (startingCell == null) {
            return;
        }
        for (SquareCell cell : startingCell.neighbors) {
            this.draw(world, cell);
        }
    }

    private void draw(World world, SquareCell cell) {
        for (int x = 0; x < cell.length; x++) {
            for (int z = 0; z < cell.length; z++) {
                if (x == 0 || x == (cell.length - 1) || z == 0 || z == (cell.length - 1)) {
                    Block block = world.getBlockAt(cell.x + x, cell.y, cell.z + z);

                    block.setType(Material.WHITE_WOOL);

                    BlockState state = block.getState();
                    Wool blockData = (Wool) state.getData();
                    blockData.setColor(COLORS[this.colorIndex]);
                    state.setData(blockData);
                    state.update(true, false);
                }
            }
        }

        this.colorIndex++;
        if (this.colorIndex >= COLORS.length) {
            this.colorIndex = 0;
        }
    }
}
