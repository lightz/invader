package ca.lightz.invasion.experimental.pathing.navmesh;

import ca.lightz.invasion.game.util.position.WorldPosition;
import org.apache.commons.lang.builder.EqualsBuilder;

import java.util.*;

public class SquareCell {
    public final int x;
    public final int y;
    public final int z;
    public final int length;

    private Double heuristicScore;
    private double estimateCost;
    private double walkingCost = Double.MAX_VALUE;
    public final int centerX;
    public final int centerZ;
    public SquareCell parent;

    public final Collection<SquareCell> neighbors = new ArrayList<>();
    private boolean checked;

    public SquareCell(int x, int y, int z, int length) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.length = length;

        this.centerX = this.x + (int) Math.floor(this.length / 2.0);
        this.centerZ = this.z + (int) Math.floor(this.length / 2.0);
    }

    public Set<String> getUsedCells() {
        Set<String> cells = new HashSet<>();

        int endX = this.x + this.length;
        int endZ = this.z + this.length;

        for (int i = this.x; i < endX; i++) {
            for (int k = this.z; k < endZ; k++) {
                cells.add(i + ":" + this.y + ":" + k);
            }
        }
        return cells;
    }

    public boolean isLinkedTo(SquareCell other) {
        return this.layersAreNear(other) && ((this.isNextOnX(other) && this.isOnSameZLine(other)) || (this.isNextOnZ(other) && this.isOnSameXLine(other)));
    }

    private boolean layersAreNear(SquareCell other) {
        return this.y == other.y || (this.y + 1) == other.y || (this.y - 1) == other.y;
    }

    private boolean isNextOnX(SquareCell other) {
        return this.x == (other.x + other.length) || (this.x + this.length) == other.x || this.x == other.x;
    }

    private boolean isNextOnZ(SquareCell other) {
        return this.z == (other.z + other.length) || (this.z + this.length) == other.z || this.z == other.z;
    }

    private boolean isOnSameZLine(SquareCell other) {
        if (this.z <= other.z && other.z <= (this.z + this.length - 1)) {
            return true;
        }
        int otherEndZ = other.z + other.length - 1;
        return this.z <= otherEndZ && otherEndZ <= (this.z + this.length - 1);
    }

    private boolean isOnSameXLine(SquareCell other) {
        if (this.x <= other.x && other.x <= (this.x + this.length - 1)) {
            return true;
        }
        int otherEndX = other.x + other.length - 1;
        return this.x <= otherEndX && otherEndX <= (this.x + this.length - 1);
    }

    public void addNeighbor(SquareCell neighbor) {
        if (!this.neighbors.contains(neighbor)) {
            this.neighbors.add(neighbor);
        }
    }

    public boolean contains(WorldPosition position) {
        if (position.getBlockY() != this.y) {
            return false;
        }
        int x = position.getBlockX();
        if (x < this.x || x > (this.x + this.length - 1)) {
            return false;
        }
        int z = position.getBlockZ();
        if (z < this.z || z > (this.z + this.length - 1)) {
            return false;
        }
        return true;
    }

    public double getHeuristicScore(SquareCell end) {
        if (this.heuristicScore == null) {
            this.heuristicScore = this.manhattanDistance(end);
        }
        return this.heuristicScore;
    }

    public double getWalkingCost() {
        return this.walkingCost;
    }

    public void setWalkingCost(double walkingCost) {
        this.walkingCost = walkingCost;
        this.estimateCost = this.heuristicScore != null ? this.heuristicScore + this.walkingCost : this.walkingCost;
    }

    public double manhattanDistance(SquareCell end) {
        return Math.abs(this.centerX - end.centerX) + Math.abs(this.centerZ - end.centerZ) + Math.abs(this.y - end.y);
    }

    public void reset() {
        this.parent = null;
        this.heuristicScore = null;
        this.walkingCost = Double.MAX_VALUE;
        this.estimateCost = 0;
        this.checked = false;
    }

    public void setChecked() {
        this.checked = true;
    }

    public boolean wasChecked() {
        return this.checked;
    }

    public static class Comparators {
        public static Comparator<SquareCell> ESTIMATE_COST = Comparator.comparingDouble(o -> o.estimateCost);
    }

    @Override
    public boolean equals(Object other) {
        if (!super.equals(other)) {
            return false;
        }
        if (!(other instanceof SquareCell)) {
            return false;
        }
        if (other == this) {
            return true;
        }

        SquareCell otherCell = (SquareCell) other;
        return new EqualsBuilder().
                append(this.x, otherCell.x).
                append(this.y, otherCell.y).
                append(this.z, otherCell.z).
                append(this.length, otherCell.length).
                append(this.centerX, otherCell.centerX).
                append(this.centerZ, otherCell.centerZ).
                isEquals();
    }
}
