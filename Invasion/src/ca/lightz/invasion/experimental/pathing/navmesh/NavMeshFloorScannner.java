package ca.lightz.invasion.experimental.pathing.navmesh;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.material.Openable;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class NavMeshFloorScannner {
    //TODO handle snow. Water + bottom half slab edge. Lily pads.

    private static final int BUILD_HEIGHT = 256;
    private static final Set<Material> SKIP_FLOOR_CHECK = new HashSet<>();
    private static final Set<Material> FENCES = new HashSet<>();
    private static final Set<Material> SOLID_BUT_TRAVERSABLE = new HashSet<>();
    private Set<String> scanned = new HashSet<>();
    private List<SquareCell> squareCells = new LinkedList<>();

    static {
        SKIP_FLOOR_CHECK.add(Material.WATER);
        SKIP_FLOOR_CHECK.add(Material.LADDER);

        FENCES.add(Material.OAK_FENCE);
        FENCES.add(Material.OAK_FENCE_GATE);
        FENCES.add(Material.ACACIA_FENCE);
        FENCES.add(Material.ACACIA_FENCE_GATE);
        FENCES.add(Material.BIRCH_FENCE);
        FENCES.add(Material.BIRCH_FENCE_GATE);
        FENCES.add(Material.DARK_OAK_FENCE);
        FENCES.add(Material.DARK_OAK_FENCE_GATE);
        FENCES.add(Material.JUNGLE_FENCE);
        FENCES.add(Material.JUNGLE_FENCE_GATE);
        FENCES.add(Material.NETHER_BRICK_FENCE);
        FENCES.add(Material.SPRUCE_FENCE);
        FENCES.add(Material.SPRUCE_FENCE_GATE);
        FENCES.add(Material.COBBLESTONE_WALL);
        FENCES.add(Material.MOSSY_COBBLESTONE_WALL);

        SOLID_BUT_TRAVERSABLE.add(Material.LADDER);
        SOLID_BUT_TRAVERSABLE.add(Material.VINE);
        SOLID_BUT_TRAVERSABLE.add(Material.BLACK_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.BLUE_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.BROWN_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.CYAN_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.GRAY_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.GREEN_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.LIGHT_BLUE_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.LIGHT_GRAY_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.LIME_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.MAGENTA_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.ORANGE_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.PINK_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.PURPLE_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.RED_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.WHITE_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.YELLOW_CARPET);
        SOLID_BUT_TRAVERSABLE.add(Material.COBWEB);
        SOLID_BUT_TRAVERSABLE.add(Material.RAIL);
        SOLID_BUT_TRAVERSABLE.add(Material.ACTIVATOR_RAIL);
        SOLID_BUT_TRAVERSABLE.add(Material.DETECTOR_RAIL);
        SOLID_BUT_TRAVERSABLE.add(Material.POWERED_RAIL);
        SOLID_BUT_TRAVERSABLE.add(Material.STONE_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.LIGHT_WEIGHTED_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.HEAVY_WEIGHTED_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.ACACIA_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.BIRCH_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.DARK_OAK_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.JUNGLE_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.OAK_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.SPRUCE_PRESSURE_PLATE);
        SOLID_BUT_TRAVERSABLE.add(Material.STONE_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.ACACIA_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.BIRCH_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.DARK_OAK_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.JUNGLE_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.OAK_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.SPRUCE_BUTTON);
        SOLID_BUT_TRAVERSABLE.add(Material.TRIPWIRE);
        SOLID_BUT_TRAVERSABLE.add(Material.TRIPWIRE_HOOK);
        SOLID_BUT_TRAVERSABLE.add(Material.LEVER);
    }

    public List<SquareCell> getNavMesh() {
        return this.squareCells;
    }

    public void scan(World world) {
        Location center = world.getWorldBorder().getCenter();
        double borderSize = world.getWorldBorder().getSize();
        double halfSize = borderSize / 2;
        Location corner = new Location(world, center.getX() - halfSize, 0, center.getZ() - halfSize);

        int max = (int) Math.ceil(borderSize);
        max = Math.min(max, 800);
        Block cornerBlock = corner.getBlock();
        Block current;

        for (int y = 0; y < BUILD_HEIGHT; y++) {
            for (int x = 0; x < max; x++) {
                for (int z = 0; z < max; z++) {
                    current = cornerBlock.getRelative(x, y, z);
                    if (this.isWalkable(current)) {
                        int squareMaxLength = x > z ? max - x : max - z;
                        SquareCell squareCell = this.buildBiggestCell(current, y, squareMaxLength);
                        this.scanned.addAll(squareCell.getUsedCells());
                        this.squareCells.add(squareCell);
                        z += (squareCell.length - 1);
                    }
                }
            }
        }

        this.linkSquareCells();
    }

    private void linkSquareCells() {
        for (SquareCell origin : this.squareCells) {
            for (SquareCell target : this.squareCells) {
                if (origin.equals(target)) {
                    continue;
                }
                if (this.areLinked(origin, target)) {
                    origin.addNeighbor(target);
                    target.addNeighbor(origin);
                }
            }
        }
    }

    private boolean areLinked(SquareCell first, SquareCell second) {
        return first.isLinkedTo(second) || second.isLinkedTo(first);
    }

    private SquareCell buildBiggestCell(Block corner, int y, int max) {
        int size = this.getMaxSize(corner, max);
        return new SquareCell(corner.getX(), y, corner.getZ(), size);
    }

    private int getMaxSize(Block corner, int max) {
        int currentMax = 0;

        outerloop:
        for (int i = 1; i < max; i++) {
            for (int k = 0; k < i; k++) {
                Block first = corner.getRelative(k, 0, i);
                if (!this.isWalkable(first)) {
                    break outerloop;
                }
                Block second = corner.getRelative(i, 0, k);
                if (!this.isWalkable(second)) {
                    break outerloop;
                }
            }
            Block outwardCorner = corner.getRelative(i, 0, i);
            if (!this.isWalkable(outwardCorner)) {
                break;
            }
            currentMax = i;
        }

        return currentMax + 1; //currentMax = index, index starts at 0 so we add 1 for actual size
    }

    private boolean isWalkable(Block block) {
        return this.allowMovement(block) && this.isNotInNavMesh(block) && (this.canSkipFloorCheck(block) || this.hasFloor(block)) && this.hasHeadClearance(block);
    }

    private boolean isNotInNavMesh(Block block) {
        return !this.scanned.contains(block.getX() + ":" + block.getY() + ":" + block.getZ());
    }

    private boolean allowMovement(Block block) {
        Material blockType = block.getType();
        return !blockType.isSolid() || SOLID_BUT_TRAVERSABLE.contains(blockType) || this.isWoodenOpenable(block);
    }

    private boolean isWoodenOpenable(Block block) {
        BlockData blockData = block.getBlockData();
        Material type = block.getType();
        return (blockData instanceof Openable) && (type != Material.IRON_DOOR && type != Material.IRON_TRAPDOOR);
    }

    private boolean canSkipFloorCheck(Block block) {
        return SKIP_FLOOR_CHECK.contains(block.getType());
    }

    private boolean hasHeadClearance(Block block) {
        Material headType = block.getRelative(BlockFace.UP).getType();
        return !headType.isSolid() || headType == Material.LADDER;
    }

    private boolean hasFloor(Block block) {
        Material down = block.getRelative(BlockFace.DOWN).getType();
        return down.isSolid() && !this.isFence(down);
    }

    private boolean isFence(Material material) {
        return FENCES.contains(material);
    }
}
