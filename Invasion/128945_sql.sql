-- phpMyAdmin SQL Dump
-- version 4.1.13
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 21 Avril 2018 à 19:37
-- Version du serveur :  5.5.59-0+deb8u1-log
-- Version de PHP :  5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `128945_sql`
--

-- --------------------------------------------------------

--
-- Structure de la table `map`
--

CREATE TABLE IF NOT EXISTS `map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `world_name` varchar(64) NOT NULL,
  `spawn_room` int(11) NOT NULL,
  `wool_map_zone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`world_name`),
  UNIQUE KEY `spawn_room` (`spawn_room`),
  KEY `wool_map_zone_id` (`wool_map_zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `map`
--

INSERT INTO `map` (`id`, `name`, `world_name`, `spawn_room`, `wool_map_zone_id`) VALUES
(1, 'Azura', 'Azura', 2, 9),
(2, 'Illumina', 'Illumina', 55, 10),
(3, 'Nordic', 'Nordic', 94, 18);

-- --------------------------------------------------------

--
-- Structure de la table `map_objective`
--

CREATE TABLE IF NOT EXISTS `map_objective` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `map_id` (`map_id`),
  KEY `objective_id` (`objective_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Contenu de la table `map_objective`
--

INSERT INTO `map_objective` (`id`, `map_id`, `objective_id`) VALUES
(11, 1, 1),
(12, 1, 2),
(13, 1, 3),
(14, 1, 4),
(15, 1, 5),
(16, 1, 6),
(17, 1, 7),
(18, 1, 8),
(19, 2, 9),
(20, 2, 10),
(21, 2, 11),
(22, 2, 12),
(23, 2, 13),
(24, 2, 14),
(25, 3, 15),
(26, 3, 16),
(27, 3, 17),
(28, 3, 18),
(29, 3, 19),
(30, 3, 20),
(32, 3, 21),
(33, 3, 22),
(34, 3, 23);

-- --------------------------------------------------------

--
-- Structure de la table `map_team`
--

CREATE TABLE IF NOT EXISTS `map_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  KEY `map_id` (`map_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `map_team`
--

INSERT INTO `map_team` (`id`, `team_id`, `map_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 1, 2),
(5, 3, 2),
(6, 9, 2),
(9, 10, 3),
(10, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `objective`
--

CREATE TABLE IF NOT EXISTS `objective` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `lock_on_capture` tinyint(1) NOT NULL DEFAULT '0',
  `control` int(11) NOT NULL,
  `starting_team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `starting_team_id` (`starting_team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `objective`
--

INSERT INTO `objective` (`id`, `name`, `is_locked`, `lock_on_capture`, `control`, `starting_team_id`) VALUES
(1, 'Outpost', 1, 0, 100, 2),
(2, 'Farms', 0, 1, 100, 3),
(3, 'Town Hall', 0, 1, 100, 3),
(4, 'Plaza', 0, 1, 100, 3),
(5, 'Church', 0, 1, 100, 3),
(6, 'Ship', 1, 0, 100, 2),
(7, 'Castle', 0, 1, 100, 3),
(8, 'Courtyard', 0, 1, 100, 3),
(9, 'Tunnels', 1, 0, 100, 9),
(10, 'Docks', 0, 1, 100, 3),
(11, 'Guard Post', 0, 1, 100, 3),
(12, 'Inn', 0, 1, 100, 3),
(13, 'Mining Company', 0, 1, 100, 3),
(14, 'Treasure Room', 0, 1, 100, 3),
(15, 'Outpost', 1, 0, 100, 10),
(16, 'Guard Tower', 0, 1, 100, 2),
(17, 'Front Gate', 0, 1, 100, 2),
(18, 'Market', 0, 1, 100, 2),
(19, 'Blacksmith', 0, 1, 100, 2),
(20, 'Barracks', 0, 1, 100, 2),
(21, 'Crops Fields', 0, 1, 100, 2),
(22, 'Watch Tower', 0, 1, 100, 2),
(23, 'Manor', 0, 1, 100, 2);

-- --------------------------------------------------------

--
-- Structure de la table `objective_flag_zone`
--

CREATE TABLE IF NOT EXISTS `objective_flag_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objective_id` (`objective_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Contenu de la table `objective_flag_zone`
--

INSERT INTO `objective_flag_zone` (`id`, `objective_id`, `zone_id`) VALUES
(1, 1, 29),
(5, 5, 32),
(6, 6, 33),
(9, 9, 41),
(10, 10, 36),
(12, 12, 38),
(13, 13, 39),
(16, 2, 28),
(17, 3, 30),
(18, 4, 31),
(21, 7, 34),
(22, 8, 35),
(25, 11, 37),
(28, 14, 40),
(29, 17, 62),
(30, 18, 63),
(31, 16, 64),
(32, 19, 65),
(33, 20, 66),
(34, 21, 67),
(35, 22, 68),
(36, 23, 69);

-- --------------------------------------------------------

--
-- Structure de la table `objective_zone`
--

CREATE TABLE IF NOT EXISTS `objective_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objective_id` (`objective_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `objective_zone`
--

INSERT INTO `objective_zone` (`id`, `objective_id`, `zone_id`) VALUES
(1, 2, 1),
(2, 1, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 10, 11),
(10, 11, 12),
(11, 12, 13),
(12, 13, 14),
(13, 14, 15),
(14, 9, 17),
(15, 23, 27),
(16, 22, 26),
(17, 21, 25),
(18, 20, 24),
(19, 19, 23),
(20, 18, 20),
(21, 17, 19),
(22, 16, 21),
(23, 15, 22);

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(500) NOT NULL,
  `perks` text NOT NULL,
  `lastUnit` varchar(500) NOT NULL DEFAULT 'Swordsman',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Structure de la table `spawn`
--

CREATE TABLE IF NOT EXISTS `spawn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `world` varchar(64) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  `pitch` double NOT NULL,
  `yaw` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=157 ;

--
-- Contenu de la table `spawn`
--

INSERT INTO `spawn` (`id`, `world`, `x`, `y`, `z`, `pitch`, `yaw`) VALUES
(2, 'Azura', -316, 42, -41, 7, 232),
(3, 'Azura', -442, 40, -142, 20, 238),
(4, 'Azura', -438, 40, -139, 4, 207),
(5, 'Azura', -442, 40, -146, -1, 269),
(6, 'Azura', -433, 40, -143, 2, 250),
(7, 'Azura', -434, 40, -149, 3, 290),
(8, 'Azura', -426, 40, -142, 3, 268),
(9, 'Azura', -426, 40, -151, 5, 310),
(10, 'Azura', -266, 37, -175, -0, 313),
(11, 'Azura', -254, 36, -142, 0, 202),
(12, 'Azura', -248, 36, -139, -2, 168),
(13, 'Azura', -257, 36, -184, 1, 346),
(14, 'Azura', -253, 36, -180, 1, 6),
(15, 'Azura', -251, 36, -175, -2, 331),
(16, 'Azura', -275, 48, -271, 13, 307),
(17, 'Azura', -275, 48, -282, 13, 272),
(18, 'Azura', -275, 48, -246, 15, 242),
(19, 'Azura', -273, 47, -236, 10, 280),
(20, 'Azura', -249, 44, -261, 7, 313),
(21, 'Azura', -251, 44, -253, -0, 220),
(22, 'Azura', -223, 36, -175, -2, 286),
(23, 'Azura', -224, 36, -169, -0, 267),
(24, 'Azura', -225, 36, -145, -3, 220),
(25, 'Azura', -193, 36, -152, -1, 155),
(26, 'Azura', -195, 36, -198, 0, 11),
(27, 'Azura', -210, 36, -186, -0, -41),
(28, 'Azura', -47, 36, -180, -0, 74),
(29, 'Azura', -48, 36, -169, -1, 101),
(30, 'Azura', -91, 36, -186, -1, 302),
(31, 'Azura', -79, 36, -184, 0, 313),
(32, 'Azura', -91, 36, -168, 5, 238),
(33, 'Azura', -55, 36, -185, -1, 11),
(34, 'Azura', -132, 39, -333, 1, 359),
(35, 'Azura', -122, 36, -348, -0, 343),
(36, 'Azura', -112, 36, -331, -2, 34),
(37, 'Azura', -112, 36, -331, -2, 34),
(38, 'Azura', -144, 36, -340, -1, 353),
(39, 'Azura', -71, 70, -342, 2, 3),
(40, 'Azura', -57, 57, -348, 0, 28),
(41, 'Azura', -47, 65, -344, 3, 87),
(42, 'Azura', -78, 57, -328, -12, 202),
(43, 'Azura', -93, 56, -346, -0, 281),
(44, 'Azura', -87, 56, -347, -3, 299),
(45, 'Azura', -87, 56, -347, -3, 299),
(46, 'Azura', -135, 36, -300, 1, 353),
(47, 'Azura', -123, 31, -298, 4, 336),
(48, 'Azura', -123, 31, -269, 0, 196),
(49, 'Azura', -113, 31, -250, 0, 187),
(50, 'Azura', -61, 36, -269, -15, 172),
(51, 'Azura', -70, 36, -271, -10, 182),
(52, 'Azura', -99, 36, -273, -15, 207),
(53, 'Azura', -99, 31, -256, -18, 189),
(54, 'Azura', -109, 31, -255, -8, 227),
(55, 'Illumina', 86, 145, 223, 0, -125),
(56, 'Illumina', 78, 103, 149, -0, -14),
(57, 'Illumina', 77, 103, 153, 6, -37),
(58, 'Illumina', 81, 103, 143, 1, 10),
(59, 'Illumina', 79, 103, 133, -0, 359),
(60, 'Illumina', 66, 103, 215, 0, 137),
(61, 'Illumina', 56, 103, 207, 4, 174),
(62, 'Illumina', 69, 103, 209, -0, 133),
(63, 'Illumina', 82, 103, 205, -0, 91),
(64, 'Illumina', 79, 103, 205, -0, 224),
(65, 'Illumina', 93, 103, 203, 2, 186),
(66, 'Illumina', 89, 103, 192, -15, 247),
(67, 'Illumina', 71, 103, 210, -1, 228),
(68, 'Illumina', 159, 109, 193, -0, 177),
(69, 'Illumina', 150, 109, 193, 1, 212),
(70, 'Illumina', 152, 109, 186, 1, 232),
(71, 'Illumina', 140, 109, 188, -126.3, 1.5),
(72, 'Illumina', 151, 109, 176, 2, 267),
(73, 'Illumina', 157, 109, 179, 3, 253),
(74, 'Illumina', 159, 109, 170, 7, 312),
(75, 'Illumina', 138, 124, 68, 6, 76),
(76, 'Illumina', 137, 124, 80, 3, 125),
(77, 'Illumina', 142, 129, 103, 1, 158),
(78, 'Illumina', 80, 118, 50, -1, 313),
(79, 'Illumina', 115, 109, 92, -0, 118),
(80, 'Illumina', 104, 109, 77, -3, 73),
(81, 'Illumina', 109, 109, 81, 0, 78),
(82, 'Illumina', 100, 109, 86, -3, 85),
(83, 'Illumina', 68, 129, 172, 3, 192),
(84, 'Illumina', 93, 117, 129, 1, 132),
(85, 'Illumina', 95, 117, 141, 2, 55),
(86, 'Illumina', 54, 125, 135, 1, 291),
(87, 'Illumina', 63, 129, 175, 2, 301),
(88, 'Illumina', 67, 125, 134, -2, 349),
(89, 'Illumina', 69, 125, 142, -1, 352),
(90, 'Illumina', 125, 132, 199, 0, 213),
(91, 'Illumina', 125, 132, 189, -3, 281),
(92, 'Illumina', 102, 124, 193, -0, 269),
(93, 'Illumina', 125, 124, 190, 3, 273),
(94, 'Nordic', 16, 25, -111, 5, 270),
(95, 'Nordic', 32, 12, 19, 5, 129),
(96, 'Nordic', 34, 12, 13, 0, 115),
(97, 'Nordic', 8, 12, 27, 0, 196),
(98, 'Nordic', 0, 12, 23, -0, 220),
(99, 'Nordic', -19, 19, -166, -6, 41),
(100, 'Nordic', -28, 18, -169, -10, 15),
(101, 'Nordic', -37, 17, -171, -11, 354),
(102, 'Nordic', -20, 22, -152, -1, 67),
(103, 'Nordic', -34, 21, -143, -1, 216),
(104, 'Nordic', -40, 17, -151, -12, 228),
(105, 'Nordic', -33, 18, -152, -6, 224),
(106, 'Nordic', -28, 22, -141, -6, 207),
(107, 'Nordic', -9, 29, -179, 1, 346),
(108, 'Nordic', -9, 29, -172, -10, 320),
(109, 'Nordic', -14, 28, -160, -11, 241),
(110, 'Nordic', -27, 19, -174, -16, 296),
(111, 'Nordic', 7, 34, -177, 6, 41),
(112, 'Nordic', 16, 34, -177, 1, 71),
(113, 'Nordic', 5, 34, -156, 2, 147),
(114, 'Nordic', 4, 34, -151, 0, 170),
(115, 'Nordic', 17, 39, -141, -0, 169),
(116, 'Nordic', 9, 39, -141, -3, 210),
(117, 'Nordic', 9, 39, -150, -8, 228),
(118, 'Nordic', 9, 40, -159, 2, 246),
(119, 'Nordic', 13, 39, -171, 1, 331),
(120, 'Nordic', 18, 46, -176, 3, 300),
(121, 'Nordic', 34, 46, -169, 5, 74),
(122, 'Nordic', 34, 46, -178, 1, 8),
(123, 'Nordic', 29, 46, -176, -0, 351),
(124, 'Nordic', 19, 52, -189, 1, 345),
(125, 'Nordic', 24, 52, -191, 1, 22),
(126, 'Nordic', 29, 46, -166, 5, 158),
(127, 'Nordic', 34, 46, -173, 1, 97),
(128, 'Nordic', 36, 46, -159, -8, 151),
(129, 'Nordic', 35, 46, -179, -1, 28),
(130, 'Nordic', 34, 54, -149, -5, 23),
(131, 'Nordic', 26, 50, -144, 3, 352),
(132, 'Nordic', 37, 50, -136, -0, 7),
(133, 'Nordic', 34, 50, -136, 2, 352),
(134, 'Nordic', 39, 50, -124, -1, 150),
(135, 'Nordic', 9, 46, -121, 3, 240),
(136, 'Nordic', 19, 50, -119, 4, 233),
(137, 'Nordic', 38, 50, -132, -0, 115),
(138, 'Nordic', 55, 38, -152, 3, 41),
(139, 'Nordic', 50, 38, -152, 1, 4),
(140, 'Nordic', 45, 38, -145, 0, 354),
(141, 'Nordic', 39, 39, -141, 5, 275),
(142, 'Nordic', 58, 40, -153, 10, 57),
(143, 'Nordic', 70, 37, -136, -0, 102),
(144, 'Nordic', 49, 38, -107, 1, 172),
(145, 'Nordic', 24, 50, -122, 2, 337),
(146, 'Nordic', 37, 50, -122, 1, 35),
(147, 'Nordic', 25, 50, -116, 0, 336),
(148, 'Nordic', 32, 50, -119, 3, 12),
(149, 'Nordic', 40, 50, -90, -0, 146),
(150, 'Nordic', 51, 50, -93, -2, 115),
(151, 'Nordic', 51, 50, -99, -1, 99),
(152, 'Nordic', 46, 50, -90, -1, 133),
(153, 'Nordic', 59, 50, -73, 1, 89),
(154, 'Nordic', 54, 50, -61, -0, 199),
(155, 'Nordic', 49, 55, -84, -3, 346),
(156, 'Nordic', 69, 55, -88, 0, 91);

-- --------------------------------------------------------

--
-- Structure de la table `structure`
--

CREATE TABLE IF NOT EXISTS `structure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('GATE','BATTLE_RAM') NOT NULL,
  `world` varchar(64) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `z` int(11) NOT NULL,
  `data` varchar(512) NOT NULL,
  `map_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `map_id` (`map_id`),
  KEY `team_id` (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `structure`
--

INSERT INTO `structure` (`id`, `type`, `world`, `x`, `y`, `z`, `data`, `map_id`, `team_id`) VALUES
(1, 'GATE', 'Azura', -230, 36, -157, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,false', 1, 3),
(2, 'GATE', 'Azura', -141, 31, -150, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,true;', 1, 3),
(3, 'GATE', 'Azura', -132, 40, -241, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,true;z_scan_size,Integer,1', 1, 3),
(4, 'GATE', 'Azura', -132, 40, -244, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,true;z_scan_size,Integer,1', 1, 3),
(5, 'GATE', 'Azura', -69, 46, -328, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,true;X_scan_size,Integer,2', 1, 3),
(7, 'GATE', 'Azura', -206, 37, -351, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,true;z_scan_size,Integer,4;x_scan_size,Integer,4', 1, 3),
(8, 'GATE', 'Azura', -206, 36, -336, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,true;z_scan_size,Integer,5;', 1, 3),
(9, 'GATE', 'Azura', -68, 41, -300, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,false;', 1, 3),
(10, 'BATTLE_RAM', 'Azura', -233, 36, -157, 'direction,String,EAST;', 1, 2),
(11, 'BATTLE_RAM', 'Azura', -68, 41, -297, 'direction,String,NORTH;', 1, 2),
(12, 'GATE', 'Illumina', 81, 102, 157, 'max_hitpoints,Integer,50;hitpoints,Integer,50;destroy_manually,Boolean,true;z_scan_size,Integer,2;x_scan_size,Integer,4', 2, 3),
(13, 'GATE', 'Illumina', 81, 102, 162, 'max_hitpoints,Integer,50;hitpoints,Integer,50;destroy_manually,Boolean,true;z_scan_size,Integer,2;x_scan_size,Integer,4', 2, 3),
(14, 'BATTLE_RAM', 'Nordic', 18, 39, -167, 'direction,String,EAST;', 3, 10),
(15, 'Gate', 'Nordic', 21, 39, -167, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,false', 3, 2),
(16, 'GATE', 'Nordic', -3, 33, -166, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,True', 3, 2),
(17, 'GATE', 'Nordic', 50, 50, -74, 'max_hitpoints,Integer,100;hitpoints,Integer,100;destroy_manually,Boolean,True', 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `primary_color` enum('YELLOW','WHITE','BROWN','ORANGE','MAGENTA','CYAN','BLACK','BLUE','LIGHT_BLUE','GREEN','LIME','PINK','GRAY','PURPLE','SILVER','RED') NOT NULL DEFAULT 'RED',
  `secondary_color` enum('RED','YELLOW','BLACK','BROWN','SILVER','CYAN','GRAY','LIME','LIGHT_BLUE','BLUE','GREEN','ORANGE','PURPLE','MAGENTA','PINK','WHITE') NOT NULL DEFAULT 'BLACK',
  `chat_color` enum('BLACK','DARK_BLUE','DARK_GREEN','DARK_AQUA','DARK_RED','DARK_PURPLE','GOLD','DARK_GRAY','BLUE','GREEN','AQUA','RED','LIGHT_PURPLE','YELLOW','WHITE','GRAY') NOT NULL DEFAULT 'BLUE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `team`
--

INSERT INTO `team` (`id`, `name`, `primary_color`, `secondary_color`, `chat_color`) VALUES
(1, 'Neutral', 'BLACK', 'GRAY', 'GRAY'),
(2, 'Barbarian', 'RED', 'BLACK', 'RED'),
(3, 'Defender', 'BLUE', 'YELLOW', 'BLUE'),
(9, 'Goblin', 'BROWN', 'RED', 'RED'),
(10, 'Knights', 'BLUE', 'YELLOW', 'BLUE');

-- --------------------------------------------------------

--
-- Structure de la table `team_spawn`
--

CREATE TABLE IF NOT EXISTS `team_spawn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `spawn_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  KEY `spawn_id` (`spawn_id`),
  KEY `objective_id` (`objective_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=201 ;

--
-- Contenu de la table `team_spawn`
--

INSERT INTO `team_spawn` (`id`, `team_id`, `spawn_id`, `objective_id`) VALUES
(1, 2, 3, 1),
(2, 2, 4, 1),
(3, 2, 5, 1),
(4, 2, 6, 1),
(5, 2, 7, 1),
(6, 2, 8, 1),
(7, 2, 9, 1),
(8, 2, 10, 2),
(9, 2, 11, 2),
(10, 2, 12, 2),
(11, 2, 13, 2),
(12, 2, 14, 2),
(13, 2, 15, 2),
(14, 3, 10, 2),
(15, 3, 11, 2),
(16, 3, 12, 2),
(17, 3, 13, 2),
(18, 3, 14, 2),
(19, 3, 15, 2),
(32, 2, 16, 3),
(33, 3, 16, 3),
(34, 2, 17, 3),
(35, 3, 17, 3),
(36, 2, 18, 3),
(37, 3, 18, 3),
(38, 2, 19, 3),
(39, 3, 19, 3),
(40, 2, 20, 3),
(41, 3, 20, 3),
(42, 2, 21, 3),
(43, 3, 21, 3),
(44, 2, 22, 4),
(45, 3, 22, 4),
(46, 2, 23, 4),
(47, 3, 23, 4),
(48, 2, 25, 4),
(49, 3, 25, 4),
(50, 2, 26, 4),
(51, 3, 26, 4),
(52, 2, 27, 4),
(53, 3, 27, 4),
(54, 2, 24, 4),
(55, 3, 24, 4),
(56, 2, 28, 5),
(57, 3, 28, 5),
(58, 2, 29, 5),
(59, 3, 29, 5),
(60, 2, 30, 5),
(61, 3, 30, 5),
(62, 2, 31, 5),
(63, 3, 31, 5),
(64, 2, 32, 5),
(65, 3, 32, 5),
(66, 2, 33, 5),
(67, 3, 33, 5),
(68, 2, 34, 6),
(69, 2, 35, 6),
(70, 2, 36, 6),
(71, 2, 37, 6),
(72, 2, 38, 6),
(73, 2, 39, 7),
(74, 3, 40, 7),
(75, 3, 41, 7),
(76, 3, 42, 7),
(77, 3, 43, 7),
(78, 3, 44, 7),
(79, 3, 45, 8),
(80, 3, 46, 8),
(81, 3, 47, 8),
(82, 3, 48, 8),
(83, 3, 49, 8),
(84, 2, 50, 8),
(85, 2, 51, 8),
(86, 2, 52, 8),
(87, 2, 53, 8),
(88, 2, 54, 8),
(94, 9, 56, 9),
(95, 9, 57, 9),
(96, 9, 58, 9),
(97, 9, 59, 9),
(98, 3, 60, 10),
(99, 3, 61, 10),
(100, 3, 62, 10),
(101, 3, 63, 10),
(102, 9, 64, 10),
(103, 9, 65, 10),
(104, 9, 60, 10),
(105, 9, 67, 10),
(106, 3, 68, 11),
(107, 3, 69, 11),
(108, 3, 70, 11),
(109, 3, 71, 11),
(110, 9, 71, 11),
(111, 9, 72, 11),
(112, 9, 73, 11),
(113, 9, 74, 11),
(114, 3, 75, 12),
(115, 3, 76, 12),
(116, 3, 77, 12),
(117, 3, 78, 12),
(118, 9, 79, 12),
(119, 9, 80, 12),
(120, 9, 81, 12),
(121, 9, 82, 12),
(122, 3, 83, 13),
(123, 3, 84, 13),
(124, 3, 85, 13),
(125, 3, 86, 13),
(126, 9, 86, 13),
(127, 9, 87, 13),
(128, 9, 88, 13),
(129, 9, 89, 13),
(130, 3, 90, 14),
(131, 3, 91, 14),
(132, 3, 92, 14),
(133, 3, 93, 14),
(134, 9, 92, 14),
(135, 10, 95, 15),
(136, 10, 96, 15),
(137, 10, 97, 15),
(138, 10, 98, 15),
(139, 2, 99, 16),
(140, 2, 100, 16),
(141, 2, 101, 16),
(142, 2, 102, 16),
(143, 10, 103, 16),
(144, 10, 104, 16),
(145, 10, 105, 16),
(146, 10, 106, 16),
(147, 10, 107, 17),
(148, 10, 108, 17),
(149, 10, 109, 17),
(150, 10, 110, 17),
(151, 2, 111, 17),
(152, 2, 112, 17),
(153, 2, 113, 17),
(154, 2, 114, 17),
(155, 10, 115, 18),
(156, 10, 116, 18),
(157, 10, 117, 18),
(158, 10, 118, 18),
(163, 2, 119, 18),
(164, 2, 120, 18),
(165, 2, 121, 18),
(166, 10, 122, 19),
(167, 10, 123, 19),
(168, 10, 124, 19),
(169, 10, 125, 19),
(170, 2, 126, 19),
(171, 2, 127, 19),
(172, 2, 128, 19),
(173, 2, 129, 19),
(174, 10, 130, 20),
(175, 10, 131, 20),
(176, 10, 132, 20),
(177, 10, 133, 20),
(178, 2, 134, 20),
(179, 2, 135, 20),
(180, 2, 136, 20),
(181, 2, 137, 20),
(182, 10, 138, 21),
(183, 10, 139, 21),
(184, 10, 140, 21),
(185, 10, 141, 21),
(186, 2, 142, 21),
(187, 2, 143, 21),
(188, 2, 144, 21),
(189, 10, 145, 22),
(190, 10, 146, 22),
(191, 10, 147, 22),
(192, 10, 148, 22),
(193, 2, 149, 22),
(194, 2, 150, 22),
(195, 2, 151, 22),
(196, 2, 152, 22),
(197, 10, 153, 23),
(198, 2, 154, 23),
(199, 2, 155, 23),
(200, 2, 156, 23);

-- --------------------------------------------------------

--
-- Structure de la table `zone`
--

CREATE TABLE IF NOT EXISTS `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `world` varchar(64) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `z` int(11) NOT NULL,
  `data` varchar(128) NOT NULL DEFAULT 'radius,Integer,5;height,Integer,5;',
  `figure` enum('Cylinder','RectangularPrism') NOT NULL DEFAULT 'Cylinder',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Contenu de la table `zone`
--

INSERT INTO `zone` (`id`, `world`, `x`, `y`, `z`, `data`, `figure`) VALUES
(1, 'Azura', -256, 36, -164, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(2, 'Azura', -421, 40, -151, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(3, 'Azura', -263, 45, -259, 'radius,Integer,4;height,Integer,5;', 'Cylinder'),
(4, 'Azura', -205, 36, -170, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(5, 'Azura', -71, 37, -136, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(6, 'Azura', -133, 35, -354, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(7, 'Azura', -72, 70, -341, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(8, 'Azura', -117, 31, -286, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(9, 'Azura', -319, 41, -63, 'xLength,Integer,29;height,Integer,3;zLength,Integer,25', 'RectangularPrism'),
(10, 'Illumina', 85, 139, 205, 'xLength,Integer,15;height,Integer,6;zLength,Integer,20', 'RectangularPrism'),
(11, 'Illumina', 51, 103, 190, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(12, 'Illumina', 159, 109, 175, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(13, 'Illumina', 103, 134, 62, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(14, 'Illumina', 80, 126, 146, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(15, 'Illumina', 125, 139, 195, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(17, 'Illumina', 77, 103, 153, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(18, 'Nordic', 30, 21, -117, 'xLength,Integer,13;height,Integer,6;zLength,Integer,11', 'RectangularPrism'),
(19, 'Nordic', -7, 29, -167, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(20, 'Nordic', 14, 39, -150, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(21, 'Nordic', -40, 37, -146, 'radius,Integer,2;height,Integer,5;', 'Cylinder'),
(22, 'Nordic', 31, 12, 17, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(23, 'Nordic', 22, 52, -188, 'radius,Integer,4;height,Integer,5;', 'Cylinder'),
(24, 'Nordic', 37, 54, -144, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(25, 'Nordic', 44, 38, -142, 'radius,Integer,5;height,Integer,5;', 'Cylinder'),
(26, 'Nordic', 29, 59, -116, 'radius,Integer,2;height,Integer,5;', 'Cylinder'),
(27, 'Nordic', 66, 55, -73, 'radius,Integer,4;height,Integer,5;', 'Cylinder'),
(28, 'Azura', -276, 36, -184, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(29, 'Azura', -441, 40, -171, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(30, 'Azura', -283, 45, -279, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(31, 'Azura', -225, 36, -190, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(32, 'Azura', -91, 37, -156, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(33, 'Azura', -153, 35, -374, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(34, 'Azura', -92, 70, -361, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(35, 'Azura', -137, 31, -306, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(36, 'Illumina', 31, 103, 170, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(37, 'Illumina', 139, 109, 155, 'xLength,Integer,40;height,Integer,15;zLength,Integer,40;', 'RectangularPrism'),
(38, 'Illumina', 83, 120, 42, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(39, 'Illumina', 60, 126, 126, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(40, 'Illumina', 105, 139, 175, 'xLength,Integer,40;height,Integer,25;zLength,Integer,40;', 'RectangularPrism'),
(41, 'Illumina', 57, 103, 133, 'xLength,Integer,10;height,Integer,5;zLength,Integer,10;', 'RectangularPrism'),
(62, 'Nordic', -10, 29, -175, 'xLength,Integer,15;height,Integer,25;zLength,Integer,20;', 'RectangularPrism'),
(63, 'Nordic', 5, 39, -155, 'xLength,Integer,10;height,Integer,10;zLength,Integer,10;', 'RectangularPrism'),
(64, 'Nordic', -45, 15, -150, 'xLength,Integer,15;height,Integer,40;zLength,Integer,15;', 'RectangularPrism'),
(65, 'Nordic', 18, 52, -190, 'xLength,Integer,10;height,Integer,10;zLength,Integer,5;', 'RectangularPrism'),
(66, 'Nordic', 28, 54, -152, 'xLength,Integer,20;height,Integer,10;zLength,Integer,30;', 'RectangularPrism'),
(67, 'Nordic', 41, 39, -148, 'xLength,Integer,10;height,Integer,10;zLength,Integer,15;', 'RectangularPrism'),
(68, 'Nordic', 15, 50, -120, 'xLength,Integer,10;height,Integer,25;zLength,Integer,15;', 'RectangularPrism'),
(69, 'Nordic', 45, 50, -92, 'xLength,Integer,35;height,Integer,30;zLength,Integer,40;', 'RectangularPrism');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `map`
--
ALTER TABLE `map`
  ADD CONSTRAINT `map_ibfk_1` FOREIGN KEY (`spawn_room`) REFERENCES `spawn` (`id`),
  ADD CONSTRAINT `map_ibfk_2` FOREIGN KEY (`wool_map_zone_id`) REFERENCES `zone` (`id`);

--
-- Contraintes pour la table `map_objective`
--
ALTER TABLE `map_objective`
  ADD CONSTRAINT `map_objective_ibfk_1` FOREIGN KEY (`map_id`) REFERENCES `map` (`id`),
  ADD CONSTRAINT `map_objective_ibfk_2` FOREIGN KEY (`objective_id`) REFERENCES `objective` (`id`);

--
-- Contraintes pour la table `map_team`
--
ALTER TABLE `map_team`
  ADD CONSTRAINT `map_team_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  ADD CONSTRAINT `map_team_ibfk_2` FOREIGN KEY (`map_id`) REFERENCES `map` (`id`);

--
-- Contraintes pour la table `objective`
--
ALTER TABLE `objective`
  ADD CONSTRAINT `objective_ibfk_1` FOREIGN KEY (`starting_team_id`) REFERENCES `team` (`id`);

--
-- Contraintes pour la table `objective_flag_zone`
--
ALTER TABLE `objective_flag_zone`
  ADD CONSTRAINT `objective_flag_zone_ibfk_1` FOREIGN KEY (`objective_id`) REFERENCES `objective` (`id`),
  ADD CONSTRAINT `objective_flag_zone_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`);

--
-- Contraintes pour la table `objective_zone`
--
ALTER TABLE `objective_zone`
  ADD CONSTRAINT `objective_zone_ibfk_1` FOREIGN KEY (`objective_id`) REFERENCES `objective` (`id`),
  ADD CONSTRAINT `objective_zone_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`);

--
-- Contraintes pour la table `structure`
--
ALTER TABLE `structure`
  ADD CONSTRAINT `structure_ibfk_1` FOREIGN KEY (`map_id`) REFERENCES `map` (`id`),
  ADD CONSTRAINT `structure_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

--
-- Contraintes pour la table `team_spawn`
--
ALTER TABLE `team_spawn`
  ADD CONSTRAINT `team_spawn_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  ADD CONSTRAINT `team_spawn_ibfk_2` FOREIGN KEY (`spawn_id`) REFERENCES `spawn` (`id`),
  ADD CONSTRAINT `team_spawn_ibfk_3` FOREIGN KEY (`objective_id`) REFERENCES `objective` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
